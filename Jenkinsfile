@Library(value='tiki/jenkins', changelog=false) _

pipeline {
    agent {
        kubernetes {
            defaultContainer 'golang'
            yamlMergeStrategy merge()
            inheritFrom 'dsp-jnlp dsp-go-121'
        }
    }

    options {
        // do not clean TAG build logs
        buildDiscarder(env.TAG_NAME == null ? logRotator(daysToKeepStr: '30') : null)
        // ensures that automated branch indexing doesn't trigger branch job builds,
        // but also destroys the its feature to trigger on bitbucket commit push
        // solution: additional bitbucketPush trigger below
        overrideIndexTriggers(false)
    }

    triggers {
        // ensure that the branch job itself gets triggered on bitbucket commit push
        bitbucketPush()
    }

    stages {
        stage('Prepare') {
            steps {
                bitbucketStatusNotify(buildState: 'INPROGRESS')
            }
        }
        stage('Test') {
            steps {
                sh '''go get github.com/tebeka/go2xunit'''
                sh '''2>&1 go test -v -race -cover ./... | tee gotest.out'''
                sh '''go2xunit -fail -input gotest.out -output tests.xml'''
            }
        }
    }

    post {
        failure {
            doFailureSteps()
        }
        fixed {
            doFixedSteps()
        }
        success {
            bitbucketStatusNotify(buildState: 'SUCCESSFUL')
        }
        always {
            junit 'tests.xml'
        }
    }
}
