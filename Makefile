SHELL=/bin/bash
.DEFAULT_GOAL := help

.PHONY: help
help:  ## shows the Makefile targets with information
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) |  awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: test
test: ## run tests
	go test -v -race -cover ./...