package auth

import (
	"context"
	"github.com/coreos/go-oidc"
	"golang.org/x/oauth2"
)

type JwtContext interface {
	UserFromToken(ctx context.Context, token string, clientId string) (string, error)
	RolesFromToken(ctx context.Context, token string, clientId string) (TokenRoles, error)
	OAuthConfig(ctx context.Context, clientId string, clientSecret string) (*oauth2.Config, error)
	ExchangeToken(ctx context.Context, token string, fromClient string, toClient string) (*oauth2.Token, error)
	ExchangeTokenWithConfig(ctx context.Context, token string, toClient string, oauthConfig oauth2.Config) (*oauth2.Token, error)
	GetToken(ctx context.Context, client string, username string, pwd string) (*oauth2.Token, error)
	GetTokenFromLiteralAccessToken(ctx context.Context, literalAccessToken, clientId string, withOfflineRefresh bool) (*oauth2.Token, error)
	GetTokenFromLiteralAccessTokenWithConfig(ctx context.Context, literalAccessToken string, withOfflineRefresh bool, oauthConfig oauth2.Config) (*oauth2.Token, error)
	RefreshToken(ctx context.Context, refreshToken string, client string) (*oauth2.Token, error)
	RefreshTokenWithConfig(ctx context.Context, refreshToken string, oauthConfig oauth2.Config) (*oauth2.Token, error)
	Sourcify(ctx context.Context, token *oauth2.Token, client string) (oauth2.TokenSource, error)
	OAuthEndpoint(ctx context.Context) (*oauth2.Endpoint, error)
	IdToken(ctx context.Context, token, clientId string) (*oidc.IDToken, error)
}
