package auth

import (
	"context"
	"fmt"
	"github.com/coreos/go-oidc"
	"github.com/rs/zerolog/log"
	"golang.org/x/oauth2"
	"sync"
	"time"
)

type JwtContextImpl struct {
	sync.Mutex
	realmUrl string
	provider *oidc.Provider
	verifier *oidc.IDTokenVerifier
}

func (c *JwtContextImpl) UserFromToken(ctx context.Context, token string, clientId string) (string, error) {

	idToken, err := c.IdToken(ctx, token, clientId)

	if err != nil {
		return "", err
	}

	return TokenUserFromIDToken(idToken)
}

func (c *JwtContextImpl) oidcProvider(ctx context.Context) (*oidc.Provider, error) {

	c.Lock()
	defer c.Unlock()

	if c.provider != nil {
		return c.provider, nil
	}

	if oidcProvider, err := oidc.NewProvider(ctx, c.realmUrl); err != nil {
		return nil, err
	} else {
		c.provider = oidcProvider
	}

	return c.provider, nil
}

func (c *JwtContextImpl) getVerifier(ctx context.Context, clientId string) (*oidc.IDTokenVerifier, error) {
	if provider, err := c.oidcProvider(ctx); err != nil {
		return nil, err
	} else if c.verifier != nil {
		return c.verifier, nil
	} else {
		config := oidc.Config{ClientID: clientId}
		return provider.Verifier(&config), nil
	}
}

func (c *JwtContextImpl) RolesFromToken(ctx context.Context, token, clientId string) (TokenRoles, error) {

	idToken, err := c.IdToken(ctx, token, clientId)

	if err != nil {
		return nil, err
	}

	return TokenRolesFromIDToken(idToken)
}

func (c *JwtContextImpl) OAuthConfig(ctx context.Context, clientId string, clientSecret string) (*oauth2.Config, error) {
	endpoint, err := c.OAuthEndpoint(ctx)
	if err != nil {
		return nil, err
	}

	return &oauth2.Config{
		ClientID:     clientId,
		ClientSecret: clientSecret,
		Endpoint:     *endpoint,
		Scopes:       nil,
		RedirectURL:  "",
	}, nil
}

// Deprecated: Use specialized GetTokenFromLiteralAccessToken and RefreshToken funcs instead
func (c *JwtContextImpl) ExchangeToken(ctx context.Context, token string, fromClient string, toClient string) (*oauth2.Token, error) {
	oauthConfig, err := c.OAuthConfig(ctx, fromClient, "")
	if err != nil {
		return nil, err
	}

	return c.ExchangeTokenWithConfig(ctx, token, toClient, *oauthConfig)
}

// Deprecated: Use specialized GetTokenFromLiteralAccessToken and RefreshToken funcs instead
func (c *JwtContextImpl) ExchangeTokenWithConfig(ctx context.Context, token string, toClient string, oauthConfig oauth2.Config) (*oauth2.Token, error) {
	return oauthConfig.Exchange(ctx, "",
		oauth2.SetAuthURLParam("grant_type", "urn:ietf:params:oauth:grant-type:token-exchange"),
		oauth2.SetAuthURLParam("subject_token", token),
		oauth2.SetAuthURLParam("subject_token_type", "urn:ietf:params:oauth:token-type:access_token"),
		oauth2.SetAuthURLParam("requested_token_type", "urn:ietf:params:oauth:token-type:refresh_token"),
		oauth2.SetAuthURLParam("audience", toClient))
}

func (c *JwtContextImpl) GetToken(ctx context.Context, client string, username string, pwd string) (*oauth2.Token, error) {
	oauthConfig, err := c.OAuthConfig(ctx, client, "")
	if err != nil {
		return nil, err
	}

	return oauthConfig.PasswordCredentialsToken(ctx, username, pwd)
}

func (c *JwtContextImpl) GetTokenFromLiteralAccessToken(ctx context.Context, literalAccessToken, clientId string, withOfflineRefresh bool) (*oauth2.Token, error) {
	oauthConfig, err := c.OAuthConfig(ctx, clientId, "")
	if err != nil {
		return nil, err
	}

	return c.GetTokenFromLiteralAccessTokenWithConfig(ctx, literalAccessToken, withOfflineRefresh, *oauthConfig)
}

func (c *JwtContextImpl) GetTokenFromLiteralAccessTokenWithConfig(ctx context.Context, literalAccessToken string, withOfflineRefresh bool, oauthConfig oauth2.Config) (*oauth2.Token, error) {

	authParams := []oauth2.AuthCodeOption{
		oauth2.SetAuthURLParam("grant_type", "urn:ietf:params:oauth:grant-type:token-exchange"),
		oauth2.SetAuthURLParam("subject_token", literalAccessToken),
		oauth2.SetAuthURLParam("subject_token_type", "urn:ietf:params:oauth:token-type:access_token"),
	}

	// Attention, using oauthConfig.Scopes instead of this DOES NOT WORK!
	if withOfflineRefresh {
		authParams = append(authParams, oauth2.SetAuthURLParam("scope", oidc.ScopeOfflineAccess))
	}

	//todo because of keycloak bug KEYCLOAK-10866, we re-attempt to retrieve token for up to three times
	token, err := oauthConfig.Exchange(ctx, "", authParams...)
	for i := 0; i < 2 && err != nil; i++ {
		time.Sleep(1 * time.Second)
		token, err = oauthConfig.Exchange(ctx, "", authParams...)
		if i == 1 {
			log.Ctx(ctx).Warn().Msg("performed already 3 attempts to retrieve access token from literal")
		}
	}

	return token, err
}

func (c *JwtContextImpl) RefreshToken(ctx context.Context, refreshToken string, client string) (*oauth2.Token, error) {
	oauthConfig, err := c.OAuthConfig(ctx, client, "")
	if err != nil {
		return nil, err
	}

	return c.RefreshTokenWithConfig(ctx, refreshToken, *oauthConfig)
}

func (c *JwtContextImpl) RefreshTokenWithConfig(ctx context.Context, refreshToken string, oauthConfig oauth2.Config) (*oauth2.Token, error) {
	return oauthConfig.Exchange(ctx, "",
		oauth2.SetAuthURLParam("grant_type", "refresh_token"),
		oauth2.SetAuthURLParam("refresh_token", refreshToken))
}

func (c *JwtContextImpl) Sourcify(ctx context.Context, token *oauth2.Token, client string) (oauth2.TokenSource, error) {
	oauthConfig, err := c.OAuthConfig(ctx, client, "")
	if err != nil {
		return nil, err
	}

	return oauthConfig.TokenSource(ctx, token), nil
}

func (c *JwtContextImpl) OAuthEndpoint(ctx context.Context) (*oauth2.Endpoint, error) {
	provider, err := c.oidcProvider(ctx)

	if err != nil {
		return nil, err
	}

	endpoint := provider.Endpoint()

	return &endpoint, nil

}

func (c *JwtContextImpl) IdToken(ctx context.Context, token, clientId string) (*oidc.IDToken, error) {
	if verifier, err := c.getVerifier(ctx, clientId); err != nil {
		return nil, fmt.Errorf("unable to resolve token verifier for clientId %s (%w)", clientId, err)
	} else if verifiedToken, err := verifier.Verify(ctx, token); err != nil {
		return nil, fmt.Errorf("could not verify token for clientId %s (%w)", clientId, err)
	} else {
		return verifiedToken, nil
	}
}
