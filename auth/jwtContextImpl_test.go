package auth

//TODO: rework this tests with TIKIDSP-887 as it needs https://auth.tiki-dsp.io
//func TestRolesFromLiteralToken(t *testing.T) {
//	ctx := context.Background()
//
//	testJwtCtx := JwtContextImpl{
//		realmUrl: "https://auth.tiki-dsp.io/auth/realms/tiki",
//	}
//	_, err := testJwtCtx.oidcProvider(ctx)
//	assert.NoError(t, err)
//	testJwtCtx.verifier = testJwtCtx.provider.Verifier(&oidc.Config{
//		ClientID:        "ai-examples-scala-dev",
//		SkipExpiryCheck: true, // because of test
//	})
//
//	ctx = WithJwtContext(ctx, "https://auth.tiki-dsp.io/auth/realms/tiki", &testJwtCtx)
//
//	jwtContext := JwtContextFromContext(ctx, "https://auth.tiki-dsp.io/auth/realms/tiki")
//
//	testToken := "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJUMm5acllQUHNiSWlIdUY0bkEwa1hkQTE2eHZnbG5EY1VUd" +
//		"G5wdWVtVnBvIn0.eyJqdGkiOiJmN2ZlNmJlNy0wOTc3LTRjMjctOTE2OC03ZjQ4OGY2NDljYzgiLCJleHAiOjE1NjAzNjk4MTAsIm5iZiI" +
//		"6MCwiaWF0IjoxNTYwMzY5NTEwLCJpc3MiOiJodHRwczovL2F1dGgudGlraS1kc3AuaW8vYXV0aC9yZWFsbXMvdGlraSIsImF1ZCI6WyJyZ" +
//		"WFsbS1tYW5hZ2VtZW50IiwiYWktZXhhbXBsZXMtc2NhbGEtZGV2IiwiYWktZXhhbXBsZXMtcHl0aG9uLWRldi13ZWItcHkzNiIsImFpLWV" +
//		"4YW1wbGVzLXNjYWxhLWRldi13ZWItc2MyMTEiLCJhaS1leGFtcGxlcy1weXRob24tZGV2IiwiYnJva2VyIiwiZHNwLWluZnJhc3RydWN0d" +
//		"XJlIiwiYWNjb3VudCJdLCJzdWIiOiIyYmUxNTVjZi05OTEyLTQ4MjMtODRhYS0xOTZkYWYzOTc3N2UiLCJ0eXAiOiJCZWFyZXIiLCJhenA" +
//		"iOiJhaS1leGFtcGxlcy1wcm92aXNpb25lci1kZXYiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiJkNmIxOWNkNC1kZDJkLTQ0N" +
//		"DUtYjk5YS03ZjQzMDNiMmUyNTQiLCJhY3IiOiIxIiwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbImp1cHl0ZXJodWItdXNlciIsIm9mZmx" +
//		"pbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iXX0sInJlc291cmNlX2FjY2VzcyI6eyJyZWFsbS1tYW5hZ2VtZW50Ijp7InJvbGVzI" +
//		"jpbInZpZXctaWRlbnRpdHktcHJvdmlkZXJzIiwidmlldy1yZWFsbSIsIm1hbmFnZS1pZGVudGl0eS1wcm92aWRlcnMiLCJpbXBlcnNvbmF" +
//		"0aW9uIiwicmVhbG0tYWRtaW4iLCJjcmVhdGUtY2xpZW50IiwibWFuYWdlLXVzZXJzIiwicXVlcnktcmVhbG1zIiwidmlldy1hdXRob3Jpe" +
//		"mF0aW9uIiwicXVlcnktY2xpZW50cyIsInF1ZXJ5LXVzZXJzIiwibWFuYWdlLWV2ZW50cyIsIm1hbmFnZS1yZWFsbSIsInZpZXctZXZlbnR" +
//		"zIiwidmlldy11c2VycyIsInZpZXctY2xpZW50cyIsIm1hbmFnZS1hdXRob3JpemF0aW9uIiwibWFuYWdlLWNsaWVudHMiLCJxdWVyeS1nc" +
//		"m91cHMiXX0sImFpLWV4YW1wbGVzLXByb3Zpc2lvbmVyLWRldiI6eyJyb2xlcyI6WyJkc3AtZGVwbG95IiwiZHNwLWRlY29tbWlzc2lvbiI" +
//		"sImRzcC1wcm92aXNpb24iXX0sImFpLWV4YW1wbGVzLXNjYWxhLWRldiI6eyJyb2xlcyI6WyJkc3AtZGVwbG95IiwiZHNwLXByb3Zpc2lvb" +
//		"iJdfSwiYWktZXhhbXBsZXMtcHl0aG9uLWRldi13ZWItcHkzNiI6eyJyb2xlcyI6WyJjYXBhYmlsaXR5LTEiXX0sImFpLWV4YW1wbGVzLXN" +
//		"jYWxhLWRldi13ZWItc2MyMTEiOnsicm9sZXMiOlsiY2FwYWJpbGl0eS0xIl19LCJhaS1leGFtcGxlcy1weXRob24tZGV2Ijp7InJvbGVzI" +
//		"jpbImRzcC1kZXBsb3kiLCJkc3AtcHJvdmlzaW9uIl19LCJicm9rZXIiOnsicm9sZXMiOlsicmVhZC10b2tlbiJdfSwiZHNwLWluZnJhc3R" +
//		"ydWN0dXJlIjp7InJvbGVzIjpbImRzcC1zZXR1cCJdfSwiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY" +
//		"2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJwcm9maWxlIGVtYWlsIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsIm5" +
//		"hbWUiOiJTaG93IENhc2UiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJzaG93Y2FzZSIsImdpdmVuX25hbWUiOiJTaG93IiwiZmFtaWx5X25hb" +
//		"WUiOiJDYXNlIn0.UUC78OztTIuMBdXkHnoVyLBqO61EnMFkBpMwx2iIAd9yg4jLrfLAh0tK-FQ2SKHGCkOs4wqiPV_0MaFM8d2dIvpWzhf" +
//		"0J7ALpr_Ip_JZ7tW8CLHTXZbFDYu3uebEnhuJbaQ2fhpTTVYKRUZPtnJOedMM2thDvPcHYsKIRXgn8RRIEjuNU_YZu1flJfjr00Wf2RHkv" +
//		"L8voMCL_AjuDUd8-bb0hsbYMcaC9kRaokNGzKsbwHR71KbPAjZa4WoSLpzWwFX0Biy5Q0w6ao5rkBty2_VMdGAFGWdYuBMFZqK18CGcvQp" +
//		"1zgtkoFh-ciX7DJxzBV7iuBwRxy9w1hSYp-PiNQ"
//
//	_, err = jwtContext.RolesFromToken(ctx, testToken, "ai-examples-scala-dev")
//	assert.NoError(t, err)
//
//	testJwtCtx.verifier = testJwtCtx.provider.Verifier(&oidc.Config{
//		ClientID:        "blblbl",
//		SkipExpiryCheck: true, // because of test
//	})
//	_, err = jwtContext.RolesFromToken(ctx, testToken, "blblbl")
//	assert.Error(t, err)
//}
//
//func TestExchangeToken(t *testing.T) {
//	ctx := context.Background()
//	ctx = WithJwtContextStorage(ctx)
//
//	jwtContext := JwtContextFromContext(ctx, "https://auth.tiki-dsp.io/auth/realms/test-realm")
//
//	token, err := jwtContext.GetToken(ctx, "login-client", "test-user", "tEsT-PaSsWoRd")
//
//	if err != nil {
//		assert.Fail(t, err.Error())
//	} else {
//		exchangeConfig, err2 := jwtContext.OAuthConfig(ctx, "exchange-client", "66fe35ce-0b51-4d40-a0e3-6d6d14f41bed")
//		assert.NoError(t, err2)
//		exchangeToken, err2 := jwtContext.ExchangeTokenWithConfig(ctx, token.AccessToken, "exchange-client", *exchangeConfig)
//		assert.NoError(t, err2)
//
//		_, err2 = jwtContext.ExchangeTokenWithConfig(ctx, exchangeToken.AccessToken, "login-client", *exchangeConfig)
//		assert.NoError(t, err2)
//
//		_, err2 = jwtContext.ExchangeTokenWithConfig(ctx, exchangeToken.AccessToken, "non-existing-client", *exchangeConfig)
//		assert.Error(t, err2)
//
//		_, err2 = jwtContext.ExchangeToken(ctx, exchangeToken.AccessToken, "exchange-client", "login-client")
//		assert.NoError(t, err2)
//
//		_, err2 = jwtContext.ExchangeToken(ctx, exchangeToken.AccessToken, "exchange-client", "non-existing-client")
//		assert.Error(t, err2)
//	}
//}
//
//func TestGetToken(t *testing.T) {
//	ctx := context.Background()
//
//	testJwtCtx := JwtContextImpl{
//		realmUrl: "https://auth.tiki-dsp.io/auth/realms/test-realm",
//	}
//	_, err := testJwtCtx.oidcProvider(ctx)
//	assert.NoError(t, err)
//	testJwtCtx.verifier = testJwtCtx.provider.Verifier(&oidc.Config{
//		ClientID:        "login-client",
//		SkipExpiryCheck: true, // because of test
//	})
//
//	ctx = WithJwtContext(ctx, "https://auth.tiki-dsp.io/auth/realms/test-realm", &testJwtCtx)
//
//	jwtContext := JwtContextFromContext(ctx, "https://auth.tiki-dsp.io/auth/realms/test-realm")
//
//	_, err = jwtContext.GetToken(ctx, "login-client", "test-user", "tEsT-PaSsWoRd")
//	assert.NoError(t, err, "GetToken has failed")
//}
//
//func TestGetTokenFromLiteralAccessToken(t *testing.T) {
//	ctx := context.Background()
//	ctx = WithJwtContextStorage(ctx)
//
//	jwtContext := JwtContextFromContext(ctx, "https://auth.tiki-dsp.io/auth/realms/test-realm")
//
//	token, err := jwtContext.GetToken(ctx, "login-client", "test-user", "tEsT-PaSsWoRd")
//	assert.NoError(t, err, "GetToken has failed")
//
//	token2, err := jwtContext.GetTokenFromLiteralAccessToken(ctx, token.AccessToken, "login-client", false)
//	assert.NoError(t, err, "AccessToken verification has failed")
//	assert.True(t, token2.Valid(), "GetTokenFromLiteralAccessToken produced an invalid token")
//}
//
//func TestRefreshToken(t *testing.T) {
//	ctx := context.Background()
//	ctx = WithJwtContextStorage(ctx)
//
//	jwtContext := JwtContextFromContext(ctx, "https://auth.tiki-dsp.io/auth/realms/test-realm")
//
//	token, err := jwtContext.GetToken(ctx, "login-client", "test-user", "tEsT-PaSsWoRd")
//	assert.NoError(t, err, "GetToken has failed")
//	assert.True(t, token.Extra("refresh_expires_in") != 0., "Refresh token never expires")
//
//	refreshedOnlineToken, err := jwtContext.GetTokenFromLiteralAccessToken(ctx, token.AccessToken, "login-client", false)
//	assert.NoError(t, err, "GetTokenFromLiteralAccessToken has failed")
//	refreshedOnlineToken, err = jwtContext.RefreshToken(ctx, refreshedOnlineToken.RefreshToken, "login-client")
//	assert.NoError(t, err, "RefreshToken has failed")
//	assert.True(t, refreshedOnlineToken.Extra("refresh_expires_in") != 0., "Refresh token never expires")
//
//	refreshedOfflineToken, err := jwtContext.GetTokenFromLiteralAccessToken(ctx, token.AccessToken, "login-client", true)
//	assert.NoError(t, err, "GetTokenFromLiteralAccessToken has failed")
//	refreshedOfflineToken, err = jwtContext.RefreshToken(ctx, refreshedOfflineToken.RefreshToken, "login-client")
//	assert.NoError(t, err, "RefreshToken has failed")
//	assert.True(t, refreshedOfflineToken.Extra("refresh_expires_in") == 0., "Refresh token expires")
//}
//func TestSourcify(t *testing.T) {
//	ctx := context.Background()
//	ctx = WithJwtContextStorage(ctx)
//
//	jwtContext := JwtContextFromContext(ctx, "https://auth.tiki-dsp.io/auth/realms/test-realm")
//
//	token, err := jwtContext.GetToken(ctx, "login-client", "test-user", "tEsT-PaSsWoRd")
//	assert.NoError(t, err, "GetToken has failed")
//
//	tokenSource, err := jwtContext.Sourcify(ctx, token, "login-client")
//	assert.NoError(t, err, "Sourcify has failed")
//
//	token, err = tokenSource.Token()
//	assert.NoError(t, err, "Sourcify has failed")
//	assert.True(t, token.Valid(), "Sourcify produced an invalid token")
//}
