package auth

import (
	"context"
	"github.com/rs/zerolog/log"
	"sync"
)

type jwtContextStorageKey int

var jwtContextStorageKeyValue jwtContextStorageKey

type specificJwtContextEntry struct{ Url string }

type storage struct {
	sync.Mutex
	entries map[string]JwtContext
}

func jwtContextStorageFromContext(ctx context.Context) *storage {
	if jwtCtx, ok := ctx.Value(jwtContextStorageKeyValue).(*storage); ok {
		return jwtCtx
	} else {
		return nil
	}
}

func WithJwtContextStorage(ctx context.Context) context.Context {
	return context.WithValue(ctx, jwtContextStorageKeyValue, &storage{entries: make(map[string]JwtContext)})
}

func WithJwtContext(ctx context.Context, realmUrl string, jwtContext JwtContext) context.Context {
	return context.WithValue(ctx, specificJwtContextEntry{Url: realmUrl}, jwtContext)
}

func JwtContextFromContext(ctx context.Context, realmUrl string) JwtContext {

	logger := log.Ctx(ctx).With().Str("module", "JwtContext").Logger()

	//let's check whether a specific instance was set for <realmUrl> and return it...
	if specific := ctx.Value(specificJwtContextEntry{Url: realmUrl}); specific != nil {
		return specific.(JwtContext)
	}

	//...if not do a fallback
	contextStorage := jwtContextStorageFromContext(ctx)

	if contextStorage != nil {
		//storage was activated
		contextStorage.Lock()
		defer contextStorage.Unlock()

		existing, found := contextStorage.entries[realmUrl]
		if !found {
			logger.Trace().Str("realm-url", realmUrl).Msg("creating new stored context for realm")
			existing = &JwtContextImpl{realmUrl: realmUrl}
			contextStorage.entries[realmUrl] = existing
		} else {
			logger.Trace().Str("realm-url", realmUrl).Msg("reusing existing stored context")
		}

		return existing
	} else {
		//no storage; transient behaviour
		logger.Trace().Str("realm-url", realmUrl).Msg("creating new transient context for realm")
		return &JwtContextImpl{realmUrl: realmUrl}
	}
}
