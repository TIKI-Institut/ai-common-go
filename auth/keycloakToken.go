package auth

import (
	"fmt"
	"github.com/coreos/go-oidc"
	"strings"
)

type clientRoleClaimsFromKeycloak struct {
	Clients map[string]clientRolesFromKeycloak `json:"resource_access"`
}

type preferredUsernameFromKeycloak struct {
	Username string `json:"Preferred_username"`
}

type clientRolesFromKeycloak struct {
	Roles []string `json:"roles"`
}

func TokenRolesFromIDToken(oidcToken *oidc.IDToken) (TokenRoles, error) {

	deserializedClaims := clientRoleClaimsFromKeycloak{}

	if err := oidcToken.Claims(&deserializedClaims); err != nil {
		return nil, err
	}

	result := tokenRoles{clientRoles: make(map[string][]string)}

	for k, l := range deserializedClaims.Clients {
		result.clientRoles[k] = l.Roles
	}

	return &result, nil
}

func RealmFromIDToken(oidcToken *oidc.IDToken) (string, error) {
	issuerParts := strings.Split(oidcToken.Issuer, "/realms/")
	if len(issuerParts) != 2 {
		return "", fmt.Errorf("unable to extract realm from IDToken, issuer format not recognized")
	} else if len(issuerParts[1]) == 0 {
		return "", fmt.Errorf("unable to extract realm from IDToken, issuer does not contain realm")
	} else {
		return issuerParts[1], nil
	}
}

func TokenUserFromIDToken(oidcToken *oidc.IDToken) (string, error) {

	deserializedClaims := preferredUsernameFromKeycloak{}

	if err := oidcToken.Claims(&deserializedClaims); err != nil {
		return "", err
	}

	return deserializedClaims.Username, nil
}
