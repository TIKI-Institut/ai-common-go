package auth

import "context"

type RawTokenSource interface {
	RawToken(ctx context.Context) (string, error)
}

type roleCheckingRawTokenSourceKey int

var roleCheckingRawTokenKeySourceValue roleCheckingRawTokenSourceKey

func WithRawTokenSource(ctx context.Context, rawTokenSource RawTokenSource) context.Context {
	return context.WithValue(ctx, roleCheckingRawTokenKeySourceValue, rawTokenSource)
}
func RawTokenSourceFromContext(ctx context.Context) RawTokenSource {
	if rawTokenSource, ok := ctx.Value(roleCheckingRawTokenKeySourceValue).(RawTokenSource); ok {
		return rawTokenSource
	} else {
		return nil
	}
}
