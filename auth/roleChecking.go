package auth

import (
	"context"
	"errors"
	"fmt"
)

var MissingRolesError = fmt.Errorf("missing roles")

type RoleCheckFailedError struct {
	Cause error
}

func (r *RoleCheckFailedError) Error() string {
	return fmt.Sprintf("role check failed (%s)", r.Cause)
}

func (r *RoleCheckFailedError) Unwrap() error {
	return r.Cause
}

//ResolveTokenRoles validates <rawToken>; checks if it is from <jwtIssuer>.
//Extracts all user roles and returns the roles container
//If an error occurs it will always be an RoleCheckFailedError
func ResolveTokenRoles(ctx context.Context, jwtIssuer string, rawToken string, loginClientId string) (TokenRoles, error) {

	if rawToken == "" {
		return nil, WrapAsRoleCheckFailedError(errors.New("token is empty"))
	}

	if jwtIssuer == "" {
		return nil, WrapAsRoleCheckFailedError(errors.New("jwt issuer is empty"))
	}

	if loginClientId == "" {
		return nil, WrapAsRoleCheckFailedError(errors.New("login clientId is empty"))
	}

	//create the JWT context
	jwtContext := JwtContextFromContext(ctx, jwtIssuer)

	tokenRoles, err := jwtContext.RolesFromToken(ctx, rawToken, loginClientId)
	if err != nil {
		return nil, WrapAsRoleCheckFailedError(fmt.Errorf("unable to verify token for client %s (error :%w)", loginClientId, err))
	}

	return tokenRoles, nil
}

func WrapAsRoleCheckFailedError(err error) error {

	if err != nil {
		if convError, ok := err.(*RoleCheckFailedError); ok {
			return convError
		} else {
			return &RoleCheckFailedError{Cause: err}
		}
	}

	return nil
}
