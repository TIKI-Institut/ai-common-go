package auth

//RoleCheckingHandlerFunc should return MissingRolesError if the check failed due to a missing role or unsatisfied user capabilities
type RoleCheckingHandlerFunc func(loginClientId string, tokenRoles TokenRoles) error

//DefaultRoleCheckingHandlerFunc uses the login client to look for the requested roles
//If an error occurs it will always be an RoleCheckFailedError
func DefaultRoleCheckingHandlerFunc(roles ...string) RoleCheckingHandlerFunc {
	return func(loginClientId string, tokenRoles TokenRoles) error {
		if !HasRoles(tokenRoles, loginClientId, roles...) {
			return &RoleCheckFailedError{Cause: MissingRolesError}
		}
		return nil
	}
}

//ClientRoleCheckingHandlerFunc uses the given clientId to look for the requested roles
//If an error occurs it will always be an RoleCheckFailedError
func ClientRoleCheckingHandlerFunc(clientId string, roles ...string) RoleCheckingHandlerFunc {
	return func(_ string, tokenRoles TokenRoles) error {
		if !HasRoles(tokenRoles, clientId, roles...) {
			return &RoleCheckFailedError{Cause: MissingRolesError}
		}
		return nil
	}
}
