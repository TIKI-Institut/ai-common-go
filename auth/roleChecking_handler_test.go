package auth

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

type testTokenRoles map[string][]string

func (t testTokenRoles) ClientRoles(clientId string) []string {
	return t[clientId]
}

func TestDefaultRoleCheckingHandlerFunc(t *testing.T) {

	t.Run("has roles", func(t *testing.T) {
		roles := testTokenRoles{"some-login-client": {"A", "B"}}
		handler := DefaultRoleCheckingHandlerFunc("A")
		assert.NoError(t, handler("some-login-client", roles))
	})

	t.Run("missing roles", func(t *testing.T) {
		roles := testTokenRoles{"some-login-client": {"A", "B"}}
		handler := DefaultRoleCheckingHandlerFunc("D")
		assert.Error(t, handler("some-login-client", roles))
	})

}

func TestClientRoleCheckingHandlerFunc(t *testing.T) {

	t.Run("has roles", func(t *testing.T) {
		roles := testTokenRoles{"some-login-client": {"A", "B"}}
		handler := ClientRoleCheckingHandlerFunc("some-login-client", "A")
		assert.NoError(t, handler("some-login-client", roles))
	})

	t.Run("missing roles", func(t *testing.T) {
		roles := testTokenRoles{"some-login-client": {"A", "B"}}
		handler := ClientRoleCheckingHandlerFunc("some-other-client", "A")
		assert.Error(t, handler("some-login-client", roles))
	})

}
