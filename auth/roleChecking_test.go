package auth

import (
	"context"
	"errors"
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

type testGetOfflineTokenFailureJwtContext struct {
	*testing.T
	JwtContext
	roles map[string][]string
}

func (tJwtContext *testGetOfflineTokenFailureJwtContext) RolesFromToken(_ context.Context, token string, clientId string) (TokenRoles, error) {

	assert.EqualValues(tJwtContext, "some-token", token)
	assert.EqualValues(tJwtContext, "clientId", clientId)

	return testTokenRoles(tJwtContext.roles), nil
}

func TestCheckRoles(t *testing.T) {

	assertError := func(t *testing.T, err error, expectedCauseError string) {
		assert.Error(t, err)
		var targetErr *RoleCheckFailedError
		assert.True(t, errors.As(err, &targetErr))
		assert.EqualError(t, targetErr.Cause, expectedCauseError)
	}

	t.Run("raw token source with empty token", func(t *testing.T) {
		ctx := context.Background()
		tokenRoles, err := ResolveTokenRoles(ctx, "jwtIssuer", "", "clientId")
		assert.Nil(t, tokenRoles)
		assertError(t, err, "token is empty")
	})

	t.Run("empty jwt issuer", func(t *testing.T) {
		ctx := context.Background()
		tokenRoles, err := ResolveTokenRoles(ctx, "", "some-token", "clientId")
		assert.Nil(t, tokenRoles)
		assertError(t, err, "jwt issuer is empty")
	})

	t.Run("empty login clientId", func(t *testing.T) {
		ctx := context.Background()
		tokenRoles, err := ResolveTokenRoles(ctx, "jwtIssuer", "some-token", "")
		assert.Nil(t, tokenRoles)
		assertError(t, err, "login clientId is empty")
	})

	t.Run("positive test", func(t *testing.T) {
		ctx := context.Background()
		ctx = WithJwtContext(ctx, "jwtIssuer", &testGetOfflineTokenFailureJwtContext{T: t, roles: map[string][]string{"clientId": {"A", "B", "C"}}})
		tokenRoles, err := ResolveTokenRoles(ctx, "jwtIssuer", "some-token", "clientId")
		assert.NotNil(t, tokenRoles)
		assert.NoError(t, err)
	})
}

func TestWrapError(t *testing.T) {
	t.Run("nil", func(t *testing.T) {
		wErr := WrapAsRoleCheckFailedError(nil)
		assert.Nil(t, wErr)
	})

	t.Run("wrapped", func(t *testing.T) {
		wErr := WrapAsRoleCheckFailedError(&RoleCheckFailedError{Cause: fmt.Errorf("some-error")})

		assert.NotNil(t, wErr)
		var wrappedErr *RoleCheckFailedError
		if errors.As(wErr, &wrappedErr) {
			assert.EqualError(t, wrappedErr.Cause, "some-error")
		} else {
			assert.Fail(t, "wErr is not of type RoleCheckFailedError")
		}
	})

	t.Run("unwrapped", func(t *testing.T) {
		wErr := WrapAsRoleCheckFailedError(fmt.Errorf("some-error"))
		assert.NotNil(t, wErr)
		var wrappedErr *RoleCheckFailedError
		if errors.As(wErr, &wrappedErr) {
			assert.EqualError(t, wrappedErr.Cause, "some-error")
		} else {
			assert.Fail(t, "wErr is not of type RoleCheckFailedError")
		}
	})

}
