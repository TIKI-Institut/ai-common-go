package auth

type TokenRoles interface {
	ClientRoles(clientId string) []string
}

type tokenRoles struct {
	clientRoles map[string][]string
}

func HasRoles(holder TokenRoles, clientId string, roles ...string) bool {
	storedRoles := holder.ClientRoles(clientId)

	if len(storedRoles) < len(roles) {
		return false
	}

ProvidedRolesLoop:
	for _, providedRole := range roles {
		for _, storedRole := range storedRoles {
			if storedRole == providedRole {
				continue ProvidedRolesLoop
			}
		}
		return false
	}
	return true

}

//goland:noinspection GoUnusedExportedFunction
func HasAnyRole(holder TokenRoles, clientId string, roles ...string) bool {
	storedRoles := holder.ClientRoles(clientId)

	for _, providedRole := range roles {
		for _, storedRole := range storedRoles {
			if storedRole == providedRole {
				return true
			}
		}
	}
	return false
}

func (t *tokenRoles) ClientRoles(clientId string) []string {
	return t.clientRoles[clientId]
}
