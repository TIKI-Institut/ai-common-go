package cli

import "errors"
import "k8s.io/client-go/util/exec"

type CommandExecutor interface {
	ExecuteCommand(string) (stdout string, stderr string, err error)
	CheckErrorForExitCode(err error) (int, bool)
}

type Command interface {
	RawCommand() string
	ProcessResults(stdout string, stderr string, exitCode int) error
}

type UnexpectedResultError struct {
	StdOut          string
	StdErr          string
	ExitCode        *int
	UnderlyingError error
}

func (u *UnexpectedResultError) Error() string {
	return u.UnderlyingError.Error()
}

func ExecuteCommand(executor CommandExecutor, cmd Command) error {

	rawCommand := cmd.RawCommand()
	stdout, stderr, err := executor.ExecuteCommand(rawCommand)

	exitCode := 0
	exitCodeFound := false

	if err != nil {
		var exitError exec.ExitError
		if !errors.As(err, &exitError) {
			return &UnexpectedResultError{StdOut: stdout, StdErr: stderr, ExitCode: nil, UnderlyingError: err}
		}

		exitCode, exitCodeFound = executor.CheckErrorForExitCode(err)
	}

	processingError := cmd.ProcessResults(stdout, stderr, exitCode)

	if processingError != nil {
		if exitCodeFound {
			return &UnexpectedResultError{StdOut: stdout, StdErr: stderr, ExitCode: &exitCode, UnderlyingError: processingError}
		}
		return &UnexpectedResultError{StdOut: stdout, StdErr: stderr, ExitCode: nil, UnderlyingError: processingError}
	}

	return nil
}
