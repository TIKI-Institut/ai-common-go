package cli

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"k8s.io/client-go/util/exec"
	"testing"
)

type executeCommandArgs struct {
	command string
}

type executeCommandRetVals struct {
	stdout string
	stderr string
	err    error
}

type checkErrorForExitCodeArgs struct {
	err error
}

type checkErrorForExitCodeRetVals struct {
	exitCode      int
	exitCodeFound bool
}

type testCommandExecutor struct {
	*testing.T
	executeCommandExpectedArgs        executeCommandArgs
	executeCommandRetVals             executeCommandRetVals
	checkErrorForExitCodeExpectedArgs checkErrorForExitCodeArgs
	checkErrorForExitCodeRetVals      checkErrorForExitCodeRetVals
}

func NewTestCommandExecutor(
	t *testing.T,
	executeCommandExpectedArgs executeCommandArgs,
	executeCommandRetVals executeCommandRetVals,
	checkErrorForExitCodeExpectedArgs checkErrorForExitCodeArgs,
	checkErrorForExitCodeRetVals checkErrorForExitCodeRetVals) *testCommandExecutor {

	return &testCommandExecutor{
		T:                                 t,
		executeCommandExpectedArgs:        executeCommandExpectedArgs,
		executeCommandRetVals:             executeCommandRetVals,
		checkErrorForExitCodeExpectedArgs: checkErrorForExitCodeExpectedArgs,
		checkErrorForExitCodeRetVals:      checkErrorForExitCodeRetVals,
	}
}

func (t testCommandExecutor) ExecuteCommand(s string) (stdout string, stderr string, err error) {
	assert.EqualValues(t.T, t.executeCommandExpectedArgs.command, s)
	return t.executeCommandRetVals.stdout, t.executeCommandRetVals.stderr, t.executeCommandRetVals.err
}

func (t testCommandExecutor) CheckErrorForExitCode(err error) (int, bool) {
	assert.EqualValues(t.T, t.checkErrorForExitCodeExpectedArgs.err, err)
	return t.checkErrorForExitCodeRetVals.exitCode, t.checkErrorForExitCodeRetVals.exitCodeFound
}

type rawCommandRetVals struct {
	command string
}

type processResultsExpectedArgs struct {
	stdout   string
	stderr   string
	exitCode int
}

type processResultsRetVals struct {
	err error
}

type testCommand struct {
	*testing.T
	rawCommandRetVal           rawCommandRetVals
	processResultsExpectedArgs processResultsExpectedArgs
	processResultsRetVal       processResultsRetVals
}

func NewTestCommand(
	t *testing.T,
	rawCommandRetVals rawCommandRetVals,
	processResultsExpectedArgs processResultsExpectedArgs,
	proccessResultsRetVals processResultsRetVals) *testCommand {

	return &testCommand{
		T:                          t,
		rawCommandRetVal:           rawCommandRetVals,
		processResultsExpectedArgs: processResultsExpectedArgs,
		processResultsRetVal:       proccessResultsRetVals,
	}
}

func (t testCommand) RawCommand() string {
	return t.rawCommandRetVal.command
}

func (t testCommand) ProcessResults(stdout string, stderr string, exitCode int) error {
	assert.EqualValues(t.T, t.processResultsExpectedArgs.stdout, stdout)
	assert.EqualValues(t.T, t.processResultsExpectedArgs.stderr, stderr)
	assert.EqualValues(t.T, t.processResultsExpectedArgs.exitCode, exitCode)
	return t.processResultsRetVal.err
}

type executeCommandTestCase struct {
	executor *testCommandExecutor
	command  *testCommand
	result   error
}

func TestExecuteCommand(t *testing.T) {
	testExitCode := 1234
	testExitError := exec.CodeExitError{
		Err:  errors.New("some error"),
		Code: testExitCode,
	}

	tcs := map[string]executeCommandTestCase{
		"command succeeds without an error": {
			executor: NewTestCommandExecutor(t,
				executeCommandArgs{"test-command"},
				executeCommandRetVals{"test stdout", "", nil},
				checkErrorForExitCodeArgs{nil},
				checkErrorForExitCodeRetVals{0, false}),

			command: NewTestCommand(t,
				rawCommandRetVals{"test-command"},
				processResultsExpectedArgs{"test stdout", "", 0},
				processResultsRetVals{nil}),

			result: nil,
		},
		"command fails with exec.ExitError; exitCode found": {
			executor: NewTestCommandExecutor(t,
				executeCommandArgs{"test-command"},
				executeCommandRetVals{"test stdout", "test stderr", testExitError},
				checkErrorForExitCodeArgs{testExitError},
				checkErrorForExitCodeRetVals{testExitCode, true}),

			command: NewTestCommand(t,
				rawCommandRetVals{"test-command"},
				processResultsExpectedArgs{"test stdout", "test stderr", testExitCode},
				processResultsRetVals{testExitError}),

			result: &UnexpectedResultError{StdOut: "test stdout", StdErr: "test stderr", ExitCode: &testExitCode, UnderlyingError: testExitError},
		},
		"command fails with exec.ExitError; exitCode not found": {
			executor: NewTestCommandExecutor(t,
				executeCommandArgs{"test-command"},
				executeCommandRetVals{"test stdout", "test stderr", testExitError},
				checkErrorForExitCodeArgs{testExitError},
				checkErrorForExitCodeRetVals{testExitCode, false}),

			command: NewTestCommand(t,
				rawCommandRetVals{"test-command"},
				processResultsExpectedArgs{"test stdout", "test stderr", testExitCode},
				processResultsRetVals{errors.New("some error")}),

			result: &UnexpectedResultError{StdOut: "test stdout", StdErr: "test stderr", ExitCode: nil, UnderlyingError: errors.New("some error")},
		},
		"command fails with non exec.ExitError": {
			executor: NewTestCommandExecutor(t,
				executeCommandArgs{"test-command"},
				executeCommandRetVals{"test stdout", "test stderr", errors.New("some error")},
				checkErrorForExitCodeArgs{nil},
				checkErrorForExitCodeRetVals{0, false}),

			command: NewTestCommand(t,
				rawCommandRetVals{"test-command"},
				processResultsExpectedArgs{"test stdout", "test stderr", 0},
				processResultsRetVals{errors.New("some error")}),

			result: &UnexpectedResultError{StdOut: "test stdout", StdErr: "test stderr", ExitCode: nil, UnderlyingError: errors.New("some error")},
		},
	}

	for testName, tc := range tcs {
		t.Run(testName, func(t *testing.T) {
			assert.EqualValues(t, tc.result, ExecuteCommand(tc.executor, tc.command))
		})
	}

}
