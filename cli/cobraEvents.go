package cli

import "github.com/spf13/cobra"

func JoinCommandEvents(left func(cmd *cobra.Command, args []string) error, right func(cmd *cobra.Command, args []string) error, rightBeforeLeft bool) func(cmd *cobra.Command, args []string) error {
	var a, b = left, right

	if rightBeforeLeft {
		a, b = right, left
	}

	if a == nil {
		return b
	}

	if b == nil {
		return a
	}

	return func(cmd *cobra.Command, args []string) error {
		if err := a(cmd, args); err != nil {
			return err
		}
		return b(cmd, args)
	}
}
