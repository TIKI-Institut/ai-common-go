package cli

import (
	"github.com/spf13/cobra"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestJoinCommandEvents(t *testing.T) {

	t.Run("right before left", func(t *testing.T) {

		t.Run("first is nil", func(t *testing.T) {
			check := 0
			s := func(cmd *cobra.Command, args []string) error {
				check = 2
				return nil
			}
			e := JoinCommandEvents(nil, s, true)
			err := e(nil, nil)
			assert.NoError(t, err)
			assert.EqualValues(t, 2, check)
		})

		t.Run("second is nil", func(t *testing.T) {
			check := 0
			f := func(cmd *cobra.Command, args []string) error {
				check = 1
				return nil
			}
			e := JoinCommandEvents(f, nil, true)
			err := e(nil, nil)
			assert.NoError(t, err)
			assert.EqualValues(t, 1, check)
		})

		t.Run("both are nil", func(t *testing.T) {
			e := JoinCommandEvents(nil, nil, true)
			assert.Nil(t, e)
		})

		t.Run("none is nil", func(t *testing.T) {
			var check []int
			f := func(cmd *cobra.Command, args []string) error {
				check = append(check, 1)
				return nil
			}
			s := func(cmd *cobra.Command, args []string) error {
				check = append(check, 2)
				return nil
			}
			e := JoinCommandEvents(f, s, true)
			err := e(nil, nil)
			assert.NoError(t, err)
			assert.EqualValues(t, []int{2, 1}, check)

		})
	})

	t.Run("left before right", func(t *testing.T) {

		t.Run("first is nil", func(t *testing.T) {
			check := 0
			s := func(cmd *cobra.Command, args []string) error {
				check = 2
				return nil
			}
			e := JoinCommandEvents(nil, s, false)
			err := e(nil, nil)
			assert.NoError(t, err)
			assert.EqualValues(t, 2, check)
		})

		t.Run("second is nil", func(t *testing.T) {
			check := 0
			f := func(cmd *cobra.Command, args []string) error {
				check = 1
				return nil
			}
			e := JoinCommandEvents(f, nil, false)
			err := e(nil, nil)
			assert.NoError(t, err)
			assert.EqualValues(t, 1, check)
		})

		t.Run("both are nil", func(t *testing.T) {
			e := JoinCommandEvents(nil, nil, false)
			assert.Nil(t, e)
		})

		t.Run("none is nil", func(t *testing.T) {
			var check []int
			f := func(cmd *cobra.Command, args []string) error {
				check = append(check, 1)
				return nil
			}
			s := func(cmd *cobra.Command, args []string) error {
				check = append(check, 2)
				return nil
			}
			e := JoinCommandEvents(f, s, false)
			err := e(nil, nil)
			assert.NoError(t, err)
			assert.EqualValues(t, []int{1, 2}, check)

		})
	})

}
