package cli

import (
	"context"
	"os"
	"os/signal"
)

func NewContextWithSignalListener(ctx context.Context, signalChannel <-chan os.Signal) context.Context {

	newCtx, cancel := context.WithCancel(ctx)

	go func(cancelFunction context.CancelFunc, sc <-chan os.Signal) {
		for {
			select {
			case <-sc:
				cancelFunction()
				return
			case <-ctx.Done():
				return

			}
		}
	}(cancel, signalChannel)

	return newCtx
}

func SignalListener(signals ...os.Signal) <-chan os.Signal {
	interruptChan := make(chan os.Signal)
	signal.Notify(interruptChan, signals...)
	return interruptChan
}
