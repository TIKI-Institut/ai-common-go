package cli

import (
	"context"
	"github.com/stretchr/testify/assert"
	"os"
	"sync"
	"syscall"
	"testing"
	"time"
)

func TestSigTermListener(t *testing.T) {

	t.Run("noop", func(t *testing.T) {
		ctx := context.TODO()
		sigChannel := make(chan os.Signal)
		ctx = NewContextWithSignalListener(ctx, sigChannel)
		defer close(sigChannel)
		assert.NoError(t, ctx.Err())
	})

	t.Run("external context abortion", func(t *testing.T) {

		ctx, cancel := context.WithCancel(context.TODO())
		sigChannel := make(chan os.Signal)
		ctx = NewContextWithSignalListener(ctx, sigChannel)
		defer close(sigChannel)

		hitTimeout := false
		wg := sync.WaitGroup{}
		wg.Add(2)

		go func() {
			cancel()
			wg.Done()
		}()

		go func() {
			select {
			case <-time.After(500 * time.Millisecond):
				hitTimeout = true
				break
			case <-ctx.Done():
				break
			}

			wg.Done()
		}()

		wg.Wait()

		assert.IsType(t, context.Canceled, ctx.Err())
		assert.False(t, hitTimeout)
	})

	t.Run("context timed out", func(t *testing.T) {

		ctx, cancel := context.WithTimeout(context.TODO(), 100*time.Millisecond)
		sigChannel := make(chan os.Signal)
		ctx = NewContextWithSignalListener(ctx, sigChannel)
		defer close(sigChannel)

		hitTimeout := false
		wg := sync.WaitGroup{}
		wg.Add(1)

		go func() {
			select {
			case <-time.After(500 * time.Millisecond):
				hitTimeout = true
				break
			case <-ctx.Done():
				break
			}

			wg.Done()
		}()

		wg.Wait()
		cancel()

		assert.IsType(t, context.DeadlineExceeded, ctx.Err())
		assert.False(t, hitTimeout)
	})

	t.Run("sending a correct signal", func(t *testing.T) {
		ctx := context.TODO()
		sigChannel := make(chan os.Signal)
		ctx = NewContextWithSignalListener(ctx, sigChannel)
		defer close(sigChannel)

		hitTimeout := false
		wg := sync.WaitGroup{}
		wg.Add(1)

		go func() {
			select {
			case <-time.After(500 * time.Millisecond):
				hitTimeout = true
				break
			case <-ctx.Done():
				break
			}

			wg.Done()
		}()

		go func() {
			sigChannel <- syscall.SIGINT
		}()

		wg.Wait()

		assert.IsType(t, context.Canceled, ctx.Err())
		assert.False(t, hitTimeout)
	})
}
