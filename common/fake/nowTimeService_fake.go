package fake

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/common"
	"context"
	"time"
)

func WithNowTime(ctx context.Context, t time.Time) context.Context {
	return common.WithNowTimeService(ctx, func() time.Time { return t })
}
