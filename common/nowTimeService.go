package common

import (
	"context"
	"time"
)

type nowTimeServiceContextKey int

var nowTimeServiceContextKeyValue nowTimeServiceContextKey

type timeService func() time.Time

func WithNowTimeService(ctx context.Context, timeFnc timeService) context.Context {
	return context.WithValue(ctx, nowTimeServiceContextKeyValue, timeFnc)
}

func NowTime(ctx context.Context) time.Time {
	existing := ctx.Value(nowTimeServiceContextKeyValue)
	if existing != nil {
		return (existing.(timeService))()
	}
	return time.Now()
}
