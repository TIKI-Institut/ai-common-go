package common

import (
	"context"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestNowTimeService(t *testing.T) {

	t.Run("fallback to time.Now()", func(t *testing.T) {
		now := time.Now()
		//this is not really deterministic; i know
		assert.Less(t, NowTime(context.Background()).Sub(now), time.Millisecond)
	})

	t.Run("set time", func(t *testing.T) {
		targetNowTime := time.Date(2023, 11, 01, 0, 0, 0, 0, time.UTC)
		ctx := context.Background()
		ctx = WithNowTimeService(ctx, func() time.Time { return targetNowTime })
		assert.EqualValues(t, targetNowTime, NowTime(ctx))
	})

}
