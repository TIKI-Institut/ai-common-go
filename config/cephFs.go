package config

import (
	"github.com/rs/zerolog/log"
)

type CephFs struct {
	CephFsMount           string `mapstructure:"cephFsMount"`           // The local directory containing the CephFs Volumes are presented (mounted)
	CephFsVolumeDirectory string `mapstructure:"cephFsVolumeDirectory"` // The local directory containing the CephFs Volumes are presented (mounted)
	StorageClass          string `mapstructure:"storageClass"`          // StorageClass filter (only serve virtual FS of this storage-class)
}

func (f *CephFs) Verify() error {

	if f.CephFsMount == "" {
		log.Warn().Msg("'cephFsMount' of CephFsFilesystemConfiguration must not be empty, CephFS will not be served by this instance!")
	}

	if f.CephFsVolumeDirectory == "" {
		log.Warn().Msg("'cephFsVolumeDirectory' of CephFsFilesystemConfiguration must not be empty, CephFS will not be served by this instance!")
	}

	return nil
}
