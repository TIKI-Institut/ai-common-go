package config

import (
	"bytes"
	"github.com/spf13/viper"
	"gopkg.in/yaml.v2"
	"strings"
)

func initWithConfigObject(emptyConf interface{}) (*viper.Viper, error) {

	v := viper.New()

	v.SetConfigType("yaml")

	// set default values in viper.
	// Viper needs to know if a key exists in order to override it.
	// https://github.com/spf13/viper/issues/188
	genericConfigData, err := genericConvert(emptyConf)
	if err != nil {
		return nil, err
	}

	if err := v.MergeConfigMap(genericConfigData); err != nil {
		return nil, err
	}

	return v, nil
}

func defaultViperConfig(envVarPrefix string, configInstance interface{}) (error, *viper.Viper) {

	v, err := initWithConfigObject(configInstance)
	if err != nil {
		return err, nil
	}

	if envVarPrefix != "" {
		v.SetEnvPrefix(envVarPrefix)
	}

	replacer := strings.NewReplacer(".", "_")
	v.SetEnvKeyReplacer(replacer)
	v.AutomaticEnv()
	return nil, v
}

//converts any instance into a generic map structure
func genericConvert(configInstance interface{}) (map[string]interface{}, error) {

	b, err := yaml.Marshal(&configInstance)

	if err != nil {
		return nil, err
	}

	v := viper.New()

	v.SetConfigType("yaml")

	err = v.MergeConfig(bytes.NewReader(b))
	if err != nil {
		return nil, err
	}

	return v.AllSettings(), nil
}
