package config

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/tests"
	"github.com/spf13/afero"
	"github.com/stretchr/testify/assert"
	"testing"
)

type Configuration struct {
	MqttClientConfig MqttClientConfig `mapstructure:"mqttClientConfig"`
}

func TestConfigKubernetesDeploymentBrokerFallback(t *testing.T) {

	tests.WithEnvVars(func() {
		conf := Configuration{}

		err := ReadConfig("", &conf, WithConfigFile("./testdata/kubernetes_broker_fallback.yaml"))
		assert.Nil(t, err)

		err = conf.MqttClientConfig.Verify()
		assert.Nil(t, err)

		assert.Equal(t, "123.231.123.231:11885", conf.MqttClientConfig.Broker)

	}, tests.EnvDecl{"NAMESPACE_BROKER_PORT", "123.231.123.231:11885"})

}

func TestConfigKubernetesDeploymentBrokerOverride(t *testing.T) {

	tests.WithEnvVars(func() {
		conf := Configuration{}

		err := ReadConfig("DSP", &conf, WithConfigFile("./testdata/kubernetes_broker_override.yaml"))

		assert.Nil(t, err)
		assert.Equal(t, "123.231.123.231:11885", conf.MqttClientConfig.Broker)

	}, tests.EnvDecl{"DSP_MQTTCLIENTCONFIG_BROKER", "123.231.123.231:11885"})

}

func TestReadConfigWithMissingConfigFile(t *testing.T) {

	defaultConfig := Configuration{
		MqttClientConfig: MqttClientConfig{
			Broker:              "test-broker",
			ClientId:            "test-client-id",
			MqttTlsClientConfig: MqttTlsClientConfig{},
		},
	}

	tests.WithEnvVars(func() {
		localConfig := defaultConfig

		var usedConfigOptions = []ViperOption{WithConfigName("some-config-file")}

		err := ReadConfig("AI_MQTT", &localConfig, usedConfigOptions...)
		assert.NoError(t, err)
		assert.Equal(t, "123.231.123.231:11885", localConfig.MqttClientConfig.Broker)

	}, tests.EnvDecl{"AI_MQTT_MQTTCLIENTCONFIG_BROKER", "123.231.123.231:11885"})

}

func testFileSystem() afero.Fs {

	return &afero.MemMapFs{}

}

func TestReadConfigWithOptionalConfigFile(t *testing.T) {

	defaultConfig := Configuration{
		MqttClientConfig: MqttClientConfig{
			Broker:              "test-broker",
			ClientId:            "test-client-id",
			MqttTlsClientConfig: MqttTlsClientConfig{},
		},
	}

	tests.WithEnvVars(func() {
		localConfig := defaultConfig

		fs := testFileSystem()

		var usedConfigOptions = []ViperOption{WithConfigName("some-config-file"), WithLocalPath(), WithFileSystem(fs)}

		err := ReadConfig("AI_MQTT", &localConfig, usedConfigOptions...)
		assert.NoError(t, err)

		assert.Equal(t, "123.231.123.231:11885", localConfig.MqttClientConfig.Broker)

	}, tests.EnvDecl{"AI_MQTT_MQTTCLIENTCONFIG_BROKER", "123.231.123.231:11885"})

}
