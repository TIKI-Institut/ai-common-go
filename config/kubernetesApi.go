package config

import (
	"fmt"
	"os"
)

type KubernetesApiConfiguration struct {
	InCluster            bool   `mapstructure:"inCluster"`
	KubernetesConfigFile string `mapstructure:"kubernetesConfigFile"`
}

func (c *KubernetesApiConfiguration) Verify() error {
	if !c.InCluster && c.KubernetesConfigFile != "" {
		if _, err := os.Stat(c.KubernetesConfigFile); err != nil {
			if os.IsNotExist(err) {
				return fmt.Errorf("configured kubernetes config file %q does not exist", c.KubernetesConfigFile)
			}

			return fmt.Errorf("unexpected error while checking kubernetes config file (%+++v)", err)
		}
	}
	return nil
}
