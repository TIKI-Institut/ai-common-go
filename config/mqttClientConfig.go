package config

import (
	"fmt"
	"github.com/rs/zerolog/log"
	"os"
)

type MqttTlsClientConfig struct {
	ServerCaFile      string `mapstructure:"serverCaFile"`
	ClientCertFile    string `mapstructure:"clientCertFile"`
	ClientCertKeyFile string `mapstructure:"clientCertKeyFile"`
}

type MqttClientConfig struct {
	Broker              string              `mapstructure:"broker"`
	ClientId            string              `mapstructure:"clientId"`
	MqttTlsClientConfig MqttTlsClientConfig `mapstructure:"mqttTlsClientConfig"`
}

// TODO log entries here are not context-scoped, so will not be transferred to any client
func (c *MqttTlsClientConfig) Verify() error {

	if c.ServerCaFile != "" && c.ClientCertFile != "" && c.ClientCertKeyFile != "" {
		return nil
	}

	if c.ServerCaFile == "" && c.ClientCertFile == "" && c.ClientCertKeyFile == "" {
		return nil
	}

	log.Warn().Msg("TLS client configuration is set only partially; tls connection might not work!")

	return nil
}

const MqttDefaultBrokerEnv = "NAMESPACE_BROKER_PORT"

// TODO log entries here are not context-scoped, so will not be transferred to any client
func (c *MqttClientConfig) Verify() error {

	if c.Broker == "" {
		log.Warn().Str("env", MqttDefaultBrokerEnv).Msg("No mqtt broker address set in config. Will fall back to kubernetes env var")

		if value, ok := os.LookupEnv(MqttDefaultBrokerEnv); ok {
			c.Broker = value
		} else {
			return fmt.Errorf("env var %q not set. cannot resolve broker address", MqttDefaultBrokerEnv)
		}
	}

	if c.ClientId == "" {
		return fmt.Errorf("field %q must be set", "clientid")
	}

	return c.MqttTlsClientConfig.Verify()
}
