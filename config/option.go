package config

import (
	"github.com/spf13/afero"
	"github.com/spf13/viper"
)

const defaultConfigFilenameWithoutExt = "config"

type ViperOption func(v *viper.Viper)

func WithDefaultConfigName() ViperOption {
	return WithConfigName(defaultConfigFilenameWithoutExt)
}

/// PATH (directory which get searched for config files)
func WithConfigPath(path string) ViperOption {
	return func(v *viper.Viper) {
		v.AddConfigPath(path)
	}
}

/// ConfigName (config file base name i.e. "config" => config.*)
func WithConfigName(configName string) ViperOption {
	return func(v *viper.Viper) {
		v.SetConfigName(configName)
	}
}

/// ConfigType (config file extension i.e. "yaml" => [ConfigName].yaml)
func WithConfigType(configType string) ViperOption {
	return func(v *viper.Viper) {
		v.SetConfigType(configType)
	}
}

/// ConfigFile (complete config file name i.e. "some-config-file.json" => some-config-file.json)
func WithConfigFile(configFile string) ViperOption {
	return func(v *viper.Viper) {
		v.SetConfigFile(configFile)
	}
}

/// FileSystem (sets the used filesystem to read config file from)
func WithFileSystem(fs afero.Fs) ViperOption {
	return func(v *viper.Viper) {
		v.SetFs(fs)
	}
}

func WithLocalPath() ViperOption {
	return WithConfigPath(".")
}
