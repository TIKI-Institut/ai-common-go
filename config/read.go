package config

import (
	"fmt"
	"github.com/rs/zerolog/log"
	"github.com/spf13/afero"
	"github.com/spf13/viper"
	"gopkg.in/yaml.v2"
)

func ReadConfig(envVarPrefix string, configInstance interface{}, options ...ViperOption) error {
	if err := defaultReadConfig(envVarPrefix, configInstance, options...); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			//if config file is missing, it will be ignored
			// TODO This log is not context-scoped, so will not be transferred to any client
			log.Debug().Err(err).Msg("file was not found, will proceed without reading it")

			//knockout all config file based options to ensure there will not be an "ConfigFileNotFoundError"
			tempVirtualConfigFile := "config.yaml"
			memoryFileSystem, err := prepareDummyFilesystem(tempVirtualConfigFile, configInstance)
			if err != nil {
				return fmt.Errorf("unable to prepare in-memory filesystem (%w)", err)
			}
			var fallbackOptions = append(options, WithConfigFile(tempVirtualConfigFile), WithFileSystem(memoryFileSystem))
			return defaultReadConfig(envVarPrefix, configInstance, fallbackOptions...)
		}
		return err
	}

	return nil
}

func defaultReadConfig(envVarPrefix string, configInstance interface{}, options ...ViperOption) error {
	err, v := defaultViperConfig(envVarPrefix, configInstance)

	if err != nil {
		return err
	}

	for _, opt := range options {
		opt(v)
	}

	// if config not found, use only ENV
	if err = v.MergeInConfig(); err != nil {
		return err
	}

	return v.Unmarshal(configInstance)
}

func prepareDummyFilesystem(targetedFile string, instance interface{}) (afero.Fs, error) {
	memoryFileSystem := &afero.MemMapFs{}

	f, err := memoryFileSystem.Create(targetedFile)
	if err != nil {
		return nil, fmt.Errorf("unable to create file %s (%w)", targetedFile, err)
	}

	enc := yaml.NewEncoder(f)
	if err = enc.Encode(instance); err != nil {
		return nil, fmt.Errorf("unable to write given instance to memory file %s (%w)", targetedFile, err)
	}

	if err = f.Close(); err != nil {
		return nil, fmt.Errorf("unable to close file %s (%w)", targetedFile, err)
	}

	return memoryFileSystem, nil
}
