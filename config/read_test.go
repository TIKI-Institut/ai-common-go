package config

import (
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

type TestConfigObject struct {
	A  string `mapstructure:"a"`
	B  int    `mapstructure:"b"`
	CA string `mapstructure:"c_a"`
	C  struct {
		D float32 `mapstructure:"d"`
	} `mapstructure:"c"`
}

func TestReadOnlyFromEnvVar(t *testing.T) {

	target := TestConfigObject{}

	err := os.Setenv("CONFIG_TEST_C_D", "7.11")
	assert.NoError(t, err)
	err = os.Setenv("CONFIG_TEST_A", "FOO")
	assert.NoError(t, err)

	err = ReadConfig("CONFIG_TEST", &target)
	assert.NoError(t, err)

	assert.Equal(t, "FOO", target.A)
	assert.Equal(t, float32(7.11), target.C.D)

	err = os.Unsetenv("CONFIG_TEST_C_D")
	assert.NoError(t, err)
	err = os.Unsetenv("CONFIG_TEST_A")
	assert.NoError(t, err)
}

func TestReadFromEnvConfiguredFile(t *testing.T) {
	target := TestConfigObject{}
	err := ReadConfig("", &target, WithDefaultConfigName(), WithLocalPath())
	assert.NoError(t, err)

	assert.Equal(t, "Blub local file", target.A)
	assert.Equal(t, 4712, target.B)
	assert.Equal(t, "Some sample string", target.CA)
}

func TestReadConfigFromFileJson(t *testing.T) {

	target := TestConfigObject{}

	err := ReadConfig("", &target, WithConfigFile("./testdata/test_correct_file.json"))
	assert.NoError(t, err)

	assert.Equal(t, "Blub", target.A)
	assert.Equal(t, 4711, target.B)
	assert.Equal(t, "Some sample string", target.CA)
}

func TestReadConfigFromFileYaml(t *testing.T) {

	target := TestConfigObject{}

	err := ReadConfig( "", &target, WithConfigFile("./testdata/test.yaml"))
	assert.NoError(t, err)

	assert.Equal(t, "Blub", target.A)
	assert.Equal(t, 4711, target.B)
	assert.Equal(t, "Some sample string", target.CA)
}

func TestReadConfigFromFileWithoutExtensionYaml(t *testing.T) {

	target := TestConfigObject{}

	err := ReadConfig("", &target, WithConfigPath("./testdata"), WithConfigName("testYaml"))
	assert.NoError(t, err)

	assert.Equal(t, "Blub", target.A)
	assert.Equal(t, 4711, target.B)
	assert.Equal(t, "Some sample string", target.CA)
}

func TestReadConfigFromFileWithoutExtensionStartingWithADot(t *testing.T) {

	target := TestConfigObject{}

	err := ReadConfig("", &target, WithConfigPath("./testdata"), WithConfigName(".testYaml"))
	assert.NoError(t, err)

	assert.Equal(t, "Blub", target.A)
	assert.Equal(t, 4711, target.B)
	assert.Equal(t, "Some sample string", target.CA)
}

func TestReadConfigFromFileWithoutExtensionJson(t *testing.T) {

	target := TestConfigObject{}

	err := ReadConfig("", &target, WithConfigPath("./testdata"), WithConfigName("testJson"))
	assert.NoError(t, err)

	assert.Equal(t, "Blub", target.A)
	assert.Equal(t, 4711, target.B)
	assert.Equal(t, "Some sample string", target.CA)
}

func TestOverrideThroughEnvVar(t *testing.T) {

	target := TestConfigObject{}

	err := os.Setenv("CONFIG_TEST_A", "FOO")
	assert.NoError(t, err)

	err = ReadConfig( "CONFIG_TEST", &target, WithConfigFile("./testdata/test_correct_file.json"))
	assert.NoError(t, err)

	assert.Equal(t, "FOO", target.A)
	assert.Equal(t, 4711, target.B)

	err = os.Unsetenv("CONFIG_TEST_A")
	assert.NoError(t, err)
}

func TestDeepOverrideThroughEnvVar(t *testing.T) {

	target := TestConfigObject{}

	err := os.Setenv("CONFIG_TEST_C_D", "7.11")
	assert.NoError(t, err)

	err = ReadConfig("CONFIG_TEST", &target, WithConfigFile("./testdata/test_correct_file.json"))
	assert.NoError(t, err)

	assert.Equal(t, "Blub", target.A)
	assert.Equal(t, 4711, target.B)
	assert.Equal(t, float32(7.11), target.C.D)

	err = os.Unsetenv("CONFIG_TEST_C_D")
	assert.NoError(t, err)
}
