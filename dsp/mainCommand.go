package dsp

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/cli"
	roleStore "bitbucket.org/TIKI-Institut/ai-common-go/v2/dsp/roles"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/qos"
	"context"
	"github.com/spf13/cobra"
)

func MainCommand(ctx context.Context) *cobra.Command {
	mainCommand := &cobra.Command{}
	mainCommand.AddCommand(rolesCommand())
	mainCommand.AddCommand(qosRegisterCommand())
	mainCommand.AddCommand(qosRunCommand())

	if ctx == nil {
		ctx = context.Background()
	}

	ctx = roleStore.EnsureContextWithRoleCollector(ctx)
	ctx = qos.EnsureContextWithQosEntryCollector(ctx)

	mainCommand.SetContext(ctx)

	return mainCommand
}

//goland:noinspection GoUnusedExportedFunction
func AddSubCommand(cmd *cobra.Command, subCmd *cobra.Command, roles []string) {

	cmd.AddCommand(subCmd)

	if roles != nil {
		//Need to register roles
		roleStore.Add(cmd.Context(), roles...)
		subCmd.PreRunE = cli.JoinCommandEvents(subCmd.PreRunE, roleChecking(roles), false)
	}

}
