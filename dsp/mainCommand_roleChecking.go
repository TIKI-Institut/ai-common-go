package dsp

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/auth"
	"fmt"
	"github.com/spf13/cobra"
	"os"
)

// role checking needs a set of env vars to work (when it gets deployed by ai-provisioner / deployment server within DSP)
//
//  - JWT_ISSUER         the token ISSUER that will be used to validate all provided tokens
//                       (i.e. https://auth.tiki-dsp.io/auth/realms/tiki)
//
//  - OAUTH_CLIENT_ID    the client that will be used to validate all provided tokens
//                       (i.e. ai-examples-python-dev-web-py36)
func roleChecking(roles []string) func(cmd *cobra.Command, args []string) error {
	return func(cmd *cobra.Command, args []string) error {
		//get target clientId the token needs to be checked against
		oauthClientId, isSet := os.LookupEnv("OAUTH_CLIENT_ID")
		if !isSet {
			return &auth.RoleCheckFailedError{Cause: fmt.Errorf(`env var "OAUTH_CLIENT_ID" not set`)}
		}

		rawToken, err := readRawToken(cmd.Context())
		if err != nil {
			return err
		}

		jwtIssuer, isSet := os.LookupEnv("JWT_ISSUER")
		if !isSet {
			return &auth.RoleCheckFailedError{Cause: fmt.Errorf(`env var "JWT_ISSUER" not set`)}
		}

		tokenRoles, err := auth.ResolveTokenRoles(cmd.Context(), jwtIssuer, rawToken, oauthClientId)
		if err != nil {
			return err
		}

		roleChecker := auth.DefaultRoleCheckingHandlerFunc(roles...)

		return roleChecker(oauthClientId, tokenRoles)
	}
}
