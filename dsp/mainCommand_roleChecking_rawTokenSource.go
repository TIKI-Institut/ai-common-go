package dsp

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/auth"
	"context"
	"fmt"
	"github.com/rs/zerolog/log"
	"io/fs"
	"os"
	"time"
)

type roleCheckFileSystemContextKey int

var roleCheckFileSystemContextKeyValue roleCheckFileSystemContextKey

type roleCheckRetryPeriodContextKey int

var roleCheckRetryPeriodContextKeyValue roleCheckRetryPeriodContextKey

var tokenFile = "dsp/auth/token"

func readRawToken(ctx context.Context) (string, error) {

	logger := log.Ctx(ctx)
	fileSys := getRoleCheckingFS(ctx)

	var rawToken string
	var err error

	for i := 0; i <= 2; i++ {
		rawToken, err = readTokenFromFS(fileSys, tokenFile)

		if err != nil {
			logger.Error().Err(err).Str("file", tokenFile).Dur("retry in", retryPeriod(ctx)).Msg("cannot read from file, will retry reading")
			time.Sleep(retryPeriod(ctx))

			//after 3 attempts
			if i == 2 {
				return "", &auth.RoleCheckFailedError{Cause: fmt.Errorf("cannot read from file %q after 3 attempts (error :%w)", tokenFile, err)}
			} else {
				continue
			}
		} else {
			break
		}

	}
	return rawToken, nil
}

func getRoleCheckingFS(ctx context.Context) fs.FS {

	ctxFileSystem := ctx.Value(roleCheckFileSystemContextKeyValue)

	if ctxFileSystem == nil {
		return os.DirFS("/")
	}

	conversion, ok := ctxFileSystem.(fs.FS)

	if !ok {
		log.Ctx(ctx).Warn().Msg("unable to convert given FileSystem to type fs.FS; falling back to HDD")
		return os.DirFS("/")
	}

	return conversion
}

func readTokenFromFS(fSystem fs.FS, tokenFile string) (string, error) {
	var err error
	var file fs.File

	file, err = fSystem.Open(tokenFile)

	if err != nil {
		return "", fmt.Errorf("unable to open file %s (%w)", tokenFile, err)
	}

	var fileStats fs.FileInfo
	fileStats, err = file.Stat()

	if err != nil {
		return "", fmt.Errorf("unable to get file stats (%w)", err)
	}

	fileSize := int(fileStats.Size())
	fileContent := make([]byte, fileSize)

	var readBytes int
	readBytes, err = file.Read(fileContent)
	if err != nil {
		return "", fmt.Errorf("unable to read file (%w)", err)
	}

	if readBytes != fileSize {
		return "", fmt.Errorf("unable to read complete file read %d off %d bytes", readBytes, fileSize)
	}

	err = file.Close()
	if err != nil {
		return "", fmt.Errorf("unable to close file (%w)", err)
	}

	return string(fileContent), nil
}

func retryPeriod(ctx context.Context) time.Duration {
	if retryPeriod := ctx.Value(roleCheckRetryPeriodContextKeyValue); retryPeriod != nil {
		return retryPeriod.(time.Duration)
	}
	return 10 * time.Second
}
