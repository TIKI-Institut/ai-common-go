package dsp

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/auth"
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"io/fs"
	"testing"
	"testing/fstest"
	"time"
)

type retryAwareFS int

type testTokenFile string

func (t *testTokenFile) Name() string               { return "token" }
func (t *testTokenFile) Size() int64                { return int64(len(string(*t))) }
func (t *testTokenFile) Mode() fs.FileMode          { return fs.ModePerm }
func (t *testTokenFile) ModTime() time.Time         { return time.Time{} }
func (t *testTokenFile) IsDir() bool                { return false }
func (t *testTokenFile) Sys() interface{}           { return nil }
func (t *testTokenFile) Stat() (fs.FileInfo, error) { return t, nil }
func (t *testTokenFile) Read(i []byte) (int, error) { return copy(i, []byte(*t)), nil }
func (t *testTokenFile) Close() error               { return nil }

func (r *retryAwareFS) Open(name string) (fs.File, error) {

	if name != tokenFile {
		return nil, &fs.PathError{
			Op:   "Open",
			Path: name,
			Err:  fs.ErrNotExist,
		}
	}
	if *r == 0 {
		f := testTokenFile("some-token")
		return &f, nil
	}

	*r--
	return nil, &fs.PathError{
		Op:   "Open",
		Path: name,
		Err:  fs.ErrNotExist,
	}
}

func TestRawTokenSource(t *testing.T) {

	t.Run("found file", func(t *testing.T) {
		tempFs := fstest.MapFS{
			tokenFile: &fstest.MapFile{Data: []byte("some-token")},
		}

		ctx := context.Background()
		ctx = context.WithValue(ctx, roleCheckFileSystemContextKeyValue, tempFs)
		ctx = context.WithValue(ctx, roleCheckRetryPeriodContextKeyValue, 100*time.Millisecond)

		token, err := readRawToken(ctx)

		assert.EqualValues(t, "some-token", token)
		assert.NoError(t, err)
	})

	t.Run("found file (in retry)", func(t *testing.T) {
		fs := retryAwareFS(2)
		ctx := context.Background()
		ctx = context.WithValue(ctx, roleCheckFileSystemContextKeyValue, &fs)
		ctx = context.WithValue(ctx, roleCheckRetryPeriodContextKeyValue, 100*time.Millisecond)

		token, err := readRawToken(ctx)

		assert.EqualValues(t, "some-token", token)
		assert.NoError(t, err)
	})

	t.Run("no file found(to many retries)", func(t *testing.T) {
		fs := retryAwareFS(20)
		ctx := context.Background()
		ctx = context.WithValue(ctx, roleCheckFileSystemContextKeyValue, &fs)
		ctx = context.WithValue(ctx, roleCheckRetryPeriodContextKeyValue, 100*time.Millisecond)

		token, err := readRawToken(ctx)

		assert.EqualValues(t, "", token)
		assert.Error(t, err)
		var targetErr *auth.RoleCheckFailedError
		assert.True(t, errors.As(err, &targetErr))
		assert.EqualError(t, targetErr.Cause, "cannot read from file \"dsp/auth/token\" after 3 attempts (error :unable to open file dsp/auth/token (Open dsp/auth/token: file does not exist))")
	})



}
