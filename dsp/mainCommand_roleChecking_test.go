package dsp

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/auth"
	roleStore "bitbucket.org/TIKI-Institut/ai-common-go/v2/dsp/roles"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/tests"
	"context"
	"errors"
	"fmt"
	"github.com/spf13/cobra"
	"github.com/stretchr/testify/assert"
	"testing"
	"testing/fstest"
	"time"
)

type testTokenRoles []string

func (t testTokenRoles) ClientRoles(clientId string) []string {
	return t
}

type testGetOfflineTokenFailureJwtContext struct {
	*testing.T
	auth.JwtContext
	roles []string
}

func (tJwtContext *testGetOfflineTokenFailureJwtContext) RolesFromToken(ctx context.Context, token string, clientId string) (auth.TokenRoles, error) {

	assert.EqualValues(tJwtContext, "some-token", token)
	assert.EqualValues(tJwtContext, "oauthClientId", clientId)

	return testTokenRoles(tJwtContext.roles), nil

}

func TestRoleChecking(t *testing.T) {

	t.Run("with roles", func(t *testing.T) {

		t.Run("no roles file", func(t *testing.T) {

			tests.WithEnvVars(func() {
				ctx := roleStore.EnsureContextWithRoleCollector(context.Background())
				ctx = context.WithValue(ctx, roleCheckRetryPeriodContextKeyValue, time.Second)
				root := MainCommand(ctx)
				called := false
				command := cobra.Command{
					Use: "cmd",
					RunE: func(cmd *cobra.Command, args []string) error {
						called = true
						return nil
					},
				}
				AddSubCommand(root, &command, []string{"a", "b", "c"})
				root.SetArgs([]string{"cmd"})
				err := root.Execute()
				assert.Error(t, err)
				var targetErr *auth.RoleCheckFailedError
				assert.True(t, errors.As(err, &targetErr))
				assert.EqualError(t, targetErr.Cause, fmt.Sprintf("cannot read from file \"dsp/auth/token\" after 3 attempts (error :unable to open file dsp/auth/token (%s))", osSpecificMissingFileError()))
				assert.False(t, called)
			}, tests.EnvDecl{Name: "OAUTH_CLIENT_ID", Value: "oauthClientId"})

		})

		t.Run("with roles file", func(t *testing.T) {
			t.Run("missing ENV (JWT_ISSUER)", func(t *testing.T) {

				tests.WithEnvVars(func() {
					tempFs := fstest.MapFS{
						tokenFile: &fstest.MapFile{Data: []byte("some-data")},
					}

					ctx := roleStore.EnsureContextWithRoleCollector(context.Background())
					ctx = context.WithValue(ctx, roleCheckFileSystemContextKeyValue, tempFs)

					root := MainCommand(ctx)
					called := false
					command := cobra.Command{
						Use: "cmd",
						RunE: func(cmd *cobra.Command, args []string) error {
							called = true
							return nil
						},
					}
					AddSubCommand(root, &command, []string{"a", "b", "c"})

					root.SetArgs([]string{"cmd"})
					err := root.Execute()
					assert.Error(t, err)
					var targetErr *auth.RoleCheckFailedError
					assert.True(t, errors.As(err, &targetErr))
					assert.EqualError(t, targetErr.Cause, "env var \"JWT_ISSUER\" not set")
					assert.False(t, called)
				}, tests.EnvDecl{Name: "OAUTH_CLIENT_ID", Value: "oauthClientId"})

			})

			t.Run("missing ENV (OAUTH_CLIENT_ID)", func(t *testing.T) {

				tests.WithEnvVars(func() {

					tempFs := fstest.MapFS{
						tokenFile: &fstest.MapFile{Data: []byte("some-data")},
					}

					ctx := roleStore.EnsureContextWithRoleCollector(context.Background())
					ctx = context.WithValue(ctx, roleCheckFileSystemContextKeyValue, tempFs)

					root := MainCommand(ctx)
					called := false
					command := cobra.Command{
						Use: "cmd",
						RunE: func(cmd *cobra.Command, args []string) error {
							called = true
							return nil
						},
					}
					AddSubCommand(root, &command, []string{"a", "b", "c"})

					root.SetArgs([]string{"cmd"})
					err := root.Execute()
					assert.Error(t, err)
					var targetErr *auth.RoleCheckFailedError
					assert.True(t, errors.As(err, &targetErr))
					assert.EqualError(t, targetErr.Cause, "env var \"OAUTH_CLIENT_ID\" not set")
					assert.False(t, called)
				}, tests.EnvDecl{Name: "JWT_ISSUER", Value: "jwtIssuer"})

			})

			t.Run("missing role", func(t *testing.T) {

				tests.WithEnvVars(func() {

					tempFs := fstest.MapFS{
						tokenFile: &fstest.MapFile{Data: []byte("some-token")},
					}

					ctx := roleStore.EnsureContextWithRoleCollector(context.Background())
					ctx = context.WithValue(ctx, roleCheckFileSystemContextKeyValue, tempFs)
					ctx = auth.WithJwtContext(ctx, "jwtIssuer", &testGetOfflineTokenFailureJwtContext{T: t, roles: []string{"a", "c"}})

					root := MainCommand(ctx)
					called := false
					command := cobra.Command{
						Use: "cmd",
						RunE: func(cmd *cobra.Command, args []string) error {
							called = true
							return nil
						},
					}
					AddSubCommand(root, &command, []string{"a", "b", "c"})

					root.SetArgs([]string{"cmd"})
					err := root.Execute()
					assert.Error(t, err)
					var targetErr *auth.RoleCheckFailedError
					assert.True(t, errors.As(err, &targetErr))
					assert.EqualError(t, targetErr.Cause, "missing roles")
					assert.False(t, called)
				}, tests.EnvDecl{Name: "JWT_ISSUER", Value: "jwtIssuer"}, tests.EnvDecl{Name: "OAUTH_CLIENT_ID", Value: "oauthClientId"})

			})

			t.Run("positive check", func(t *testing.T) {

				tests.WithEnvVars(func() {

					tempFs := fstest.MapFS{
						tokenFile: &fstest.MapFile{Data: []byte("some-token")},
					}

					ctx := roleStore.EnsureContextWithRoleCollector(context.Background())
					ctx = context.WithValue(ctx, roleCheckFileSystemContextKeyValue, tempFs)
					ctx = auth.WithJwtContext(ctx, "jwtIssuer", &testGetOfflineTokenFailureJwtContext{T: t, roles: []string{"a", "b", "c"}})

					root := MainCommand(ctx)
					called := false
					command := cobra.Command{
						Use: "cmd",
						RunE: func(cmd *cobra.Command, args []string) error {
							called = true
							return nil
						},
					}
					AddSubCommand(root, &command, []string{"a", "b", "c"})

					root.SetArgs([]string{"cmd"})
					err := root.Execute()
					assert.Nil(t, err)
					assert.True(t, called)
				}, tests.EnvDecl{Name: "JWT_ISSUER", Value: "jwtIssuer"}, tests.EnvDecl{Name: "OAUTH_CLIENT_ID", Value: "oauthClientId"})

			})

		})

	})

	t.Run("no roles", func(t *testing.T) {
		ctx := roleStore.EnsureContextWithRoleCollector(context.Background())
		ctx = context.WithValue(ctx, roleCheckRetryPeriodContextKeyValue, time.Second)
		root := MainCommand(ctx)
		called := false
		command := cobra.Command{
			Use: "cmd",
			RunE: func(cmd *cobra.Command, args []string) error {
				called = true
				return nil
			},
		}
		AddSubCommand(root, &command, []string{})

		root.SetArgs([]string{"cmd"})
		err := root.Execute()
		assert.Error(t, err)
		var targetErr *auth.RoleCheckFailedError
		assert.True(t, errors.As(err, &targetErr))
		assert.Error(t, targetErr.Cause)
		assert.False(t, called)
	})

	t.Run("without roles (insecure)", func(t *testing.T) {
		ctx := roleStore.EnsureContextWithRoleCollector(context.Background())
		root := MainCommand(ctx)
		called := false
		command := cobra.Command{
			Use: "cmd",
			RunE: func(cmd *cobra.Command, args []string) error {
				called = true
				return nil
			},
		}
		AddSubCommand(root, &command, nil)

		root.SetArgs([]string{"cmd"})
		err := root.Execute()
		assert.NoError(t, err)
		assert.True(t, called)
	})

}
