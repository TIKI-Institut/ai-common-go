package dsp

import (
	roleStore "bitbucket.org/TIKI-Institut/ai-common-go/v2/dsp/roles"
	"context"
	"github.com/spf13/cobra"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestRoleRegistration(t *testing.T) {

	t.Run("with roles", func(t *testing.T) {
		root := MainCommand(context.Background())
		command := cobra.Command{}
		AddSubCommand(root, &command, []string{"a", "b", "c"})
		assert.Nil(t, root.PreRunE)
		assert.NotNil(t, command.PreRunE)
		assert.EqualValues(t, []string{"a", "b", "c"}, roleStore.Get(root.Context()))
	})

	t.Run("no roles", func(t *testing.T) {
		root := MainCommand(context.Background())
		command := cobra.Command{}
		AddSubCommand(root, &command, []string{})
		assert.Nil(t, root.PreRunE)
		assert.NotNil(t, command.PreRunE)
		assert.EqualValues(t, []string(nil), roleStore.Get(root.Context()))
	})

	t.Run("without roles (insecure)", func(t *testing.T) {
		root := MainCommand(context.Background())
		command := cobra.Command{
			Use: "test",
		}
		AddSubCommand(root, &command, nil)
		assert.Nil(t, root.PreRunE)
		assert.Nil(t, command.PreRunE)
		assert.EqualValues(t, []string(nil), roleStore.Get(root.Context()))
	})

}
