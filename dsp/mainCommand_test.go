package dsp

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/auth"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/tests"
	"context"
	"errors"
	"fmt"
	"github.com/spf13/cobra"
	"github.com/stretchr/testify/assert"
	"testing"
	"testing/fstest"
	"time"
)

func TestMainCommand(t *testing.T) {

	assertError := func(t *testing.T, err error, expectedCauseError string) {
		assert.Error(t, err)
		var targetErr *auth.RoleCheckFailedError
		assert.True(t, errors.As(err, &targetErr))
		assert.EqualError(t, targetErr.Cause, expectedCauseError)
	}

	createRoot := func(ctx context.Context, args ...string) *cobra.Command {
		if len(args) == 0 {
			args = []string{""}
		}

		cmd := MainCommand(ctx)
		cmd.SetArgs(args)
		return cmd
	}

	t.Run("root command", func(t *testing.T) {
		cmd := createRoot(context.Background())
		assert.NoError(t, cmd.Execute())
	})

	t.Run("command without role checking", func(t *testing.T) {
		var cmdRan = false
		subCmd := cobra.Command{
			Use: "no-role",
			RunE: func(cmd *cobra.Command, args []string) error {
				cmdRan = true
				return nil
			},
		}

		rootCmd := createRoot(context.Background(), "no-role")

		AddSubCommand(rootCmd, &subCmd, nil)
		assert.NoError(t, rootCmd.Execute())
		assert.True(t, cmdRan)
	})

	t.Run("command with role checking", func(t *testing.T) {

		t.Run("env var OAUTH_CLIENT_ID missing", func(t *testing.T) {
			var cmdRan = false
			subCmd := cobra.Command{
				Use: "role-cmd",
				RunE: func(cmd *cobra.Command, args []string) error {
					cmdRan = true
					return nil
				},
			}

			rootCmd := createRoot(context.Background(), "role-cmd")

			AddSubCommand(rootCmd, &subCmd, []string{"some-role"})
			assertError(t, rootCmd.Execute(), "env var \"OAUTH_CLIENT_ID\" not set")
			assert.False(t, cmdRan)
		})

		t.Run("no token file", func(t *testing.T) {

			tests.WithEnvVars(func() {
				var cmdRan = false
				subCmd := cobra.Command{
					Use: "role-cmd",
					RunE: func(cmd *cobra.Command, args []string) error {
						cmdRan = true
						return nil
					},
				}

				ctx := context.Background()
				ctx = context.WithValue(ctx, roleCheckRetryPeriodContextKeyValue, 100*time.Millisecond)
				rootCmd := createRoot(ctx, "role-cmd")

				AddSubCommand(rootCmd, &subCmd, []string{"some-role"})
				assertError(t, rootCmd.Execute(), fmt.Sprintf("cannot read from file \"dsp/auth/token\" after 3 attempts (error :unable to open file dsp/auth/token (%s))", osSpecificMissingFileError()))
				assert.False(t, cmdRan)

			}, tests.EnvDecl{Name: "OAUTH_CLIENT_ID", Value: "oauthClientId"})

		})

		t.Run("env var JWT_ISSUER missing", func(t *testing.T) {

			tests.WithEnvVars(func() {
				var cmdRan = false
				subCmd := cobra.Command{
					Use: "role-cmd",
					RunE: func(cmd *cobra.Command, args []string) error {
						cmdRan = true
						return nil
					},
				}

				tempFs := fstest.MapFS{
					tokenFile: &fstest.MapFile{Data: []byte("some-token")},
				}

				ctx := context.Background()
				ctx = context.WithValue(ctx, roleCheckRetryPeriodContextKeyValue, 100*time.Millisecond)
				ctx = context.WithValue(ctx, roleCheckFileSystemContextKeyValue, tempFs)

				rootCmd := createRoot(ctx, "role-cmd")

				AddSubCommand(rootCmd, &subCmd, []string{"some-role"})
				assertError(t, rootCmd.Execute(), "env var \"JWT_ISSUER\" not set")
				assert.False(t, cmdRan)

			}, tests.EnvDecl{Name: "OAUTH_CLIENT_ID", Value: "oauthClientId"})
		})

		t.Run("missing role", func(t *testing.T) {

			tests.WithEnvVars(func() {
				var cmdRan = false
				subCmd := cobra.Command{
					Use: "role-cmd",
					RunE: func(cmd *cobra.Command, args []string) error {
						cmdRan = true
						return nil
					},
				}

				tempFs := fstest.MapFS{
					tokenFile: &fstest.MapFile{Data: []byte("some-token")},
				}

				ctx := context.Background()
				ctx = context.WithValue(ctx, roleCheckRetryPeriodContextKeyValue, 100*time.Millisecond)
				ctx = context.WithValue(ctx, roleCheckFileSystemContextKeyValue, tempFs)
				ctx = auth.WithJwtContext(ctx, "jwtIssuer", &testGetOfflineTokenFailureJwtContext{T: t, roles: []string{"A", "B", "C"}})

				rootCmd := createRoot(ctx, "role-cmd")

				AddSubCommand(rootCmd, &subCmd, []string{"some-role"})
				assertError(t, rootCmd.Execute(), "missing roles")
				assert.False(t, cmdRan)

			}, tests.EnvDecl{Name: "OAUTH_CLIENT_ID", Value: "oauthClientId"}, tests.EnvDecl{Name: "JWT_ISSUER", Value: "jwtIssuer"})
		})

		t.Run("positive check", func(t *testing.T) {

			tests.WithEnvVars(func() {
				var cmdRan = false
				subCmd := cobra.Command{
					Use: "role-cmd",
					RunE: func(cmd *cobra.Command, args []string) error {
						cmdRan = true
						return nil
					},
				}

				tempFs := fstest.MapFS{
					tokenFile: &fstest.MapFile{Data: []byte("some-token")},
				}

				ctx := context.Background()
				ctx = context.WithValue(ctx, roleCheckRetryPeriodContextKeyValue, 100*time.Millisecond)
				ctx = context.WithValue(ctx, roleCheckFileSystemContextKeyValue, tempFs)
				ctx = auth.WithJwtContext(ctx, "jwtIssuer", &testGetOfflineTokenFailureJwtContext{T: t, roles: []string{"A", "B", "C"}})

				rootCmd := createRoot(ctx, "role-cmd")

				AddSubCommand(rootCmd, &subCmd, []string{"B"})
				assert.NoError(t, rootCmd.Execute())
				assert.True(t, cmdRan)

			}, tests.EnvDecl{Name: "OAUTH_CLIENT_ID", Value: "oauthClientId"}, tests.EnvDecl{Name: "JWT_ISSUER", Value: "jwtIssuer"})
		})

		t.Run("command with login checking", func(t *testing.T) {
			tests.WithEnvVars(func() {
				var cmdRan = false
				subCmd := cobra.Command{
					Use: "role-cmd",
					RunE: func(cmd *cobra.Command, args []string) error {
						cmdRan = true
						return nil
					},
				}

				tempFs := fstest.MapFS{
					tokenFile: &fstest.MapFile{Data: []byte("some-token")},
				}

				ctx := context.Background()
				ctx = context.WithValue(ctx, roleCheckRetryPeriodContextKeyValue, 100*time.Millisecond)
				ctx = context.WithValue(ctx, roleCheckFileSystemContextKeyValue, tempFs)
				ctx = auth.WithJwtContext(ctx, "jwtIssuer", &testGetOfflineTokenFailureJwtContext{T: t, roles: []string{"A", "B", "C"}})

				rootCmd := createRoot(ctx, "role-cmd")

				AddSubCommand(rootCmd, &subCmd, []string{})
				assert.NoError(t, rootCmd.Execute())
				assert.True(t, cmdRan)

			}, tests.EnvDecl{Name: "OAUTH_CLIENT_ID", Value: "oauthClientId"}, tests.EnvDecl{Name: "JWT_ISSUER", Value: "jwtIssuer"})
		})

	})

}
