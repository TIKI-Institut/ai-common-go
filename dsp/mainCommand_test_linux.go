package dsp

func osSpecificMissingFileError() string {
	return "open dsp/auth/token: no such file or directory"
}
