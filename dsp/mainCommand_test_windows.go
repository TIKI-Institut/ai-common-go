package dsp

func osSpecificMissingFileError() string {
	return "open dsp/auth/token: The system cannot find the path specified."
}
