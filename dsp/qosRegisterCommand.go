package dsp

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/mqtt/client"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/qos"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/qos/model"
	"context"
	"encoding/json"
	mqtt "github.com/eclipse/paho.mqtt.golang"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
)

func executeRegisterCommand(ctx context.Context, mqttClient mqtt.Client) {
	if token := mqttClient.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}
	defer mqttClient.Disconnect(250)
	endpoint := model.AsyncQosEndpoint{
		Entries:         qos.AsyncEntries(ctx),
		FlavorReference: model.CurrentFlavor(),
	}
	serialized, err := json.Marshal(endpoint)
	if err != nil {
		// TODO This log is not context-scoped, so will not be transferred to any client
		log.Error().Err(err).Msg("can not marshal AsyncQosEndpoint")
		return
	}
	token := mqttClient.Publish("qos/register/async", 0, false, serialized)
	token.Wait()

}

func qosRegisterCommand() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "qos-register",
		Short: "Registers all async QoS endpoints. Executed during 'provision' step of ai-provisioner",
		Long:  ``,
		Run: func(cmd *cobra.Command, args []string) {
			executeRegisterCommand(cmd.Context(), mqtt.NewClient(client.NewNamespaceClientOptions("qos-mqtt")))
		},
	}

	return cmd
}
