package dsp

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/qos"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/tests"
	"context"
	mqtt "github.com/eclipse/paho.mqtt.golang"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

type TestToken struct {
}

func (t TestToken) Wait() bool {
	return false
}

func (t TestToken) WaitTimeout(time.Duration) bool {
	panic("implement me")
}

func (t TestToken) Error() error {
	panic("implement me")
}

type TestClient struct {
	msg *string
}

func (t TestClient) IsConnected() bool {
	panic("implement me")
}

func (t TestClient) IsConnectionOpen() bool {
	panic("implement me")
}

func (t TestClient) Connect() mqtt.Token {
	return TestToken{}
}

func (t TestClient) Disconnect(_ uint) {

}

func (t TestClient) Publish(_ string, _ byte, _ bool, payload interface{}) mqtt.Token {
	pl := payload.([]byte)
	*t.msg = string(pl)
	return TestToken{}
}

func (t TestClient) Subscribe(_ string, _ byte, _ mqtt.MessageHandler) mqtt.Token {
	panic("implement me")
}

func (t TestClient) SubscribeMultiple(_ map[string]byte, _ mqtt.MessageHandler) mqtt.Token {
	panic("implement me")
}

func (t TestClient) Unsubscribe(_ ...string) mqtt.Token {
	panic("implement me")
}

func (t TestClient) AddRoute(_ string, _ mqtt.MessageHandler) {
	panic("implement me")
}

func (t TestClient) OptionsReader() mqtt.ClientOptionsReader {
	panic("implement me")
}

func TestQosRegisterCommand(t *testing.T) {
	tests.WithEnvVars(func() {
		ctx := qos.EnsureContextWithQosEntryCollector(context.Background())

		qos.RegisterAsyncEntry(ctx, qos.NewFunctionEntry("test1", func() float64 { return 23.3 }))
		message := ""
		testClient := TestClient{msg: &message}
		executeRegisterCommand(ctx, testClient)
		assert.Equal(t, message, "{\"entries\":[{\"name\":\"test1\",\"type\":\"Async\"}],\"flavorReference\":{\"name\":\"dspName\",\"principal\":\"\",\"environment\":\"\",\"flavorName\":\"\",\"flavorVersion\":\"\"}}")
	}, tests.EnvDecl{Name: "DSP_NAME", Value: "dspName"})
}
