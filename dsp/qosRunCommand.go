package dsp

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/mqtt/client"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/qos"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/qos/model"
	"context"
	"encoding/json"
	"fmt"
	mqtt "github.com/eclipse/paho.mqtt.golang"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"strconv"
)

// TODO log entries here are not context-scoped, so will not be transferred to any client
func executeRunCommand(ctx context.Context, client mqtt.Client, args []string) {
	if len(args) < 2 {
		log.Error().Msgf("qos-run missing arguments. Required positional arguments transactionId, entryName. Current arguments: %s", args)
		return
	}

	transactionId, err := strconv.ParseInt(args[0], 10, 64)
	if err != nil {
		log.Error().Err(err).Msgf("Invalid transactionId argument: %s", args[0])
		return
	}
	entryName := args[1]
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		log.Error().Err(token.Error()).Send()
		return
	}
	defer client.Disconnect(250)
	entry, entryExists := qos.AsyncEntry(ctx, entryName)
	var result model.AsyncQosRunResult
	if entryExists {
		score := entry.Run()
		msg := fmt.Sprintf("Async QoS run for %s completed", entryName)
		result = model.SuccessAsyncResult(transactionId, msg, score, entry.Name())
	} else {
		msg := fmt.Sprintf("Async QoS entry %s was not found", entryName)
		log.Error().Msg(msg)
		result = model.NotFoundAsyncResult(transactionId, msg)

	}
	serialized, err := json.Marshal(result)
	if err != nil {
		log.Error().Err(err).Msg("can not marshal AsyncQosRunResult")
	}
	token := client.Publish("qos/result/async", 0, false, serialized)
	token.Wait()
}

func qosRunCommand() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "qos-run",
		Short: "Runs a previously declared async QoS endpoint.",
		Long:  ``,
		Run: func(cmd *cobra.Command, args []string) {
			executeRunCommand(cmd.Context(), mqtt.NewClient(client.NewNamespaceClientOptions("qos-mqtt")), args)
		},
	}

	return cmd
}
