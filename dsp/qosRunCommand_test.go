package dsp

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/qos"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/qos/model"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/tests"
	"context"
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestAsyncQos(t *testing.T) {

	t.Run("EntryRegistrationThroughCommand", func(t *testing.T) {
		ctx := context.Background()
		mainCmd := MainCommand(ctx)

		assert.Len(t, qos.AsyncEntries(mainCmd.Context()), 0)
		assert.Len(t, qos.SyncEntries(mainCmd.Context()), 0)
		qos.RegisterAsyncEntry(mainCmd.Context(), qos.NewFunctionEntry(
			"testRunResult",
			func() float64 {
				return 54.32
			},
		))
		assert.Len(t, qos.AsyncEntries(mainCmd.Context()), 1)
		assert.Len(t, qos.SyncEntries(mainCmd.Context()), 0)
	})

	t.Run("EntryRun", func(t *testing.T) {
		ctx := context.Background()
		mainCmd := MainCommand(ctx)

		qos.RegisterAsyncEntry(mainCmd.Context(), qos.NewFunctionEntry(
			"testRunResult",
			func() float64 {
				return 54.32
			},
		))

		tests.WithEnvVars(func() {
			message := ""
			testClient := TestClient{msg: &message}
			executeRunCommand(mainCmd.Context(), testClient, []string{"23", "testRunResult"})
			result := model.AsyncQosRunResult{}
			assert.NoError(t, json.Unmarshal([]byte(message), &result))
			assert.Equal(t, int64(23), result.Id)
			assert.EqualValues(t, 54.32, *result.Score)
			assert.Equal(t, model.SUCCESS, result.Status)
			assert.Equal(t, "Async QoS run for testRunResult completed", result.StatusMessage)
			assert.Equal(t, model.QosEntryModel{
				Name: "testRunResult",
				Type: "Async",
			}, *result.QosEntry)
			assert.Equal(t, "dspName", result.FlavorReference.Name)
		}, tests.EnvDecl{Name: "DSP_NAME", Value: "dspName"})
	})
}
