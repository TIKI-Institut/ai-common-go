package roles

import (
	"context"
	"fmt"
	"sync"
)

type roleCollectorContextKey int

var roleCollectorContextKeyValue roleCollectorContextKey

type roleCollector struct {
	sync.RWMutex
	knownRoles []string
}

func EnsureContextWithRoleCollector(ctx context.Context) context.Context {

	if existingCollector := getCollector(ctx, false); existingCollector == nil {
		collector := &roleCollector{}
		return context.WithValue(ctx, roleCollectorContextKeyValue, collector)
	}

	return ctx
}

func Add(ctx context.Context, roles ...string) {
	collector := getCollector(ctx, true)
	collector.Lock()
	defer collector.Unlock()
	collector.knownRoles = removeDuplicates(append(collector.knownRoles, roles...))
}

func getCollector(ctx context.Context, panicIfAbsent bool) *roleCollector {
	roleCollector, ok := ctx.Value(roleCollectorContextKeyValue).(*roleCollector)
	if !ok && panicIfAbsent {
		panic(fmt.Errorf("no roleCollector was found in the supplied context"))
	}
	return roleCollector
}

func Get(ctx context.Context) []string {
	collector := getCollector(ctx, true)
	collector.RLock()
	defer collector.RUnlock()
	return collector.knownRoles
}

func removeDuplicates(strSlice []string) []string {
	allKeys := make(map[string]bool)
	var list []string
	for _, item := range strSlice {
		if _, value := allKeys[item]; !value {
			allKeys[item] = true
			list = append(list, item)
		}
	}
	return list
}
