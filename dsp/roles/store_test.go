package roles

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"sync"
	"testing"
)

func TestEnsureContextWithRoleCollector(t *testing.T) {
	testContext := context.Background()

	t.Run("with single role", func(t *testing.T) {

		testRole := "role-1"

		outcome := EnsureContextWithRoleCollector(testContext)

		// adding
		Add(outcome, testRole)

		// get
		roles := Get(outcome)

		assert.Equal(t, testRole, roles[0])
	})

	t.Run("with multiple roles", func(t *testing.T) {

		testRole_1 := "role-1"
		testRole_2 := "role-2"
		testRole_3 := "role-3"

		outcome := EnsureContextWithRoleCollector(testContext)

		// adding
		Add(outcome, testRole_1)
		Add(outcome, testRole_2)
		Add(outcome, testRole_3)

		// get
		roles := Get(outcome)

		assert.Equal(t, testRole_1, roles[0])
		assert.Equal(t, testRole_2, roles[1])
		assert.Equal(t, testRole_3, roles[2])
	})

	t.Run("with multiple roles async", func(t *testing.T) {

		wg := sync.WaitGroup{}

		var roleList []string

		for i := 0; i < 100; i++ {
			roleList = append(roleList, fmt.Sprintf("role-%d", i))
		}

		outcome := EnsureContextWithRoleCollector(testContext)

		// adding
		for _, role := range roleList {
			wg.Add(1)
			go func(role string, wg *sync.WaitGroup) {
				Add(outcome, role)
				wg.Done()
			}(role, &wg)
		}

		wg.Wait()

		// get
		roles := Get(outcome)

		for _, role := range roleList {
			assert.Contains(t, roles, role)
		}
	})

	t.Run("remove duplicates test", func(t *testing.T) {

		wg := sync.WaitGroup{}

		var roleList []string

		for i := 0; i < 100; i++ {
			roleList = append(roleList, fmt.Sprintf("role-%d", i))
		}

		outcome := EnsureContextWithRoleCollector(testContext)

		// adding
		for _, role := range roleList {
			wg.Add(1)
			go func(role string, wg *sync.WaitGroup) {
				Add(outcome, role)
				wg.Done()
			}(role, &wg)
		}

		wg.Wait()

		// get
		roles := Get(outcome)

		for _, role := range roleList {
			assert.Contains(t, roles, role)
		}
	})

}
