package dsp

import (
	roleStore "bitbucket.org/TIKI-Institut/ai-common-go/v2/dsp/roles"
	"encoding/json"
	"github.com/spf13/cobra"
	"io"
	"os"
	"path/filepath"
)

var rolesFile string

type rolesStruct struct {
	Roles []string `json:"roles"`
}

func rolesCommand() *cobra.Command {

	cmd := &cobra.Command{
		Use:   "roles",
		Short: "shows all referenced roles within jobs and web endpoints",
		Long:  ``,
		Args:  cobra.ExactArgs(0),
		RunE: func(cmd *cobra.Command, args []string) error {

			var wc io.WriteCloser = nil

			if rolesFile != "" {
				dir := filepath.Dir(rolesFile)

				if _, err := os.Stat(dir); os.IsNotExist(err) {
					_ = os.Mkdir(dir, os.ModePerm)
				}

				file, err := os.Create(rolesFile)

				if err != nil {
					return err
				}

				wc = file
			} else {
				wc = os.Stdout
			}

			roles := rolesStruct{Roles: roleStore.Get(cmd.Context())}

			defer wc.Close()
			return json.NewEncoder(wc).Encode(roles)
		},
	}

	cmd.PersistentFlags().StringVarP(&rolesFile, "file", "", "", "the target file where the roles should be placed.")

	return cmd
}
