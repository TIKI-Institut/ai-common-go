package web

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/auth"
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

// auth stuff
var dspRawTokenKey = "dsp-deployment-raw-token"

var unprotectedMethods = []string{"OPTIONS"}
var jwtHeaderName = "Authorization"
var jwtHeaderType = "Bearer"

// TODO Compare with the same middleware in ai-provisioner and consolidate
func injectRawTokenMiddleware(c *gin.Context) {

	_, rawTokenExists := c.Get(dspRawTokenKey)

	if rawTokenExists {
		c.Next()
	} else {

		token, err := getToken(c)

		if err != nil {
			c.String(401, err.Error())
			c.Abort()
		} else {

			if token == "" {
				c.String(401, "No token provided")
				c.Abort()
			}

			c.Set(dspRawTokenKey, token)
			c.Next()
		}
	}
}

// TODO Compare with the same middleware in ai-provisioner and consolidate
func needsRolesMiddleware(applicationContext context.Context, roles []string) gin.HandlerFunc {

	return func(c *gin.Context) {

		storedRawToken, rawTokenExists := c.Get(dspRawTokenKey)

		if !rawTokenExists {
			c.String(http.StatusUnauthorized, "No token provided")
			c.Abort()
			return
		}

		rawToken, couldConvert := storedRawToken.(string)

		if !couldConvert {
			c.String(http.StatusUnauthorized, "Cannot read token")
			c.Abort()
			return
		}

		jwtContext := auth.JwtContextFromContext(applicationContext, c.GetString("JWT_ISSUER"))
		if tokenRoles, err := jwtContext.RolesFromToken(applicationContext, rawToken, c.GetString("OAUTH_CLIENT_ID")); err != nil {
			c.String(http.StatusUnauthorized, err.Error())
			c.Abort()
			return
		} else if !auth.HasRoles(tokenRoles, c.GetString("OAUTH_CLIENT_ID"), roles...) {
			c.String(http.StatusForbidden, fmt.Sprintf("missing one or more of folowing roles %s", strings.Join(roles, ", ")))
			c.Abort()
			return
		}

		c.Next()
	}
}

func getToken(c *gin.Context) (string, error) {

	for _, v := range unprotectedMethods {
		if v == c.Request.Method {
			return "", nil
		}
	}

	//Verify we have the auth header
	jwtHeader := c.GetHeader(jwtHeaderName)

	if jwtHeader == "" {
		return "", fmt.Errorf("header %q not set", jwtHeaderName)
	}

	//Make sure the header is in a valid format that we are expecting, ie
	//<HeaderName>: <HeaderType(optional)> <JWT>
	parts := strings.Split(jwtHeader, " ")

	if parts[0] != jwtHeaderType || len(parts) != 2 {
		return "", fmt.Errorf("bad header %q. Expecde value '%s <JWT>'", jwtHeaderName, jwtHeaderType)
	}
	return parts[1], nil
}
