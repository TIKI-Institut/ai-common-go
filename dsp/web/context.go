package web

import (
	"context"
)

type webEngineKey int

var webEngineValueKey webEngineKey

//goland:noinspection GoNameStartsWithPackageName
func WebEngineFromContext(ctx context.Context) *WebEngine {
	return ctx.Value(webEngineValueKey).(*WebEngine)
}
