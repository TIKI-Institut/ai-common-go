package web

import (
	"github.com/gin-gonic/gin"
)

type GroupDeclaration interface {
	GinEndpointDeclaration
	RegisterGroup(relativePath string, roles []string) GroupDeclaration
	RegisterEndpoint(method string, roles []string, handlers ...gin.HandlerFunc)
	RegisterEndpointPath(method string, relativePath string, roles []string, handlers ...gin.HandlerFunc)
}

type GinEndpointDeclaration interface {
	Apply(target *gin.RouterGroup)
}
