package web

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/httpServer/auth"
	"context"
	"github.com/gin-gonic/gin"
)

type registrationEndpoint struct {
	context  context.Context
	method   string
	roles    []string
	handlers []gin.HandlerFunc
}

func (e *registrationEndpoint) Apply(target *gin.RouterGroup) {

	var localHandlers []gin.HandlerFunc

	if e.roles != nil {
		localHandlers = append(localHandlers, auth.SetRawTokenMiddleware, auth.RequireRolesMiddleware(e.roles...))
	}

	userEndpointRoot := target.Group("", localHandlers...)
	userEndpointRoot.Handle(e.method, "", e.handlers...)
}
