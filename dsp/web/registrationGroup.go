package web

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/httpServer/auth"
	"context"
	"github.com/gin-gonic/gin"
)
import roleStore "bitbucket.org/TIKI-Institut/ai-common-go/v2/dsp/roles"


type groupDeclaration struct {
	context      context.Context
	relativePath string
	roles        []string

	subRegistrations []GinEndpointDeclaration
}

func (g *groupDeclaration) Apply(target *gin.RouterGroup) {

	var localHandlers []gin.HandlerFunc

	if g.roles != nil {
		localHandlers = append(localHandlers, auth.SetRawTokenMiddleware, auth.RequireRolesMiddleware(g.roles...))
	}

	myGroup := target.Group(g.relativePath, localHandlers...)

	for _, e := range g.subRegistrations {
		e.Apply(myGroup)
	}

}

func (g *groupDeclaration) RegisterEndpoint(method string, roles []string, handlers ...gin.HandlerFunc) {

	if roles != nil {
		//Need to register roles
		roleStore.Add(g.context, roles...)
	}

	g.subRegistrations = append(g.subRegistrations, &registrationEndpoint{g.context,method, roles, handlers})
}

func (g *groupDeclaration) RegisterEndpointPath(method string, relativePath string, roles []string, handlers ...gin.HandlerFunc) {
	g.RegisterGroup(relativePath, roles).RegisterEndpoint(method, nil, handlers...)
}

func (g *groupDeclaration) RegisterGroup(relativePath string, roles []string) GroupDeclaration {

	if roles != nil {
		//Need to register roles
		roleStore.Add(g.context, roles...)
	}

	group := &groupDeclaration{g.context,relativePath, roles, []GinEndpointDeclaration{}}
	g.subRegistrations = append(g.subRegistrations, group)

	return group
}
