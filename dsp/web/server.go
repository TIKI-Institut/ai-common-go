package web

import (
	commonAuth "bitbucket.org/TIKI-Institut/ai-common-go/v2/auth"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/dsp"
	roleStore "bitbucket.org/TIKI-Institut/ai-common-go/v2/dsp/roles"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/httpServer"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/httpServer/auth"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/httpServer/errorHandling"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/httpServer/logging"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/qos"
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/spf13/cobra"
	"net/http"
	"os"
	"strings"
	"time"
)

//goland:noinspection GoNameStartsWithPackageName
type WebEngine struct {
	context.Context
	GroupDeclaration

	gin *gin.Engine

	rootGroup *gin.RouterGroup
	qosGroup  *gin.RouterGroup
}

//this is supposed to work only within a dsp environment.

//goland:noinspection GoUnusedExportedFunction
func NewMainCommand(engine *WebEngine) *cobra.Command {
	cmd := dsp.MainCommand(engine.Context)

	webCmd := cobra.Command{
		Use:   "run-web",
		Short: "run web server with endpoint fragments",
		Long:  "",
		Args:  cobra.ExactArgs(0),
		RunE: func(cmd *cobra.Command, args []string) error {
			return engine.Start()
		},
	}
	cmd.AddCommand(&webCmd)

	return cmd
}

//goland:noinspection GoUnusedExportedFunction
func NewWebEngine(ctx context.Context) *WebEngine {

	ctx = roleStore.EnsureContextWithRoleCollector(ctx)
	ctx = qos.EnsureContextWithQosEntryCollector(ctx)
	ctx = commonAuth.WithJwtContextStorage(ctx)

	rootGroup := &groupDeclaration{ctx, "", nil, []GinEndpointDeclaration{}}
	return &WebEngine{Context: ctx, GroupDeclaration: rootGroup}
}

// Start needs a set of set env vars to work.
// they get set into the gin context and will be supplied to registered endpoints
//
//  - JWT_ISSUER         the token ISSUER that will be used to validate all provided tokens
//                       (i.e. https://auth.tiki-dsp.io/auth/realms/tiki)
//
//  - OAUTH_CLIENT_ID    the client that will be used to validate all provided tokens
//                       (i.e. ai-examples-python-dev-web-py36)
func (s *WebEngine) Start() error {
	//1. get issuer URL from env vars...
	//for info see top of this file
	oauthIssuerUrl, isSet := os.LookupEnv("JWT_ISSUER")
	if !isSet {
		return fmt.Errorf(`env var "JWT_ISSUER" not set`)
	}

	//get target clientId the token needs to be checked against
	oauthClientId, isSet := os.LookupEnv("OAUTH_CLIENT_ID")
	if !isSet {
		return fmt.Errorf(`env var "OAUTH_CLIENT_ID" not set`)
	}

	engine := gin.New()

	logging.SetupZerolog(engine)
	engine.Use(errorHandling.ErrorHandlingMiddleware)
	engine.Use(errorHandling.PanicCatchingMiddleware)

	engine.Use(auth.StaticLoginClientMiddleware(oauthClientId))
	engine.Use(auth.StaticJwtIssuerMiddleware(oauthIssuerUrl))

	engine.NoRoute(func(c *gin.Context) {
		errorHandling.AbortWithError(c, http.StatusNotFound, fmt.Errorf("requested resource %++v was not found", c.Request.URL))
	})

	//2. set root group (deployment path)
	//The web path under this web application is present
	deploymentPath, isSet := os.LookupEnv("DSP_WEB_DEPLOYMENT_PATH")
	if !isSet {
		return fmt.Errorf("env var \"DSP_WEB_DEPLOYMENT_PATH\" not set")
	}

	//this app is hosted under a specific path...
	deploymentPath = strings.TrimSuffix(deploymentPath, "/")
	s.rootGroup = engine.Group(deploymentPath)

	//register all stuff
	s.Apply(s.rootGroup)

	s.qosGroup = engine.Group("/qos")
	{
		s.qosGroup.GET("/list", qos.ListSyncEntriesHandler(s))
		s.qosGroup.GET("/run", qos.RunSyncEntryEndpointHandler(s))
	}

	return httpServer.ServeWithContext(s, ":5000", engine, time.Second)
}
