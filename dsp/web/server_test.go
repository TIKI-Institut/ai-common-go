package web

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/cli"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/tests"
	"context"
	"github.com/stretchr/testify/assert"
	"os"
	"sync"
	"syscall"
	"testing"
	"time"
)

func TestServerStartWithContext(t *testing.T) {

	ctx, cancel := context.WithCancel(context.Background())

	engine := NewWebEngine(ctx)

	wg := sync.WaitGroup{}
	wg.Add(1)

	go func() {
		tests.WithEnvVars(func() {
			assert.NoError(t, engine.Start())
		},
			tests.EnvDecl{Name: "JWT_ISSUER", Value: "jwtIssuer"},
			tests.EnvDecl{Name: "OAUTH_CLIENT_ID", Value: "oauthClientId"},
			tests.EnvDecl{Name: "DSP_WEB_DEPLOYMENT_PATH", Value: "deploymentPath"})

		wg.Done()
	}()

	<-time.After(250 * time.Millisecond)
	cancel()

	wg.Wait()

}

func TestServerStartWithContextOsSignalTermination(t *testing.T) {

	signalChannel := make(chan os.Signal)
	ctx := cli.NewContextWithSignalListener(context.Background(), signalChannel)

	engine := NewWebEngine(ctx)

	wg := sync.WaitGroup{}
	wg.Add(1)

	go func() {
		tests.WithEnvVars(func() {
			assert.NoError(t, engine.Start())
		},
			tests.EnvDecl{Name: "JWT_ISSUER", Value: "jwtIssuer"},
			tests.EnvDecl{Name: "OAUTH_CLIENT_ID", Value: "oauthClientId"},
			tests.EnvDecl{Name: "DSP_WEB_DEPLOYMENT_PATH", Value: "deploymentPath"})

		wg.Done()
	}()

	<-time.After(250 * time.Millisecond)
	signalChannel <- syscall.SIGINT

	wg.Wait()

}
