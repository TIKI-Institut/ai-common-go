package httpClient

import (
	"encoding/base64"
	"fmt"
	"golang.org/x/oauth2"
	"k8s.io/client-go/transport"
	"net/http"
	"time"
)

var defaultHttpRequestTimeout = time.Second * 60

type RestClient interface {
	RestClientDelete
	RestClientGet
	RestClientPost
	RestClientPut
	WithBearerTokenAuthentication(tokenSource oauth2.TokenSource) RestClient
	WithBasicAuthentication(user string, password string) RestClient
	WithHeader(key, value string) RestClient
	WithTransport(tr http.RoundTripper) RestClient
	WithTimeout(timeout time.Duration) RestClient
}

func New() RestClient {
	hClient := http.DefaultClient
	hClient.Timeout = defaultHttpRequestTimeout
	return NewWithClient(hClient)
}

func NewWithClient(client *http.Client) RestClient {
	return restClient{client: client}
}

type headerAddingRoundTripper struct {
	key     string
	value   string
	Wrapped http.RoundTripper
}

func (t *headerAddingRoundTripper) RoundTrip(r *http.Request) (*http.Response, error) {
	r.Header.Set(t.key, t.value)

	tr := t.Wrapped

	if tr == nil {
		tr = http.DefaultTransport
	}

	return tr.RoundTrip(r)
}

type restClient struct {
	client *http.Client
	RestClient
}

func (r restClient) WithBearerTokenAuthentication(tokenSource oauth2.TokenSource) RestClient {
	return r.WithTransport(transport.TokenSourceWrapTransport(tokenSource)(r.client.Transport))
}

func (r restClient) WithBasicAuthentication(user string, password string) RestClient {
	staticToken := oauth2.StaticTokenSource(&oauth2.Token{
		AccessToken: base64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%s:%s", user, password))),
		TokenType:   "Basic",
	})
	return r.WithTransport(transport.TokenSourceWrapTransport(staticToken)(r.client.Transport))
}

func (r restClient) WithHeader(key, value string) RestClient {
	return r.WithTransport(&headerAddingRoundTripper{
		key:     key,
		value:   value,
		Wrapped: r.client.Transport,
	})
}

func (r restClient) WithTransport(tr http.RoundTripper) RestClient {
	return restClient{client: &http.Client{
		Transport:     tr,
		CheckRedirect: r.client.CheckRedirect,
		Jar:           r.client.Jar,
		Timeout:       r.client.Timeout,
	}}
}

func (r restClient) WithTimeout(timeout time.Duration) RestClient {
	return restClient{client: &http.Client{
		Transport:     r.client.Transport,
		CheckRedirect: r.client.CheckRedirect,
		Jar:           r.client.Jar,
		Timeout:       timeout,
	}}
}
