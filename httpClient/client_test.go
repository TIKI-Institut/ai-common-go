package httpClient

import (
	"bytes"
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"golang.org/x/oauth2"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"
)

type jsonResponse struct {
	Msg string `json:"msg"`
}

type roundTripFunc func(req *http.Request) *http.Response

// RoundTrip .
func (f roundTripFunc) RoundTrip(req *http.Request) (*http.Response, error) {
	return f(req), nil
}

var (
	providedJsonMessage = &jsonResponse{Msg: "Hello, client"}
	providedTokenSource = oauth2.StaticTokenSource(&oauth2.Token{
		AccessToken: "<token>",
		TokenType:   "Bearer",
	})
)

func TestRestClient_WithBasicAuth(t *testing.T) {
	withHttpServer(t, func(url string, client *http.Client) (response *http.Response, e error) {
		restClient := New().WithBasicAuthentication("username", "secretpassword")
		return restClient.Get(context.TODO(), url)
	}, func(r *http.Request) {
		assertAuthTokenInRequestHeader(t, "Basic dXNlcm5hbWU6c2VjcmV0cGFzc3dvcmQ=", r)
	})
}

func TestRestClient_With_Cancelled_Context(t *testing.T) {

	hitTimeout := false

	withHttpServer(t, func(url string, client *http.Client) (response *http.Response, e error) {
		ctx := context.Background()
		ctx, cancel := context.WithCancel(ctx)
		restClient := New()

		go func() {
			time.Sleep(time.Millisecond * 50)
			cancel()
		}()

		resp, err := restClient.Get(ctx, url)

		if assert.Error(t, err) {
			assert.EqualError(t, err, fmt.Sprintf("Get %q: context canceled", url))
		}
		return resp, nil
	}, func(r *http.Request) {
		//Imitate long response time
		select {
		case <-time.After(time.Millisecond * 100):
			hitTimeout = true
		case <-r.Context().Done():
		}
	})

	assert.False(t, hitTimeout)
}

func TestRestClient_Get_With_Nil_Context(t *testing.T) {
	restClient := New()
	_, err := restClient.Get(nil, "http://test-url")
	if assert.Error(t, err) {
		assert.Contains(t, err.Error(), "net/http: nil Context")
	}
}

func TestRestClient_WithHeader(t *testing.T) {
	withHttpServer(t, func(url string, client *http.Client) (response *http.Response, e error) {
		restClient := New().WithHeader("foo", "bar")
		return restClient.Get(context.TODO(), url)
	}, func(r *http.Request) {
		assertRequestHeader(t, "foo", "bar", r)
	})
}

func httpServer(_ *testing.T, assertion func(*http.Request)) *httptest.Server {
	return httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assertion(r)
	}))
}

func withHttpServer(t *testing.T, fn func(url string, client *http.Client) (*http.Response, error), assertion func(r *http.Request)) {
	ts := httpServer(t, assertion)
	defer ts.Close()

	_, err := fn(ts.URL, ts.Client())
	assert.NoError(t, err)
}

func assertAuthTokenInRequestHeader(t *testing.T, auth string, r *http.Request) {
	assertRequestHeader(t, "Authorization", auth, r)
}

func assertRequestHeader(t *testing.T, header string, value string, r *http.Request) {
	assert.Equal(t, r.Header.Get(header), value)
}

func assertRequestStringPayload(t *testing.T, payload string, r *http.Request) {
	buf := new(bytes.Buffer)
	_, err := buf.ReadFrom(r.Body)

	if err != nil {
		t.Fail()
	} else {
		bodyString := buf.String()
		assert.EqualValues(t, payload, bodyString)
	}
}

func assertNoRequestPayload(t *testing.T, r *http.Request) {
	assert.EqualValues(t, 0, r.ContentLength)
}

func assertRequestMultipartPayload(t *testing.T, parts map[string]io.Reader, r *http.Request) {
	err := r.ParseMultipartForm(1024)
	if err != nil {
		assert.NoError(t, err)
	}

	for key, part := range parts {
		switch partTyped := part.(type) {
		case *os.File:
			fileFromReq, _, err := r.FormFile(key)
			assert.NoError(t, err)

			fileBytesFromReq, err := ioutil.ReadAll(fileFromReq)
			assert.NoError(t, err)

			fileBytesFromProvided, err := ioutil.ReadAll(partTyped)
			assert.NoError(t, err)

			assert.EqualValues(t, fileBytesFromProvided, fileBytesFromReq)

			err = fileFromReq.Close()
			assert.NoError(t, err)
		default:
			providedBytes, err := ioutil.ReadAll(partTyped)
			assert.NoError(t, err)

			stringFromReq := r.FormValue(key)
			assert.EqualValues(t, string(providedBytes), stringFromReq)
		}
	}
}
