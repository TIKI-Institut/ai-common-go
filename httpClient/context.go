package httpClient

import "context"

type restClientContextKey int

var restClientContextKeyValue restClientContextKey

// FromContext returns the previously set context or creates a new one
func FromContext(ctx context.Context) RestClient {

	existing := ctx.Value(restClientContextKeyValue)
	if existing != nil {
		return existing.(RestClient)
	}

	return New()
}

func ContextWithRestClient(ctx context.Context, client RestClient) context.Context {
	return context.WithValue(ctx, restClientContextKeyValue, client)
}
