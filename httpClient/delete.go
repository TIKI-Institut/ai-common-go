package httpClient

import (
	"context"
	"net/http"
)

type RestClientDelete interface {
	Delete(ctx context.Context, url string) (*http.Response, error)
	DeleteJson(ctx context.Context, url string, body interface{}) (*http.Response, error)
}

func (r restClient) Delete(ctx context.Context, url string) (*http.Response, error) {
	req, err := r.request(ctx, http.MethodDelete, url, nil)
	if err != nil {
		return nil, err
	}
	return r.client.Do(req)
}

func (r restClient) DeleteJson(ctx context.Context, url string, body interface{}) (*http.Response, error) {
	req, err := r.requestWithJsonPayload(ctx, http.MethodDelete, url, body)
	if err != nil {
		return nil, err
	}
	return r.client.Do(req)
}
