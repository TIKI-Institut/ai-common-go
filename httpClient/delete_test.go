package httpClient

import (
	"context"
	"net/http"
	"testing"
)

func TestRestClient_Delete(t *testing.T) {
	withHttpServer(t, func(url string, client *http.Client) (response *http.Response, e error) {
		restClient := New().WithBearerTokenAuthentication(providedTokenSource)
		return restClient.Delete(context.TODO(), url)
	},
		func(r *http.Request) {
			assertAuthTokenInRequestHeader(t, "Bearer <token>", r)
			assertNoRequestPayload(t, r)
		},
	)
}

func TestRestClient_DeleteJson(t *testing.T) {
	withHttpServer(t, func(url string, client *http.Client) (response *http.Response, e error) {
		restClient := New().WithBearerTokenAuthentication(providedTokenSource)
		return restClient.DeleteJson(context.TODO(), url, providedJsonMessage)
	}, func(r *http.Request) {
		assertAuthTokenInRequestHeader(t, "Bearer <token>", r)
		assertRequestStringPayload(t, `{"msg":"Hello, client"}`, r)
	})
}
