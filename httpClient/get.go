package httpClient

import (
	"context"
	"encoding/json"
	"net/http"
)

type RestClientGet interface {
	Get(ctx context.Context, url string) (*http.Response, error)
	GetAsJson(ctx context.Context, url string, bodyTarget interface{}, responseHandler func(response *http.Response) error) error
}

func (r restClient) Get(ctx context.Context, url string) (*http.Response, error) {
	req, err := r.request(ctx, http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}
	return r.client.Do(req)
}

// GET on the given url to retrieve a json payload
// Return error when GET is impossible or response can not be decoded to json
func (r restClient) GetAsJson(ctx context.Context, url string, bodyTarget interface{}, responseHandler func(response *http.Response) error) error {
	res, err := r.Get(ctx, url)
	if err != nil {
		return err
	}

	defer func() {
		if res.Body != nil {
			if err := res.Body.Close(); err != nil {
				panic(err)
			}
		}
	}()

	if err := responseHandler(res); err != nil {
		return err
	}

	dec := json.NewDecoder(res.Body)

	return dec.Decode(bodyTarget)
}
