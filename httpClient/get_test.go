package httpClient

import (
	"bytes"
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"net/http"
	"strings"
	"testing"
)

func TestRestClient_Get(t *testing.T) {
	withHttpServer(t, func(url string, client *http.Client) (response *http.Response, e error) {
		restClient := New().WithBearerTokenAuthentication(providedTokenSource)
		return restClient.Get(context.TODO(), url)
	}, func(r *http.Request) {
		assertAuthTokenInRequestHeader(t, "Bearer <token>", r)
		assertNoRequestPayload(t, r)
	})
}

func TestDefaultGetWithJsonResponse(t *testing.T) {
	var fn roundTripFunc = func(req *http.Request) *http.Response {
		return &http.Response{
			StatusCode: 200,
			Body:       ioutil.NopCloser(strings.NewReader(`{"Msg":"test-result"}`)),
			Header:     make(http.Header),
		}
	}

	client := NewWithClient(&http.Client{Transport: fn})
	var result jsonResponse
	err := client.GetAsJson(context.TODO(),
		"test-url",
		&result,
		func(res *http.Response) error {
			if res.StatusCode != http.StatusOK {
				return fmt.Errorf("error executing GET on %s (%s)", "test-url", "default error message")
			}
			return nil
		})

	assert.Equal(t, "test-result", result.Msg)
	assert.Nil(t, err)
}

func TestDefaultGetWithJsonResponseNotOkStatus(t *testing.T) {
	var fn roundTripFunc = func(req *http.Request) *http.Response {
		return &http.Response{
			StatusCode: 500,
			Body:       ioutil.NopCloser(bytes.NewBuffer(nil)),
			Header:     make(http.Header),
		}
	}

	client := NewWithClient(&http.Client{Transport: fn})
	var result jsonResponse
	err := client.GetAsJson(context.TODO(),
		"test-url",
		&result,
		func(res *http.Response) error {
			if res.StatusCode != http.StatusOK {
				return fmt.Errorf("error executing GET on %s (%s)", "test-url", "default error message")
			}
			return nil
		})

	assert.Error(t, err)
	assert.Equal(t, "error executing GET on test-url (default error message)", err.Error())
}
