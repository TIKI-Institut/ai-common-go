package httpClient

import (
	"context"
	"io"
	"mime/multipart"
	"net/http"
)

type RestClientPost interface {
	Post(ctx context.Context, url string) (*http.Response, error)
	PostJson(ctx context.Context, url string, body interface{}) (*http.Response, error)
	PostMultipart(ctx context.Context, url string, multipartWriter func(writer *multipart.Writer) error) (*http.Response, error)
	PostMultipartChunked(ctx context.Context, url string, multipartWriter func(writer *multipart.Writer)) (*http.Response, error)
}

func (r restClient) Post(ctx context.Context, url string) (*http.Response, error) {
	req, err := r.request(ctx, http.MethodPost, url, nil)
	if err != nil {
		return nil, err
	}
	return r.client.Do(req)
}

func (r restClient) PostJson(ctx context.Context, url string, body interface{}) (*http.Response, error) {
	req, err := r.requestWithJsonPayload(ctx, http.MethodPost, url, body)
	if err != nil {
		return nil, err
	}
	return r.client.Do(req)
}

// PostMultipart performs multipart form post with provided parts. Pay attention that it does NOT close io.Reader's of
// parts! It is task of caller to close Readers
func (r restClient) PostMultipart(ctx context.Context, url string, multipartWriter func(writer *multipart.Writer) error) (*http.Response, error) {
	req, err := r.requestWithMultipartPayload(ctx, http.MethodPost, url, multipartWriter)
	if err != nil {
		return nil, err
	}
	return r.client.Do(req)
}

// PostMultipartChunked performs multipart form post without storing whole content to memory.
func (r restClient) PostMultipartChunked(ctx context.Context, url string, multipartWriter func(writer *multipart.Writer)) (*http.Response, error) {
	pr, pw := io.Pipe()
	w := multipart.NewWriter(pw)
	defer pr.Close()
	go func() {
		defer pw.Close()
		multipartWriter(w)
		defer w.Close()
	}()

	if req, err := http.NewRequestWithContext(ctx, http.MethodPost, url, pr); err != nil {
		return nil, err
	} else {
		req.Header.Set("Content-Type", w.FormDataContentType())
		return r.client.Do(req)
	}

}
