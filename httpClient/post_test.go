package httpClient

import (
	"bytes"
	"context"
	"github.com/stretchr/testify/assert"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"testing"
)

func TestRestClient_Post(t *testing.T) {
	withHttpServer(t, func(url string, client *http.Client) (response *http.Response, e error) {
		restClient := New().WithBearerTokenAuthentication(providedTokenSource)
		return restClient.Post(context.TODO(), url)
	},
		func(r *http.Request) {
			assertAuthTokenInRequestHeader(t, "Bearer <token>", r)
			assertNoRequestPayload(t, r)
		},
	)
}

func TestRestClient_PostJson(t *testing.T) {
	withHttpServer(t, func(url string, client *http.Client) (response *http.Response, e error) {
		restClient := New().WithBearerTokenAuthentication(providedTokenSource)
		return restClient.PostJson(context.TODO(), url, providedJsonMessage)
	}, func(r *http.Request) {
		assertAuthTokenInRequestHeader(t, "Bearer <token>", r)
		assertRequestStringPayload(t, `{"msg":"Hello, client"}`, r)
	})
}

func TestRestClient_PostJson_WithContext(t *testing.T) {
	withHttpServer(t, func(url string, client *http.Client) (response *http.Response, e error) {
		ctx := context.Background()
		ctx = context.WithValue(ctx, "testKey", "testValue")
		restClient := New().WithBearerTokenAuthentication(providedTokenSource)
		resp, err := restClient.PostJson(ctx, url, providedJsonMessage)
		assert.Equal(t, "testValue", resp.Request.Context().Value("testKey"))
		return resp, err
	}, func(r *http.Request) {
		assertAuthTokenInRequestHeader(t, "Bearer <token>", r)
		assertRequestStringPayload(t, `{"msg":"Hello, client"}`, r)
	})
}

func TestRestClient_PostJsonWithNotNilInterfaceAndNilValue(t *testing.T) {
	var body []string
	withHttpServer(t, func(url string, client *http.Client) (response *http.Response, e error) {
		restClient := New().WithBearerTokenAuthentication(providedTokenSource)
		return restClient.PostJson(context.TODO(), url, body)
	}, func(r *http.Request) {
		assertAuthTokenInRequestHeader(t, "Bearer <token>", r)
		assertRequestStringPayload(t, "", r)
	})
}

func TestRestClient_PostMultipart(t *testing.T) {
	providedFile := createTestFile(t)
	providedMultipartMessage := map[string]io.Reader{
		"jsonPart": bytes.NewReader([]byte(`{"msg":"Hello, client"}`)),
		"filePart": providedFile,
	}

	expectedFile, err := os.Open(providedFile.Name())
	assert.NoError(t, err)

	expectedMultipartMessage := map[string]io.Reader{
		"jsonPart": bytes.NewReader([]byte(`{"msg":"Hello, client"}`)),
		"filePart": expectedFile,
	}

	defer func() {
		cleanupResources(t, providedFile, expectedFile)
	}()

	multipartWriter := func(writer *multipart.Writer) error {
		return testMultipartWriter(t, writer, providedMultipartMessage, providedFile)
	}

	withHttpServer(t, func(url string, client *http.Client) (response *http.Response, e error) {
		restClient := New().WithBearerTokenAuthentication(providedTokenSource)
		return restClient.PostMultipart(context.TODO(), url, multipartWriter)
	}, func(r *http.Request) {
		assertAuthTokenInRequestHeader(t, "Bearer <token>", r)
		assertRequestMultipartPayload(t, expectedMultipartMessage, r)
	})
}

func TestRestClient_PostMultipartChunked(t *testing.T) {
	providedFile := createTestFile(t)
	providedMultipartMessage := map[string]io.Reader{
		"jsonPart": bytes.NewReader([]byte(`{"msg":"Hello, client"}`)),
		"filePart": providedFile,
	}

	expectedFile, err := os.Open(providedFile.Name())
	assert.NoError(t, err)

	expectedMultipartMessage := map[string]io.Reader{
		"jsonPart": bytes.NewReader([]byte(`{"msg":"Hello, client"}`)),
		"filePart": expectedFile,
	}

	defer func() {
		cleanupResources(t, providedFile, expectedFile)
	}()

	multipartWriter := func(writer *multipart.Writer) {
		testMultipartWriter(t, writer, providedMultipartMessage, providedFile)
	}

	withHttpServer(t, func(url string, client *http.Client) (response *http.Response, e error) {
		restClient := New().WithBearerTokenAuthentication(providedTokenSource)
		return restClient.PostMultipartChunked(context.TODO(), url, multipartWriter)
	}, func(r *http.Request) {
		assertAuthTokenInRequestHeader(t, "Bearer <token>", r)
		assertRequestMultipartPayload(t, expectedMultipartMessage, r)
	})
}

func testMultipartWriter(t *testing.T, writer *multipart.Writer, providedMultipartMessage map[string]io.Reader, providedFile *os.File) error {
	fw, err := writer.CreateFormField("jsonPart")
	assert.NoError(t, err)
	written, err := io.Copy(fw, providedMultipartMessage["jsonPart"])
	assert.NoError(t, err)
	assert.NotZero(t, written)

	fw2, err := writer.CreateFormFile("filePart", providedFile.Name())
	assert.NoError(t, err)
	written, err = io.Copy(fw2, providedFile)
	assert.NoError(t, err)
	assert.NotZero(t, written)
	return err
}

func createTestFile(t *testing.T) *os.File {
	providedFile, err := os.CreateTemp("", "")
	assert.NoError(t, err)
	_, err = providedFile.WriteString("test content of file")
	assert.NoError(t, err)
	_, err = providedFile.Seek(0, 0) // reset to the beginning for subsequent read
	assert.NoError(t, err)

	return providedFile
}

func cleanupResources(t *testing.T, providedFile *os.File, expectedFile *os.File) {
	err := providedFile.Close()
	assert.NoError(t, err)
	err = expectedFile.Close()
	assert.NoError(t, err)
	err = os.Remove(providedFile.Name())
	assert.NoError(t, err)
}

func TestRestClient_PostMultipart_With_Context(t *testing.T) {
	multipartWriter := func(writer *multipart.Writer) error {
		return nil
	}

	withHttpServer(t, func(url string, client *http.Client) (response *http.Response, e error) {
		ctx := context.Background()
		ctx = context.WithValue(ctx, "testKey", "testValue")
		restClient := New().WithBearerTokenAuthentication(providedTokenSource)
		resp, err := restClient.PostMultipart(ctx, url, multipartWriter)
		assert.Equal(t, "testValue", resp.Request.Context().Value("testKey"))
		return resp, err
	}, func(r *http.Request) {
		assertAuthTokenInRequestHeader(t, "Bearer <token>", r)
	})
}
