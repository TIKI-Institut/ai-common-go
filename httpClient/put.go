package httpClient

import (
	"context"
	"net/http"
)

type RestClientPut interface {
	Put(ctx context.Context, url string) (*http.Response, error)
	PutJson(ctx context.Context, url string, body interface{}) (*http.Response, error)
}

func (r restClient) Put(ctx context.Context, url string) (*http.Response, error) {
	req, err := r.request(ctx, http.MethodPut, url, nil)
	if err != nil {
		return nil, err
	}
	return r.client.Do(req)
}

func (r restClient) PutJson(ctx context.Context, url string, body interface{}) (*http.Response, error) {
	req, err := r.requestWithJsonPayload(ctx, http.MethodPut, url, body)
	if err != nil {
		return nil, err
	}
	return r.client.Do(req)
}
