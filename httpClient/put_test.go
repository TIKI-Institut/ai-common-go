package httpClient

import (
	"context"
	"net/http"
	"testing"
)

func TestRestClient_Put(t *testing.T) {
	withHttpServer(t, func(url string, client *http.Client) (response *http.Response, e error) {
		restClient := New().WithBearerTokenAuthentication(providedTokenSource)
		return restClient.Put(context.TODO(), url)
	},
		func(r *http.Request) {
			assertAuthTokenInRequestHeader(t, "Bearer <token>", r)
			assertNoRequestPayload(t, r)
		},
	)
}

func TestRestClient_PutJson(t *testing.T) {
	withHttpServer(t, func(url string, client *http.Client) (response *http.Response, e error) {
		restClient := New().WithBearerTokenAuthentication(providedTokenSource)
		return restClient.PutJson(context.TODO(), url, providedJsonMessage)
	}, func(r *http.Request) {
		assertAuthTokenInRequestHeader(t, "Bearer <token>", r)
		assertRequestStringPayload(t, `{"msg":"Hello, client"}`, r)
	})
}
