package httpClient

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/common/check"
	"bytes"
	"context"
	"encoding/json"
	"io"
	"mime/multipart"
	"net/http"
)

func (r restClient) request(ctx context.Context, method, url string, body io.Reader) (*http.Request, error) {

	request, err := http.NewRequestWithContext(ctx, method, url, body)

	if err != nil {
		return nil, err
	}

	return request, nil
}

func (r restClient) requestWithJsonPayload(ctx context.Context, method, url string, body interface{}) (*http.Request, error) {

	var bodyReader io.Reader = nil

	if !check.IsNil(body) {
		data, err := json.Marshal(body)

		if err != nil {
			return nil, err
		}

		bodyReader = bytes.NewReader(data)
	}

	request, err := r.request(ctx, method, url, bodyReader)

	if err != nil {
		return nil, err
	}

	request.Header.Set("Content-Type", "application/json; charset=utf-8")

	return request, nil
}

func (r restClient) requestWithMultipartPayload(ctx context.Context, method, url string, multipartWriter func(writer *multipart.Writer) error) (*http.Request, error) {
	// Prepare a form that you will submit to that URL.
	var b bytes.Buffer
	w := multipart.NewWriter(&b)

	if err := multipartWriter(w); err != nil {
		return nil, err
	}

	// Don't forget to close the multipart writer.
	// If you don't close it, your request will be missing the terminating boundary.
	if err := w.Close(); err != nil {
		return nil, err
	}

	// Now that you have a form, you can submit it to your handler.
	if req, err := http.NewRequestWithContext(ctx, method, url, &b); err != nil {
		return nil, err
	} else {
		// Don't forget to set the content type, this will contain the boundary.
		req.Header.Set("Content-Type", w.FormDataContentType())
		return req, nil
	}
}
