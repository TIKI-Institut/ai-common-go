package httpClient

import (
	"fmt"
	"net/http"
)

// Extract the given http header values
// Return error when the given http header was not found
func ExtractHeader(res *http.Response, target string) ([]string, error) {

	v, found := res.Header[target]

	if !found {
		return nil, fmt.Errorf("unable to find header %s", target)
	}

	return v, nil
}

