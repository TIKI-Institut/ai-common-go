package httpClient

import (
	"bytes"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"net/http"
	"testing"
)

func TestExtractHeader(t *testing.T) {
	testHeader := http.Header{}
	testHeader.Add("header1", "value1")

	response := &http.Response{
		StatusCode: 200,
		Body:       ioutil.NopCloser(bytes.NewBuffer(nil)),
		Header:     testHeader,
	}

	extractedHeader, err := ExtractHeader(response, "Header1")

	assert.Equal(t, "value1", extractedHeader[0])
	assert.Nil(t, err)
}

func TestExtractHeaderNotFound(t *testing.T) {
	testHeader := http.Header{}
	testHeader.Add("header1", "value1")

	response := &http.Response{
		StatusCode: 200,
		Body:       ioutil.NopCloser(bytes.NewBuffer(nil)),
		Header:     testHeader,
	}

	extractedHeader, err := ExtractHeader(response, "Header2")

	assert.Nil(t, extractedHeader)
	assert.Error(t, err)
}
