package auth

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/auth"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/httpServer/errorHandling"
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

// RequireRolesMiddleware takes all needed information from the current ginContext and checks the existence of the required roles within the current loginClient (taken from LoginClientFromRequestContext)
// It requires SetRawTokenIntoRequestContext, SetJwtIssuerIntoRequestContext and SetLoginClientIntoRequestContext to be called before its usage
func RequireRolesMiddleware(roles ...string) gin.HandlerFunc {
	return func(gCtx *gin.Context) {
		//take care of login client
		loginClient, found := LoginClientFromRequestContext(gCtx)
		if !found {
			errorHandling.AbortWithError(gCtx, http.StatusUnauthorized, &auth.RoleCheckFailedError{Cause: fmt.Errorf("no login client set")})
			return
		}
		PerformRoleChecking(gCtx, loginClient, auth.DefaultRoleCheckingHandlerFunc(roles...))
	}
}

// CheckRolesMiddleware takes all needed information from the current ginContext and uses the given handler <roleCheckingHandlerFunc>
// Any error returned from <roleCheckingHandlerFunc> will result in a rejected request
// It requires SetRawTokenIntoRequestContext, SetJwtIssuerIntoRequestContext and SetLoginClientIntoRequestContext to be called before its usage
func CheckRolesMiddleware(roleCheckingHandlerFunc auth.RoleCheckingHandlerFunc) gin.HandlerFunc {
	return func(gCtx *gin.Context) {
		//take care of login client
		loginClient, found := LoginClientFromRequestContext(gCtx)
		if !found {
			errorHandling.AbortWithError(gCtx, http.StatusUnauthorized, &auth.RoleCheckFailedError{Cause: fmt.Errorf("no login client set")})
			return
		}
		PerformRoleChecking(gCtx, loginClient, roleCheckingHandlerFunc)
	}
}

// RequireRolesFromLoginClientMiddleware takes the <raw token> and the <jwt issuer> from the current ginContext and checks the existence of the required roles
// It requires SetRawTokenIntoRequestContext and  SetJwtIssuerIntoRequestContext to be called before its usage
// the target login client will be taken from <loginClient>
func RequireRolesFromLoginClientMiddleware(loginClient string, roles ...string) gin.HandlerFunc {
	return func(gCtx *gin.Context) {
		PerformRoleChecking(gCtx, loginClient, auth.DefaultRoleCheckingHandlerFunc(roles...))
	}
}

// CheckRolesFromLoginClientMiddleware takes the <raw token> and the <jwt issuer> from the current ginContext and uses the given handler <roleCheckingHandlerFunc>
// Any error returned from <roleCheckingHandlerFunc> will result in a rejected request
// It requires SetRawTokenIntoRequestContext and  SetJwtIssuerIntoRequestContext to be called before its usage
// the target login client will be taken from <loginClient>
func CheckRolesFromLoginClientMiddleware(loginClient string, roleCheckingHandlerFunc auth.RoleCheckingHandlerFunc) gin.HandlerFunc {
	return func(gCtx *gin.Context) { PerformRoleChecking(gCtx, loginClient, roleCheckingHandlerFunc) }
}

// PerformRoleChecking takes the <raw token> and the <jwt issuer> from the current ginContext and uses the given handler <roleCheckingHandlerFunc>
// It requires SetRawTokenIntoRequestContext and  SetJwtIssuerIntoRequestContext to be called before its usage
// the target login client will be taken from <loginClient>
func PerformRoleChecking(gCtx *gin.Context, loginClient string, roleCheckingHandlerFunc auth.RoleCheckingHandlerFunc) {

	//take care of raw token
	rawToken, found := RawTokenFromRequestContext(gCtx)
	if !found {
		errorHandling.AbortWithError(gCtx, http.StatusUnauthorized, &auth.RoleCheckFailedError{Cause: fmt.Errorf("no token set")})
		return
	}

	//take care of jwt issuer
	jwtIssuer, found := JwtIssuerFromRequestContext(gCtx)
	if !found {
		errorHandling.AbortWithError(gCtx, http.StatusUnauthorized, &auth.RoleCheckFailedError{Cause: fmt.Errorf("no jwt issuer set")})
		return
	}

	tokenRoles, err := auth.ResolveTokenRoles(gCtx.Request.Context(), jwtIssuer, rawToken, loginClient)
	if err != nil {
		errorHandling.AbortWithError(gCtx, http.StatusUnauthorized, err)
		return
	}

	err = roleCheckingHandlerFunc(loginClient, tokenRoles)

	if err != nil {
		err = auth.WrapAsRoleCheckFailedError(err)

		var wrappedErr *auth.RoleCheckFailedError
		if errors.As(err, &wrappedErr) {
			if errors.Is(wrappedErr.Cause, auth.MissingRolesError) {
				errorHandling.AbortWithError(gCtx, http.StatusForbidden, err)
			} else {
				errorHandling.AbortWithError(gCtx, http.StatusUnauthorized, err)
			}
		} else {
			errorHandling.AbortWithError(gCtx, http.StatusUnauthorized, err)
		}

		return
	}

	gCtx.Next()
}
