package auth

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/auth"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/httpServer/errorHandling"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/httpServer/logging"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/tests"
	"context"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

type testTokenRoles map[string][]string

func (t testTokenRoles) ClientRoles(clientId string) []string {
	return t[clientId]
}

type testGetOfflineTokenJwtContext struct {
	*testing.T
	auth.JwtContext
	roles       map[string][]string
	token       string
	loginClient string
}

func (tJwtContext *testGetOfflineTokenJwtContext) RolesFromToken(_ context.Context, token string, clientId string) (auth.TokenRoles, error) {
	assert.EqualValues(tJwtContext, tJwtContext.token, token)
	assert.EqualValues(tJwtContext, tJwtContext.loginClient, clientId)

	return testTokenRoles(tJwtContext.roles), nil
}

func TestRequireRolesMiddleware(t *testing.T) {

	setupRecorderEx := func(ctx context.Context, roleMiddleware gin.HandlerFunc, loginClient string, jwtIssuer string, rawToken string) (*httptest.ResponseRecorder, bool) {
		visited := false
		recorder := tests.TestMiddleware(ctx, "GET", nil, func(g *gin.Engine) {

			//setup error handling
			logging.SetupZerolog(g)
			g.Use(errorHandling.ErrorHandlingMiddleware)
			g.Use(errorHandling.PanicCatchingMiddleware)

			if len(loginClient) > 0 {
				g.Use(func(c *gin.Context) {
					SetLoginClientIntoRequestContext(c, loginClient)
					c.Next()
				})
			}

			if len(jwtIssuer) > 0 {
				g.Use(func(c *gin.Context) {
					SetJwtIssuerIntoRequestContext(c, jwtIssuer)
					c.Next()
				})
			}

			if len(rawToken) > 0 {
				g.Use(func(c *gin.Context) {
					SetRawTokenIntoRequestContext(c, rawToken)
					c.Next()
				})
			}
			g.Use(roleMiddleware)
			g.GET("/", func(c *gin.Context) {
				visited = true
			})
		})

		return recorder, visited
	}

	t.Run("RequireRolesMiddleware", func(t *testing.T) {

		authLoginClient := "some-login-client"
		token := "some-token"

		setupRecorder := func(ctx context.Context, loginClientId string, jwtIssuer string, rawToken string) (*httptest.ResponseRecorder, bool) {
			return setupRecorderEx(ctx, RequireRolesMiddleware("A", "B"), loginClientId, jwtIssuer, rawToken)
		}

		t.Run("no token", func(t *testing.T) {
			ctx := context.Background()

			recorder, visited := setupRecorder(ctx, "some-login-client", "some-jwt-issuer", "")

			assert.False(t, visited, "the request should not have been executed")
			errorHandling.AssertRequestResponse(t, recorder, http.StatusUnauthorized,
				errorHandling.ErrorResponseAssertion{StatusCode: 401, Detail: "role check failed (no token set)"})
		})

		t.Run("no jwt issuer", func(t *testing.T) {
			ctx := context.Background()

			recorder, visited := setupRecorder(ctx, "some-login-client", "", "some-token")

			assert.False(t, visited, "the request should not have been executed")
			errorHandling.AssertRequestResponse(t, recorder, http.StatusUnauthorized,
				errorHandling.ErrorResponseAssertion{StatusCode: 401, Detail: "role check failed (no jwt issuer set)"})
		})

		t.Run("no login client", func(t *testing.T) {
			ctx := context.Background()

			recorder, visited := setupRecorder(ctx, "", "some-jwt-issuer", "some-token")

			assert.False(t, visited, "the request should not have been executed")
			errorHandling.AssertRequestResponse(t, recorder, http.StatusUnauthorized,
				errorHandling.ErrorResponseAssertion{StatusCode: 401, Detail: "role check failed (no login client set)"})
		})

		t.Run("missing roles", func(t *testing.T) {
			ctx := context.Background()
			ctx = auth.WithJwtContext(ctx, "some-jwt-issuer", &testGetOfflineTokenJwtContext{T: t, loginClient: authLoginClient, token: token, roles: map[string][]string{"some-login-client": {"A", "C"}}})

			recorder, visited := setupRecorder(ctx, "some-login-client", "some-jwt-issuer", "some-token")

			assert.False(t, visited, "the request should not have been executed")
			errorHandling.AssertRequestResponse(t, recorder, http.StatusForbidden,
				errorHandling.ErrorResponseAssertion{StatusCode: 403, Detail: "role check failed (missing roles)"})
		})

		t.Run("no roles on this login client", func(t *testing.T) {
			ctx := context.Background()
			ctx = auth.WithJwtContext(ctx, "some-jwt-issuer", &testGetOfflineTokenJwtContext{T: t, loginClient: authLoginClient, token: token, roles: map[string][]string{"some-other-login-client": {"A", "C"}}})

			recorder, visited := setupRecorder(ctx, "some-login-client", "some-jwt-issuer", "some-token")

			assert.False(t, visited, "the request should not have been executed")
			errorHandling.AssertRequestResponse(t, recorder, http.StatusForbidden,
				errorHandling.ErrorResponseAssertion{StatusCode: 403, Detail: "role check failed (missing roles)"})
		})

		t.Run("having all roles", func(t *testing.T) {
			ctx := context.Background()
			ctx = auth.WithJwtContext(ctx, "some-jwt-issuer", &testGetOfflineTokenJwtContext{T: t, loginClient: authLoginClient, token: token, roles: map[string][]string{"some-login-client": {"A", "B", "C"}}})

			recorder, visited := setupRecorder(ctx, "some-login-client", "some-jwt-issuer", "some-token")

			assert.True(t, visited, "the request should have been executed")
			assert.EqualValues(t, http.StatusOK, recorder.Code)
		})
	})

	t.Run("CheckRolesMiddleware", func(t *testing.T) {

		authLoginClient := "some-login-client"
		token := "some-token"

		myRoleCheckingHandler := func(unwrappedError bool) auth.RoleCheckingHandlerFunc {
			return func(loginClientId string, roles auth.TokenRoles) error {
				if auth.HasRoles(roles, "some-login-client", "B") {
					return nil
				}

				if unwrappedError {
					return auth.MissingRolesError
				} else {
					return &auth.RoleCheckFailedError{Cause: auth.MissingRolesError}
				}
			}
		}

		setupRecorder := func(ctx context.Context, wrappedError bool, loginClientId string, jwtIssuer string, rawToken string) (*httptest.ResponseRecorder, bool) {
			return setupRecorderEx(ctx, CheckRolesMiddleware(myRoleCheckingHandler(wrappedError)), loginClientId, jwtIssuer, rawToken)
		}

		t.Run("no token", func(t *testing.T) {
			ctx := context.Background()

			recorder, visited := setupRecorder(ctx, true, "some-login-client", "some-jwt-issuer", "")

			assert.False(t, visited, "the request should not have been executed")
			errorHandling.AssertRequestResponse(t, recorder, http.StatusUnauthorized,
				errorHandling.ErrorResponseAssertion{StatusCode: 401, Detail: "role check failed (no token set)"})
		})

		t.Run("no jwt issuer", func(t *testing.T) {
			ctx := context.Background()

			recorder, visited := setupRecorder(ctx, true, "some-login-client", "", "some-token")

			assert.False(t, visited, "the request should not have been executed")
			errorHandling.AssertRequestResponse(t, recorder, http.StatusUnauthorized,
				errorHandling.ErrorResponseAssertion{StatusCode: 401, Detail: "role check failed (no jwt issuer set)"})
		})

		t.Run("no login client", func(t *testing.T) {
			ctx := context.Background()

			recorder, visited := setupRecorder(ctx, true, "", "some-jwt-issuer", "some-token")

			assert.False(t, visited, "the request should not have been executed")
			errorHandling.AssertRequestResponse(t, recorder, http.StatusUnauthorized,
				errorHandling.ErrorResponseAssertion{StatusCode: 401, Detail: "role check failed (no login client set)"})
		})

		t.Run("missing roles", func(t *testing.T) {
			ctx := context.Background()
			ctx = auth.WithJwtContext(ctx, "some-jwt-issuer", &testGetOfflineTokenJwtContext{T: t, loginClient: authLoginClient, token: token, roles: map[string][]string{"some-login-client": {"A", "C"}}})

			recorder, visited := setupRecorder(ctx, true, "some-login-client", "some-jwt-issuer", "some-token")

			assert.False(t, visited, "the request should not have been executed")
			errorHandling.AssertRequestResponse(t, recorder, http.StatusForbidden,
				errorHandling.ErrorResponseAssertion{StatusCode: 403, Detail: "role check failed (missing roles)"})
		})

		t.Run("missing roles (unwrapped error)", func(t *testing.T) {
			ctx := context.Background()
			ctx = auth.WithJwtContext(ctx, "some-jwt-issuer", &testGetOfflineTokenJwtContext{T: t, loginClient: authLoginClient, token: token, roles: map[string][]string{"some-login-client": {"A", "C"}}})

			recorder, visited := setupRecorder(ctx, false, "some-login-client", "some-jwt-issuer", "some-token")

			assert.False(t, visited, "the request should not have been executed")
			errorHandling.AssertRequestResponse(t, recorder, http.StatusForbidden,
				errorHandling.ErrorResponseAssertion{StatusCode: 403, Detail: "role check failed (missing roles)"})
		})

		t.Run("no roles on this login client", func(t *testing.T) {
			ctx := context.Background()
			ctx = auth.WithJwtContext(ctx, "some-jwt-issuer", &testGetOfflineTokenJwtContext{T: t, loginClient: authLoginClient, token: token, roles: map[string][]string{"some-other-login-client": {"A", "C"}}})

			recorder, visited := setupRecorder(ctx, true, "some-login-client", "some-jwt-issuer", "some-token")

			assert.False(t, visited, "the request should not have been executed")
			errorHandling.AssertRequestResponse(t, recorder, http.StatusForbidden,
				errorHandling.ErrorResponseAssertion{StatusCode: 403, Detail: "role check failed (missing roles)"})
		})

		t.Run("having all roles", func(t *testing.T) {
			ctx := context.Background()
			ctx = auth.WithJwtContext(ctx, "some-jwt-issuer", &testGetOfflineTokenJwtContext{T: t, loginClient: authLoginClient, token: token, roles: map[string][]string{"some-login-client": {"A", "B", "C"}}})

			recorder, visited := setupRecorder(ctx, true, "some-login-client", "some-jwt-issuer", "some-token")

			assert.True(t, visited, "the request should have been executed")
			assert.EqualValues(t, http.StatusOK, recorder.Code)
		})
	})

	t.Run("PerformRoleChecking", func(t *testing.T) {
		authLoginClient := "some-login-client"
		token := "some-token"

		myRoleCheckingHandler := func(unwrappedError bool) auth.RoleCheckingHandlerFunc {
			return func(loginClientId string, roles auth.TokenRoles) error {
				if auth.HasRoles(roles, "some-login-client", "B") {
					return nil
				}

				if unwrappedError {
					return auth.MissingRolesError
				} else {
					return &auth.RoleCheckFailedError{Cause: auth.MissingRolesError}
				}
			}
		}

		setupRecorder := func(ctx context.Context, wrappedError bool, loginClientId string, jwtIssuer string, rawToken string) (*httptest.ResponseRecorder, bool) {
			return setupRecorderEx(ctx, func(c *gin.Context) { PerformRoleChecking(c, "some-login-client", myRoleCheckingHandler(wrappedError)) }, loginClientId, jwtIssuer, rawToken)
		}

		t.Run("no token", func(t *testing.T) {
			ctx := context.Background()

			recorder, visited := setupRecorder(ctx, true, "some-login-client", "some-jwt-issuer", "")

			assert.False(t, visited, "the request should not have been executed")
			errorHandling.AssertRequestResponse(t, recorder, http.StatusUnauthorized,
				errorHandling.ErrorResponseAssertion{StatusCode: 401, Detail: "role check failed (no token set)"})
		})

		t.Run("no jwt issuer", func(t *testing.T) {
			ctx := context.Background()

			recorder, visited := setupRecorder(ctx, true, "some-login-client", "", "some-token")

			assert.False(t, visited, "the request should not have been executed")
			errorHandling.AssertRequestResponse(t, recorder, http.StatusUnauthorized,
				errorHandling.ErrorResponseAssertion{StatusCode: 401, Detail: "role check failed (no jwt issuer set)"})
		})

		t.Run("missing roles", func(t *testing.T) {
			ctx := context.Background()
			ctx = auth.WithJwtContext(ctx, "some-jwt-issuer", &testGetOfflineTokenJwtContext{T: t, loginClient: authLoginClient, token: token, roles: map[string][]string{"some-login-client": {"A", "C"}}})

			recorder, visited := setupRecorder(ctx, true, "some-login-client", "some-jwt-issuer", "some-token")

			assert.False(t, visited, "the request should not have been executed")
			errorHandling.AssertRequestResponse(t, recorder, http.StatusForbidden,
				errorHandling.ErrorResponseAssertion{StatusCode: 403, Detail: "role check failed (missing roles)"})
		})

		t.Run("missing roles (unwrapped error)", func(t *testing.T) {
			ctx := context.Background()
			ctx = auth.WithJwtContext(ctx, "some-jwt-issuer", &testGetOfflineTokenJwtContext{T: t, loginClient: authLoginClient, token: token, roles: map[string][]string{"some-login-client": {"A", "C"}}})

			recorder, visited := setupRecorder(ctx, false, "some-login-client", "some-jwt-issuer", "some-token")

			assert.False(t, visited, "the request should not have been executed")
			errorHandling.AssertRequestResponse(t, recorder, http.StatusForbidden,
				errorHandling.ErrorResponseAssertion{StatusCode: 403, Detail: "role check failed (missing roles)"})
		})

		t.Run("no roles on this login client", func(t *testing.T) {
			ctx := context.Background()
			ctx = auth.WithJwtContext(ctx, "some-jwt-issuer", &testGetOfflineTokenJwtContext{T: t, loginClient: authLoginClient, token: token, roles: map[string][]string{"some-other-login-client": {"A", "C"}}})

			recorder, visited := setupRecorder(ctx, true, "some-login-client", "some-jwt-issuer", "some-token")

			assert.False(t, visited, "the request should not have been executed")
			errorHandling.AssertRequestResponse(t, recorder, http.StatusForbidden,
				errorHandling.ErrorResponseAssertion{StatusCode: 403, Detail: "role check failed (missing roles)"})
		})

		t.Run("having all roles", func(t *testing.T) {
			ctx := context.Background()
			ctx = auth.WithJwtContext(ctx, "some-jwt-issuer", &testGetOfflineTokenJwtContext{T: t, loginClient: authLoginClient, token: token, roles: map[string][]string{"some-login-client": {"A", "B", "C"}}})

			recorder, visited := setupRecorder(ctx, true, "some-login-client", "some-jwt-issuer", "some-token")

			assert.True(t, visited, "the request should have been executed")
			assert.EqualValues(t, http.StatusOK, recorder.Code)
		})
	})

}
