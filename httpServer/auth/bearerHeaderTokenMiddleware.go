package auth

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/auth"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/httpServer/errorHandling"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

const (
	jwtHeaderName  = "Authorization"
	jwtHeaderType  = "Bearer"
	dspRawTokenKey = "dsp-raw-token"
)

var unprotectedMethods = []string{http.MethodOptions}

// HeaderExtractor should extract and return the raw access token from the given request.
// Any returned error will be used to abort the current ongoing request
type HeaderExtractor func(c *gin.Context) (string, error)

func DefaultHeaderExtraction(c *gin.Context) (string, error) {

	if jwtHeader := c.GetHeader(jwtHeaderName); jwtHeader == "" { //Validate we have the auth header
		return "", fmt.Errorf("header %s not set", jwtHeaderName)
	} else if parts := strings.Split(jwtHeader, " "); parts[0] != jwtHeaderType || len(parts) != 2 {
		//Make sure the header is in a valid format that we are expecting, ie
		//<HeaderName>: <HeaderType(optional)> <JWT>
		return "", fmt.Errorf("bad header %s. Expected value '%s <JWT>'", jwtHeaderName, jwtHeaderType)
	} else if parts[1] == "" {
		return "", fmt.Errorf("no token provided during context registration")
	} else {
		return parts[1], nil
	}
}

func CustomSetRawTokenMiddleware(extractor HeaderExtractor) gin.HandlerFunc {

	return func(c *gin.Context) {
		// Skip for marked requests
		for _, v := range unprotectedMethods {
			if v == c.Request.Method {
				c.Next()
				return
			}
		}

		//skip if there is already a token within the request
		if _, rawTokenExists := RawTokenFromRequestContext(c); rawTokenExists {
			c.Next()
			return
		}

		rawToken, err := extractor(c)

		if err != nil {
			errorHandling.AbortWithError(c, http.StatusUnauthorized, &auth.RoleCheckFailedError{Cause: err})
			return
		}

		SetRawTokenIntoRequestContext(c, rawToken)
		c.Next()
	}
}

var SetRawTokenMiddleware = CustomSetRawTokenMiddleware(DefaultHeaderExtraction)

func SetRawTokenIntoRequestContext(ctx *gin.Context, rawToken string) {
	ctx.Set(dspRawTokenKey, rawToken)
}

func RawTokenFromRequestContext(c *gin.Context) (string, bool) {
	rawToken, _ := c.Get(dspRawTokenKey)
	if result, ok := rawToken.(string); !ok {
		return "", false
	} else {
		return result, true
	}
}
