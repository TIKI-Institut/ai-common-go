package auth

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/httpServer/errorHandling"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/httpServer/logging"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/tests"
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

func TestSetRawTokenMiddleware(t *testing.T) {
	//assertError := func(t *testing.T, err error, expectedCauseError string) {
	//	assert.Error(t, err)
	//	var targetErr *auth.RoleCheckFailedError
	//	assert.True(t, errors.As(err, &targetErr))
	//	assert.EqualError(t, targetErr.Cause, expectedCauseError)
	//}

	t.Run("no auth token set", func(t *testing.T) {

		ctx := context.Background()

		recorder := tests.TestMiddleware(ctx, "GET", nil, func(g *gin.Engine) {

			//setup error handling
			logging.SetupZerolog(g)
			g.Use(errorHandling.ErrorHandlingMiddleware)
			g.Use(errorHandling.PanicCatchingMiddleware)

			g.Use(SetRawTokenMiddleware)
			g.GET("/", func(c *gin.Context) {
				assert.Fail(t, "the request should not have been executed")
			})
		})

		assert.Equal(t, http.StatusUnauthorized, recorder.Code)
		errorHandling.AssertRequestResponse(t, recorder, http.StatusUnauthorized,
			errorHandling.ErrorResponseAssertion{StatusCode: 401, Detail: "role check failed (header Authorization not set)"},
		)
	})

	t.Run("auth token set", func(t *testing.T) {
		ctx := context.Background()
		recorder := tests.TestMiddleware(ctx, "GET", func(r *http.Request) {
			r.Header.Add("Authorization", "Bearer some-token")
		}, func(g *gin.Engine) {
			g.Use(SetRawTokenMiddleware)
			g.GET("/", func(c *gin.Context) {
				token, found := RawTokenFromRequestContext(c)
				assert.True(t, found)
				assert.EqualValues(t, "some-token", token)
				c.Status(http.StatusOK)
			})
		})

		assert.Equal(t, http.StatusOK, recorder.Code)
	})

	t.Run("ignore token extraction for OPTION request", func(t *testing.T) {
		ctx := context.Background()
		recorder := tests.TestMiddleware(ctx, http.MethodOptions, func(r *http.Request) {
			r.Header.Add("Authorization", "Bearer some-token")
		}, func(g *gin.Engine) {
			g.Use(SetRawTokenMiddleware)
			g.OPTIONS("/", func(c *gin.Context) {
				_, found := RawTokenFromRequestContext(c)
				assert.False(t, found)
				c.Status(http.StatusOK)
			})
		})

		assert.Equal(t, http.StatusOK, recorder.Code)
		assert.EqualValues(t, ``, recorder.Body.String())
	})

	t.Run("custom auth token", func(t *testing.T) {

		customRawTokenExtraction := func(c *gin.Context) (string, error) {

			if jwtHeader := c.GetHeader("X-FORWARDED-ACCESS-TOKEN"); jwtHeader == "" { //Validate we have the auth header
				return "", fmt.Errorf("header %s not set", "X-FORWARDED-ACCESS-TOKEN")
			} else {
				return jwtHeader, nil
			}
		}

		t.Run("set", func(t *testing.T) {
			ctx := context.Background()
			recorder := tests.TestMiddleware(ctx, "GET", func(r *http.Request) {
				r.Header.Add("X-FORWARDED-ACCESS-TOKEN", "some-token")
			}, func(g *gin.Engine) {
				g.Use(CustomSetRawTokenMiddleware(customRawTokenExtraction))
				g.GET("/", func(c *gin.Context) {
					token, found := RawTokenFromRequestContext(c)
					assert.True(t, found)
					assert.EqualValues(t, "some-token", token)
					c.Status(http.StatusOK)
				})
			})

			assert.Equal(t, http.StatusOK, recorder.Code)
		})

		t.Run("not set", func(t *testing.T) {
			ctx := context.Background()
			recorder := tests.TestMiddleware(ctx, "GET", func(r *http.Request) {
				r.Header.Add("Authorization", "some-token")
			}, func(g *gin.Engine) {
				g.Use(CustomSetRawTokenMiddleware(customRawTokenExtraction))
				g.GET("/", func(c *gin.Context) {
					token, found := RawTokenFromRequestContext(c)
					assert.True(t, found)
					assert.EqualValues(t, "some-token", token)
					c.Status(http.StatusOK)
				})
			})

			assert.Equal(t, http.StatusOK, recorder.Code)
		})

	})
}
