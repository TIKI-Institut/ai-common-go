package auth

import "github.com/gin-gonic/gin"

const (
	dspJwtIssuerKey = "dsp-jwt-issuer"
)

func StaticJwtIssuerMiddleware(jwtIssuer string) gin.HandlerFunc {
	return func(gCtx *gin.Context) {
		SetJwtIssuerIntoRequestContext(gCtx, jwtIssuer)
		gCtx.Next()
	}
}

func SetJwtIssuerIntoRequestContext(ctx *gin.Context, jwtIssuer string) {
	ctx.Set(dspJwtIssuerKey, jwtIssuer)
}

func JwtIssuerFromRequestContext(c *gin.Context) (string, bool) {
	jwtIssuer, _ := c.Get(dspJwtIssuerKey)
	if result, ok := jwtIssuer.(string); !ok {
		return "", false
	} else {
		return result, true
	}
}
