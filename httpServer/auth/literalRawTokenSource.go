package auth

import "context"

type literalRawTokenSource string

func (b *literalRawTokenSource) RawToken(ctx context.Context) (string, error) {
	return string(*b), nil
}
