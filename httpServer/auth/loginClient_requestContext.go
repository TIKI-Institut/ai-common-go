package auth

import "github.com/gin-gonic/gin"

const (
	dspLoginClientKey = "dsp-login-client"
)

func StaticLoginClientMiddleware(loginClient string) gin.HandlerFunc {
	return func(gCtx *gin.Context) {
		SetLoginClientIntoRequestContext(gCtx, loginClient)
		gCtx.Next()
	}
}

func SetLoginClientIntoRequestContext(ctx *gin.Context, loginClient string) {
	ctx.Set(dspLoginClientKey, loginClient)
}

func LoginClientFromRequestContext(ctx *gin.Context) (string, bool) {
	loginClient, _ := ctx.Get(dspLoginClientKey)
	if result, ok := loginClient.(string); !ok {
		return "", false
	} else {
		return result, true
	}
}
