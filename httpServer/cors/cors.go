package cors

import (
	"errors"
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

func DefaultCorsHeaders(writer http.ResponseWriter) {
	writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
	writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, DELETE")
}

//goland:noinspection GoUnusedExportedFunction
func CustomCors(allowOrigins []string, headerHandler func(writer http.ResponseWriter)) gin.HandlerFunc {
	return func(c *gin.Context) {
		if !validateOrigin(c.Request.Header.Get("Origin"), allowOrigins) {
			c.AbortWithStatus(http.StatusForbidden)
			return
		}

		asteriskOrigin := false
		if len(allowOrigins) > 0 {
			c.Writer.Header().Set("Access-Control-Allow-Origin", strings.Join(allowOrigins, ", "))

			for _, origin := range allowOrigins {
				if origin == "*" {
					asteriskOrigin = true
				}
			}
		} else if origin := c.Request.Header.Get("Origin"); origin != "" {
			c.Writer.Header().Set("Access-Control-Allow-Origin", origin)
		} else {
			c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
			asteriskOrigin = true
		}

		if !asteriskOrigin {
			c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		}

		if headerHandler != nil {
			headerHandler(c.Writer)
		}

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(http.StatusNoContent) // Using 204 is better than 200 when the request status is OPTIONS
			return
		}

		c.Next()
	}
}

//goland:noinspection GoUnusedExportedFunction
func Cors(allowOrigins []string) gin.HandlerFunc {
	return CustomCors(allowOrigins, DefaultCorsHeaders)
}

func validateOrigin(origin string, allowOrigins []string) bool {
	if len(allowOrigins) == 0 {
		return true
	}
	for _, value := range allowOrigins {
		if value == origin {
			return true
		}
	}

	wildcardOrigins := parseWildcardOrigins(allowOrigins)
	if len(wildcardOrigins) > 0 && validateWildcardOrigin(wildcardOrigins, origin) {
		return true
	}

	return false
}

func parseWildcardOrigins(allowOrigins []string) [][]string {
	var wRules [][]string

	for _, o := range allowOrigins {
		if !strings.Contains(o, "*") {
			continue
		}

		if c := strings.Count(o, "*"); c > 1 {
			panic(errors.New("only one * is allowed").Error())
		}

		i := strings.Index(o, "*")
		if i == 0 {
			wRules = append(wRules, []string{"*", o[1:]})
			continue
		}
		if i == (len(o) - 1) {
			wRules = append(wRules, []string{o[:i-1], "*"})
			continue
		}

		wRules = append(wRules, []string{o[:i], o[i+1:]})
	}

	return wRules
}

func validateWildcardOrigin(wildcardOrigins [][]string, origin string) bool {
	for _, w := range wildcardOrigins {
		if w[0] == "*" && strings.HasSuffix(origin, w[1]) {
			return true
		}
		if w[1] == "*" && strings.HasPrefix(origin, w[0]) {
			return true
		}
		if strings.HasPrefix(origin, w[0]) && strings.HasSuffix(origin, w[1]) {
			return true
		}
	}

	return false
}
