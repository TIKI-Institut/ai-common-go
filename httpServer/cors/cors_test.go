package cors

import (
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestCorsMiddleware(t *testing.T) {

	setup := func(origin string, method string) (*httptest.ResponseRecorder, *gin.Context) {
		recorder := httptest.NewRecorder()
		ginCtx, _ := gin.CreateTestContext(recorder)

		var err error
		ginCtx.Request, err = http.NewRequest(method, "", nil)
		if err != nil {
			panic(err)
		}

		if len(origin) > 0 {
			ginCtx.Request.Header.Set("Origin", origin)
		}

		return recorder, ginCtx
	}

	assertNoBodyWasWritten := func(t *testing.T, recorder *httptest.ResponseRecorder) {
		assert.False(t, recorder.Flushed)
	}

	assertAccessControlAllowOriginHeader := func(t *testing.T, recorder *httptest.ResponseRecorder, header string) {
		assert.EqualValues(t, header, recorder.Header().Get("Access-Control-Allow-Origin"))
	}

	assertAccessControlAllowCredentialsHeader := func(t *testing.T, recorder *httptest.ResponseRecorder) {
		assert.EqualValues(t, "true", recorder.Header().Get("Access-Control-Allow-Credentials"))
	}

	assertNoAccessControlAllowCredentialsHeader := func(t *testing.T, recorder *httptest.ResponseRecorder) {
		assert.EqualValues(t, "", recorder.Header().Get("Access-Control-Allow-Credentials"))
	}

	assertAborted := func(t *testing.T, ginCtx *gin.Context) { assert.True(t, ginCtx.IsAborted()) }

	assertNotAborted := func(t *testing.T, ginCtx *gin.Context) { assert.False(t, ginCtx.IsAborted()) }

	assertStatusNoContent := func(t *testing.T, recorder *httptest.ResponseRecorder) {
		assert.EqualValues(t, http.StatusNoContent, recorder.Code)
	}

	assertStatusForbidden := func(t *testing.T, recorder *httptest.ResponseRecorder) {
		assert.EqualValues(t, http.StatusForbidden, recorder.Code)
	}

	assertStatusOk := func(t *testing.T, recorder *httptest.ResponseRecorder) {
		assert.EqualValues(t, http.StatusOK, recorder.Code)
	}

	t.Run("non OPTIONS request", func(t *testing.T) {
		t.Run("no restrictions", func(t *testing.T) {
			t.Run("no request origin set", func(t *testing.T) {
				recorder, ginCtx := setup("", http.MethodGet)
				CustomCors(nil, nil)(ginCtx)

				assertNotAborted(t, ginCtx)
				assertNoBodyWasWritten(t, recorder)
				assertAccessControlAllowOriginHeader(t, recorder, "*")
				assertNoAccessControlAllowCredentialsHeader(t, recorder)
				assertStatusOk(t, recorder)
			})

			t.Run("request origin set", func(t *testing.T) {
				recorder, ginCtx := setup("some-origin", http.MethodGet)
				CustomCors(nil, nil)(ginCtx)

				assertNotAborted(t, ginCtx)
				assertNoBodyWasWritten(t, recorder)
				assertAccessControlAllowOriginHeader(t, recorder, "some-origin")
				assertAccessControlAllowCredentialsHeader(t, recorder)
				assertStatusOk(t, recorder)
			})
		})

		t.Run("single origin is allowed", func(t *testing.T) {

			t.Run("no request origin set", func(t *testing.T) {
				recorder, ginCtx := setup("", http.MethodGet)
				CustomCors([]string{"some-origin"}, nil)(ginCtx)

				assertAborted(t, ginCtx)
				assertNoBodyWasWritten(t, recorder)
				assertAccessControlAllowOriginHeader(t, recorder, "")
				assertNoAccessControlAllowCredentialsHeader(t, recorder)
				assertStatusForbidden(t, recorder)
			})

			t.Run("not allowed request origin set", func(t *testing.T) {
				recorder, ginCtx := setup("some-other-origin", http.MethodGet)
				CustomCors([]string{"some-origin"}, nil)(ginCtx)

				assertAborted(t, ginCtx)
				assertNoBodyWasWritten(t, recorder)
				assertAccessControlAllowOriginHeader(t, recorder, "")
				assertNoAccessControlAllowCredentialsHeader(t, recorder)
				assertStatusForbidden(t, recorder)
			})

			t.Run("request origin set", func(t *testing.T) {
				recorder, ginCtx := setup("some-origin", http.MethodGet)
				CustomCors([]string{"some-origin"}, nil)(ginCtx)

				assertNotAborted(t, ginCtx)
				assertNoBodyWasWritten(t, recorder)
				assertAccessControlAllowOriginHeader(t, recorder, "some-origin")
				assertAccessControlAllowCredentialsHeader(t, recorder)
				assertStatusOk(t, recorder)
			})
		})

		t.Run("multiple origin is allowed", func(t *testing.T) {

			t.Run("no request origin set", func(t *testing.T) {
				recorder, ginCtx := setup("", http.MethodGet)
				CustomCors([]string{"some-origin-A", "some-origin-B"}, nil)(ginCtx)

				assertAborted(t, ginCtx)
				assertNoBodyWasWritten(t, recorder)
				assertAccessControlAllowOriginHeader(t, recorder, "")
				assertNoAccessControlAllowCredentialsHeader(t, recorder)
				assertStatusForbidden(t, recorder)
			})

			t.Run("not allowed request origin set", func(t *testing.T) {
				recorder, ginCtx := setup("some-other-origin", http.MethodGet)
				CustomCors([]string{"some-origin-A", "some-origin-B"}, nil)(ginCtx)

				assertAborted(t, ginCtx)
				assertNoBodyWasWritten(t, recorder)
				assertAccessControlAllowOriginHeader(t, recorder, "")
				assertNoAccessControlAllowCredentialsHeader(t, recorder)
				assertStatusForbidden(t, recorder)
			})

			t.Run("request origin set", func(t *testing.T) {
				recorder, ginCtx := setup("some-origin-B", http.MethodGet)
				CustomCors([]string{"some-origin-A", "some-origin-B"}, nil)(ginCtx)

				assertNotAborted(t, ginCtx)
				assertNoBodyWasWritten(t, recorder)
				assertAccessControlAllowOriginHeader(t, recorder, "some-origin-A, some-origin-B")
				assertAccessControlAllowCredentialsHeader(t, recorder)
				assertStatusOk(t, recorder)
			})
		})

		t.Run("wildcard", func(t *testing.T) {
			t.Run("no request origin set", func(t *testing.T) {
				recorder, ginCtx := setup("", http.MethodGet)
				CustomCors([]string{"some-origin-A", "*"}, nil)(ginCtx)

				assertNotAborted(t, ginCtx)
				assertNoBodyWasWritten(t, recorder)
				assertAccessControlAllowOriginHeader(t, recorder, "some-origin-A, *")
				assertNoAccessControlAllowCredentialsHeader(t, recorder)
				assertStatusOk(t, recorder)
			})

			t.Run("not allowed request origin set", func(t *testing.T) {
				recorder, ginCtx := setup("some-other-origin", http.MethodGet)
				CustomCors([]string{"some-origin-A", "*"}, nil)(ginCtx)

				assertNotAborted(t, ginCtx)
				assertNoBodyWasWritten(t, recorder)
				assertAccessControlAllowOriginHeader(t, recorder, "some-origin-A, *")
				assertNoAccessControlAllowCredentialsHeader(t, recorder)
				assertStatusOk(t, recorder)
			})

			t.Run("request origin set", func(t *testing.T) {
				recorder, ginCtx := setup("some-origin-B", http.MethodGet)
				CustomCors([]string{"some-origin-A", "*"}, nil)(ginCtx)

				assertNotAborted(t, ginCtx)
				assertNoBodyWasWritten(t, recorder)
				assertAccessControlAllowOriginHeader(t, recorder, "some-origin-A, *")
				assertNoAccessControlAllowCredentialsHeader(t, recorder)
				assertStatusOk(t, recorder)
			})
		})

		t.Run("wildcard prefix", func(t *testing.T) {
			t.Run("no request origin set", func(t *testing.T) {
				recorder, ginCtx := setup("", http.MethodGet)
				CustomCors([]string{"some-origin-A", "*-origin-B"}, nil)(ginCtx)

				assertAborted(t, ginCtx)
				assertNoBodyWasWritten(t, recorder)
				assertAccessControlAllowOriginHeader(t, recorder, "")
				assertNoAccessControlAllowCredentialsHeader(t, recorder)
				assertStatusForbidden(t, recorder)
			})

			t.Run("not allowed request origin set", func(t *testing.T) {
				recorder, ginCtx := setup("some-other-origin", http.MethodGet)
				CustomCors([]string{"some-origin-A", "*-origin-B"}, nil)(ginCtx)

				assertAborted(t, ginCtx)
				assertNoBodyWasWritten(t, recorder)
				assertAccessControlAllowOriginHeader(t, recorder, "")
				assertNoAccessControlAllowCredentialsHeader(t, recorder)
				assertStatusForbidden(t, recorder)
			})

			t.Run("request origin set", func(t *testing.T) {
				recorder, ginCtx := setup("some-origin-B", http.MethodGet)
				CustomCors([]string{"some-origin-A", "*-origin-B"}, nil)(ginCtx)

				assertNotAborted(t, ginCtx)
				assertNoBodyWasWritten(t, recorder)
				assertAccessControlAllowOriginHeader(t, recorder, "some-origin-A, *-origin-B")
				assertAccessControlAllowCredentialsHeader(t, recorder)
				assertStatusOk(t, recorder)
			})
		})

		t.Run("wildcard suffix", func(t *testing.T) {
			t.Run("no request origin set", func(t *testing.T) {
				recorder, ginCtx := setup("", http.MethodGet)
				CustomCors([]string{"some-origin-A", "some-origin-*"}, nil)(ginCtx)

				assertAborted(t, ginCtx)
				assertNoBodyWasWritten(t, recorder)
				assertAccessControlAllowOriginHeader(t, recorder, "")
				assertNoAccessControlAllowCredentialsHeader(t, recorder)
				assertStatusForbidden(t, recorder)
			})

			t.Run("not allowed request origin set", func(t *testing.T) {
				recorder, ginCtx := setup("some-other-origin", http.MethodGet)
				CustomCors([]string{"some-origin-A", "some-origin-*"}, nil)(ginCtx)

				assertAborted(t, ginCtx)
				assertNoBodyWasWritten(t, recorder)
				assertAccessControlAllowOriginHeader(t, recorder, "")
				assertNoAccessControlAllowCredentialsHeader(t, recorder)
				assertStatusForbidden(t, recorder)
			})

			t.Run("request origin set", func(t *testing.T) {
				recorder, ginCtx := setup("some-origin-B", http.MethodGet)
				CustomCors([]string{"some-origin-A", "some-origin-*"}, nil)(ginCtx)

				assertNotAborted(t, ginCtx)
				assertNoBodyWasWritten(t, recorder)
				assertAccessControlAllowOriginHeader(t, recorder, "some-origin-A, some-origin-*")
				assertAccessControlAllowCredentialsHeader(t, recorder)
				assertStatusOk(t, recorder)
			})
		})

		t.Run("multiple wildcards", func(t *testing.T) {
			_, ginCtx := setup("", http.MethodOptions)
			assert.Panics(t, func() {
				CustomCors([]string{"some-origin-A", "some-origin-*-*"}, nil)(ginCtx)
			})
		})

		t.Run("additional custom handler", func(t *testing.T) {
			recorder, ginCtx := setup("some-origin-B", http.MethodGet)
			wasCalled := false

			CustomCors([]string{"some-origin-A", "some-origin-*"}, func(writer http.ResponseWriter) {
				writer.Header().Set("some-custom-header", "header-value")
				wasCalled = true
			})(ginCtx)

			assert.True(t, wasCalled)

			assertNotAborted(t, ginCtx)
			assertNoBodyWasWritten(t, recorder)
			assertAccessControlAllowOriginHeader(t, recorder, "some-origin-A, some-origin-*")
			assertAccessControlAllowCredentialsHeader(t, recorder)
			assertStatusOk(t, recorder)

			assert.EqualValues(t, "header-value", recorder.Header().Get("some-custom-header"))
		})

	})

	t.Run("OPTIONS request", func(t *testing.T) {
		t.Run("no restrictions", func(t *testing.T) {
			t.Run("no request origin set", func(t *testing.T) {
				recorder, ginCtx := setup("", http.MethodOptions)
				CustomCors(nil, nil)(ginCtx)

				assertAborted(t, ginCtx)
				assertNoBodyWasWritten(t, recorder)
				assertAccessControlAllowOriginHeader(t, recorder, "*")
				assertNoAccessControlAllowCredentialsHeader(t, recorder)
				assertStatusNoContent(t, recorder)
			})

			t.Run("request origin set", func(t *testing.T) {
				recorder, ginCtx := setup("some-origin", http.MethodOptions)
				CustomCors(nil, nil)(ginCtx)

				assertAborted(t, ginCtx)
				assertNoBodyWasWritten(t, recorder)
				assertAccessControlAllowOriginHeader(t, recorder, "some-origin")
				assertAccessControlAllowCredentialsHeader(t, recorder)
				assertStatusNoContent(t, recorder)
			})
		})

		t.Run("single origin is allowed", func(t *testing.T) {

			t.Run("no request origin set", func(t *testing.T) {
				recorder, ginCtx := setup("", http.MethodOptions)
				CustomCors([]string{"some-origin"}, nil)(ginCtx)

				assertAborted(t, ginCtx)
				assertNoBodyWasWritten(t, recorder)
				assertAccessControlAllowOriginHeader(t, recorder, "")
				assertNoAccessControlAllowCredentialsHeader(t, recorder)
				assertStatusForbidden(t, recorder)
			})

			t.Run("not allowed request origin set", func(t *testing.T) {
				recorder, ginCtx := setup("some-other-origin", http.MethodOptions)
				CustomCors([]string{"some-origin"}, nil)(ginCtx)

				assertAborted(t, ginCtx)
				assertNoBodyWasWritten(t, recorder)
				assertAccessControlAllowOriginHeader(t, recorder, "")
				assertNoAccessControlAllowCredentialsHeader(t, recorder)
				assertStatusForbidden(t, recorder)
			})

			t.Run("request origin set", func(t *testing.T) {
				recorder, ginCtx := setup("some-origin", http.MethodOptions)
				CustomCors([]string{"some-origin"}, nil)(ginCtx)

				assertAborted(t, ginCtx)
				assertNoBodyWasWritten(t, recorder)
				assertAccessControlAllowOriginHeader(t, recorder, "some-origin")
				assertAccessControlAllowCredentialsHeader(t, recorder)
				assertStatusNoContent(t, recorder)
			})
		})

		t.Run("multiple origin is allowed", func(t *testing.T) {

			t.Run("no request origin set", func(t *testing.T) {
				recorder, ginCtx := setup("", http.MethodOptions)
				CustomCors([]string{"some-origin-A", "some-origin-B"}, nil)(ginCtx)

				assertAborted(t, ginCtx)
				assertNoBodyWasWritten(t, recorder)
				assertAccessControlAllowOriginHeader(t, recorder, "")
				assertNoAccessControlAllowCredentialsHeader(t, recorder)
				assertStatusForbidden(t, recorder)
			})

			t.Run("not allowed request origin set", func(t *testing.T) {
				recorder, ginCtx := setup("some-other-origin", http.MethodOptions)
				CustomCors([]string{"some-origin-A", "some-origin-B"}, nil)(ginCtx)

				assertAborted(t, ginCtx)
				assertNoBodyWasWritten(t, recorder)
				assertAccessControlAllowOriginHeader(t, recorder, "")
				assertNoAccessControlAllowCredentialsHeader(t, recorder)
				assertStatusForbidden(t, recorder)
			})

			t.Run("request origin set", func(t *testing.T) {
				recorder, ginCtx := setup("some-origin-B", http.MethodOptions)
				CustomCors([]string{"some-origin-A", "some-origin-B"}, nil)(ginCtx)

				assertAborted(t, ginCtx)
				assertNoBodyWasWritten(t, recorder)
				assertAccessControlAllowOriginHeader(t, recorder, "some-origin-A, some-origin-B")
				assertAccessControlAllowCredentialsHeader(t, recorder)
				assertStatusNoContent(t, recorder)
			})
		})

		t.Run("wildcard", func(t *testing.T) {
			t.Run("no request origin set", func(t *testing.T) {
				recorder, ginCtx := setup("", http.MethodOptions)
				CustomCors([]string{"some-origin-A", "*"}, nil)(ginCtx)

				assertAborted(t, ginCtx)
				assertNoBodyWasWritten(t, recorder)
				assertAccessControlAllowOriginHeader(t, recorder, "some-origin-A, *")
				assertNoAccessControlAllowCredentialsHeader(t, recorder)
				assertStatusNoContent(t, recorder)
			})

			t.Run("not allowed request origin set", func(t *testing.T) {
				recorder, ginCtx := setup("some-other-origin", http.MethodOptions)
				CustomCors([]string{"some-origin-A", "*"}, nil)(ginCtx)

				assertAborted(t, ginCtx)
				assertNoBodyWasWritten(t, recorder)
				assertAccessControlAllowOriginHeader(t, recorder, "some-origin-A, *")
				assertNoAccessControlAllowCredentialsHeader(t, recorder)
				assertStatusNoContent(t, recorder)
			})

			t.Run("request origin set", func(t *testing.T) {
				recorder, ginCtx := setup("some-origin-B", http.MethodOptions)
				CustomCors([]string{"some-origin-A", "*"}, nil)(ginCtx)

				assertAborted(t, ginCtx)
				assertNoBodyWasWritten(t, recorder)
				assertAccessControlAllowOriginHeader(t, recorder, "some-origin-A, *")
				assertNoAccessControlAllowCredentialsHeader(t, recorder)
				assertStatusNoContent(t, recorder)
			})
		})

		t.Run("wildcard prefix", func(t *testing.T) {
			t.Run("no request origin set", func(t *testing.T) {
				recorder, ginCtx := setup("", http.MethodOptions)
				CustomCors([]string{"some-origin-A", "*-origin-B"}, nil)(ginCtx)

				assertAborted(t, ginCtx)
				assertNoBodyWasWritten(t, recorder)
				assertAccessControlAllowOriginHeader(t, recorder, "")
				assertNoAccessControlAllowCredentialsHeader(t, recorder)
				assertStatusForbidden(t, recorder)
			})

			t.Run("not allowed request origin set", func(t *testing.T) {
				recorder, ginCtx := setup("some-other-origin", http.MethodOptions)
				CustomCors([]string{"some-origin-A", "*-origin-B"}, nil)(ginCtx)

				assertAborted(t, ginCtx)
				assertNoBodyWasWritten(t, recorder)
				assertAccessControlAllowOriginHeader(t, recorder, "")
				assertNoAccessControlAllowCredentialsHeader(t, recorder)
				assertStatusForbidden(t, recorder)
			})

			t.Run("request origin set", func(t *testing.T) {
				recorder, ginCtx := setup("some-origin-B", http.MethodOptions)
				CustomCors([]string{"some-origin-A", "*-origin-B"}, nil)(ginCtx)

				assertAborted(t, ginCtx)
				assertNoBodyWasWritten(t, recorder)
				assertAccessControlAllowOriginHeader(t, recorder, "some-origin-A, *-origin-B")
				assertAccessControlAllowCredentialsHeader(t, recorder)
				assertStatusNoContent(t, recorder)
			})
		})

		t.Run("wildcard suffix", func(t *testing.T) {
			t.Run("no request origin set", func(t *testing.T) {
				recorder, ginCtx := setup("", http.MethodOptions)
				CustomCors([]string{"some-origin-A", "some-origin-*"}, nil)(ginCtx)

				assertAborted(t, ginCtx)
				assertNoBodyWasWritten(t, recorder)
				assertAccessControlAllowOriginHeader(t, recorder, "")
				assertNoAccessControlAllowCredentialsHeader(t, recorder)
				assertStatusForbidden(t, recorder)
			})

			t.Run("not allowed request origin set", func(t *testing.T) {
				recorder, ginCtx := setup("some-other-origin", http.MethodOptions)
				CustomCors([]string{"some-origin-A", "some-origin-*"}, nil)(ginCtx)

				assertAborted(t, ginCtx)
				assertNoBodyWasWritten(t, recorder)
				assertAccessControlAllowOriginHeader(t, recorder, "")
				assertNoAccessControlAllowCredentialsHeader(t, recorder)
				assertStatusForbidden(t, recorder)
			})

			t.Run("request origin set", func(t *testing.T) {
				recorder, ginCtx := setup("some-origin-B", http.MethodOptions)
				CustomCors([]string{"some-origin-A", "some-origin-*"}, nil)(ginCtx)

				assertAborted(t, ginCtx)
				assertNoBodyWasWritten(t, recorder)
				assertAccessControlAllowOriginHeader(t, recorder, "some-origin-A, some-origin-*")
				assertAccessControlAllowCredentialsHeader(t, recorder)
				assertStatusNoContent(t, recorder)
			})
		})

		t.Run("multiple wildcards", func(t *testing.T) {
			_, ginCtx := setup("", http.MethodOptions)
			assert.Panics(t, func() {
				CustomCors([]string{"some-origin-A", "some-origin-*-*"}, nil)(ginCtx)
			})
		})

		t.Run("additional custom handler", func(t *testing.T) {
			recorder, ginCtx := setup("some-origin-B", http.MethodOptions)
			wasCalled := false

			CustomCors([]string{"some-origin-A", "some-origin-*"}, func(writer http.ResponseWriter) {
				writer.Header().Set("some-custom-header", "header-value")
				wasCalled = true
			})(ginCtx)

			assert.True(t, wasCalled)

			assertAborted(t, ginCtx)
			assertNoBodyWasWritten(t, recorder)
			assertAccessControlAllowOriginHeader(t, recorder, "some-origin-A, some-origin-*")
			assertAccessControlAllowCredentialsHeader(t, recorder)
			assertStatusNoContent(t, recorder)

			assert.EqualValues(t, "header-value", recorder.Header().Get("some-custom-header"))
		})

	})

}
