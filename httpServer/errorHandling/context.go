package errorHandling

import (
	"context"
	"github.com/gin-contrib/requestid"
	"github.com/gin-gonic/gin"
	"github.com/lithammer/shortuuid"
)

type uniqueIdFactory func() string
type uniqueIdFactoryKey int

var uniqueIdFactoryKeyValue uniqueIdFactoryKey

func withUniqueIdFactory(ctx context.Context, factory uniqueIdFactory) context.Context {
	return context.WithValue(ctx, uniqueIdFactoryKeyValue, factory)
}

func resolveUniqueId(ctx context.Context) string {
	return resolveUniqueIdFactory(ctx)()
}

func resolveRequestId(gCtx *gin.Context) string {
	rid := requestid.Get(gCtx)
	if len(rid) > 0 {
		return rid
	}
	return resolveUniqueId(gCtx)
}

func resolveUniqueIdFactory(ctx context.Context) uniqueIdFactory {
	if customFactory, ok := ctx.Value(uniqueIdFactoryKeyValue).(uniqueIdFactory); ok {
		return customFactory
	}
	return shortuuid.New
}
