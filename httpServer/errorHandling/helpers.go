package errorHandling

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"net/http/httptest"
	"testing"
)

type ErrorResponseAssertion struct {
	StatusCode int
	Detail     string
}

// AssertFirstGinError checks the enlisted errors within gin.Context.
// The assertion looks for the first error; it shall be an error submitted by AbortWithError or AbortWithErrorAndStacktrace
func AssertFirstGinError(t *testing.T, ginCtx *gin.Context, expectedStatusCode int, expectedErrorMessage string) {

	var firstError *errorEntry

	for _, e := range ginCtx.Errors {
		var ok bool
		if firstError, ok = e.Err.(*errorEntry); ok {
			break
		}
	}

	if assert.NotNil(t, firstError) {
		assert.NotEqualValues(t, NoStatusCode, firstError.Status)
		assert.EqualValues(t, expectedStatusCode, int(firstError.Status))

		var err error
		err = firstError
		assert.EqualError(t, err, expectedErrorMessage)
	}
}

// AssertGinErrors checks the enlisted errors within gin.Context.
// The assertion looks for any error submitted by AbortWithError or AbortWithErrorAndStacktrace
func AssertGinErrors(t *testing.T, ginCtx *gin.Context, errors ...ErrorResponseAssertion) {

	var ginErrors []*errorEntry

	for _, e := range ginCtx.Errors {
		if tmpError, ok := e.Err.(*errorEntry); ok {
			ginErrors = append(ginErrors, tmpError)
		}
	}

	if assert.Len(t, ginErrors, len(errors)) {
		for i, e := range ginErrors {
			assert.EqualValues(t, errors[i].StatusCode, int(e.Status))
			assert.EqualError(t, e, errors[i].Detail)
		}
	}
}

// FirstGinError checks the enlisted errors within gin.Context.
// It returns the first error; it shall be an error submitted by AbortWithError or AbortWithErrorAndStacktrace
func FirstGinError(ginCtx *gin.Context) error {

	for _, e := range ginCtx.Errors {
		if err, ok := e.Err.(*errorEntry); ok {
			return err.originalError
		}
	}

	return nil
}

// AssertPreparedResponseStatusCode checks the current gin.Context to not have a committed HTTP status code within the response
// It searches the enlisted errors for a prepared status code which will be picked up by ErrorHandlingMiddleware
func AssertPreparedResponseStatusCode(t *testing.T, ginCtx *gin.Context, expectedStatusCode int) {

	var sCode = NoStatusCode

	for _, e := range ginCtx.Errors {
		if firstError, ok := e.Err.(*errorEntry); ok {
			sCode = firstError.Status
		}

		if sCode != NoStatusCode {
			break
		}
	}

	if assert.NotNil(t, sCode) {
		assert.EqualValues(t, expectedStatusCode, int(sCode))
	}
}

// AssertRequestResponse checks the recorded response for a httptest.ResponseRecorder
func AssertRequestResponse(t *testing.T, rec *httptest.ResponseRecorder, expectedStatusCode int, errors ...ErrorResponseAssertion) {

	assert.EqualValues(t, expectedStatusCode, rec.Code)

	allResponseErrors := ErrorResponse{}
	if assert.NoError(t, json.Unmarshal(rec.Body.Bytes(), &allResponseErrors)) &&
		assert.Len(t, allResponseErrors, len(errors)) {

		for i, expectedError := range errors {
			assert.EqualValuesf(t, expectedError.StatusCode, allResponseErrors[i].Status, "Status code differs in error #%d", i)
			assert.EqualValuesf(t, expectedError.Detail, allResponseErrors[i].Detail, "Detail message code differs in error #%d", i)
		}
	}
}
