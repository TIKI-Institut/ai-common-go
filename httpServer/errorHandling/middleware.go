package errorHandling

import (
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"net/http"
	"slices"
)

// AbortWithError aborts the current request.
func AbortWithError(gCtx *gin.Context, code int, err ...error) {
	abortWithError(gCtx, code, false, err...)
}

// AbortWithErrorAndStacktrace aborts the current request; it attaches the stack trace to the error; it will be printed into the logs but not as response.
func AbortWithErrorAndStacktrace(gCtx *gin.Context, code int, err ...error) {
	abortWithError(gCtx, code, true, err...)
}

func abortWithError(gCtx *gin.Context, code int, appendStackTrace bool, errors ...error) {
	gCtx.Abort()

	allErrors := unwrapErrors(errors...)

	for i := 0; i < len(allErrors); i++ {
		wrappedError := convertToErrorEntry(resolveUniqueIdFactory(gCtx), allErrors[i])

		//there might be a set StatusCode
		if wrappedError.Status == NoStatusCode {
			wrappedError.Status = StatusCode(code)
		}

		if appendStackTrace {
			wrappedError.appendStackTrace()
		}

		var convertedErr error
		convertedErr = wrappedError
		_ = gCtx.Error(convertedErr)
	}

}

func unwrapErrors(errs ...error) []error {
	var target MultiError = nil

	slices.Reverse(errs)

	pop := func() error {
		top := errs[len(errs)-1]
		errs = errs[:len(errs)-1]
		return top
	}

	push := func(newErrors []error) {
		errs = append(errs, newErrors...)
	}

	var result []error

	for len(errs) > 0 {
		nextError := pop()

		if errors.As(nextError, &target) {
			wrappedErrors := target.WrappedErrors()
			slices.Reverse(wrappedErrors)
			push(wrappedErrors)
			continue
		}

		result = append(result, nextError)
	}

	return result
}

func convertToErrorEntry(idFactory uniqueIdFactory, err error) *errorEntry {
	var wrappedError *errorEntry

	if converted, ok := err.(*errorEntry); ok {
		//try to convert to *errorEntry directly
		wrappedError = converted
	} else if converted, ok := err.(*ErrorMessage); ok {
		//try to convert to *ErrorMessage
		wrappedError = &errorEntry{
			ErrorMessage: *converted,
		}
	} else {
		var code *string = nil
		if codedError, success := err.(interface{ ErrorCode() string }); success {
			c := codedError.ErrorCode()
			code = &c
		}

		var meta map[string]interface{} = nil
		if codedError, success := err.(interface{ ErrorMeta() map[string]interface{} }); success {
			meta = codedError.ErrorMeta()
		}

		wrappedError = &errorEntry{
			ErrorMessage: ErrorMessage{
				Meta:   meta,
				Code:   code,
				Detail: err.Error(),
			},
			originalError: err,
		}
	}

	if len(wrappedError.Id) == 0 {
		wrappedError.Id = idFactory()
	}

	return wrappedError
}

func isPublicError(e *errorEntry, ginError *gin.Error) bool {

	//is this a gin.Error?
	switch ginError.Type {
	case gin.ErrorTypePublic:
		fallthrough
	case gin.ErrorTypeBind:
		return true
	}

	//anything below 500 is a "public error"
	if e.Status != NoStatusCode {
		return (e.Status) < http.StatusInternalServerError
	}

	return false
}

var writeErrorsContextKey = "_errorHandling_do_not_write_errors"

func writeErrors(gCtx *gin.Context) bool {
	return gCtx.Value(writeErrorsContextKey) == nil
}

func DontWriteErrors(gCtx *gin.Context) {
	gCtx.Set(writeErrorsContextKey, true)
}

// ErrorHandlingMiddleware takes the last registered Error and creates a response based on this error
//
//goland:noinspection GoNameStartsWithPackageName
func ErrorHandlingMiddleware(gCtx *gin.Context) {

	gCtx.Next()

	if len(gCtx.Errors) == 0 {
		//no errors....
		return
	}

	//collect all errors
	//there might be errors which shall not be displayed to the customer; filter them
	var publicErrors []*errorEntry
	var privateErrors []*errorEntry

	//requestId is a unique for this set of errors which all occurred during the same request
	requestId := resolveRequestId(gCtx)

	entryFromError := func(idFactory uniqueIdFactory, err error, meta interface{}) *errorEntry {
		wrappedError := convertToErrorEntry(idFactory, err)

		if meta != nil {
			if wrappedError.internals == nil {
				wrappedError.internals = map[string]interface{}{}
			}

			wrappedError.internals[internalsEntryGinErrorMeta] = meta
		}

		return wrappedError
	}

	//Convert all existing errors from gin.Context
	errorIdFactory := resolveUniqueIdFactory(gCtx)
	for _, e := range gCtx.Errors {
		entry := entryFromError(errorIdFactory, e.Err, e.Meta)

		//sieving public/private
		if isPublicError(entry, e) {
			publicErrors = append(publicErrors, entry)
		} else {
			privateErrors = append(privateErrors, entry)
		}
	}

	//if there are only private errors we attach a synthetic public error to ensure a response is sent
	if len(publicErrors) == 0 {
		syntheticPublicError := entryFromError(func() string { return requestId }, fmt.Errorf("please refer to the id of this error when contacting support"), nil)
		syntheticPublicError.Status = StatusCode(http.StatusInternalServerError)

		publicErrors = []*errorEntry{syntheticPublicError}
	}

	//all errors will be logged to the logger attached to the current context.Context
	logger := log.Ctx(gCtx).With().Fields(map[string]interface{}{"request-id": requestId}).Logger()

	printErrors := func(errors []*errorEntry, logger zerolog.Logger) {
		if len(errors) == 0 {
			return
		}
		for _, e := range errors {
			event := logger.Err(e).Interface("error-object", &e)

			if e.internals != nil {
				event = event.Interface("internals", e.internals)
			}

			event.Send()
		}
	}
	alterLogger := func(logger zerolog.Logger, isPrivateError bool) zerolog.Logger {
		return logger.With().Bool("private-error", isPrivateError).Logger()
	}

	printErrors(publicErrors, alterLogger(logger, false))
	printErrors(privateErrors, alterLogger(logger, true))

	//non aborted requests supposedly write their own content
	if !gCtx.IsAborted() {
		return
	}

	//some errors (public) will be rendered to the customer
	var errorCode = NoStatusCode

	// try to infer error code from public errors
	for i := range publicErrors {
		if publicErrors[i].Status != NoStatusCode {
			errorCode = publicErrors[i].Status
			break
		}
	}

	//fallback to http.StatusInternalServerError if no error code was set
	for errorCode == NoStatusCode {
		errorCode = StatusCode(http.StatusInternalServerError)
	}

	//this request might be marked to not write errors
	if !writeErrors(gCtx) {
		gCtx.Status(int(errorCode))
		return
	}

	gCtx.JSON(int(errorCode), publicErrors)
}
