package errorHandling

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/tests"
	"bufio"
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/rs/zerolog"
	"github.com/stretchr/testify/assert"
	"io"
	"net/http"
	"strconv"
	"strings"
	"sync/atomic"
	"testing"
)

type testPanicObject struct {
}

func (t testPanicObject) String() string {
	return "some-weird-object"
}

type testMetadataError struct {
}

func (t testMetadataError) Error() string {
	return "error-with-meta"
}

func (t testMetadataError) ErrorMeta() map[string]interface{} {
	return map[string]interface{}{"custom-meta": "some-info"}
}

type testErrorCodeError struct {
}

func (t testErrorCodeError) Error() string {
	return "error-with-error-code"
}

func (t testErrorCodeError) ErrorCode() string {
	return "custom-error-code"
}

type testErrorCodeAndMetaError struct {
}

func (t testErrorCodeAndMetaError) Error() string {
	return "error-with-error-code-and-meta"
}

func (t testErrorCodeAndMetaError) ErrorCode() string {
	return "custom-error-code-with-meta"
}

func (t testErrorCodeAndMetaError) ErrorMeta() map[string]interface{} {
	return map[string]interface{}{"custom-meta": "custom-meta-with-error-code"}
}

type testLogger struct {
	*testing.T
	allWrittenBytes bytes.Buffer
}

func (t *testLogger) Write(p []byte) (n int, err error) {

	t.T.Logf(string(p))
	t.allWrittenBytes.Write(p)
	return len(p), nil
}
func (t *testLogger) WrittenBytes() string {
	return t.allWrittenBytes.String()
}

func TestErrorHandlingMiddleware(t *testing.T) {

	setupLogging := func(t *testing.T, ctx context.Context) (context.Context, *testLogger) {
		target := &testLogger{T: t}
		testLog := zerolog.New(target).Level(zerolog.TraceLevel)
		return testLog.WithContext(ctx), target
	}

	makeUniqIdFactory := func(target *uint32) uniqueIdFactory {
		return func() string {
			return strconv.Itoa(int(atomic.AddUint32(target, 1)))
		}
	}

	expectedLogObjects := func(t *testing.T, msgs ...string) []map[string]interface{} {
		result := make([]map[string]interface{}, len(msgs))

		for i, r := range msgs {
			loggedContent := map[string]interface{}{}
			assert.NoError(t, json.Unmarshal([]byte(r), &loggedContent))
			result[i] = loggedContent
		}

		return result
	}

	getLogLines := func(s string) []string {
		var lines []string
		sc := bufio.NewScanner(strings.NewReader(s))
		for sc.Scan() {
			lines = append(lines, sc.Text())
		}
		return lines
	}

	assertStackTrace := func(t *testing.T, loggedStrings string, expectedNumberOfLogs int, targetRows ...int) []map[string]interface{} {
		//asserting the contents of stacktrace is fuzzy bcs the fixed paths are included.
		//we extract the stack trace and assume the rest.
		//the stack trace itself shall not be empty...
		loggedLines := getLogLines(loggedStrings)
		//there should only be one line
		assert.Len(t, loggedLines, expectedNumberOfLogs)

		contains := func(i int) bool {
			for _, x := range targetRows {
				if x == i {
					return true
				}
			}
			return false
		}

		result := make([]map[string]interface{}, len(loggedLines))

		for rIdx, r := range loggedLines {
			loggedContent := map[string]interface{}{}
			assert.NoError(t, json.Unmarshal([]byte(r), &loggedContent))

			if contains(rIdx) {
				//remove stacktrace....
				trace := (loggedContent["internals"].(map[string]interface{}))["stack-trace"]
				delete(loggedContent["internals"].(map[string]interface{}), "stack-trace")
				//the trace shall not be empty
				assert.NotEmpty(t, trace)
			}

			result[rIdx] = loggedContent
		}

		return result
	}

	testList := []struct {
		name                   string
		requestModifier        func(req *http.Request)
		requestHandler         func(*testing.T) func(c *gin.Context)
		expectedLogMessages    []string
		messagesWithStacktrace []int
		expectedStatusCode     int
		expectedBody           string
		addPanicHandler        bool
		withError              bool
	}{
		{
			name: "no error",
			requestHandler: func(t *testing.T) func(c *gin.Context) {
				return func(c *gin.Context) {
					c.JSON(http.StatusOK, gin.H{})
				}
			},
			expectedLogMessages:    nil,
			messagesWithStacktrace: nil,
			expectedStatusCode:     http.StatusOK,
			expectedBody:           `{}`,
		},
		{
			name: "default hidden error",
			requestHandler: func(t *testing.T) func(c *gin.Context) {
				return func(c *gin.Context) {
					AbortWithError(c, http.StatusInternalServerError, fmt.Errorf("some-error"))
				}
			},
			expectedLogMessages: []string{
				`{"level":"error","request-id":"2","private-error":false,"error":"please refer to the id of this error when contacting support","error-object":{"id":"2","status":"500","detail":"please refer to the id of this error when contacting support"}}`,
				`{"level":"error","request-id":"2","private-error":true,"error":"some-error","error-object":{"id":"1","status":"500","detail":"some-error"}}`,
			},
			messagesWithStacktrace: nil,
			expectedStatusCode:     http.StatusInternalServerError,
			expectedBody:           `[{"id":"2","status":"500","detail":"please refer to the id of this error when contacting support"}]`,
		},
		{
			name: "default public error",
			requestHandler: func(t *testing.T) func(c *gin.Context) {
				return func(c *gin.Context) {
					AbortWithError(c, http.StatusUnauthorized, fmt.Errorf("some-error"))
				}
			},
			expectedLogMessages: []string{
				`{"level":"error","request-id":"2","private-error":false,"error":"some-error","error-object":{"id":"1","status":"401","detail":"some-error"}}`,
			},
			messagesWithStacktrace: nil,
			expectedStatusCode:     http.StatusUnauthorized,
			expectedBody:           `[{"id":"1","status":"401","detail":"some-error"}]`,
		},
		{
			name: "error with stacktrace",
			requestHandler: func(t *testing.T) func(c *gin.Context) {
				return func(c *gin.Context) {
					AbortWithErrorAndStacktrace(c, http.StatusUnauthorized, fmt.Errorf("some-error"))
				}
			},
			expectedLogMessages: []string{
				`{"level":"error","request-id":"2","private-error":false,"error":"some-error","error-object":{"id":"1","status":"401","detail":"some-error"}, "internals":{}}`,
			},
			messagesWithStacktrace: []int{0},
			expectedStatusCode:     http.StatusUnauthorized,
			expectedBody:           `[{"id":"1","status":"401","detail":"some-error"}]`,
		},
		{
			name: "public errors / print both to customer",
			requestHandler: func(t *testing.T) func(c *gin.Context) {
				return func(c *gin.Context) {
					c.Error(newErrorEntry(http.StatusForbidden, "some-initial-error", false))
					AbortWithError(c, http.StatusUnauthorized, fmt.Errorf("some-error"))
				}
			},
			expectedLogMessages: []string{
				`{"level":"error","request-id":"2","private-error":false,"error":"some-initial-error","error-object":{"id":"3","status":"403","detail":"some-initial-error"}}`,
				`{"level":"error","request-id":"2","private-error":false,"error":"some-error","error-object":{"id":"1","status":"401","detail":"some-error"}}`,
			},
			messagesWithStacktrace: []int{},
			expectedStatusCode:     http.StatusForbidden,
			expectedBody:           `[{"id":"3","status":"403","detail":"some-initial-error"},{"id":"1","status":"401","detail":"some-error"}]`,
		},
		{
			name: "public errors / hide stack trace from public errors",
			requestHandler: func(t *testing.T) func(c *gin.Context) {
				return func(c *gin.Context) {
					c.Error(newErrorEntry(http.StatusForbidden, "some-initial-error", true))
					AbortWithError(c, http.StatusUnauthorized, fmt.Errorf("some-error"))
				}
			},
			expectedLogMessages: []string{
				`{"level":"error","request-id":"2","private-error":false,"error":"some-initial-error","error-object":{"id":"3","status":"403","detail":"some-initial-error"}, "internals":{}}`,
				`{"level":"error","request-id":"2","private-error":false,"error":"some-error","error-object":{"id":"1","status":"401","detail":"some-error"}}`,
			},
			messagesWithStacktrace: []int{0},
			expectedStatusCode:     http.StatusForbidden,
			expectedBody:           `[{"id":"3","status":"403","detail":"some-initial-error"},{"id":"1","status":"401","detail":"some-error"}]`,
		},
		{
			name: "public errors / hide stack trace from multiple public errors",
			requestHandler: func(t *testing.T) func(c *gin.Context) {
				return func(c *gin.Context) {
					c.Error(newErrorEntry(http.StatusBadRequest, "some-bad-request-error", true))
					c.Error(newErrorEntry(http.StatusForbidden, "some-forbidden-error", false))
					c.Error(newErrorEntry(http.StatusUnauthorized, "some-unauthorized-error", true))
					c.Error(newErrorEntry(http.StatusInternalServerError, "some-internal-error", true))
					AbortWithError(c, http.StatusUnauthorized, fmt.Errorf("some-error"))
				}
			},
			expectedLogMessages: []string{
				`{"level":"error","request-id":"2","private-error":false,"error":"some-bad-request-error","error-object":{"id":"3","status":"400","detail":"some-bad-request-error"}, "internals":{}}`,
				`{"level":"error","request-id":"2","private-error":false,"error":"some-forbidden-error","error-object":{"id":"4","status":"403","detail":"some-forbidden-error"}}`,
				`{"level":"error","request-id":"2","private-error":false,"error":"some-unauthorized-error","error-object":{"id":"5","status":"401","detail":"some-unauthorized-error"}, "internals":{}}`,
				`{"level":"error","request-id":"2","private-error":false,"error":"some-error","error-object":{"id":"1","status":"401","detail":"some-error"}}`,
				`{"level":"error","request-id":"2","private-error":true,"error":"some-internal-error","error-object":{"id":"6","status":"500","detail":"some-internal-error"}, "internals":{}}`,
			},
			messagesWithStacktrace: []int{0, 2, 4},
			expectedStatusCode:     http.StatusBadRequest,
			expectedBody:           `[{"id":"3","status":"400","detail":"some-bad-request-error"},{"id":"4","status":"403","detail":"some-forbidden-error"},{"id":"5","status":"401","detail":"some-unauthorized-error"},{"id":"1","status":"401","detail":"some-error"}]`,
		},
		{
			name: "public errors / hide stack trace from multiple public errors; take status code from first error",
			requestHandler: func(t *testing.T) func(c *gin.Context) {
				return func(c *gin.Context) {
					c.Error(newErrorEntry(http.StatusForbidden, "some-forbidden-error", false))
					c.Error(newErrorEntry(http.StatusBadRequest, "some-bad-request-error", true))
					c.Error(newErrorEntry(http.StatusUnauthorized, "some-unauthorized-error", true))
					c.Error(newErrorEntry(http.StatusInternalServerError, "some-internal-error", true))
					AbortWithError(c, http.StatusUnauthorized, fmt.Errorf("some-error"))
				}
			},
			expectedLogMessages: []string{
				`{"level":"error","request-id":"2","private-error":false,"error":"some-forbidden-error","error-object":{"id":"3","status":"403","detail":"some-forbidden-error"}}`,
				`{"level":"error","request-id":"2","private-error":false,"error":"some-bad-request-error","error-object":{"id":"4","status":"400","detail":"some-bad-request-error"}, "internals":{}}`,
				`{"level":"error","request-id":"2","private-error":false,"error":"some-unauthorized-error","error-object":{"id":"5","status":"401","detail":"some-unauthorized-error"}, "internals":{}}`,
				`{"level":"error","request-id":"2","private-error":false,"error":"some-error","error-object":{"id":"1","status":"401","detail":"some-error"}}`,
				`{"level":"error","request-id":"2","private-error":true,"error":"some-internal-error","error-object":{"id":"6","status":"500","detail":"some-internal-error"}, "internals":{}}`,
			},
			messagesWithStacktrace: []int{1, 2, 4},
			expectedStatusCode:     http.StatusForbidden,
			expectedBody:           `[{"id":"3","status":"403","detail":"some-forbidden-error"},{"id":"4","status":"400","detail":"some-bad-request-error"},{"id":"5","status":"401","detail":"some-unauthorized-error"},{"id":"1","status":"401","detail":"some-error"}]`,
		},
		{
			name: "public errors / hide stack trace from multiple public errors; take status code from first error",
			requestHandler: func(t *testing.T) func(c *gin.Context) {
				return func(c *gin.Context) {
					c.Error(newErrorEntry(http.StatusForbidden, "some-forbidden-error", false))
					c.Error(newErrorEntry(http.StatusBadRequest, "some-bad-request-error", true))
					c.Error(newErrorEntry(http.StatusUnauthorized, "some-unauthorized-error", true))
					c.Error(newErrorEntry(http.StatusInternalServerError, "some-internal-error", true))
					AbortWithError(c, http.StatusUnauthorized, fmt.Errorf("some-error"))
				}
			},
			expectedLogMessages: []string{
				`{"level":"error","request-id":"2","private-error":false,"error":"some-forbidden-error","error-object":{"id":"3","status":"403","detail":"some-forbidden-error"}}`,
				`{"level":"error","request-id":"2","private-error":false,"error":"some-bad-request-error","error-object":{"id":"4","status":"400","detail":"some-bad-request-error"}, "internals":{}}`,
				`{"level":"error","request-id":"2","private-error":false,"error":"some-unauthorized-error","error-object":{"id":"5","status":"401","detail":"some-unauthorized-error"}, "internals":{}}`,
				`{"level":"error","request-id":"2","private-error":false,"error":"some-error","error-object":{"id":"1","status":"401","detail":"some-error"}}`,
				`{"level":"error","request-id":"2","private-error":true,"error":"some-internal-error","error-object":{"id":"6","status":"500","detail":"some-internal-error"}, "internals":{}}`,
			},
			messagesWithStacktrace: []int{1, 2, 4},
			expectedStatusCode:     http.StatusForbidden,
			expectedBody:           `[{"id":"3","status":"403","detail":"some-forbidden-error"},{"id":"4","status":"400","detail":"some-bad-request-error"},{"id":"5","status":"401","detail":"some-unauthorized-error"},{"id":"1","status":"401","detail":"some-error"}]`,
		},
		{
			name: "gin native errors",
			requestModifier: func(req *http.Request) {
				req.Body = io.NopCloser(bytes.NewReader([]byte(`""`)))
			},
			requestHandler: func(t *testing.T) func(c *gin.Context) {
				return func(c *gin.Context) {
					body := map[string]interface{}{}
					err := c.MustBindWith(&body, binding.JSON)
					assert.Error(t, err)
				}
			},
			expectedLogMessages: []string{
				`{"level":"error","request-id":"1","private-error":false,"error":"json: cannot unmarshal string into Go value of type map[string]interface {}","error-object":{"id":"2","status":"0","detail":"json: cannot unmarshal string into Go value of type map[string]interface {}"}}`,
			},
			messagesWithStacktrace: []int{},
			expectedStatusCode:     http.StatusBadRequest,
			expectedBody:           `[{"id":"2","status":"0","detail":"json: cannot unmarshal string into Go value of type map[string]interface {}"}]`,
		},
		{
			name: "gin native errors mixed in stuff",
			requestModifier: func(req *http.Request) {
				req.Body = io.NopCloser(bytes.NewReader([]byte(`""`)))
			},
			requestHandler: func(t *testing.T) func(c *gin.Context) {
				return func(c *gin.Context) {
					body := map[string]interface{}{}
					err := c.MustBindWith(&body, binding.JSON)

					c.Error(fmt.Errorf("some-panic-error"))
					AbortWithError(c, http.StatusAlreadyReported, fmt.Errorf("some-already-reported-error"))

					assert.Error(t, err)
				}
			},
			expectedLogMessages: []string{
				`{"level":"error","request-id":"2","private-error":false,"error":"json: cannot unmarshal string into Go value of type map[string]interface {}","error-object":{"id":"3","status":"0","detail":"json: cannot unmarshal string into Go value of type map[string]interface {}"}}`,
				`{"level":"error","request-id":"2","private-error":false,"error":"some-already-reported-error","error-object":{"id":"1","status":"208","detail":"some-already-reported-error"}}`,
				`{"level":"error","request-id":"2","private-error":true,"error":"some-panic-error","error-object":{"id":"4","status":"0","detail":"some-panic-error"}}`,
			},
			messagesWithStacktrace: []int{},
			expectedStatusCode:     http.StatusBadRequest,
			expectedBody:           `[{"id":"3","status":"0","detail":"json: cannot unmarshal string into Go value of type map[string]interface {}"},{"id":"1","status":"208","detail":"some-already-reported-error"}]`,
		},
		{
			name: "custom error with additional metadata",
			requestHandler: func(t *testing.T) func(c *gin.Context) {
				return func(c *gin.Context) {
					AbortWithError(c, http.StatusBadRequest, testMetadataError{})
				}
			},
			addPanicHandler: true,
			expectedLogMessages: []string{
				`{"level":"error","request-id":"2","private-error":false,"error":"error-with-meta","error-object":{"id":"1","status":"400","detail":"error-with-meta","meta":{"custom-meta":"some-info"}}}`,
			},
			messagesWithStacktrace: []int{1},
			expectedStatusCode:     http.StatusBadRequest,
			expectedBody:           `[{"id":"1","status":"400","detail":"error-with-meta","meta":{"custom-meta":"some-info"}}]`,
		},
		{
			name: "custom error with additional error code",
			requestHandler: func(t *testing.T) func(c *gin.Context) {
				return func(c *gin.Context) {
					AbortWithError(c, http.StatusBadRequest, testErrorCodeError{})
				}
			},
			addPanicHandler: true,
			expectedLogMessages: []string{
				`{"level":"error","request-id":"2","private-error":false,"error":"error-with-error-code","error-object":{"id":"1","status":"400","detail":"error-with-error-code","code":"custom-error-code"}}`,
			},
			messagesWithStacktrace: []int{1},
			expectedStatusCode:     http.StatusBadRequest,
			expectedBody:           `[{"id":"1","status":"400","detail":"error-with-error-code","code":"custom-error-code"}]`,
		},
		{
			name: "custom error with additional error code and meta",
			requestHandler: func(t *testing.T) func(c *gin.Context) {
				return func(c *gin.Context) {
					AbortWithError(c, http.StatusBadRequest, testErrorCodeAndMetaError{})
				}
			},
			addPanicHandler: true,
			expectedLogMessages: []string{
				`{"level":"error","request-id":"2","private-error":false,"error":"error-with-error-code-and-meta","error-object":{"id":"1","status":"400","detail":"error-with-error-code-and-meta","meta":{"custom-meta":"custom-meta-with-error-code"},"code":"custom-error-code-with-meta"}}`,
			},
			messagesWithStacktrace: []int{1},
			expectedStatusCode:     http.StatusBadRequest,
			expectedBody:           `[{"id":"1","status":"400","detail":"error-with-error-code-and-meta","meta":{"custom-meta":"custom-meta-with-error-code"},"code":"custom-error-code-with-meta"}]`,
		},
		{
			name: "panics / check for caught panic error with stack trace",
			requestHandler: func(t *testing.T) func(c *gin.Context) {
				return func(c *gin.Context) {
					panic(fmt.Errorf("some-panic-error"))
				}
			},
			addPanicHandler: true,
			expectedLogMessages: []string{
				`{"level":"error","request-id":"2","private-error":false,"error":"please refer to the id of this error when contacting support","error-object":{"id":"2","status":"500","detail":"please refer to the id of this error when contacting support"}}`,
				`{"level":"error","request-id":"2","private-error":true,"error":"caught panic in code (some-panic-error)","error-object":{"id":"1","status":"500","detail":"caught panic in code (some-panic-error)"}, "internals":{}}`,
			},
			messagesWithStacktrace: []int{1},
			expectedStatusCode:     http.StatusInternalServerError,
			expectedBody:           `[{"id":"2","status":"500","detail":"please refer to the id of this error when contacting support"}]`,
		},
		{
			name: "panics / check for caught panic string with stack trace",
			requestHandler: func(t *testing.T) func(c *gin.Context) {
				return func(c *gin.Context) {
					panic("some-panic-error")
				}
			},
			addPanicHandler: true,
			expectedLogMessages: []string{
				`{"level":"error","request-id":"2","private-error":false,"error":"please refer to the id of this error when contacting support","error-object":{"id":"2","status":"500","detail":"please refer to the id of this error when contacting support"}}`,
				`{"level":"error","request-id":"2","private-error":true,"error":"caught panic in code (some-panic-error)","error-object":{"id":"1","status":"500","detail":"caught panic in code (some-panic-error)"}, "internals":{}}`,
			},
			messagesWithStacktrace: []int{1},
			expectedStatusCode:     http.StatusInternalServerError,
			expectedBody:           `[{"id":"2","status":"500","detail":"please refer to the id of this error when contacting support"}]`,
		},
		{
			name: "panics / check for caught panic object with stack trace",
			requestHandler: func(t *testing.T) func(c *gin.Context) {
				return func(c *gin.Context) {
					panic(testPanicObject{})
				}
			},
			addPanicHandler: true,
			expectedLogMessages: []string{
				`{"level":"error","request-id":"2","private-error":false,"error":"please refer to the id of this error when contacting support","error-object":{"id":"2","status":"500","detail":"please refer to the id of this error when contacting support"}}`,
				`{"level":"error","request-id":"2","private-error":true,"error":"caught panic in code (some-weird-object)","error-object":{"id":"1","status":"500","detail":"caught panic in code (some-weird-object)"}, "internals":{}}`,
			},
			messagesWithStacktrace: []int{1},
			expectedStatusCode:     http.StatusInternalServerError,
			expectedBody:           `[{"id":"2","status":"500","detail":"please refer to the id of this error when contacting support"}]`,
		},
		{
			name: "panics / custom error with additional metadata",
			requestHandler: func(t *testing.T) func(c *gin.Context) {
				return func(c *gin.Context) {
					panic(testMetadataError{})
				}
			},
			addPanicHandler: true,
			expectedLogMessages: []string{
				`{"level":"error","request-id":"2","private-error":false,"error":"please refer to the id of this error when contacting support","error-object":{"id":"2","status":"500","detail":"please refer to the id of this error when contacting support"}}`,
				`{"level":"error","request-id":"2","private-error":true,"error":"caught panic in code (error-with-meta)","error-object":{"id":"1","status":"500","detail":"caught panic in code (error-with-meta)"}, "internals":{}}`,
			},
			messagesWithStacktrace: []int{1},
			expectedStatusCode:     http.StatusInternalServerError,
			expectedBody:           `[{"id":"2","status":"500","detail":"please refer to the id of this error when contacting support"}]`,
		},
		{
			name: "ErrorResponse",
			requestHandler: func(t *testing.T) func(c *gin.Context) {
				return func(c *gin.Context) {
					a := ErrorMessage{
						Id:     "A",
						Status: StatusCode(http.StatusNotFound),
						Detail: "some-error",
						Meta:   nil,
						Code:   nil,
					}
					b := ErrorMessage{
						Id:     "B",
						Status: StatusCode(http.StatusInternalServerError),
						Detail: "some-other-error",
						Meta:   nil,
						Code:   nil,
					}
					AbortWithError(c, http.StatusNotFound, &ErrorResponse{a, b})
				}
			},
			addPanicHandler: true,
			expectedLogMessages: []string{
				`{"level":"error","request-id":"1","private-error":false,"error":"some-error","error-object":{"id":"A","status":"404","detail":"some-error"}}`,
				`{"level":"error","request-id":"1","private-error":true,"error":"some-other-error","error-object":{"id":"B","status":"500","detail":"some-other-error"}}`,
			},
			messagesWithStacktrace: []int{},
			expectedStatusCode:     http.StatusNotFound,
			expectedBody:           `[{"id":"A","status":"404","detail":"some-error"}]`,
		},
		{
			name: "multiple errors",
			requestHandler: func(t *testing.T) func(c *gin.Context) {
				return func(c *gin.Context) {
					a := ErrorMessage{
						Id:     "A",
						Status: StatusCode(http.StatusNotFound),
						Detail: "some-error",
						Meta:   nil,
						Code:   nil,
					}
					b := ErrorMessage{
						Id:     "B",
						Status: StatusCode(http.StatusInternalServerError),
						Detail: "some-other-error",
						Meta:   nil,
						Code:   nil,
					}
					AbortWithError(c, http.StatusNotFound, &ErrorResponse{a, b}, errors.New("some-additional-error"))
				}
			},
			addPanicHandler: true,
			expectedLogMessages: []string{
				`{"level":"error","request-id":"2","private-error":false,"error":"some-error","error-object":{"id":"A","status":"404","detail":"some-error"}}`,
				`{"level":"error","request-id":"2","private-error":false,"error":"some-additional-error","error-object":{"id":"1","status":"404","detail":"some-additional-error"}}`,
				`{"level":"error","request-id":"2","private-error":true,"error":"some-other-error","error-object":{"id":"B","status":"500","detail":"some-other-error"}}`,
			},
			messagesWithStacktrace: []int{},
			expectedStatusCode:     http.StatusNotFound,
			expectedBody:           `[{"id":"A","status":"404","detail":"some-error"},{"id":"1","status":"404","detail":"some-additional-error"}]`,
		},
	}

	for _, test := range testList {

		runTest := func(suppressErrorPrinting bool) {
			name := test.name

			if suppressErrorPrinting {
				name += " (suppressed error printing)"
			}

			t.Run(name, func(t *testing.T) {
				var cnt uint32

				ctx := context.Background()
				ctx = withUniqueIdFactory(ctx, makeUniqIdFactory(&cnt))
				ctx, loggingTarget := setupLogging(t, ctx)

				recorder := tests.TestMiddleware(ctx, "GET", test.requestModifier, func(g *gin.Engine) {
					g.Use(ErrorHandlingMiddleware)
					if test.addPanicHandler {
						g.Use(PanicCatchingMiddleware)
					}

					if suppressErrorPrinting {
						g.Use(func(c *gin.Context) {
							DontWriteErrors(c)
						})
					}

					g.GET("", test.requestHandler(t))
				})

				expectedObjects := expectedLogObjects(t, test.expectedLogMessages...)

				loggedObjects := assertStackTrace(t, loggingTarget.WrittenBytes(), len(test.expectedLogMessages), test.messagesWithStacktrace...)

				assert.EqualValues(t, expectedObjects, loggedObjects)
				if !suppressErrorPrinting || test.expectedStatusCode == http.StatusOK {
					assert.EqualValues(t, test.expectedBody, recorder.Body.String())
				} else {
					assert.EqualValues(t, "", recorder.Body.String())
				}

				assert.EqualValues(t, test.expectedStatusCode, recorder.Code)
			})
		}

		runTest(false)
		runTest(true)

	}
}

type testMultiError []error

func (t testMultiError) Error() string {
	return ""
}

func (t testMultiError) WrappedErrors() []error {
	return t
}

func TestWrappedErrors(t *testing.T) {
	t.Run("simple empty", func(t *testing.T) {
		testee := testMultiError{}

		wrappedErrors := unwrapErrors(&testee)
		assert.Len(t, wrappedErrors, 0)
	})

	t.Run("simple wrapped", func(t *testing.T) {
		testee := testMultiError{fmt.Errorf("B"), fmt.Errorf("C")}

		wrappedErrors := unwrapErrors(&testee)
		assert.Len(t, wrappedErrors, 2)
		assert.EqualError(t, wrappedErrors[0], "B")
		assert.EqualError(t, wrappedErrors[1], "C")
	})

	t.Run("multiple simple wrapped", func(t *testing.T) {
		testeeA := testMultiError{fmt.Errorf("B"), fmt.Errorf("C")}
		testeeB := testMultiError{fmt.Errorf("D"), fmt.Errorf("E")}

		wrappedErrors := unwrapErrors(&testeeA, &testeeB)
		assert.Len(t, wrappedErrors, 4)
		assert.EqualError(t, wrappedErrors[0], "B")
		assert.EqualError(t, wrappedErrors[1], "C")
		assert.EqualError(t, wrappedErrors[2], "D")
		assert.EqualError(t, wrappedErrors[3], "E")
	})

	t.Run("multiple nested wrapped", func(t *testing.T) {
		testeeA := testMultiError{fmt.Errorf("B"), fmt.Errorf("C")}
		testeeB := testMultiError{fmt.Errorf("D"), fmt.Errorf("E")}
		testeeC := testMultiError{fmt.Errorf("F"), testeeB}

		wrappedErrors := unwrapErrors(&testeeA, &testeeC)
		assert.Len(t, wrappedErrors, 5)
		assert.EqualError(t, wrappedErrors[0], "B")
		assert.EqualError(t, wrappedErrors[1], "C")
		assert.EqualError(t, wrappedErrors[2], "F")
		assert.EqualError(t, wrappedErrors[3], "D")
		assert.EqualError(t, wrappedErrors[4], "E")
	})
}
