package errorHandling

import (
	"encoding/json"
	"fmt"
	"strconv"
)

type StatusCode int

const NoStatusCode = StatusCode(0)

func (s StatusCode) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf("\"%d\"", int(s))), nil
}

func (s *StatusCode) UnmarshalJSON(bytes []byte) error {

	var tmp string

	if err := json.Unmarshal(bytes, &tmp); err != nil {
		return fmt.Errorf("unable to unmarshal 'string' (%w)", err)
	}

	if len(tmp) == 0 {
		*s = NoStatusCode
		return nil
	}

	iConv, err := strconv.Atoi(tmp)

	if err != nil {
		return fmt.Errorf("could not parse %q to int (%w)", tmp, err)
	}

	*s = StatusCode(iConv)
	return nil
}
