package errorHandling

import "strings"

// ErrorMessage is a representation of an error based on https://jsonapi.org/format/#errors
type ErrorMessage struct {
	Id     string                 `json:"id"`             //A unique error id
	Status StatusCode             `json:"status"`         //The HTTP status which can result from this error
	Detail string                 `json:"detail"`         //A message containing a detailed description
	Meta   map[string]interface{} `json:"meta,omitempty"` //A meta object containing non-standard meta-information about the error.
	Code   *string                `json:"code,omitempty"` //An application-specific error code, expressed as a string value.
}

func (e *ErrorMessage) Error() string { return e.Detail }

type ErrorResponse []ErrorMessage

func (e ErrorResponse) WrappedErrors() []error {
	result := make([]error, len(e), len(e))
	for i := 0; i < len(e); i++ {
		result[i] = &e[i]
	}
	return result
}

func (e ErrorResponse) Error() string {

	messages := make([]string, len(e), len(e))

	for i := 0; i < len(e); i++ {
		messages[i] = e[i].Error()
	}

	return strings.Join(messages, "; ")
}
