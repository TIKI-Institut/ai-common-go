package errorHandling

import (
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

func TestErrorResponse(t *testing.T) {

	t.Run("WrappedErrors", func(t *testing.T) {

		a := ErrorMessage{
			Id:     "A",
			Status: StatusCode(http.StatusBadRequest),
			Detail: "some-error",
			Meta:   nil,
			Code:   nil,
		}
		b := ErrorMessage{
			Id:     "B",
			Status: StatusCode(http.StatusNotFound),
			Detail: "some-other-error",
			Meta:   nil,
			Code:   nil,
		}

		testee := ErrorResponse{
			a,
			b,
		}

		wrappedErrors := testee.WrappedErrors()
		assert.Len(t, wrappedErrors, 2)
		assert.EqualValues(t, wrappedErrors[0], &a)
		assert.EqualValues(t, wrappedErrors[1], &b)

	})

}
