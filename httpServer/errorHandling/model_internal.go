package errorHandling

import (
	"errors"
	"runtime/debug"
)

const internalsEntryStacktrace = "stack-trace"
const internalsEntryGinErrorMeta = "gin-error-meta"

// errorEntry is a representation of an error based on https://jsonapi.org/format/#errors
type errorEntry struct {
	ErrorMessage

	originalError error
	internals     map[string]interface{}
}

func (e *errorEntry) Error() string { return e.Detail }
func (e *errorEntry) appendStackTrace() {
	if e.internals == nil {
		e.internals = map[string]interface{}{}
	}

	e.internals[internalsEntryStacktrace] = string(debug.Stack())
}

func newErrorEntry(httpStatus int, details string, appendStackTrace bool) error {
	result := errorEntry{
		ErrorMessage: ErrorMessage{
			Status: StatusCode(httpStatus),
			Detail: details,
			Meta:   nil,
		},
		internals:     nil,
		originalError: errors.New(details),
	}

	if appendStackTrace {
		result.internals = map[string]interface{}{internalsEntryStacktrace: string(debug.Stack())}
	}

	return &result
}
