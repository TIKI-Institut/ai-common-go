package errorHandling

type MultiError interface {
	error
	WrappedErrors() []error
}
