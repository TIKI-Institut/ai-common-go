package errorHandling

import (
	"bytes"
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

func TestStatusCode(t *testing.T) {

	t.Run("test json serialization", func(t *testing.T) {

		t.Run("no code", func(t *testing.T) {
			testee := NoStatusCode

			buf := bytes.Buffer{}
			enc := json.NewEncoder(&buf)
			assert.NoError(t, enc.Encode(testee))
			assert.EqualValues(t, "\"0\"\n", buf.String())
		})

		t.Run("normal", func(t *testing.T) {
			testee := StatusCode(404)

			buf := bytes.Buffer{}
			enc := json.NewEncoder(&buf)
			assert.NoError(t, enc.Encode(testee))
			assert.EqualValues(t, "\"404\"\n", buf.String())
		})

	})

	t.Run("test json deserialization", func(t *testing.T) {

		t.Run("normal", func(t *testing.T) {
			testee := StatusCode(999)
			dec := json.NewDecoder(bytes.NewBufferString("\"401\""))

			assert.NoError(t, dec.Decode(&testee))
			assert.EqualValues(t, StatusCode(http.StatusUnauthorized), testee)
		})

		t.Run("empty string", func(t *testing.T) {
			testee := StatusCode(999)
			dec := json.NewDecoder(bytes.NewBufferString("\"\""))

			assert.NoError(t, dec.Decode(&testee))
			assert.EqualValues(t, NoStatusCode, testee)
		})

		t.Run("wrong type", func(t *testing.T) {
			testee := StatusCode(999)
			dec := json.NewDecoder(bytes.NewBufferString("[]"))

			assert.EqualError(t, dec.Decode(&testee), "unable to unmarshal 'string' (json: cannot unmarshal array into Go value of type string)")
			assert.EqualValues(t, StatusCode(999), testee)
		})

		t.Run("no int", func(t *testing.T) {
			testee := StatusCode(999)
			dec := json.NewDecoder(bytes.NewBufferString("\"some-string\""))

			assert.EqualError(t, dec.Decode(&testee), "could not parse \"some-string\" to int (strconv.Atoi: parsing \"some-string\": invalid syntax)")
			assert.EqualValues(t, StatusCode(999), testee)
		})

	})

}
