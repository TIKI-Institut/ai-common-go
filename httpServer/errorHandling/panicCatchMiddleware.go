package errorHandling

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

type caughtPanicError struct {
	Cause error
}

func (p *caughtPanicError) StatusCode() int    { return http.StatusInternalServerError }
func (p *caughtPanicError) Error() string      { return fmt.Sprintf("caught panic in code (%s)", p.Cause.Error()) }
func (p *caughtPanicError) Unwrap() error      { return p.Cause }

func PanicCatchingMiddleware(gCtx *gin.Context) {

	defer func() {
		if r := recover(); r != nil {
			var err error
			switch r.(type) {
			case error:
				err = r.(error)
			case string:
				err = fmt.Errorf(r.(string))
			default :
				err = fmt.Errorf("%v", r)
			}

			if err != nil {
				AbortWithErrorAndStacktrace(gCtx, http.StatusInternalServerError, &caughtPanicError{Cause: err})
			}
		}
	}()

	gCtx.Next()

}


