package logging

import (
	"github.com/gin-gonic/gin"
	"github.com/lithammer/shortuuid"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"io"
	"time"
)
import ginZeroLog "github.com/gin-contrib/logger"
import ginRequestId "github.com/gin-contrib/requestid"

var zerologRequestLoggerkey = "_zerolog_logger"

// SetupZerolog assumes that a zerolog logger is attached to the application context <g.Request.Context()>
// It reconfigures the gin router to log using this logger
// For this a request id gets injected into the current request
// Logger will be set to Error level per default, use SetupZerologWithLevel to change this
func SetupZerolog(g gin.IRoutes) {
	SetupZerologWithLevel(g, zerolog.ErrorLevel)
}

// SetupZerologWithLevel assumes that a zerolog logger is attached to the application context <g.Request.Context()>
// It reconfigures the gin router to log using this logger
// For this a request id gets injected into the current request
func SetupZerologWithLevel(g gin.IRoutes, lvl zerolog.Level) {
	g.Use(ginRequestId.New(ginRequestId.Config{Generator: shortuuid.New}))
	//any logs from this request shall contain the request-id
	g.Use(func(c *gin.Context) {
		logger := log.Ctx(c.Request.Context()).With().Str("request-id", ginRequestId.Get(c)).Logger()
		c.Set(zerologRequestLoggerkey, &logger)
	})
	//attach zerolog to gin logger ....
	g.Use(ginZeroLog.SetLogger(ginZeroLog.WithLogger(func(c *gin.Context, _ io.Writer, latency time.Duration) zerolog.Logger {
		return log.Ctx(c.Request.Context()).Level(lvl).With().
			Str("request-id", ginRequestId.Get(c)).
			Int("status", c.Writer.Status()).
			Str("method", c.Request.Method).
			Str("path", c.Request.URL.Path).
			Str("ip", c.ClientIP()).
			Dur("latency", latency).
			Str("user_agent", c.Request.UserAgent()).Logger()
	})))
}

func ZerologLogger(c *gin.Context) *zerolog.Logger {
	if logger := c.Value(zerologRequestLoggerkey); logger != nil {
		return logger.(*zerolog.Logger)
	}
	return zerolog.Ctx(c.Request.Context())
}
