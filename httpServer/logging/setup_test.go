package logging

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/tests"
	"bytes"
	"context"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

func TestSetupZerologWithInfoLevel(t *testing.T) {

	zerologBufferLogger := bytes.Buffer{}

	ctx := context.Background()
	localLogger := zerolog.New(&zerologBufferLogger)
	ctx = localLogger.WithContext(ctx)

	recorder := tests.TestMiddleware(ctx, "GET", nil, func(g *gin.Engine) {
		SetupZerologWithLevel(g, zerolog.InfoLevel)
		g.GET("", func(c *gin.Context) {
			c.String(http.StatusOK, "OK")
		})
	})

	assert.EqualValues(t, http.StatusOK, recorder.Code)

	loggingObject := map[string]interface{}{}
	assert.NoError(t, json.Unmarshal(zerologBufferLogger.Bytes(), &loggingObject))
	expectedLoggingFields := []string{"status", "method", "path", "ip", "latency", "user_agent", "request-id"}

	for _, field := range expectedLoggingFields {
		_, found := loggingObject[field]
		assert.Truef(t, found, "field %s was not found in request logging", field)
	}
}

func TestSetupZerologB(t *testing.T) {

	zerologBufferLogger := bytes.Buffer{}

	ctx := context.Background()
	localLogger := zerolog.New(&zerologBufferLogger)
	ctx = localLogger.WithContext(ctx)

	recorder := tests.TestMiddleware(ctx, "GET", nil, func(g *gin.Engine) {
		SetupZerolog(g)
		g.GET("", func(c *gin.Context) {
			c.String(http.StatusInternalServerError, "Internal Server Error")
		})
	})

	assert.EqualValues(t, http.StatusInternalServerError, recorder.Code)

	loggingObject := map[string]interface{}{}
	assert.NoError(t, json.Unmarshal(zerologBufferLogger.Bytes(), &loggingObject))
	expectedLoggingFields := []string{"status", "method", "path", "ip", "latency", "user_agent", "request-id"}

	for _, field := range expectedLoggingFields {
		_, found := loggingObject[field]
		assert.Truef(t, found, "field %s was not found in request logging", field)
	}
}
