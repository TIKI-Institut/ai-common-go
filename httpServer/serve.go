package httpServer

import (
	"context"
	"net"
	"net/http"
	"time"
)

func ServeWithContext(ctx context.Context, addr string, handler http.Handler, shutdownGracePeriod time.Duration) error {
	server := &http.Server{
		Addr: addr,
		Handler: handler,
		BaseContext: func(_ net.Listener) context.Context {return ctx},
	}

	go func() {
		select {
		case <- ctx.Done():
			//apply grace period of 1 second
			shutdownContext, shutdownCancel := context.WithDeadline(context.Background(), time.Now().Add(shutdownGracePeriod))
			server.Shutdown(shutdownContext)
			shutdownCancel()
			break

		}
	}()

	err := server.ListenAndServe()
	if err != nil && err != http.ErrServerClosed {
		return err
	}
	return nil
}