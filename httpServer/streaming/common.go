package streaming

import "github.com/gin-gonic/gin"

func setStreamingHeaders(gCtx *gin.Context, contentType string) {

	gCtx.Writer.Header().Set("Content-Type", contentType)
	gCtx.Writer.Header().Set("Cache-Control", "no-cache")
	gCtx.Writer.Header().Set("Connection", "keep-alive")
	gCtx.Writer.Header().Set("Transfer-Encoding", "chunked")

}
