package streaming

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/httpServer/errorHandling"
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"io"
	"net/http"
)

var jsonSeqContentType = "application/json-seq"

/*
JsonSequenceStreamingMiddleware modifies the request to be an 'streaming' request.
On an error the middleware prevents the error handling middleware to render additional error messages to the response as
streaming is a special use case.

This code follows IETF RFC 7464.
See : https://datatracker.ietf.org/doc/html/rfc7464
*/
func JsonSequenceStreamingMiddleware(gCtx *gin.Context) {

	setStreamingHeaders(gCtx, jsonSeqContentType)

	//do not write any errors to the client
	errorHandling.DontWriteErrors(gCtx)
}

const RecordSeparator byte = 0x1E
const LineFeed byte = 0x0A

type jsonSequenceEntryRender struct {
	target interface{}
}

func (j *jsonSequenceEntryRender) Render(writer http.ResponseWriter) error {
	j.WriteContentType(writer)

	transportBuffer := bytes.Buffer{}
	enc := json.NewEncoder(&transportBuffer)

	_, err := transportBuffer.Write([]byte{RecordSeparator})
	if err != nil {
		return fmt.Errorf("failed to write record separator (%w)", err)
	}
	err = enc.Encode(j.target)
	if err != nil {
		return fmt.Errorf("failed to enoce object (%w)", err)
	}
	_, err = transportBuffer.Write([]byte{LineFeed})
	if err != nil {
		return fmt.Errorf("failed to write line feed (%w)", err)
	}

	_, err = io.Copy(writer, &transportBuffer)

	return err
}

func (j *jsonSequenceEntryRender) WriteContentType(w http.ResponseWriter) {
	header := w.Header()
	if val := header["Content-Type"]; len(val) == 0 {
		header["Content-Type"] = []string{jsonSeqContentType}
	}
}

func RenderJsonSeqEntry(gCtx *gin.Context, target interface{}) {
	gCtx.Render(-1, &jsonSequenceEntryRender{target: target})
}
