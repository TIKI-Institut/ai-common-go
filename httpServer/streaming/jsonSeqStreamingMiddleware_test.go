package streaming

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/httpServer/errorHandling"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/httpServer/logging"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/tests"
	"context"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestJsonSequenceStreamingMiddleware(t *testing.T) {

	checkHeaders := func(t *testing.T, recorder *httptest.ResponseRecorder) {
		assert.EqualValues(t, "application/json-seq", recorder.Header().Get("Content-Type"))
		assert.EqualValues(t, "no-cache", recorder.Header().Get("Cache-Control"))
		assert.EqualValues(t, "keep-alive", recorder.Header().Get("Connection"))
		assert.EqualValues(t, "chunked", recorder.Header().Get("Transfer-Encoding"))
	}

	t.Run("clean", func(t *testing.T) {
		ctx := context.Background()
		recorder := tests.TestMiddleware(ctx, "GET", nil, func(g *gin.Engine) {

			//setup error handling
			logging.SetupZerolog(g)
			g.Use(errorHandling.ErrorHandlingMiddleware)
			g.Use(errorHandling.PanicCatchingMiddleware)

			g.Use(JsonSequenceStreamingMiddleware)
			g.GET("/", func(c *gin.Context) {
				c.AbortWithStatus(http.StatusOK)
			})
		})

		assert.Equal(t, http.StatusOK, recorder.Code)
		assert.Equal(t, "", recorder.Body.String())
		checkHeaders(t, recorder)
	})

	t.Run("custom status", func(t *testing.T) {
		ctx := context.Background()
		recorder := tests.TestMiddleware(ctx, "GET", nil, func(g *gin.Engine) {

			//setup error handling
			logging.SetupZerolog(g)
			g.Use(errorHandling.ErrorHandlingMiddleware)
			g.Use(errorHandling.PanicCatchingMiddleware)

			g.Use(JsonSequenceStreamingMiddleware)
			g.GET("/", func(c *gin.Context) {
				c.AbortWithStatus(http.StatusGone)
			})
		})

		assert.Equal(t, http.StatusGone, recorder.Code)
		assert.Equal(t, "", recorder.Body.String())
		checkHeaders(t, recorder)
	})

	t.Run("abort with error (native gin)", func(t *testing.T) {
		ctx := context.Background()
		recorder := tests.TestMiddleware(ctx, "GET", nil, func(g *gin.Engine) {

			//setup error handling
			logging.SetupZerolog(g)
			g.Use(errorHandling.ErrorHandlingMiddleware)
			g.Use(errorHandling.PanicCatchingMiddleware)

			g.Use(JsonSequenceStreamingMiddleware)
			g.GET("/", func(c *gin.Context) {
				c.AbortWithError(http.StatusGone, errors.New("some-error"))
			})
		})

		assert.Equal(t, http.StatusGone, recorder.Code)
		assert.Equal(t, "", recorder.Body.String())
		checkHeaders(t, recorder)
	})

	t.Run("abort with error (custom error handling)", func(t *testing.T) {
		ctx := context.Background()
		recorder := tests.TestMiddleware(ctx, "GET", nil, func(g *gin.Engine) {

			//setup error handling
			logging.SetupZerolog(g)
			g.Use(errorHandling.ErrorHandlingMiddleware)
			g.Use(errorHandling.PanicCatchingMiddleware)

			g.Use(JsonSequenceStreamingMiddleware)
			g.GET("/", func(c *gin.Context) {
				errorHandling.AbortWithError(c, http.StatusGone, errors.New("some-error"))
			})
		})

		assert.Equal(t, http.StatusGone, recorder.Code)
		assert.Equal(t, "", recorder.Body.String())
		checkHeaders(t, recorder)
	})

	t.Run("hard panic", func(t *testing.T) {
		ctx := context.Background()
		recorder := tests.TestMiddleware(ctx, "GET", nil, func(g *gin.Engine) {

			//setup error handling
			logging.SetupZerolog(g)
			g.Use(errorHandling.ErrorHandlingMiddleware)
			g.Use(errorHandling.PanicCatchingMiddleware)

			g.Use(JsonSequenceStreamingMiddleware)
			g.GET("/", func(c *gin.Context) {
				panic(errors.New("some-panic-error"))
			})
		})

		assert.Equal(t, http.StatusInternalServerError, recorder.Code)
		assert.Equal(t, "", recorder.Body.String())
		checkHeaders(t, recorder)
	})

	t.Run("hard panic after rendering first element", func(t *testing.T) {
		ctx := context.Background()
		recorder := tests.TestMiddleware(ctx, "GET", nil, func(g *gin.Engine) {

			//setup error handling
			logging.SetupZerolog(g)
			g.Use(errorHandling.ErrorHandlingMiddleware)
			g.Use(errorHandling.PanicCatchingMiddleware)

			g.Use(JsonSequenceStreamingMiddleware)
			g.GET("/", func(c *gin.Context) {
				RenderJsonSeqEntry(c, gin.H{})
				panic(errors.New("some-panic-error"))
			})
		})

		assert.Equal(t, http.StatusOK, recorder.Code)
		assert.Equal(t, "\x1e{}\n\n", recorder.Body.String())
		checkHeaders(t, recorder)
	})

	t.Run("abort (custom) after rendering first element", func(t *testing.T) {
		ctx := context.Background()
		recorder := tests.TestMiddleware(ctx, "GET", nil, func(g *gin.Engine) {

			//setup error handling
			logging.SetupZerolog(g)
			g.Use(errorHandling.ErrorHandlingMiddleware)
			g.Use(errorHandling.PanicCatchingMiddleware)

			g.Use(JsonSequenceStreamingMiddleware)
			g.GET("/", func(c *gin.Context) {
				RenderJsonSeqEntry(c, gin.H{})
				errorHandling.AbortWithError(c, http.StatusUnauthorized, errors.New("some-panic-error"))
			})
		})

		assert.Equal(t, http.StatusOK, recorder.Code)
		assert.Equal(t, "\x1e{}\n\n", recorder.Body.String())
		checkHeaders(t, recorder)
	})

	t.Run("abort (native) after rendering first element", func(t *testing.T) {
		ctx := context.Background()
		recorder := tests.TestMiddleware(ctx, "GET", nil, func(g *gin.Engine) {

			//setup error handling
			logging.SetupZerolog(g)
			g.Use(errorHandling.ErrorHandlingMiddleware)
			g.Use(errorHandling.PanicCatchingMiddleware)

			g.Use(JsonSequenceStreamingMiddleware)
			g.GET("/", func(c *gin.Context) {
				RenderJsonSeqEntry(c, gin.H{})
				c.AbortWithError(http.StatusUnauthorized, errors.New("some-panic-error"))
			})
		})

		assert.Equal(t, http.StatusOK, recorder.Code)
		assert.Equal(t, "\x1e{}\n\n", recorder.Body.String())
		checkHeaders(t, recorder)
	})

	t.Run("send multiple elements", func(t *testing.T) {
		ctx := context.Background()
		recorder := tests.TestMiddleware(ctx, "GET", nil, func(g *gin.Engine) {

			//setup error handling
			logging.SetupZerolog(g)
			g.Use(errorHandling.ErrorHandlingMiddleware)
			g.Use(errorHandling.PanicCatchingMiddleware)

			g.Use(JsonSequenceStreamingMiddleware)
			g.GET("/", func(c *gin.Context) {
				RenderJsonSeqEntry(c, gin.H{})
				RenderJsonSeqEntry(c, gin.H{"foo": "bar"})
				RenderJsonSeqEntry(c, gin.H{"peter": "peter"})
			})
		})

		assert.Equal(t, http.StatusOK, recorder.Code)
		assert.Equal(t, "\x1E{}\n\n\x1E{\"foo\":\"bar\"}\n\n\x1E{\"peter\":\"peter\"}\n\n", recorder.Body.String())
		checkHeaders(t, recorder)
	})
}
