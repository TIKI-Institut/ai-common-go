package streaming

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/httpServer/errorHandling"
	"github.com/gin-gonic/gin"
)

func SetupServerSentEventsMiddleware(gCtx *gin.Context) {

	setStreamingHeaders(gCtx, "text/event-stream")

	//do not write any errors to the client
	errorHandling.DontWriteErrors(gCtx)

}
