package streaming

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/httpServer/errorHandling"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/httpServer/logging"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/tests"
	"context"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

func TestSetupServerSentEventsMiddleware(t *testing.T) {

	ctx := context.Background()
	recorder := tests.TestMiddleware(ctx, "GET", nil, func(g *gin.Engine) {

		//setup error handling
		logging.SetupZerolog(g)
		g.Use(errorHandling.ErrorHandlingMiddleware)
		g.Use(errorHandling.PanicCatchingMiddleware)

		g.Use(SetupServerSentEventsMiddleware)
		g.GET("/", func(c *gin.Context) {
			c.AbortWithStatus(http.StatusOK)
		})
	})

	assert.Equal(t, http.StatusOK, recorder.Code)
	assert.Equal(t, "", recorder.Body.String())
	assert.EqualValues(t, "text/event-stream", recorder.Header().Get("Content-Type"))
	assert.EqualValues(t, "no-cache", recorder.Header().Get("Cache-Control"))
	assert.EqualValues(t, "keep-alive", recorder.Header().Get("Connection"))
	assert.EqualValues(t, "chunked", recorder.Header().Get("Transfer-Encoding"))
}
