package kubernetes

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/common"
	k8sApiContext "bitbucket.org/TIKI-Institut/ai-common-go/v2/kubernetes/context"
	"context"
	"fmt"
	"k8s.io/apimachinery/pkg/api/meta"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/kubernetes/scheme"
	"reflect"
	"time"
)

const DspChangedByAnnotation = "changed-by"
const DspChangeReasonAnnotation = "change-reason"
const DspChangeDateAnnotation = "change-date"

type Object interface {
	metaV1.Object
	runtime.Object
}

func NewK8sObject[TObj Object]() TObj {
	// type constraint enforces type parameter to be a pointer to a type e.g. *v1.Pod.
	// To get the actual type for reflect.New we must therefore call Elem twice.
	resourceType := reflect.TypeOf((*TObj)(nil)).Elem().Elem()
	return reflect.New(resourceType).Interface().(TObj)
}

// The "data" argument is always a json object!
func patchHasChanges(data []byte) bool {
	numBytes := len(data)
	if numBytes > 2 {
		return true
	}

	if numBytes < 2 {
		panic(fmt.Errorf("invalid patch"))
	}

	return data[0] != '{' && data[1] != '}'
}

func GroupVersionKindForObject(obj runtime.Object) (schema.GroupVersionKind, error) {
	// 1. Resolve GVK of object
	objectGVK := obj.GetObjectKind().GroupVersionKind()

	if objectGVK.Empty() {
		kinds, _, err := scheme.Scheme.ObjectKinds(obj)
		if err != nil {
			return schema.GroupVersionKind{}, fmt.Errorf("unable to resolve GroupVersionKind (%w)", err)
		}

		objectGVK = kinds[0]
	}

	return objectGVK, nil
}

func ClientForObject(ctx context.Context, obj runtime.Object, namespace string) (dynamic.ResourceInterface, schema.GroupVersionKind, bool, error) {

	objectGVK, err := GroupVersionKindForObject(obj)

	if err != nil {
		return nil, objectGVK, false, err
	}

	client, isNamespaceScoped, err := Client(ctx, objectGVK, namespace)
	return client, objectGVK, isNamespaceScoped, err
}

func Client(ctx context.Context, gvk schema.GroupVersionKind, namespace string) (dynamic.ResourceInterface, bool, error) {

	if restMapper, err := k8sApiContext.RestMapperFromContext(ctx); err != nil {
		return nil, false, err
	} else if dynClient, err := k8sApiContext.DynamicClientFromContext(ctx); err != nil {
		return nil, false, err
	} else {
		// 2. Find GVR
		mapping, err := restMapper.RESTMapping(gvk.GroupKind(), gvk.Version)
		if err != nil {
			return nil, false, fmt.Errorf("unable to create rest mapping (%w)", err)
		}

		// 3. Obtain REST interface for the GVR
		if mapping.Scope.Name() == meta.RESTScopeNameNamespace {
			// namespaced resources should specify the namespace
			return dynClient.Resource(mapping.Resource).Namespace(namespace), true, nil
		}
		// for cluster-wide resources
		return dynClient.Resource(mapping.Resource), false, nil
	}
}

func annotate(ctx context.Context, obj unstructured.Unstructured, changedBy string, changeReason string) unstructured.Unstructured {
	if len(changedBy) > 0 || len(changeReason) > 0 {
		annotations := obj.GetAnnotations()

		if annotations == nil {
			annotations = map[string]string{}
		}

		if len(changedBy) > 0 {
			annotations[DspLabel(DspChangedByAnnotation)] = changedBy
		}

		if len(changeReason) > 0 {
			annotations[DspLabel(DspChangeReasonAnnotation)] = changeReason
		}

		annotations[DspLabel(DspChangeDateAnnotation)] = common.NowTime(ctx).Format(time.RFC3339)

		if len(annotations) > 0 {
			obj.SetAnnotations(annotations)
		}
	}

	return obj
}
