package kubernetes

import (
	"context"
	"fmt"
	"k8s.io/apimachinery/pkg/api/errors"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	k8sJson "k8s.io/apimachinery/pkg/util/json"
	"k8s.io/apimachinery/pkg/util/strategicpatch"
	"k8s.io/client-go/kubernetes/scheme"
)

func cleanReadOnlyObjectMetaFields(meta metaV1.Object) {
	meta.SetSelfLink("")
	meta.SetUID("")
	meta.SetResourceVersion("")
	meta.SetGeneration(0)
	meta.SetCreationTimestamp(metaV1.Time{})
	meta.SetDeletionTimestamp(nil)
	meta.SetDeletionGracePeriodSeconds(nil)
	meta.SetManagedFields(nil)
}

func Apply(ctx context.Context, obj Object, changedBy, changeReason string, forceOverwrite bool) (runtime.Object, error) {

	client, objectGVK, _, err := ClientForObject(ctx, obj, obj.GetNamespace())
	if err != nil {
		return nil, err
	}

	unstructuredTarget := unstructured.Unstructured{}
	err = scheme.Scheme.Convert(obj, &unstructuredTarget, nil)
	//unstructuredTarget.Object, err = runtime.DefaultUnstructuredConverter.ToUnstructured(target)
	if err != nil {
		return nil, err
	}

	unstructuredTarget = annotate(ctx, unstructuredTarget, changedBy, changeReason)

	var serverModifiedObject runtime.Object

	originalFromServer, err := client.Get(ctx, unstructuredTarget.GetName(), metaV1.GetOptions{})
	if err != nil {
		if !errors.IsNotFound(err) {
			return nil, err
		}

		cleanReadOnlyObjectMetaFields(&unstructuredTarget)
		if serverModifiedObject, err = client.Create(ctx, &unstructuredTarget, metaV1.CreateOptions{}); err != nil {
			return nil, err
		}
	} else {
		container, err := scheme.Scheme.New(objectGVK)
		if err != nil {
			return nil, err
		}

		originalFromServerBytes, err := k8sJson.Marshal(originalFromServer)
		if err != nil {
			return nil, err
		}

		// 1. copy opaque fields
		unstructuredTarget.SetSelfLink(originalFromServer.GetSelfLink())
		unstructuredTarget.SetUID(originalFromServer.GetUID())
		unstructuredTarget.SetResourceVersion(originalFromServer.GetResourceVersion())
		unstructuredTarget.SetGeneration(originalFromServer.GetGeneration())
		unstructuredTarget.SetCreationTimestamp(originalFromServer.GetCreationTimestamp())
		unstructuredTarget.SetManagedFields(originalFromServer.GetManagedFields())

		modifiedBytes, err := k8sJson.Marshal(&unstructuredTarget)
		if err != nil {
			return nil, err
		}

		// 2. Create two-way patch for strategic merging
		patch, err := strategicpatch.CreateTwoWayMergePatch(originalFromServerBytes, modifiedBytes, container)
		if err != nil {
			return nil, err
		}

		// 3. Skip any api call if this patch will not change the resource
		if patchHasChanges(patch) {
			// 4. Create or Update the object with SSA
			//     types.ApplyPatchType indicates SSA.
			//     FieldManager specifies the field owner ID.
			serverModifiedObject, err = client.Patch(ctx, unstructuredTarget.GetName(), types.StrategicMergePatchType, patch, metaV1.PatchOptions{FieldManager: changedBy})

			if err != nil {
				if forceOverwrite {
					if err := DeleteObject(ctx, originalFromServer); err != nil {
						return nil, err
					}

					cleanReadOnlyObjectMetaFields(&unstructuredTarget)
					if serverModifiedObject, err = client.Create(ctx, &unstructuredTarget, metaV1.CreateOptions{}); err != nil {
						return nil, err
					}
				} else {
					return nil, err
				}
			}
		} else {
			serverModifiedObject = originalFromServer
		}
	}

	structuredResult, err := scheme.Scheme.New(objectGVK)
	if err != nil {
		return nil, err
	}

	err = scheme.Scheme.Convert(serverModifiedObject, structuredResult, nil)
	if err != nil {
		return nil, fmt.Errorf("unable to convert structured object to unstructured object (%w)", err)
	}

	return structuredResult, nil
}
