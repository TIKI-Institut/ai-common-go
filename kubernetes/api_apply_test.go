package kubernetes

import (
	commonFake "bitbucket.org/TIKI-Institut/ai-common-go/v2/common/fake"
	k8sTesting "bitbucket.org/TIKI-Institut/ai-common-go/v2/kubernetes/testing"
	"context"
	"github.com/stretchr/testify/assert"
	appsV1 "k8s.io/api/apps/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/utils/pointer"
	"testing"
	"time"
)

func TestCleanReadOnlyObjectMetaFields(t *testing.T) {

	t.Run("structured", func(t *testing.T) {
		now := metaV1.Now()
		objectMeta := metaV1.ObjectMeta{
			Name:                       "Name",
			GenerateName:               "GenerateName",
			Namespace:                  "Namespace",
			SelfLink:                   "SelfLink",
			UID:                        "UID",
			ResourceVersion:            "ResourceVersion",
			Generation:                 15,
			CreationTimestamp:          metaV1.Now(),
			DeletionTimestamp:          &now,
			DeletionGracePeriodSeconds: pointer.Int64(16),
			Labels:                     map[string]string{"L1": "V1"},
			Annotations:                map[string]string{"A1": "V1"},
			OwnerReferences:            []metaV1.OwnerReference{{APIVersion: "APIVersion", Kind: "Kind", Name: "Name"}},
			Finalizers:                 []string{"fin1", "fin2"},
			ManagedFields:              []metaV1.ManagedFieldsEntry{{Manager: "Manager", Operation: "Operation", APIVersion: "APIVersion"}},
		}

		cleanReadOnlyObjectMetaFields(&objectMeta)

		assert.EqualValues(t, objectMeta, metaV1.ObjectMeta{
			Name:                       "Name",
			GenerateName:               "GenerateName",
			Namespace:                  "Namespace",
			SelfLink:                   "",
			UID:                        "",
			ResourceVersion:            "",
			Generation:                 0,
			CreationTimestamp:          metaV1.Time{},
			DeletionTimestamp:          nil,
			DeletionGracePeriodSeconds: nil,
			Labels:                     map[string]string{"L1": "V1"},
			Annotations:                map[string]string{"A1": "V1"},
			OwnerReferences:            []metaV1.OwnerReference{{APIVersion: "APIVersion", Kind: "Kind", Name: "Name"}},
			Finalizers:                 []string{"fin1", "fin2"},
			ManagedFields:              nil,
		})
	})

	t.Run("unstructured", func(t *testing.T) {
		nowStr, _ := metaV1.Now().MarshalQueryParameter()
		object := unstructured.Unstructured{
			Object: map[string]interface{}{
				"metadata": map[string]interface{}{
					"name":                       "Name",
					"generateName":               "GenerateName",
					"namespace":                  "Namespace",
					"selfLink":                   "SelfLink",
					"uid":                        "UID",
					"resourceVersion":            "ResourceVersion",
					"generation":                 15,
					"creationTimestamp":          nowStr,
					"deletionTimestamp":          nowStr,
					"deletionGracePeriodSeconds": "16",
					"labels":                     map[string]string{"L1": "V1"},
					"annotations":                map[string]string{"A1": "V1"},
					"ownerReferences": []map[string]interface{}{
						{"apiVersion": "APIVersion", "kind": "Kind", "name": "Name"},
					},
					"finalizers":  []string{"fin1", "fin2"},
					"clusterName": "ClusterName",
					"managedFields": []map[string]interface{}{
						{"manager": "Manager", "operation": "Operation", "apiVersion": "APIVersion"},
					},
				},
			},
		}

		cleanReadOnlyObjectMetaFields(&object)

		assert.EqualValues(t, object, unstructured.Unstructured{
			Object: map[string]interface{}{
				"metadata": map[string]interface{}{
					"name":         "Name",
					"generateName": "GenerateName",
					"namespace":    "Namespace",
					"labels":       map[string]string{"L1": "V1"},
					"annotations":  map[string]string{"A1": "V1"},
					"ownerReferences": []map[string]interface{}{
						{"apiVersion": "APIVersion", "kind": "Kind", "name": "Name"},
					},
					"finalizers":  []string{"fin1", "fin2"},
					"clusterName": "ClusterName",
				},
			},
		})
	})

}

func TestKubernetesAPI_Apply(t *testing.T) {

	objectChangeTime := time.Date(2023, time.December, 18, 9, 0, 0, 0, time.UTC)

	t.Run("apply on non existing object", func(t *testing.T) {

		ctx, dynamicClient := k8sTesting.CreateFakeClient(nil)

		newObject := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "name-foo",
				Namespace: "namespace-foo",
			},
			Spec: appsV1.DeploymentSpec{
				MinReadySeconds: 667,
			},
		}

		target, err := Apply(ctx, &newObject, "", "", false)

		expected := newObject.DeepCopy()
		expected.CreationTimestamp = k8sTesting.FakeCreationTimestamp

		assert.NoError(t, err)
		assert.EqualValues(t, expected, target)

		clientActions := dynamicClient.Actions()
		assert.Len(t, clientActions, 2)
		k8sTesting.AssertGet(t, clientActions[0], "deployments", "namespace-foo", "name-foo")
		k8sTesting.AssertCreate(t, clientActions[1], "deployments", "namespace-foo", &appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "name-foo",
				Namespace: "namespace-foo",
			},
			Spec: appsV1.DeploymentSpec{
				MinReadySeconds: 667,
			},
		})
	})

	t.Run("apply on existing object (change property)", func(t *testing.T) {

		existingData := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "name-foo",
				Namespace: "namespace-foo",
			},
			Spec: appsV1.DeploymentSpec{
				MinReadySeconds: 666,
			},
		}

		ctx, dynamicClient := k8sTesting.CreateFakeClient(nil, &existingData)

		expected := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:              "name-foo",
				Namespace:         "namespace-foo",
				CreationTimestamp: k8sTesting.FakeCreationTimestamp,
			},
			Spec: appsV1.DeploymentSpec{
				MinReadySeconds: 667,
			},
		}

		newObject := expected.DeepCopy()

		target, err := Apply(ctx, newObject, "", "", true)

		assert.NoError(t, err)
		assert.EqualValues(t, &expected, target)

		clientActions := dynamicClient.Actions()
		assert.Len(t, clientActions, 2)
		k8sTesting.AssertGet(t, clientActions[0], "deployments", "namespace-foo", "name-foo")
		//only delta is sent....
		k8sTesting.AssertPatch(t, clientActions[1], "deployments", "namespace-foo", "name-foo", types.StrategicMergePatchType, []byte(`{"spec":{"minReadySeconds":667}}`))
	})

	t.Run("apply on existing object (add property)", func(t *testing.T) {

		existingData := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "name-foo",
				Namespace: "namespace-foo",
			},
			Spec: appsV1.DeploymentSpec{
				MinReadySeconds: 666,
			},
		}

		ctx, dynamicClient := k8sTesting.CreateFakeClient(nil, &existingData)

		expected := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:              "name-foo",
				Namespace:         "namespace-foo",
				CreationTimestamp: k8sTesting.FakeCreationTimestamp,
			},
			Spec: appsV1.DeploymentSpec{
				MinReadySeconds: 666,
				Replicas:        pointer.Int32(32),
			},
		}

		newObject := expected.DeepCopy()

		target, err := Apply(ctx, newObject, "", "", true)

		assert.NoError(t, err)
		assert.EqualValues(t, &expected, target)

		clientActions := dynamicClient.Actions()
		assert.Len(t, clientActions, 2)
		k8sTesting.AssertGet(t, clientActions[0], "deployments", "namespace-foo", "name-foo")
		//only delta is sent....
		k8sTesting.AssertPatch(t, clientActions[1], "deployments", "namespace-foo", "name-foo", types.StrategicMergePatchType, []byte(`{"spec":{"replicas":32}}`))
	})

	t.Run("apply on existing object (remove property)", func(t *testing.T) {

		existingData := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "name-foo",
				Namespace: "namespace-foo",
			},
			Spec: appsV1.DeploymentSpec{
				MinReadySeconds: 666,
				Replicas:        pointer.Int32(32),
			},
		}

		ctx, dynamicClient := k8sTesting.CreateFakeClient(nil, &existingData)

		expected := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:              "name-foo",
				Namespace:         "namespace-foo",
				CreationTimestamp: k8sTesting.FakeCreationTimestamp,
			},
			Spec: appsV1.DeploymentSpec{
				MinReadySeconds: 666,
			},
		}

		newObject := expected.DeepCopy()

		target, err := Apply(ctx, newObject, "", "", true)

		assert.NoError(t, err)
		assert.EqualValues(t, &expected, target)

		clientActions := dynamicClient.Actions()
		assert.Len(t, clientActions, 2)
		k8sTesting.AssertGet(t, clientActions[0], "deployments", "namespace-foo", "name-foo")
		//only delta is sent....
		k8sTesting.AssertPatch(t, clientActions[1], "deployments", "namespace-foo", "name-foo", types.StrategicMergePatchType, []byte(`{"spec":{"replicas":null}}`))
	})

	t.Run("apply with 'changer'", func(t *testing.T) {
		existingData := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "name-foo",
				Namespace: "namespace-foo",
			},
			Spec: appsV1.DeploymentSpec{
				MinReadySeconds: 666,
			},
		}

		ctx := commonFake.WithNowTime(context.Background(), objectChangeTime)
		ctx, dynamicClient := k8sTesting.CreateFakeClient(ctx, &existingData)

		expected := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:              "name-foo",
				Namespace:         "namespace-foo",
				CreationTimestamp: k8sTesting.FakeCreationTimestamp,
			},
			Spec: appsV1.DeploymentSpec{
				MinReadySeconds: 667,
			},
		}

		newObject := expected.DeepCopy()

		target, err := Apply(ctx, newObject, "some-service", "", true)

		expected.Annotations = map[string]string{"tiki-dsp.io/change-date": "2023-12-18T09:00:00Z", "tiki-dsp.io/changed-by": "some-service"}

		assert.NoError(t, err)
		assert.EqualValues(t, &expected, target)

		clientActions := dynamicClient.Actions()
		assert.Len(t, clientActions, 2)
		k8sTesting.AssertGet(t, clientActions[0], "deployments", "namespace-foo", "name-foo")
		//only delta is sent....
		k8sTesting.AssertPatch(t, clientActions[1], "deployments", "namespace-foo", "name-foo", types.StrategicMergePatchType, []byte(`{"metadata":{"annotations":{"tiki-dsp.io/change-date":"2023-12-18T09:00:00Z","tiki-dsp.io/changed-by":"some-service"}},"spec":{"minReadySeconds":667}}`))
	})

	t.Run("apply with 'change reason'", func(t *testing.T) {
		existingData := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "name-foo",
				Namespace: "namespace-foo",
			},
			Spec: appsV1.DeploymentSpec{
				MinReadySeconds: 666,
			},
		}

		ctx := commonFake.WithNowTime(context.Background(), objectChangeTime)
		ctx, dynamicClient := k8sTesting.CreateFakeClient(ctx, &existingData)

		expected := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:              "name-foo",
				Namespace:         "namespace-foo",
				CreationTimestamp: k8sTesting.FakeCreationTimestamp,
			},
			Spec: appsV1.DeploymentSpec{
				MinReadySeconds: 667,
			},
		}

		newObject := expected.DeepCopy()

		target, err := Apply(ctx, newObject, "", "some-change-reason", true)

		expected.Annotations = map[string]string{"tiki-dsp.io/change-date": "2023-12-18T09:00:00Z", "tiki-dsp.io/change-reason": "some-change-reason"}

		assert.NoError(t, err)
		assert.EqualValues(t, &expected, target)

		clientActions := dynamicClient.Actions()
		assert.Len(t, clientActions, 2)
		k8sTesting.AssertGet(t, clientActions[0], "deployments", "namespace-foo", "name-foo")
		//only delta is sent....
		k8sTesting.AssertPatch(t, clientActions[1], "deployments", "namespace-foo", "name-foo", types.StrategicMergePatchType, []byte(`{"metadata":{"annotations":{"tiki-dsp.io/change-date":"2023-12-18T09:00:00Z","tiki-dsp.io/change-reason":"some-change-reason"}},"spec":{"minReadySeconds":667}}`))
	})

	t.Run("apply with same object (no changes)", func(t *testing.T) {
		existingData := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "name-foo",
				Namespace: "namespace-foo",
			},
			Spec: appsV1.DeploymentSpec{
				MinReadySeconds: 666,
			},
		}

		ctx := commonFake.WithNowTime(context.Background(), objectChangeTime)
		ctx, dynamicClient := k8sTesting.CreateFakeClient(ctx, &existingData)

		newObject := existingData.DeepCopy()

		target, err := Apply(ctx, newObject, "", "", true)

		assert.NoError(t, err)
		assert.EqualValues(t, &existingData, target)

		clientActions := dynamicClient.Actions()
		assert.Len(t, clientActions, 1)
		k8sTesting.AssertGet(t, clientActions[0], "deployments", "namespace-foo", "name-foo")
	})

	t.Run("apply with same object (no property changes but with annotations)", func(t *testing.T) {
		existingData := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "name-foo",
				Namespace: "namespace-foo",
				Annotations: map[string]string{
					"tiki-dsp.io/changed-by":    "some-changer",
					"tiki-dsp.io/change-reason": "some-change-reason",
				},
			},
			Spec: appsV1.DeploymentSpec{
				MinReadySeconds: 666,
			},
		}

		ctx := commonFake.WithNowTime(context.Background(), objectChangeTime)
		ctx, dynamicClient := k8sTesting.CreateFakeClient(ctx, &existingData)

		newObject := existingData.DeepCopy()
		//Annotations are set autmatically
		delete(newObject.Annotations, "tiki-dsp.io/changed-by")
		delete(newObject.Annotations, "tiki-dsp.io/change-reason")

		target, err := Apply(ctx, newObject, "some-changer", "some-change-reason", true)

		expectingObject := existingData.DeepCopy()
		//Annotations are set automatically
		expectingObject.Annotations = map[string]string{"tiki-dsp.io/changed-by": "some-changer", "tiki-dsp.io/change-date": "2023-12-18T09:00:00Z", "tiki-dsp.io/change-reason": "some-change-reason"}

		assert.NoError(t, err)
		assert.EqualValues(t, expectingObject, target)

		clientActions := dynamicClient.Actions()
		assert.Len(t, clientActions, 2)
		k8sTesting.AssertGet(t, clientActions[0], "deployments", "namespace-foo", "name-foo")
		//only delta is sent....
		k8sTesting.AssertPatch(t, clientActions[1], "deployments", "namespace-foo", "name-foo", types.StrategicMergePatchType, []byte(`{"metadata":{"annotations":{"tiki-dsp.io/change-date":"2023-12-18T09:00:00Z"}}}`))
	})
}
