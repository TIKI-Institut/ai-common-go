package kubernetes

import (
	netV1 "github.com/k8snetworkplumbingwg/network-attachment-definition-client/pkg/apis/k8s.cni.cncf.io/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/kubernetes/scheme"
	"sync"
)

var registerCustomResources sync.Once

/* when adding additional custom resources add them here..... */
var customResources = runtime.SchemeBuilder{
	netV1.AddToScheme,
}

func init() {

	registerCustomResources.Do(func() {
		if err := customResources.AddToScheme(scheme.Scheme); err != nil {
			panic(err)
		}
	})

}
