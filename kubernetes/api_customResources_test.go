package kubernetes

import (
	k8sTesting "bitbucket.org/TIKI-Institut/ai-common-go/v2/kubernetes/testing"
	netV1 "github.com/k8snetworkplumbingwg/network-attachment-definition-client/pkg/apis/k8s.cni.cncf.io/v1"
	"github.com/stretchr/testify/assert"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"testing"
)

//as it seems we need to register the custom resources with the scheme to use them in the dynamic client
func TestCustomResource(t *testing.T) {

	ctx, dynamicClient := k8sTesting.CreateFakeClient(nil)

	nad := netV1.NetworkAttachmentDefinition{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "name-foo",
			Namespace: "namespace-foo",
		},
		Spec: netV1.NetworkAttachmentDefinitionSpec{},
	}

	target, err := Apply(ctx, &nad, "", "", false)

	assert.NoError(t, err)

	expectedObject := nad.DeepCopy()
	expectedObject.CreationTimestamp = k8sTesting.FakeCreationTimestamp
	assert.EqualValues(t, expectedObject, target)

	clientActions := dynamicClient.Actions()
	assert.Len(t, clientActions, 2)
	k8sTesting.AssertGet(t, clientActions[0], "networkattachmentdefinitions", "namespace-foo", "name-foo")
	//only delta is sent....
	//{"spec":{"minReadySeconds":667}}
	k8sTesting.AssertCreate(t, clientActions[1], "networkattachmentdefinitions", "namespace-foo", &netV1.NetworkAttachmentDefinition{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "name-foo",
			Namespace: "namespace-foo",
		}})

}
