package kubernetes

import (
	"fmt"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	"k8s.io/client-go/kubernetes/scheme"
)

func DecodeObject[T Object](bytes []byte) (T, error) {

	target := NewK8sObject[T]()

	codecFactory := serializer.NewCodecFactory(scheme.Scheme)
	deserializer := codecFactory.UniversalDeserializer()

	decodedObject, _, err := deserializer.Decode(bytes, nil, target)
	if err != nil {
		gvk, gvkError := GroupVersionKindForObject(target)
		if gvkError != nil {
			return target, fmt.Errorf("unable to decode source data (%w)", err)
		}
		return target, fmt.Errorf("unable to decode source data to object of type %s, (%w)", gvk.String(), err)
	}

	return decodedObject.(T), nil
}
