package kubernetes

import (
	k8sTesting "bitbucket.org/TIKI-Institut/ai-common-go/v2/kubernetes/testing"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	coreV1 "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"testing"
)

func TestReadObject(t *testing.T) {

	expecting := &coreV1.ConfigMap{
		TypeMeta: metaV1.TypeMeta{Kind: "ConfigMap", APIVersion: "v1"},
		ObjectMeta: metaV1.ObjectMeta{
			Name:              "cm-new",
			Namespace:         "namespace-b",
			CreationTimestamp: k8sTesting.FakeCreationTimestamp,
		},
		Data: map[string]string{
			"bar": "foo",
		},
	}

	t.Run("normal json", func(t *testing.T) {
		obj := `{"apiVersion":"v1","kind":"ConfigMap","data":{"bar":"foo"},"metadata":{"creationTimestamp":"2022-10-19T14:10:00Z","name":"cm-new","namespace":"namespace-b"}}`
		cm, err := DecodeObject[*coreV1.ConfigMap]([]byte(obj))

		require.NoError(t, err)
		assert.EqualValues(t, expecting, cm)
	})

	t.Run("normal yaml", func(t *testing.T) {
		obj := `---
apiVersion: v1
kind: ConfigMap
metadata:
  name: cm-new
  namespace: namespace-b
  creationTimestamp: 2022-10-19T14:10:00Z
data:
  bar: foo
`
		cm, err := DecodeObject[*coreV1.ConfigMap]([]byte(obj))

		require.NoError(t, err)
		assert.EqualValues(t, expecting, cm)
	})

	t.Run("normal without kind info", func(t *testing.T) {
		obj := `{"data":{"bar":"foo"},"metadata":{"creationTimestamp":"2022-10-19T14:10:00Z","name":"cm-new","namespace":"namespace-b"}}`
		cm, err := DecodeObject[*coreV1.ConfigMap]([]byte(obj))

		expectingWithoutGvk := expecting.DeepCopy()
		expectingWithoutGvk.SetGroupVersionKind(schema.EmptyObjectKind.GroupVersionKind())

		require.NoError(t, err)
		assert.EqualValues(t, expectingWithoutGvk, cm)
	})

}
