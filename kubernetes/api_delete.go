package kubernetes

import (
	"context"
	"k8s.io/apimachinery/pkg/api/errors"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
)

func DeleteObject(ctx context.Context, obj Object) error {

	objectGVK, err := GroupVersionKindForObject(obj)
	if err != nil {
		return err
	}

	return Delete(ctx, objectGVK, obj.GetNamespace(), obj.GetName())
}

func Delete(ctx context.Context, gvk schema.GroupVersionKind, namespace string, name string) error {

	// 1. get dynamic client
	client, _, err := Client(ctx, gvk, namespace)
	if err != nil {
		return err
	}

	// 2. Delete the referenced resource
	// https://kubernetes.io/docs/concepts/workloads/controllers/garbage-collection/
	propagatePolicy := metaV1.DeletePropagationBackground
	err = client.Delete(ctx, name, metaV1.DeleteOptions{PropagationPolicy: &propagatePolicy})

	if err != nil && !errors.IsNotFound(err) {
		return err
	}

	return nil
}
