package kubernetes

import (
	k8sTesting "bitbucket.org/TIKI-Institut/ai-common-go/v2/kubernetes/testing"
	"github.com/stretchr/testify/assert"
	appsV1 "k8s.io/api/apps/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"testing"
)

func TestKubernetesAPI_Delete(t *testing.T) {
	t.Run("existing resource", func(t *testing.T) {

		existingData := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "name-foo",
				Namespace: "namespace-foo",
			},
			Spec: appsV1.DeploymentSpec{
				MinReadySeconds: 666,
			},
		}

		ctx, dynamicClient := k8sTesting.CreateFakeClient(nil, &existingData)

		err := DeleteObject(ctx, &appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "name-foo",
				Namespace: "namespace-foo",
			},
			Spec:   appsV1.DeploymentSpec{},
			Status: appsV1.DeploymentStatus{},
		})

		assert.NoError(t, err)

		clientActions := dynamicClient.Actions()
		assert.Len(t, clientActions, 1)
		k8sTesting.AssertDelete(t, clientActions[0], "deployments", "namespace-foo", "name-foo")
	})

	t.Run("non-existing resource", func(t *testing.T) {

		ctx, dynamicClient := k8sTesting.CreateFakeClient(nil)

		err := DeleteObject(ctx, &appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "name-foo",
				Namespace: "namespace-foo",
			},
			Spec:   appsV1.DeploymentSpec{},
			Status: appsV1.DeploymentStatus{},
		})

		assert.NoError(t, err)

		clientActions := dynamicClient.Actions()
		assert.Len(t, clientActions, 1)
		k8sTesting.AssertDelete(t, clientActions[0], "deployments", "namespace-foo", "name-foo")
	})

	t.Run("existing resource (by name)", func(t *testing.T) {

		existingData := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "name-foo",
				Namespace: "namespace-foo",
			},
			Spec: appsV1.DeploymentSpec{
				MinReadySeconds: 666,
			},
		}

		ctx, dynamicClient := k8sTesting.CreateFakeClient(nil, &existingData)

		err := Delete(ctx, appsV1.SchemeGroupVersion.WithKind("Deployment"), "namespace-foo", "name-foo")

		assert.NoError(t, err)

		clientActions := dynamicClient.Actions()
		assert.Len(t, clientActions, 1)
		k8sTesting.AssertDelete(t, clientActions[0], "deployments", "namespace-foo", "name-foo")
	})

	t.Run("non-existing resource (by name) ", func(t *testing.T) {

		ctx, dynamicClient := k8sTesting.CreateFakeClient(nil)

		err := Delete(ctx, appsV1.SchemeGroupVersion.WithKind("Deployment"), "namespace-foo", "name-foo")

		assert.NoError(t, err)

		clientActions := dynamicClient.Actions()
		assert.Len(t, clientActions, 1)
		k8sTesting.AssertDelete(t, clientActions[0], "deployments", "namespace-foo", "name-foo")
	})

}
