package kubernetes

import (
	"context"
	"fmt"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/kubernetes/scheme"
)

func GetObject(ctx context.Context, obj Object) error {

	if len(obj.GetName()) == 0 {
		return fmt.Errorf("'name' of <obj> must be filled")
	}

	// 1. Resolve GVK of object
	objectGVK, err := GroupVersionKindForObject(obj)
	if err != nil {
		return err
	}

	// 2. Get Object from server
	serverObj, err := Get(ctx, objectGVK, obj.GetNamespace(), obj.GetName())
	if err != nil {
		return err
	}

	return scheme.Scheme.Convert(serverObj, obj, nil)
}

func Get(ctx context.Context, gvk schema.GroupVersionKind, namespace string, name string) (runtime.Object, error) {

	if len(name) == 0 {
		return nil, fmt.Errorf("parameter 'name' must be filled")
	}

	// 1. get dynamic client
	client, isNamespaceScoped, err := Client(ctx, gvk, namespace)
	if err != nil {
		return nil, err
	}

	if isNamespaceScoped && len(namespace) == 0 {
		return nil, fmt.Errorf("parameter 'namespace' must be filled for object of type %s", gvk.String())
	}

	unstructuredResult, err := client.Get(ctx, name, metaV1.GetOptions{})

	if err != nil {
		return nil, err
	}

	structuredResult, err := scheme.Scheme.New(gvk)

	if err != nil {
		return nil, err
	}

	err = scheme.Scheme.Convert(unstructuredResult, structuredResult, nil)

	if err != nil {
		return nil, err
	}

	return structuredResult, nil
}
