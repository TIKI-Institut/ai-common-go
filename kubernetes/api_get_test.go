package kubernetes

import (
	k8sTesting "bitbucket.org/TIKI-Institut/ai-common-go/v2/kubernetes/testing"
	"github.com/stretchr/testify/assert"
	appsV1 "k8s.io/api/apps/v1"
	coreV1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/utils/pointer"
	"testing"
)

func TestKubernetesAPI_Populate(t *testing.T) {

	t.Run("normal (gvk from target)", func(t *testing.T) {

		existingData := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "name-foo",
				Namespace: "namespace-foo",
			},
			Spec: appsV1.DeploymentSpec{
				MinReadySeconds: 666,
			},
		}

		ctx, dynamicClient := k8sTesting.CreateFakeClient(nil, &existingData)

		target := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "name-foo",
				Namespace: "namespace-foo",
			},
		}

		expected := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:              "name-foo",
				Namespace:         "namespace-foo",
				CreationTimestamp: k8sTesting.FakeCreationTimestamp,
			},
			Spec: appsV1.DeploymentSpec{
				MinReadySeconds: 666,
			},
		}

		err := GetObject(ctx, &target)
		assert.NoError(t, err)
		assert.EqualValues(t, expected, target)

		clientActions := dynamicClient.Actions()
		assert.Len(t, clientActions, 1)
		k8sTesting.AssertGet(t, clientActions[0], "deployments", "namespace-foo", "name-foo")
	})

	t.Run("normal (gvk from schema)", func(t *testing.T) {

		existingData := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "name-foo",
				Namespace: "namespace-foo",
			},
			Spec: appsV1.DeploymentSpec{
				MinReadySeconds: 666,
			},
		}

		ctx, dynamicClient := k8sTesting.CreateFakeClient(nil, &existingData)

		target := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "name-foo",
				Namespace: "namespace-foo",
			},
		}

		expected := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:              "name-foo",
				Namespace:         "namespace-foo",
				CreationTimestamp: k8sTesting.FakeCreationTimestamp,
			},
			Spec: appsV1.DeploymentSpec{
				MinReadySeconds: 666,
			},
		}

		err := GetObject(ctx, &target)
		assert.NoError(t, err)
		assert.EqualValues(t, expected, target)

		clientActions := dynamicClient.Actions()
		assert.Len(t, clientActions, 1)
		k8sTesting.AssertGet(t, clientActions[0], "deployments", "namespace-foo", "name-foo")
	})

	t.Run("name not filled", func(t *testing.T) {

		existingData := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "name-foo",
				Namespace: "namespace-foo",
			},
			Spec: appsV1.DeploymentSpec{
				MinReadySeconds: 666,
			},
		}

		ctx, dynamicClient := k8sTesting.CreateFakeClient(nil, &existingData)

		target := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Namespace: "namespace-foo",
			},
		}

		err := GetObject(ctx, &target)
		assert.EqualError(t, err, "'name' of <obj> must be filled")

		clientActions := dynamicClient.Actions()
		assert.Len(t, clientActions, 0)
	})

	t.Run("scoped resource (ns not filled)", func(t *testing.T) {

		existingData := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "name-foo",
				Namespace: "namespace-foo",
			},
			Spec: appsV1.DeploymentSpec{
				MinReadySeconds: 666,
			},
		}

		ctx, dynamicClient := k8sTesting.CreateFakeClient(nil, &existingData)

		target := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name: "name-foo",
			},
		}

		err := GetObject(ctx, &target)
		assert.EqualError(t, err, "parameter 'namespace' must be filled for object of type apps/v1, Kind=Deployment")

		clientActions := dynamicClient.Actions()
		assert.Len(t, clientActions, 0)
	})

	t.Run("not found", func(t *testing.T) {

		ctx, dynamicClient := k8sTesting.CreateFakeClient(nil)

		target := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "name-foo",
				Namespace: "namespace-foo",
			},
		}

		expected := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "name-foo",
				Namespace: "namespace-foo",
			},
		}

		err := GetObject(ctx, &target)
		assert.True(t, errors.IsNotFound(err))
		assert.EqualValues(t, expected, target)

		clientActions := dynamicClient.Actions()
		assert.Len(t, clientActions, 1)
		k8sTesting.AssertGet(t, clientActions[0], "deployments", "namespace-foo", "name-foo")
	})

	t.Run("not found", func(t *testing.T) {

		ctx, dynamicClient := k8sTesting.CreateFakeClient(nil)

		target := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "name-foo",
				Namespace: "namespace-foo",
			},
		}

		expected := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "name-foo",
				Namespace: "namespace-foo",
			},
		}

		err := GetObject(ctx, &target)
		assert.True(t, errors.IsNotFound(err))
		assert.EqualValues(t, expected, target)

		clientActions := dynamicClient.Actions()
		assert.Len(t, clientActions, 1)
		k8sTesting.AssertGet(t, clientActions[0], "deployments", "namespace-foo", "name-foo")
	})

	t.Run("'root' scoped resource", func(t *testing.T) {

		existingData := coreV1.Namespace{
			ObjectMeta: metaV1.ObjectMeta{
				Name:   "name-foo",
				Labels: map[string]string{"some-label": "label-foo"},
			},
		}

		ctx, dynamicClient := k8sTesting.CreateFakeClient(nil, &existingData)

		target := coreV1.Namespace{
			ObjectMeta: metaV1.ObjectMeta{
				Name: "name-foo",
			},
		}

		expected := coreV1.Namespace{
			ObjectMeta: metaV1.ObjectMeta{
				Name:              "name-foo",
				Labels:            map[string]string{"some-label": "label-foo"},
				CreationTimestamp: k8sTesting.FakeCreationTimestamp,
			},
		}

		err := GetObject(ctx, &target)
		assert.NoError(t, err)
		assert.EqualValues(t, expected, target)

		clientActions := dynamicClient.Actions()
		assert.Len(t, clientActions, 1)
		k8sTesting.AssertGet(t, clientActions[0], "namespaces", "", "name-foo")
	})

	t.Run("reuse target", func(t *testing.T) {

		existingPodA := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "name-foo",
				Namespace: "namespace-foo",
			},
			Spec: appsV1.DeploymentSpec{
				MinReadySeconds: 666,
			},
		}

		existingPodB := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "another-name-foo",
				Namespace: "another-namespace-foo",
			},
			Spec: appsV1.DeploymentSpec{
				Replicas: pointer.Int32Ptr(3),
			},
		}

		expectedA := existingPodA.DeepCopy()
		expectedB := existingPodB.DeepCopy()

		expectedA.SetCreationTimestamp(k8sTesting.FakeCreationTimestamp)
		expectedB.SetCreationTimestamp(k8sTesting.FakeCreationTimestamp)

		ctx, dynamicClient := k8sTesting.CreateFakeClient(nil, &existingPodA, &existingPodB)

		testTarget := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "name-foo",
				Namespace: "namespace-foo",
			},
		}

		err := GetObject(ctx, &testTarget)
		assert.NoError(t, err)
		assert.EqualValues(t, expectedA, &testTarget)

		testTarget.SetNamespace("another-namespace-foo")
		testTarget.SetName("another-name-foo")

		err = GetObject(ctx, &testTarget)
		assert.NoError(t, err)
		assert.EqualValues(t, expectedB, &testTarget)

		clientActions := dynamicClient.Actions()
		assert.Len(t, clientActions, 2)
		k8sTesting.AssertGet(t, clientActions[0], "deployments", "namespace-foo", "name-foo")
		k8sTesting.AssertGet(t, clientActions[1], "deployments", "another-namespace-foo", "another-name-foo")
	})

}

func TestKubernetesAPI_Get(t *testing.T) {

	t.Run("normal", func(t *testing.T) {

		existingData := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "name-foo",
				Namespace: "namespace-foo",
			},
			Spec: appsV1.DeploymentSpec{
				MinReadySeconds: 666,
			},
		}

		ctx, dynamicClient := k8sTesting.CreateFakeClient(nil, &existingData)

		target, err := Get(ctx, appsV1.SchemeGroupVersion.WithKind("Deployment"), "namespace-foo", "name-foo")

		assert.NoError(t, err)
		assert.EqualValues(t, &existingData, target)

		clientActions := dynamicClient.Actions()
		assert.Len(t, clientActions, 1)
		k8sTesting.AssertGet(t, clientActions[0], "deployments", "namespace-foo", "name-foo")
	})

	t.Run("name not filled", func(t *testing.T) {

		existingData := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "name-foo",
				Namespace: "namespace-foo",
			},
			Spec: appsV1.DeploymentSpec{
				MinReadySeconds: 666,
			},
		}

		ctx, dynamicClient := k8sTesting.CreateFakeClient(nil, &existingData)

		target, err := Get(ctx, appsV1.SchemeGroupVersion.WithKind("Deployment"), "namespace-foo", "")

		assert.EqualError(t, err, "parameter 'name' must be filled")
		assert.Nil(t, target)

		clientActions := dynamicClient.Actions()
		assert.Len(t, clientActions, 0)
	})

	t.Run("scoped resource (ns not filled)", func(t *testing.T) {

		existingData := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "name-foo",
				Namespace: "namespace-foo",
			},
			Spec: appsV1.DeploymentSpec{
				MinReadySeconds: 666,
			},
		}

		ctx, dynamicClient := k8sTesting.CreateFakeClient(nil, &existingData)

		target, err := Get(ctx, appsV1.SchemeGroupVersion.WithKind("Deployment"), "", "name-foo")

		assert.EqualError(t, err, "parameter 'namespace' must be filled for object of type apps/v1, Kind=Deployment")
		assert.Nil(t, target)

		clientActions := dynamicClient.Actions()
		assert.Len(t, clientActions, 0)
	})

	t.Run("not found", func(t *testing.T) {

		ctx, dynamicClient := k8sTesting.CreateFakeClient(nil)

		target, err := Get(ctx, appsV1.SchemeGroupVersion.WithKind("Deployment"), "namespace-foo", "name-foo")

		assert.True(t, errors.IsNotFound(err))
		assert.Nil(t, target)

		clientActions := dynamicClient.Actions()
		assert.Len(t, clientActions, 1)
		k8sTesting.AssertGet(t, clientActions[0], "deployments", "namespace-foo", "name-foo")
	})

	t.Run("'root' scoped resource", func(t *testing.T) {

		existingData := coreV1.Namespace{
			ObjectMeta: metaV1.ObjectMeta{
				Name:   "name-foo",
				Labels: map[string]string{"some-label": "label-foo"},
			},
		}

		ctx, dynamicClient := k8sTesting.CreateFakeClient(nil, &existingData)

		target, err := Get(ctx, coreV1.SchemeGroupVersion.WithKind("Namespace"), "", "name-foo")

		assert.NoError(t, err)
		assert.EqualValues(t, &existingData, target)

		clientActions := dynamicClient.Actions()
		assert.Len(t, clientActions, 1)
		k8sTesting.AssertGet(t, clientActions[0], "namespaces", "", "name-foo")
	})
}
