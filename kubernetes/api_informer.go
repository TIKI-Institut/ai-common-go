package kubernetes

import (
	k8sApiContext "bitbucket.org/TIKI-Institut/ai-common-go/v2/kubernetes/context"
	"context"
	"fmt"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/watch"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/dynamic/dynamicinformer"
	"k8s.io/client-go/tools/cache"
	"time"
)

type NewListWatcherFunc func(resourceInterface dynamic.NamespaceableResourceInterface) cache.ListerWatcher

// NewFilteredListWatcherFunc creates a NewListWatcherFunc with the specified <ctx> and <namespace>.
//
// If the optionsModifier is not nil, it will be applied to the ListOptions
// passed to the ListFunc and WatchFunc before calling List and Watch.
func NewFilteredListWatcherFunc(ctx context.Context, namespace string, optionsModifier dynamicinformer.TweakListOptionsFunc) NewListWatcherFunc {
	return func(resourceInterface dynamic.NamespaceableResourceInterface) cache.ListerWatcher {
		return &cache.ListWatch{
			ListFunc: func(options metaV1.ListOptions) (runtime.Object, error) {
				if optionsModifier != nil {
					optionsModifier(&options)
				}
				return resourceInterface.Namespace(namespace).List(ctx, options)
			},
			WatchFunc: func(options metaV1.ListOptions) (watch.Interface, error) {
				if optionsModifier != nil {
					optionsModifier(&options)
				}
				return resourceInterface.Namespace(namespace).Watch(ctx, options)
			},
		}
	}
}

// NewSharedIndexInformer create a new SharedIndexInformer with given <resyncPeriod> and <indexers>. The NewListWatcherFunc will be called
// with a NamespaceableResourceInterface for the GroupVersionResource that was resolved for <TObj>.
//
// If <newLw> is nil, a default list watcher for all namespaces with no list option modifications will be used.
//
// If <indexers> is nil, a default indexer indexing by 'namespace': <namespace> will be used.
func NewSharedIndexInformer[TObj Object](ctx context.Context, newLw NewListWatcherFunc, resyncPeriod time.Duration, indexers cache.Indexers) (TypedSharedIndexInformer[TObj], error) {

	objectGVK, err := GroupVersionKindForObject(NewK8sObject[TObj]())
	if err != nil {
		return nil, err
	}

	restMapper, err := k8sApiContext.RestMapperFromContext(ctx)
	if err != nil {
		return nil, fmt.Errorf("unable to resolve current rest mapper (%w)", err)
	}

	mapping, err := restMapper.RESTMapping(objectGVK.GroupKind(), objectGVK.Version)
	if err != nil {
		return nil, fmt.Errorf("unable to create rest mapping (%w)", err)
	}

	dynClient, err := k8sApiContext.DynamicClientFromContext(ctx)
	if err != nil {
		panic(fmt.Errorf("no client was registered or inflight client creation failed"))
	}

	if newLw == nil {
		// we can use context.Background here, because the informer takes a stop chan when Run is triggered
		newLw = NewFilteredListWatcherFunc(context.Background(), metaV1.NamespaceAll, nil)
	}

	if indexers == nil {
		indexers = cache.Indexers{cache.NamespaceIndex: cache.MetaNamespaceIndexFunc}
	}

	return typedSharedIndexInformer[TObj]{
		cache.NewSharedIndexInformer(
			newLw(dynClient.Resource(mapping.Resource)),
			&unstructured.Unstructured{}, // do not validate watch.Event.Object type
			resyncPeriod,
			indexers),
	}, nil
}

// StartAndSyncInformer starts the informer in a goroutine and waits until the initial cache sync is done.
// it returns a boolean that signals if the cache sync was successful.
//
// Both the informer and the cache sync can be stopped by cancelling the provided context.
func StartAndSyncInformer(ctx context.Context, informer cache.SharedIndexInformer) bool {
	go informer.Run(ctx.Done())
	return cache.WaitForCacheSync(ctx.Done(), informer.HasSynced)
}
