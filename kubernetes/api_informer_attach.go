package kubernetes

import (
	"context"
	"errors"
	"fmt"
	"k8s.io/client-go/dynamic/dynamicinformer"
	"k8s.io/client-go/tools/cache"
	"time"
)

func AttachResourceEventHandler[TObj Object](ctx context.Context, handler TypedResourceEventHandler[TObj], namespace string, resyncPeriod time.Duration, listOptionsModifier dynamicinformer.TweakListOptionsFunc, filter ResourceFilter[TObj]) error {

	if handler == nil {
		return errors.New("handler must be provided")
	}

	informer, err := NewSharedIndexInformer[TObj](ctx,
		NewFilteredListWatcherFunc(ctx, namespace, listOptionsModifier),
		resyncPeriod,
		nil,
	)

	if err != nil {
		return fmt.Errorf("unable to create shared informer (%w)", err)
	}

	if filter != nil {
		handler = TypedFilteringResourceEventHandler[TObj]{
			FilterFunc: filter,
			Handler:    handler,
		}
	}

	informer.AddTypedEventHandler(handler)
	go informer.Run(ctx.Done())

	if !cache.WaitForCacheSync(ctx.Done(), informer.HasSynced) {
		return errors.New("failed to sync shared informer")
	}

	return nil
}
