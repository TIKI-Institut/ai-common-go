package kubernetes

import (
	commonFake "bitbucket.org/TIKI-Institut/ai-common-go/v2/common/fake"
	k8sApiTesting "bitbucket.org/TIKI-Institut/ai-common-go/v2/kubernetes/testing"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/logging"
	"context"
	"github.com/rs/zerolog"
	"github.com/stretchr/testify/assert"
	coreV1 "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	dynamicFake "k8s.io/client-go/dynamic/fake"
	"sync/atomic"
	"testing"
	"time"
)

func TestAttachResourceEventHandler(t *testing.T) {

	k8sObjectChangeDate := time.Date(2023, time.December, 18, 9, 0, 0, 0, time.UTC)

	setup := func(t *testing.T, objects ...runtime.Object) (context.Context, context.CancelFunc, *dynamicFake.FakeDynamicClient) {

		ctx, dynamicClient := k8sApiTesting.CreateFakeClient(nil, objects...)

		ctx = commonFake.WithNowTime(ctx, k8sObjectChangeDate)
		ctx, _ = logging.AttachTestLogger(t, ctx, zerolog.TraceLevel)

		var cancelFnc context.CancelFunc
		ctx, cancelFnc = context.WithCancel(ctx)

		return ctx, cancelFnc, dynamicClient
	}

	cm := func(name string, namespace string) *coreV1.ConfigMap {
		return &coreV1.ConfigMap{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      name,
				Namespace: namespace,
			},
		}
	}

	timeCorrected := func(target *coreV1.ConfigMap) *coreV1.ConfigMap {
		target.CreationTimestamp = k8sApiTesting.FakeCreationTimestamp
		return target
	}

	withAnnotaions := func(target *coreV1.ConfigMap) *coreV1.ConfigMap {
		target.Annotations = map[string]string{
			"tiki-dsp.io/change-reason": "unit-test-reason",
			"tiki-dsp.io/changed-by":    "unit-test",
			"tiki-dsp.io/change-date":   "2023-12-18T09:00:00Z",
		}
		return target
	}

	failAllEventHandler := func(t *testing.T) TypedResourceEventHandlerFuncs[*coreV1.ConfigMap] {
		return TypedResourceEventHandlerFuncs[*coreV1.ConfigMap]{
			OnAddFunc:             func(obj *coreV1.ConfigMap) { t.FailNow() },
			OnUpdateFunc:          func(oldObj, newObj *coreV1.ConfigMap) { t.FailNow() },
			OnDeleteFunc:          func(obj *coreV1.ConfigMap) { t.FailNow() },
			OnDeleteUnknownFunc:   func(i interface{}, key string) { t.FailNow() },
			OnConversionErrorFunc: func(i interface{}, stage ConversionStage, conversionError error) { t.FailNow() },
		}
	}

	t.Run("nil handler", func(t *testing.T) {
		ctx, cancel, dynamicClient := setup(t, cm("cm1", "some-namespace"))

		err := AttachResourceEventHandler[*coreV1.ConfigMap](ctx, nil, metaV1.NamespaceAll, 0, nil, nil)
		assert.EqualError(t, err, "handler must be provided")

		<-time.After(time.Millisecond * 150)
		cancel()
		<-time.After(time.Millisecond * 150)

		actions := dynamicClient.Actions()
		assert.Len(t, actions, 0)
	})

	t.Run("show initial elements", func(t *testing.T) {
		ctx, cancel, dynamicClient := setup(t, cm("cm1", "some-namespace"))

		callCount := int32(0)

		testee := failAllEventHandler(t)

		testee.OnAddFunc = func(obj *coreV1.ConfigMap) {
			assert.EqualValues(t, timeCorrected(cm("cm1", "some-namespace")), obj)
			atomic.AddInt32(&callCount, 1)
		}

		err := AttachResourceEventHandler[*coreV1.ConfigMap](ctx, testee, metaV1.NamespaceAll, 0, nil, nil)
		assert.NoError(t, err)

		<-time.After(time.Millisecond * 150)
		cancel()
		<-time.After(time.Millisecond * 150)

		actions := dynamicClient.Actions()
		assert.Len(t, actions, 2)

		k8sApiTesting.AssertList(t, actions[0], "configmaps", "", "", "")
		k8sApiTesting.AssertWatch(t, actions[1], "configmaps", "", "", "", "")

		assert.EqualValues(t, 1, atomic.LoadInt32(&callCount))
	})

	t.Run("present added elements", func(t *testing.T) {
		ctx, cancel, dynamicClient := setup(t)

		callCount := int32(0)

		testee := failAllEventHandler(t)

		testee.OnAddFunc = func(obj *coreV1.ConfigMap) {
			assert.EqualValues(t, withAnnotaions(timeCorrected(cm("cm1", "some-namespace"))), obj)
			atomic.AddInt32(&callCount, 1)
		}

		err := AttachResourceEventHandler[*coreV1.ConfigMap](ctx, testee, metaV1.NamespaceAll, 0, nil, nil)
		assert.NoError(t, err)

		<-time.After(time.Millisecond * 150)

		_, err = Apply(ctx, cm("cm1", "some-namespace"), "unit-test", "unit-test-reason", false)
		assert.NoError(t, err)

		<-time.After(time.Millisecond * 150)
		cancel()
		<-time.After(time.Millisecond * 150)

		actions := dynamicClient.Actions()
		assert.Len(t, actions, 4)

		k8sApiTesting.AssertList(t, actions[0], "configmaps", "", "", "")
		k8sApiTesting.AssertWatch(t, actions[1], "configmaps", "", "", "", "")
		k8sApiTesting.AssertGet(t, actions[2], "configmaps", "some-namespace", "cm1")
		k8sApiTesting.AssertCreate(t, actions[3], "configmaps", "some-namespace", withAnnotaions(cm("cm1", "some-namespace")))

		assert.EqualValues(t, 1, atomic.LoadInt32(&callCount))
	})

	t.Run("present updated elements", func(t *testing.T) {
		ctx, cancel, dynamicClient := setup(t, cm("cm1", "some-namespace"))

		addCallCount := int32(0)
		updateCallCount := int32(0)

		testee := failAllEventHandler(t)

		testee.OnAddFunc = func(obj *coreV1.ConfigMap) {
			assert.EqualValues(t, timeCorrected(cm("cm1", "some-namespace")), obj)
			atomic.AddInt32(&addCallCount, 1)
		}

		testee.OnUpdateFunc = func(oldObj, newObj *coreV1.ConfigMap) {
			assert.EqualValues(t, timeCorrected(cm("cm1", "some-namespace")), oldObj)

			newExpectedObj := withAnnotaions(timeCorrected(cm("cm1", "some-namespace")))
			newExpectedObj.Labels = map[string]string{
				"some-label": "some-label-value",
			}

			assert.EqualValues(t, newExpectedObj, newObj)
			atomic.AddInt32(&updateCallCount, 1)
		}

		err := AttachResourceEventHandler[*coreV1.ConfigMap](ctx, testee, metaV1.NamespaceAll, 0, nil, nil)
		assert.NoError(t, err)

		<-time.After(time.Millisecond * 150)

		updateCm := cm("cm1", "some-namespace")

		updateCm.Labels = map[string]string{
			"some-label": "some-label-value",
		}
		_, err = Apply(ctx, updateCm, "unit-test", "unit-test-reason", false)
		assert.NoError(t, err)

		<-time.After(time.Millisecond * 150)
		cancel()
		<-time.After(time.Millisecond * 150)

		actions := dynamicClient.Actions()
		assert.Len(t, actions, 4)

		k8sApiTesting.AssertList(t, actions[0], "configmaps", "", "", "")
		k8sApiTesting.AssertWatch(t, actions[1], "configmaps", "", "", "", "")
		k8sApiTesting.AssertGet(t, actions[2], "configmaps", "some-namespace", "cm1")
		k8sApiTesting.AssertPatch(t, actions[3], "configmaps", "some-namespace", "cm1", types.StrategicMergePatchType, []byte(`{"metadata": {"annotations":{"tiki-dsp.io/change-date": "2023-12-18T09:00:00Z","tiki-dsp.io/change-reason":"unit-test-reason","tiki-dsp.io/changed-by":"unit-test"},"labels":{"some-label":"some-label-value"}}}`))

		assert.EqualValues(t, 1, atomic.LoadInt32(&addCallCount))
		assert.EqualValues(t, 1, atomic.LoadInt32(&updateCallCount))
	})

	t.Run("present deleted elements", func(t *testing.T) {
		ctx, cancel, dynamicClient := setup(t, cm("cm1", "some-namespace"))

		addCallCount := int32(0)
		deleteCallCount := int32(0)

		testee := failAllEventHandler(t)

		testee.OnAddFunc = func(obj *coreV1.ConfigMap) {
			assert.EqualValues(t, timeCorrected(cm("cm1", "some-namespace")), obj)
			atomic.AddInt32(&addCallCount, 1)
		}
		testee.OnDeleteFunc = func(obj *coreV1.ConfigMap) {
			assert.EqualValues(t, timeCorrected(cm("cm1", "some-namespace")), obj)
			atomic.AddInt32(&deleteCallCount, 1)
		}

		err := AttachResourceEventHandler[*coreV1.ConfigMap](ctx, testee, metaV1.NamespaceAll, 0, nil, nil)
		assert.NoError(t, err)

		assert.NoError(t, err)

		<-time.After(time.Millisecond * 150)
		err = DeleteObject(ctx, cm("cm1", "some-namespace"))
		assert.NoError(t, err)

		<-time.After(time.Millisecond * 150)
		cancel()
		<-time.After(time.Millisecond * 150)

		actions := dynamicClient.Actions()
		assert.Len(t, actions, 3)

		k8sApiTesting.AssertList(t, actions[0], "configmaps", "", "", "")
		k8sApiTesting.AssertWatch(t, actions[1], "configmaps", "", "", "", "")
		k8sApiTesting.AssertDelete(t, actions[2], "configmaps", "some-namespace", "cm1")

		assert.EqualValues(t, 1, atomic.LoadInt32(&addCallCount))
		assert.EqualValues(t, 1, atomic.LoadInt32(&deleteCallCount))
	})

	t.Run("do not present unknown deleted element", func(t *testing.T) {
		ctx, cancel, dynamicClient := setup(t)

		testee := failAllEventHandler(t)

		err := AttachResourceEventHandler[*coreV1.ConfigMap](ctx, testee, metaV1.NamespaceAll, 0, nil, nil)
		assert.NoError(t, err)

		<-time.After(time.Millisecond * 150)

		err = DeleteObject(ctx, cm("cm1", "some-namespace"))
		assert.NoError(t, err)

		<-time.After(time.Millisecond * 150)
		cancel()
		<-time.After(time.Millisecond * 150)

		actions := dynamicClient.Actions()
		assert.Len(t, actions, 3)

		k8sApiTesting.AssertList(t, actions[0], "configmaps", "", "", "")
		k8sApiTesting.AssertWatch(t, actions[1], "configmaps", "", "", "", "")
		k8sApiTesting.AssertDelete(t, actions[2], "configmaps", "some-namespace", "cm1")
	})

	t.Run("apply filter function", func(t *testing.T) {
		ctx, cancel, dynamicClient := setup(t, cm("cm1", "some-namespace"))

		callCount := int32(0)

		testee := failAllEventHandler(t)

		err := AttachResourceEventHandler[*coreV1.ConfigMap](ctx, testee, metaV1.NamespaceAll, 0, nil, func(configMap *coreV1.ConfigMap) bool { return configMap.Name != "cm1" })
		assert.NoError(t, err)

		<-time.After(time.Millisecond * 150)
		cancel()
		<-time.After(time.Millisecond * 150)

		actions := dynamicClient.Actions()
		assert.Len(t, actions, 2)

		k8sApiTesting.AssertList(t, actions[0], "configmaps", "", "", "")
		k8sApiTesting.AssertWatch(t, actions[1], "configmaps", "", "", "", "")

		assert.EqualValues(t, 0, callCount)
	})

	t.Run("apply LostOptions modifier", func(t *testing.T) {
		ctx, cancel, dynamicClient := setup(t, cm("cm1", "some-namespace"))

		callCount := int32(0)

		testee := failAllEventHandler(t)

		err := AttachResourceEventHandler[*coreV1.ConfigMap](ctx, testee, metaV1.NamespaceAll, 0, func(options *metaV1.ListOptions) {
			options.LabelSelector = metaV1.FormatLabelSelector(&metaV1.LabelSelector{MatchExpressions: []metaV1.LabelSelectorRequirement{{
				Key:      "some-label",
				Operator: metaV1.LabelSelectorOpExists,
			}}})
		}, nil)
		assert.NoError(t, err)

		<-time.After(time.Millisecond * 150)
		cancel()
		<-time.After(time.Millisecond * 150)

		actions := dynamicClient.Actions()
		assert.Len(t, actions, 2)

		k8sApiTesting.AssertList(t, actions[0], "configmaps", "", "", "some-label")
		k8sApiTesting.AssertWatch(t, actions[1], "configmaps", "", "", "some-label", "")

		assert.EqualValues(t, 0, callCount)
	})
}
