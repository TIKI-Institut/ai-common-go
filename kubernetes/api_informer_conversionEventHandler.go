package kubernetes

import (
	"fmt"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/tools/cache"
)

type ConversionEventHandler[TObj Object] struct {
	Target TypedResourceEventHandler[TObj]
}

func (c ConversionEventHandler[TObj]) OnAdd(obj interface{}, _ bool) {
	conv, convErr := c.convert(obj)
	if convErr != nil {
		c.Target.OnConversionError(obj, ConversionStageAdd, convErr)
	} else {
		c.Target.OnAdd(conv)
	}
}

func (c ConversionEventHandler[TObj]) OnUpdate(oldObj, newObj interface{}) {
	oldConv, convErr := c.convert(oldObj)
	if convErr != nil {
		c.Target.OnConversionError(oldObj, ConversionStageUpdate, convErr)
		return
	}
	newConv, convErr := c.convert(newObj)
	if convErr != nil {
		c.Target.OnConversionError(newObj, ConversionStageUpdate, convErr)
		return
	}
	c.Target.OnUpdate(oldConv, newConv)
}

func (c ConversionEventHandler[TObj]) OnDelete(obj interface{}) {
	d, isUnknown := obj.(cache.DeletedFinalStateUnknown)

	if isUnknown {
		conv, convErr := c.convert(d.Obj)
		if convErr != nil {
			c.Target.OnDeleteUnknown(d.Obj, d.Key)
		} else {
			c.Target.OnDelete(conv)
		}
	} else {
		conv, convErr := c.convert(obj)
		if convErr != nil {
			c.Target.OnConversionError(obj, ConversionStageDelete, convErr)
		} else {
			c.Target.OnDelete(conv)
		}
	}
}

func (c ConversionEventHandler[TObj]) convert(i interface{}) (TObj, error) {
	obj := NewK8sObject[TObj]()
	err := scheme.Scheme.Convert(i, obj, nil)
	if err != nil {
		return obj, fmt.Errorf("unable to convert object (%w)", err)
	}
	return obj, nil
}
