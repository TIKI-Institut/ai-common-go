package kubernetes

import (
	"github.com/stretchr/testify/assert"
	coreV1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/tools/cache"
	"testing"
)

func TestConversionEventHandler(t *testing.T) {

	toUnstructured := func(o Object) *unstructured.Unstructured {
		result := &unstructured.Unstructured{}
		err := scheme.Scheme.Convert(o, result, nil)
		if err != nil {
			panic(err)
		}
		return result
	}

	t.Run("correct type", func(t *testing.T) {

		t.Run("add", func(t *testing.T) {
			called := false
			testee := ConversionEventHandler[*coreV1.ConfigMap]{
				Target: TypedResourceEventHandlerFuncs[*coreV1.ConfigMap]{
					OnAddFunc: func(obj *coreV1.ConfigMap) {
						called = true
						assert.EqualValues(t, &coreV1.ConfigMap{Data: map[string]string{"blub": "A"}}, obj)

					},
				},
			}

			testee.OnAdd(toUnstructured(&coreV1.ConfigMap{Data: map[string]string{"blub": "A"}}), false)
			assert.True(t, called)
		})
		t.Run("update", func(t *testing.T) {
			called := false
			testee := ConversionEventHandler[*coreV1.ConfigMap]{
				Target: TypedResourceEventHandlerFuncs[*coreV1.ConfigMap]{
					OnUpdateFunc: func(old, new *coreV1.ConfigMap) {
						called = true
						assert.EqualValues(t, &coreV1.ConfigMap{Data: map[string]string{"blub": "A"}}, old)
						assert.EqualValues(t, &coreV1.ConfigMap{Data: map[string]string{"blub": "B"}}, new)
					},
				},
			}

			testee.OnUpdate(toUnstructured(&coreV1.ConfigMap{Data: map[string]string{"blub": "A"}}), toUnstructured(&coreV1.ConfigMap{Data: map[string]string{"blub": "B"}}))
			assert.True(t, called)

		})
		t.Run("delete", func(t *testing.T) {

			t.Run("known state", func(t *testing.T) {
				called := false
				testee := ConversionEventHandler[*coreV1.ConfigMap]{
					Target: TypedResourceEventHandlerFuncs[*coreV1.ConfigMap]{
						OnDeleteFunc: func(obj *coreV1.ConfigMap) {
							called = true
							assert.EqualValues(t, &coreV1.ConfigMap{Data: map[string]string{"blub": "A"}}, obj)
						},
					},
				}

				testee.OnDelete(toUnstructured(&coreV1.ConfigMap{Data: map[string]string{"blub": "A"}}))
				assert.True(t, called)
			})

			t.Run("unknown state", func(t *testing.T) {
				called := false
				testee := ConversionEventHandler[*coreV1.ConfigMap]{
					Target: TypedResourceEventHandlerFuncs[*coreV1.ConfigMap]{
						OnDeleteFunc: func(obj *coreV1.ConfigMap) {
							called = true
							assert.EqualValues(t, &coreV1.ConfigMap{Data: map[string]string{"blub": "A"}}, obj)
						},
					},
				}

				testee.OnDelete(cache.DeletedFinalStateUnknown{
					Key: "",
					Obj: toUnstructured(&coreV1.ConfigMap{Data: map[string]string{"blub": "A"}}),
				})
				assert.True(t, called)
			})

		})

	})

	t.Run("conversion error", func(t *testing.T) {

		t.Run("handled", func(t *testing.T) {
			t.Run("add", func(t *testing.T) {
				called := false
				sourceObj := toUnstructured(&coreV1.Secret{Data: map[string][]byte{"blub": []byte("A")}})
				testee := ConversionEventHandler[*coreV1.ConfigMap]{
					Target: TypedResourceEventHandlerFuncs[*coreV1.ConfigMap]{
						OnAddFunc: func(obj *coreV1.ConfigMap) {
							called = true
						},
						OnConversionErrorFunc: func(i interface{}, stage ConversionStage, conversionError error) {
							assert.Same(t, sourceObj, i)
							assert.EqualValues(t, ConversionStageAdd, stage)
							assert.EqualError(t, conversionError, "unable to convert object (converting (v1.Secret) to (v1.ConfigMap): unknown conversion)")
						},
					},
				}

				testee.OnAdd(sourceObj, false)
				assert.False(t, called)
			})
			t.Run("update", func(t *testing.T) {

				t.Run("old", func(t *testing.T) {
					called := false
					oldSourceObj := toUnstructured(&coreV1.Secret{Data: map[string][]byte{"blub": []byte("A")}})
					newSourceObj := toUnstructured(&coreV1.Secret{Data: map[string][]byte{"blub": []byte("B")}})
					testee := ConversionEventHandler[*coreV1.ConfigMap]{
						Target: TypedResourceEventHandlerFuncs[*coreV1.ConfigMap]{
							OnAddFunc: func(obj *coreV1.ConfigMap) {
								called = true
							},
							OnConversionErrorFunc: func(i interface{}, stage ConversionStage, conversionError error) {
								assert.Same(t, oldSourceObj, i)
								assert.EqualValues(t, ConversionStageUpdate, stage)
								assert.EqualError(t, conversionError, "unable to convert object (converting (v1.Secret) to (v1.ConfigMap): unknown conversion)")
							},
						},
					}

					testee.OnUpdate(oldSourceObj, newSourceObj)
					assert.False(t, called)
				})

				t.Run("new", func(t *testing.T) {
					called := false
					newSourceObj := toUnstructured(&coreV1.Secret{Data: map[string][]byte{"blub": []byte("B")}})
					testee := ConversionEventHandler[*coreV1.ConfigMap]{
						Target: TypedResourceEventHandlerFuncs[*coreV1.ConfigMap]{
							OnAddFunc: func(obj *coreV1.ConfigMap) {
								called = true
							},
							OnConversionErrorFunc: func(i interface{}, stage ConversionStage, conversionError error) {
								assert.Same(t, newSourceObj, i)
								assert.EqualValues(t, ConversionStageUpdate, stage)
								assert.EqualError(t, conversionError, "unable to convert object (converting (v1.Secret) to (v1.ConfigMap): unknown conversion)")
							},
						},
					}

					testee.OnUpdate(toUnstructured(&coreV1.ConfigMap{Data: map[string]string{"blub": "A"}}), newSourceObj)
					assert.False(t, called)
				})
			})

			t.Run("delete", func(t *testing.T) {

				t.Run("known statue", func(t *testing.T) {
					called := false
					sourceObj := toUnstructured(&coreV1.Secret{Data: map[string][]byte{"blub": []byte("A")}})
					testee := ConversionEventHandler[*coreV1.ConfigMap]{
						Target: TypedResourceEventHandlerFuncs[*coreV1.ConfigMap]{
							OnDeleteFunc: func(obj *coreV1.ConfigMap) {
								called = true
							},
							OnConversionErrorFunc: func(i interface{}, stage ConversionStage, conversionError error) {
								assert.Same(t, sourceObj, i)
								assert.EqualValues(t, ConversionStageDelete, stage)
								assert.EqualError(t, conversionError, "unable to convert object (converting (v1.Secret) to (v1.ConfigMap): unknown conversion)")
							},
						},
					}

					testee.OnDelete(sourceObj)
					assert.False(t, called)
				})

				t.Run("unknown statue", func(t *testing.T) {
					called := false
					unknownCalled := false
					errorCalled := false

					sourceObj := cache.DeletedFinalStateUnknown{
						Key: "some-key",
						Obj: toUnstructured(&coreV1.Secret{Data: map[string][]byte{"blub": []byte("A")}}),
					}
					testee := ConversionEventHandler[*coreV1.ConfigMap]{
						Target: TypedResourceEventHandlerFuncs[*coreV1.ConfigMap]{
							OnDeleteFunc: func(obj *coreV1.ConfigMap) {
								called = true
							},
							OnDeleteUnknownFunc: func(i interface{}, key string) {
								unknownCalled = true
								assert.Same(t, sourceObj.Obj, i)
								assert.EqualValues(t, "some-key", key)
							},
							OnConversionErrorFunc: func(i interface{}, stage ConversionStage, conversionError error) {
								errorCalled = true
							},
						},
					}

					testee.OnDelete(sourceObj)
					assert.False(t, called)
					assert.True(t, unknownCalled)
					assert.False(t, errorCalled)
				})

			})
		})

		t.Run("unhandled", func(t *testing.T) {
			t.Run("add", func(t *testing.T) {
				called := false
				sourceObj := toUnstructured(&coreV1.Secret{Data: map[string][]byte{"blub": []byte("A")}})
				testee := ConversionEventHandler[*coreV1.ConfigMap]{
					Target: TypedResourceEventHandlerFuncs[*coreV1.ConfigMap]{
						OnAddFunc: func(obj *coreV1.ConfigMap) {
							called = true
						},
					},
				}

				testee.OnAdd(sourceObj, false)
				assert.False(t, called)
			})
			t.Run("update", func(t *testing.T) {

				t.Run("old", func(t *testing.T) {
					called := false
					oldSourceObj := toUnstructured(&coreV1.Secret{Data: map[string][]byte{"blub": []byte("A")}})
					newSourceObj := toUnstructured(&coreV1.Secret{Data: map[string][]byte{"blub": []byte("B")}})
					testee := ConversionEventHandler[*coreV1.ConfigMap]{
						Target: TypedResourceEventHandlerFuncs[*coreV1.ConfigMap]{
							OnAddFunc: func(obj *coreV1.ConfigMap) {
								called = true
							},
						},
					}

					testee.OnUpdate(oldSourceObj, newSourceObj)
					assert.False(t, called)
				})

				t.Run("new", func(t *testing.T) {
					called := false
					newSourceObj := toUnstructured(&coreV1.Secret{Data: map[string][]byte{"blub": []byte("B")}})
					testee := ConversionEventHandler[*coreV1.ConfigMap]{
						Target: TypedResourceEventHandlerFuncs[*coreV1.ConfigMap]{
							OnAddFunc: func(obj *coreV1.ConfigMap) {
								called = true
							},
						},
					}

					testee.OnUpdate(toUnstructured(&coreV1.ConfigMap{Data: map[string]string{"blub": "A"}}), newSourceObj)
					assert.False(t, called)
				})
			})
			t.Run("delete", func(t *testing.T) {

				t.Run("known statue", func(t *testing.T) {
					called := false
					sourceObj := toUnstructured(&coreV1.Secret{Data: map[string][]byte{"blub": []byte("A")}})
					testee := ConversionEventHandler[*coreV1.ConfigMap]{
						Target: TypedResourceEventHandlerFuncs[*coreV1.ConfigMap]{
							OnDeleteFunc: func(obj *coreV1.ConfigMap) {
								called = true
							},
						},
					}

					testee.OnDelete(sourceObj)
					assert.False(t, called)
				})

				t.Run("unknown statue", func(t *testing.T) {
					called := false
					unknownCalled := false

					sourceObj := cache.DeletedFinalStateUnknown{
						Key: "some-key",
						Obj: toUnstructured(&coreV1.Secret{Data: map[string][]byte{"blub": []byte("A")}}),
					}
					testee := ConversionEventHandler[*coreV1.ConfigMap]{
						Target: TypedResourceEventHandlerFuncs[*coreV1.ConfigMap]{
							OnDeleteFunc: func(obj *coreV1.ConfigMap) {
								called = true
							},
							OnDeleteUnknownFunc: func(i interface{}, key string) {
								unknownCalled = true
								assert.Same(t, sourceObj.Obj, i)
								assert.EqualValues(t, "some-key", key)
							},
						},
					}

					testee.OnDelete(sourceObj)
					assert.False(t, called)
					assert.True(t, unknownCalled)
				})

			})
		})

	})

}
