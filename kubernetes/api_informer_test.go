package kubernetes

import (
	k8sTesting "bitbucket.org/TIKI-Institut/ai-common-go/v2/kubernetes/testing"
	"context"
	"github.com/stretchr/testify/assert"
	v1 "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/tools/cache"
	"testing"
	"time"
)

func convertAll[T any](s []interface{}) []T {
	result := make([]T, len(s), len(s))

	for i := 0; i < len(s); i++ {
		result[i] = s[i].(T)
	}
	return result
}

func demoConfigMaps() []runtime.Object {
	return []runtime.Object{
		&v1.ConfigMap{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "cm-a",
				Namespace: "namespace-a",
			},
		},
		&v1.ConfigMap{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "cm-b",
				Namespace: "namespace-a",
			},
		},
		&v1.ConfigMap{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "cm-c",
				Namespace: "namespace-a",
			},
		},
		&v1.ConfigMap{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "cm-a",
				Namespace: "namespace-b",
			},
		},
		&v1.ConfigMap{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "cm-b",
				Namespace: "namespace-b",
			},
		},
	}
}

func TestSharedInformer(t *testing.T) {

	t.Run("creating a new informer does not trigger any kubernetes api requests", func(t *testing.T) {

		ctx, dynClient := k8sTesting.CreateFakeClient(context.Background())

		_, err := NewSharedIndexInformer[*v1.Pod](ctx, nil, time.Hour*24, nil)
		assert.NoError(t, err)

		actions := dynClient.Actions()
		assert.Len(t, actions, 0)
	})

	t.Run("global configMaps", func(t *testing.T) {
		ctx, dynClient := k8sTesting.CreateFakeClient(context.Background())

		informer, err := NewSharedIndexInformer[*v1.ConfigMap](ctx, nil, time.Hour*24, nil)
		assert.NoError(t, err)

		ctx, cancel := context.WithCancel(ctx)
		defer cancel()

		assert.True(t, StartAndSyncInformer(ctx, informer))

		actions := dynClient.Actions()
		assert.Len(t, actions, 2)

		k8sTesting.AssertList(t, actions[0], "configmaps", metaV1.NamespaceAll, "", "")
		k8sTesting.AssertWatch(t, actions[1], "configmaps", metaV1.NamespaceAll, "", "", "")
	})

	t.Run("global configMaps & pods", func(t *testing.T) {
		ctx, dynClient := k8sTesting.CreateFakeClient(context.Background())

		podInformer, err := NewSharedIndexInformer[*v1.ConfigMap](ctx, nil, time.Hour*24, cache.Indexers{})
		assert.NoError(t, err)

		cmInformer, err := NewSharedIndexInformer[*v1.Pod](ctx, nil, time.Hour*24, cache.Indexers{})
		assert.NoError(t, err)

		ctx, cancel := context.WithCancel(ctx)
		defer cancel()

		assert.True(t, StartAndSyncInformer(ctx, podInformer))
		assert.True(t, StartAndSyncInformer(ctx, cmInformer))

		actions := dynClient.Actions()
		assert.Len(t, actions, 4)

		k8sTesting.AssertList(t, actions[0], "configmaps", metaV1.NamespaceAll, "", "")
		k8sTesting.AssertWatch(t, actions[1], "configmaps", metaV1.NamespaceAll, "", "", "")
		k8sTesting.AssertList(t, actions[2], "pods", metaV1.NamespaceAll, "", "")
		k8sTesting.AssertWatch(t, actions[3], "pods", metaV1.NamespaceAll, "", "", "")
	})

	t.Run("namespaced configMaps & pods", func(t *testing.T) {
		ctx, dynClient := k8sTesting.CreateFakeClient(context.Background())

		// when using the list watcher func to create a ListWatcher for an informer, it is safe to use context.Background
		newLw := NewFilteredListWatcherFunc(context.Background(), "some-namespace", nil)

		podInformer, err := NewSharedIndexInformer[*v1.ConfigMap](ctx, newLw, time.Hour*24, nil)
		assert.NoError(t, err)

		cmInformer, err := NewSharedIndexInformer[*v1.Pod](ctx, newLw, time.Hour*24, nil)
		assert.NoError(t, err)

		ctx, cancel := context.WithCancel(ctx)
		defer cancel()

		assert.True(t, StartAndSyncInformer(ctx, podInformer))
		assert.True(t, StartAndSyncInformer(ctx, cmInformer))

		actions := dynClient.Actions()
		assert.Len(t, actions, 4)

		k8sTesting.AssertList(t, actions[0], "configmaps", "some-namespace", "", "")
		k8sTesting.AssertWatch(t, actions[1], "configmaps", "some-namespace", "", "", "")
		k8sTesting.AssertList(t, actions[2], "pods", "some-namespace", "", "")
		k8sTesting.AssertWatch(t, actions[3], "pods", "some-namespace", "", "", "")
	})

	t.Run("global configMaps & pods + namespaced secrets on inherited context", func(t *testing.T) {
		ctx, dynClient := k8sTesting.CreateFakeClient(context.Background())

		podInformer, err := NewSharedIndexInformer[*v1.ConfigMap](ctx, nil, time.Hour*24, nil)
		assert.NoError(t, err)

		cmInformer, err := NewSharedIndexInformer[*v1.Pod](ctx, nil, time.Hour*24, nil)
		assert.NoError(t, err)

		// when using the list watcher func to create a ListWatcher for an informer, it is safe to use context.Background
		newLw := NewFilteredListWatcherFunc(context.Background(), "some-namespace", nil)
		scrtInformer, err := NewSharedIndexInformer[*v1.Secret](ctx, newLw, time.Hour*24, nil)
		assert.NoError(t, err)

		ctx, cancel := context.WithCancel(ctx)
		defer cancel()

		assert.True(t, StartAndSyncInformer(ctx, podInformer))
		assert.True(t, StartAndSyncInformer(ctx, cmInformer))
		assert.True(t, StartAndSyncInformer(ctx, scrtInformer))

		actions := dynClient.Actions()
		assert.Len(t, actions, 6)

		k8sTesting.AssertList(t, actions[0], "configmaps", metaV1.NamespaceAll, "", "")
		k8sTesting.AssertWatch(t, actions[1], "configmaps", metaV1.NamespaceAll, "", "", "")
		k8sTesting.AssertList(t, actions[2], "pods", metaV1.NamespaceAll, "", "")
		k8sTesting.AssertWatch(t, actions[3], "pods", metaV1.NamespaceAll, "", "", "")
		k8sTesting.AssertList(t, actions[4], "secrets", "some-namespace", "", "")
		k8sTesting.AssertWatch(t, actions[5], "secrets", "some-namespace", "", "", "")
	})

	t.Run("informer with list options tweaks", func(t *testing.T) {
		ctx, dynClient := k8sTesting.CreateFakeClient(context.Background())

		informer, err := NewSharedIndexInformer[*v1.ConfigMap](
			ctx,
			NewFilteredListWatcherFunc(context.Background(), metaV1.NamespaceAll, func(options *metaV1.ListOptions) {
				options.FieldSelector = "spec.nodeName=some-node"
				options.LabelSelector = "some-label=some-value"
			}),
			time.Hour*24,
			nil)
		assert.NoError(t, err)

		ctx, cancel := context.WithCancel(ctx)
		defer cancel()

		assert.True(t, StartAndSyncInformer(ctx, informer))

		actions := dynClient.Actions()
		assert.Len(t, actions, 2)

		k8sTesting.AssertList(t, actions[0], "configmaps", metaV1.NamespaceAll, "spec.nodeName=some-node", "some-label=some-value")
		k8sTesting.AssertWatch(t, actions[1], "configmaps", metaV1.NamespaceAll, "spec.nodeName=some-node", "some-label=some-value", "")
	})

	t.Run("namespaced informer with list options tweaks", func(t *testing.T) {
		ctx, dynClient := k8sTesting.CreateFakeClient(context.Background())

		informer, err := NewSharedIndexInformer[*v1.ConfigMap](
			ctx,
			NewFilteredListWatcherFunc(context.Background(), "some-namespace", func(options *metaV1.ListOptions) {
				options.FieldSelector = "spec.nodeName=some-node"
				options.LabelSelector = "some-label=some-value"
			}),
			time.Hour*24,
			nil)
		assert.NoError(t, err)

		ctx, cancel := context.WithCancel(ctx)
		defer cancel()

		assert.True(t, StartAndSyncInformer(ctx, informer))

		actions := dynClient.Actions()
		assert.Len(t, actions, 2)

		k8sTesting.AssertList(t, actions[0], "configmaps", "some-namespace", "spec.nodeName=some-node", "some-label=some-value")
		k8sTesting.AssertWatch(t, actions[1], "configmaps", "some-namespace", "spec.nodeName=some-node", "some-label=some-value", "")
	})

	t.Run("global configMaps with default indexing", func(t *testing.T) {
		ctx, dynClient := k8sTesting.CreateFakeClient(context.Background(), demoConfigMaps()...)

		informer, err := NewSharedIndexInformer[*v1.ConfigMap](ctx, nil, time.Hour*24, nil)
		assert.NoError(t, err)

		ctx, cancel := context.WithCancel(ctx)
		defer cancel()

		assert.True(t, StartAndSyncInformer(ctx, informer))

		actions := dynClient.Actions()
		assert.Len(t, actions, 2)

		k8sTesting.AssertList(t, actions[0], "configmaps", metaV1.NamespaceAll, "", "")
		k8sTesting.AssertWatch(t, actions[1], "configmaps", metaV1.NamespaceAll, "", "", "")

		indexer := informer.GetIndexer()
		assert.Len(t, indexer.List(), 5)
		assert.Len(t, indexer.GetIndexers(), 1)

		obj, found, _ := indexer.GetByKey("namespace-a/cm-a")
		assert.True(t, found)
		k8sTesting.AssertEqualObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-a", Namespace: "namespace-a", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, obj.(runtime.Object))

		obj, found, _ = indexer.GetByKey("namespace-a/cm-b")
		assert.True(t, found)
		k8sTesting.AssertEqualObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-b", Namespace: "namespace-a", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, obj.(runtime.Object))

		obj, found, _ = indexer.GetByKey("namespace-a/cm-c")
		assert.True(t, found)
		k8sTesting.AssertEqualObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-c", Namespace: "namespace-a", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, obj.(runtime.Object))

		obj, found, _ = indexer.GetByKey("namespace-b/cm-a")
		assert.True(t, found)
		k8sTesting.AssertEqualObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-a", Namespace: "namespace-b", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, obj.(runtime.Object))

		obj, found, _ = indexer.GetByKey("namespace-b/cm-b")
		assert.True(t, found)
		k8sTesting.AssertEqualObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-b", Namespace: "namespace-b", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, obj.(runtime.Object))

		indexEntries, err := indexer.ByIndex("namespace", "namespace-a")
		assert.NoError(t, err)
		assert.Len(t, indexEntries, 3)

		aIndexEntries := convertAll[runtime.Object](indexEntries)
		k8sTesting.AssertContainsObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-a", Namespace: "namespace-a", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, aIndexEntries)
		k8sTesting.AssertContainsObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-b", Namespace: "namespace-a", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, aIndexEntries)
		k8sTesting.AssertContainsObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-c", Namespace: "namespace-a", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, aIndexEntries)

		indexEntries, err = indexer.ByIndex("namespace", "namespace-b")
		assert.Len(t, indexEntries, 2)

		assert.NoError(t, err)
		bIndexEntries := convertAll[runtime.Object](indexEntries)
		k8sTesting.AssertContainsObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-a", Namespace: "namespace-b", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, bIndexEntries)
		k8sTesting.AssertContainsObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-b", Namespace: "namespace-b", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, bIndexEntries)
	})

	t.Run("global configMaps indexed by last name char", func(t *testing.T) {
		ctx, dynClient := k8sTesting.CreateFakeClient(context.Background(), demoConfigMaps()...)

		informer, err := NewSharedIndexInformer[*v1.ConfigMap](ctx, nil, time.Hour*24, cache.Indexers{
			"name": func(object interface{}) ([]string, error) {
				cm := v1.ConfigMap{}
				if err := scheme.Scheme.Convert(object, &cm, nil); err != nil {
					return nil, err
				}

				return []string{string(cm.Name[len(cm.Name)-1])}, nil
			},
		})
		assert.NoError(t, err)

		ctx, cancel := context.WithCancel(ctx)
		defer cancel()

		assert.True(t, StartAndSyncInformer(ctx, informer))

		actions := dynClient.Actions()
		assert.Len(t, actions, 2)

		k8sTesting.AssertList(t, actions[0], "configmaps", metaV1.NamespaceAll, "", "")
		k8sTesting.AssertWatch(t, actions[1], "configmaps", metaV1.NamespaceAll, "", "", "")

		indexer := informer.GetIndexer()
		assert.Len(t, indexer.List(), 5)
		assert.Len(t, indexer.GetIndexers(), 1)

		obj, found, _ := indexer.GetByKey("namespace-a/cm-a")
		assert.True(t, found)
		k8sTesting.AssertEqualObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-a", Namespace: "namespace-a", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, obj.(runtime.Object))

		obj, found, _ = indexer.GetByKey("namespace-a/cm-b")
		assert.True(t, found)
		k8sTesting.AssertEqualObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-b", Namespace: "namespace-a", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, obj.(runtime.Object))

		obj, found, _ = indexer.GetByKey("namespace-a/cm-c")
		assert.True(t, found)
		k8sTesting.AssertEqualObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-c", Namespace: "namespace-a", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, obj.(runtime.Object))

		obj, found, _ = indexer.GetByKey("namespace-b/cm-a")
		assert.True(t, found)
		k8sTesting.AssertEqualObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-a", Namespace: "namespace-b", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, obj.(runtime.Object))

		obj, found, _ = indexer.GetByKey("namespace-b/cm-b")
		assert.True(t, found)
		k8sTesting.AssertEqualObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-b", Namespace: "namespace-b", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, obj.(runtime.Object))

		indexEntries, err := indexer.ByIndex("name", "a")
		assert.NoError(t, err)
		assert.Len(t, indexEntries, 2)

		aIndexEntries := convertAll[runtime.Object](indexEntries)
		k8sTesting.AssertContainsObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-a", Namespace: "namespace-a", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, aIndexEntries)
		k8sTesting.AssertContainsObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-a", Namespace: "namespace-b", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, aIndexEntries)

		indexEntries, err = indexer.ByIndex("name", "b")
		assert.NoError(t, err)
		assert.Len(t, indexEntries, 2)

		bIndexEntries := convertAll[runtime.Object](indexEntries)
		k8sTesting.AssertContainsObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-b", Namespace: "namespace-a", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, bIndexEntries)
		k8sTesting.AssertContainsObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-b", Namespace: "namespace-b", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, bIndexEntries)

		indexEntries, err = indexer.ByIndex("name", "c")
		assert.NoError(t, err)
		assert.Len(t, indexEntries, 1)

		cIndexEntries := convertAll[runtime.Object](indexEntries)
		k8sTesting.AssertContainsObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-c", Namespace: "namespace-a", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, cIndexEntries)
	})

}
