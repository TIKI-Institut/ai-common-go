package kubernetes

import (
	"k8s.io/client-go/tools/cache"
)

type ConversionStage string

const ConversionStageAdd = ConversionStage("Add")
const ConversionStageUpdate = ConversionStage("Update")
const ConversionStageDelete = ConversionStage("Delete")

type ResourceFilter[TObj Object] func(TObj) bool

type TypedFilteringResourceEventHandler[TObj Object] struct {
	FilterFunc ResourceFilter[TObj]
	Handler    TypedResourceEventHandler[TObj]
}

func (t TypedFilteringResourceEventHandler[TObj]) OnAdd(obj TObj) {
	if !t.FilterFunc(obj) {
		return
	}
	t.Handler.OnAdd(obj)
}

func (t TypedFilteringResourceEventHandler[TObj]) OnUpdate(oldObj, newObj TObj) {
	newer := t.FilterFunc(newObj)
	older := t.FilterFunc(oldObj)
	switch {
	case newer && older:
		t.Handler.OnUpdate(oldObj, newObj)
	case newer && !older:
		t.Handler.OnAdd(newObj)
	case !newer && older:
		t.Handler.OnDelete(oldObj)
	default:
		// do nothing
	}
}

func (t TypedFilteringResourceEventHandler[TObj]) OnDelete(obj TObj) {
	if !t.FilterFunc(obj) {
		return
	}
	t.Handler.OnDelete(obj)
}

func (t TypedFilteringResourceEventHandler[TObj]) OnDeleteUnknown(i interface{}, key string) {
	t.Handler.OnDeleteUnknown(i, key)
}

func (t TypedFilteringResourceEventHandler[TObj]) OnConversionError(i interface{}, stage ConversionStage, conversionError error) {
	t.Handler.OnConversionError(i, stage, conversionError)
}

type TypedResourceEventHandler[TObj Object] interface {
	OnAdd(obj TObj)
	OnUpdate(oldObj, newObj TObj)
	OnDelete(obj TObj)
	OnDeleteUnknown(i interface{}, key string)
	OnConversionError(i interface{}, stage ConversionStage, conversionError error)
}

type TypedResourceEventHandlerFuncs[TObj Object] struct {
	OnAddFunc             func(obj TObj)
	OnUpdateFunc          func(oldObj, newObj TObj)
	OnDeleteFunc          func(obj TObj)
	OnDeleteUnknownFunc   func(i interface{}, key string)
	OnConversionErrorFunc func(i interface{}, stage ConversionStage, conversionError error)
}

func (t TypedResourceEventHandlerFuncs[TObj]) OnAdd(obj TObj) {
	if t.OnAddFunc != nil {
		t.OnAddFunc(obj)
	}
}
func (t TypedResourceEventHandlerFuncs[TObj]) OnUpdate(oldObj, newObj TObj) {
	if t.OnUpdateFunc != nil {
		t.OnUpdateFunc(oldObj, newObj)
	}
}
func (t TypedResourceEventHandlerFuncs[TObj]) OnDelete(obj TObj) {
	if t.OnDeleteFunc != nil {
		t.OnDeleteFunc(obj)
	}
}
func (t TypedResourceEventHandlerFuncs[TObj]) OnDeleteUnknown(i interface{}, key string) {
	if t.OnDeleteUnknownFunc != nil {
		t.OnDeleteUnknownFunc(i, key)
	}
}
func (t TypedResourceEventHandlerFuncs[TObj]) OnConversionError(i interface{}, stage ConversionStage, conversionError error) {
	if t.OnConversionErrorFunc != nil {
		t.OnConversionErrorFunc(i, stage, conversionError)
	}
}

type TypedSharedIndexInformer[TObj Object] interface {
	cache.SharedIndexInformer
	AddTypedEventHandler(handler TypedResourceEventHandler[TObj])
}

type typedSharedIndexInformer[TObj Object] struct {
	cache.SharedIndexInformer
}

func (t typedSharedIndexInformer[TObj]) AddTypedEventHandler(handler TypedResourceEventHandler[TObj]) {
	t.AddEventHandler(ConversionEventHandler[TObj]{handler})
}
