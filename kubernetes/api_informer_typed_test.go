package kubernetes

import (
	"errors"
	"github.com/stretchr/testify/assert"
	coreV1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"testing"
)

func TestTypedResourceEventHandlerFuncs(t *testing.T) {

	targetA := &coreV1.ConfigMap{Data: map[string]string{"blub": "A"}}
	targetB := &coreV1.ConfigMap{Data: map[string]string{"blub": "B"}}

	t.Run("OnAdd", func(t *testing.T) {
		called := false

		testee := TypedResourceEventHandlerFuncs[*coreV1.ConfigMap]{
			OnAddFunc: func(obj *coreV1.ConfigMap) {
				called = true
				assert.Same(t, targetA, obj)
			},
		}

		testee.OnAdd(targetA)
		assert.True(t, called)
	})

	t.Run("OnUpdate", func(t *testing.T) {
		called := false
		testee := TypedResourceEventHandlerFuncs[*coreV1.ConfigMap]{
			OnUpdateFunc: func(old, new *coreV1.ConfigMap) {
				called = true
				assert.Same(t, targetA, old)
				assert.Same(t, targetB, new)
			},
		}

		testee.OnUpdate(targetA, targetB)
		assert.True(t, called)
	})

	t.Run("OnDelete", func(t *testing.T) {
		called := false
		testee := TypedResourceEventHandlerFuncs[*coreV1.ConfigMap]{
			OnDeleteFunc: func(obj *coreV1.ConfigMap) {
				called = true
				assert.Same(t, targetA, obj)
			},
		}

		testee.OnDelete(targetA)
		assert.True(t, called)
	})

	t.Run("OnDeleteUnknown", func(t *testing.T) {
		called := false
		testee := TypedResourceEventHandlerFuncs[*coreV1.ConfigMap]{
			OnDeleteUnknownFunc: func(i interface{}, key string) {
				called = true
				assert.EqualValues(t, targetA, i)
				assert.EqualValues(t, "some-key", key)
			},
		}

		testee.OnDeleteUnknown(targetA, "some-key")
		assert.True(t, called)
	})

	t.Run("OnConversionErrorFunc", func(t *testing.T) {
		called := false
		targetObject := &coreV1.ConfigMap{Data: map[string]string{"blub": "A"}}
		targetError := errors.New("some-error")
		testee := TypedResourceEventHandlerFuncs[*coreV1.ConfigMap]{
			OnConversionErrorFunc: func(i interface{}, stage ConversionStage, conversionError error) {
				called = true
				assert.Same(t, targetObject, i)
				assert.EqualValues(t, ConversionStageDelete, stage)
				assert.Same(t, targetError, conversionError)
			},
		}

		testee.OnConversionError(targetObject, ConversionStageDelete, targetError)
		assert.True(t, called)
	})
}

func TestTypedFilteringResourceEventHandler(t *testing.T) {

	nonFilterMetadata := metav1.ObjectMeta{
		Labels: map[string]string{"filter": "false"},
	}

	targetA := &coreV1.ConfigMap{Data: map[string]string{"blub": "A"}}
	targetB := &coreV1.ConfigMap{Data: map[string]string{"blub": "B"}}

	targetAWithLabels := &coreV1.ConfigMap{Data: map[string]string{"blub": "A"}, ObjectMeta: *nonFilterMetadata.DeepCopy()}
	targetBWithLabels := &coreV1.ConfigMap{Data: map[string]string{"blub": "B"}, ObjectMeta: *nonFilterMetadata.DeepCopy()}

	labelFilterFunc := func(obj *coreV1.ConfigMap) bool { return len(obj.Labels) > 0 }

	t.Run("OnAdd", func(t *testing.T) {
		t.Run("filtered", func(t *testing.T) {
			called := false
			testee := TypedFilteringResourceEventHandler[*coreV1.ConfigMap]{
				FilterFunc: labelFilterFunc,
				Handler: TypedResourceEventHandlerFuncs[*coreV1.ConfigMap]{
					OnAddFunc: func(obj *coreV1.ConfigMap) {
						called = true
						assert.Same(t, targetA, obj)
					},
				},
			}

			testee.OnAdd(targetA)
			assert.False(t, called)
		})

		t.Run("unfiltered", func(t *testing.T) {
			called := false
			testee := TypedFilteringResourceEventHandler[*coreV1.ConfigMap]{
				FilterFunc: labelFilterFunc,
				Handler: TypedResourceEventHandlerFuncs[*coreV1.ConfigMap]{
					OnAddFunc: func(obj *coreV1.ConfigMap) {
						called = true
						assert.Same(t, targetAWithLabels, obj)
					},
				},
			}

			testee.OnAdd(targetAWithLabels)
			assert.True(t, called)
		})
	})

	t.Run("OnUpdate", func(t *testing.T) {

		t.Run("unfiltered", func(t *testing.T) {
			addCalled := false
			updateCalled := false
			deleteCalled := false

			testee := TypedFilteringResourceEventHandler[*coreV1.ConfigMap]{
				FilterFunc: labelFilterFunc,
				Handler: TypedResourceEventHandlerFuncs[*coreV1.ConfigMap]{
					OnUpdateFunc: func(oldObj, newObj *coreV1.ConfigMap) {
						updateCalled = true
						assert.Same(t, targetAWithLabels, oldObj)
						assert.Same(t, targetBWithLabels, newObj)
					},
					OnAddFunc: func(obj *coreV1.ConfigMap) {
						addCalled = true
					},
					OnDeleteFunc: func(obj *coreV1.ConfigMap) {
						deleteCalled = true
					},
				},
			}

			testee.OnUpdate(targetAWithLabels, targetBWithLabels)

			assert.False(t, addCalled)
			assert.True(t, updateCalled)
			assert.False(t, deleteCalled)
		})

		t.Run("old filtered", func(t *testing.T) {
			addCalled := false
			updateCalled := false
			deleteCalled := false

			testee := TypedFilteringResourceEventHandler[*coreV1.ConfigMap]{
				FilterFunc: labelFilterFunc,
				Handler: TypedResourceEventHandlerFuncs[*coreV1.ConfigMap]{
					OnUpdateFunc: func(oldObj, newObj *coreV1.ConfigMap) {
						updateCalled = true
					},
					OnAddFunc: func(obj *coreV1.ConfigMap) {
						addCalled = true
						assert.Same(t, targetBWithLabels, obj)
					},
					OnDeleteFunc: func(obj *coreV1.ConfigMap) {
						deleteCalled = true
					},
				},
			}

			testee.OnUpdate(targetA, targetBWithLabels)

			assert.True(t, addCalled)
			assert.False(t, updateCalled)
			assert.False(t, deleteCalled)
		})
		t.Run("new filtered", func(t *testing.T) {
			addCalled := false
			updateCalled := false
			deleteCalled := false

			testee := TypedFilteringResourceEventHandler[*coreV1.ConfigMap]{
				FilterFunc: labelFilterFunc,
				Handler: TypedResourceEventHandlerFuncs[*coreV1.ConfigMap]{
					OnUpdateFunc: func(oldObj, newObj *coreV1.ConfigMap) {
						updateCalled = true
					},
					OnAddFunc: func(obj *coreV1.ConfigMap) {
						addCalled = true
					},
					OnDeleteFunc: func(obj *coreV1.ConfigMap) {
						deleteCalled = true
						assert.Same(t, targetAWithLabels, obj)
					},
				},
			}

			testee.OnUpdate(targetAWithLabels, targetB)

			assert.False(t, addCalled)
			assert.False(t, updateCalled)
			assert.True(t, deleteCalled)
		})
		t.Run("both filtered", func(t *testing.T) {
			addCalled := false
			updateCalled := false
			deleteCalled := false

			testee := TypedFilteringResourceEventHandler[*coreV1.ConfigMap]{
				FilterFunc: labelFilterFunc,
				Handler: TypedResourceEventHandlerFuncs[*coreV1.ConfigMap]{
					OnUpdateFunc: func(oldObj, newObj *coreV1.ConfigMap) {
						updateCalled = true
					},
					OnAddFunc: func(obj *coreV1.ConfigMap) {
						addCalled = true
					},
					OnDeleteFunc: func(obj *coreV1.ConfigMap) {
						deleteCalled = true
					},
				},
			}

			testee.OnUpdate(targetA, targetB)

			assert.False(t, addCalled)
			assert.False(t, updateCalled)
			assert.False(t, deleteCalled)
		})
	})

	t.Run("OnDelete", func(t *testing.T) {
		t.Run("filtered", func(t *testing.T) {
			called := false
			testee := TypedFilteringResourceEventHandler[*coreV1.ConfigMap]{
				FilterFunc: labelFilterFunc,
				Handler: TypedResourceEventHandlerFuncs[*coreV1.ConfigMap]{
					OnDeleteFunc: func(obj *coreV1.ConfigMap) {
						called = true
						assert.Same(t, targetA, obj)
					},
				},
			}

			testee.OnDelete(targetA)
			assert.False(t, called)
		})

		t.Run("unfiltered", func(t *testing.T) {
			called := false
			testee := TypedFilteringResourceEventHandler[*coreV1.ConfigMap]{
				FilterFunc: labelFilterFunc,
				Handler: TypedResourceEventHandlerFuncs[*coreV1.ConfigMap]{
					OnDeleteFunc: func(obj *coreV1.ConfigMap) {
						called = true
						assert.Same(t, targetAWithLabels, obj)
					},
				},
			}

			testee.OnDelete(targetAWithLabels)
			assert.True(t, called)
		})
	})

	t.Run("OnDeleteUnknown", func(t *testing.T) {
		//shall never be filtered
		called := false
		testee := TypedFilteringResourceEventHandler[*coreV1.ConfigMap]{
			FilterFunc: func(obj *coreV1.ConfigMap) bool { return false },
			Handler: TypedResourceEventHandlerFuncs[*coreV1.ConfigMap]{
				OnDeleteUnknownFunc: func(i interface{}, key string) {
					called = true
					assert.EqualValues(t, targetA, i)
					assert.EqualValues(t, "some-key", key)
				},
			},
		}

		testee.OnDeleteUnknown(targetA, "some-key")
		assert.True(t, called)
	})

	t.Run("OnConversionErrorFunc", func(t *testing.T) {
		//shall never be filtered
		called := false
		targetObject := &coreV1.ConfigMap{Data: map[string]string{"blub": "A"}}
		targetError := errors.New("some-error")
		testee := TypedFilteringResourceEventHandler[*coreV1.ConfigMap]{
			FilterFunc: func(obj *coreV1.ConfigMap) bool { return false },
			Handler: TypedResourceEventHandlerFuncs[*coreV1.ConfigMap]{
				OnConversionErrorFunc: func(i interface{}, stage ConversionStage, conversionError error) {
					called = true
					assert.Same(t, targetObject, i)
					assert.EqualValues(t, ConversionStageDelete, stage)
					assert.Same(t, targetError, conversionError)
				},
			},
		}

		testee.OnConversionError(targetObject, ConversionStageDelete, targetError)
		assert.True(t, called)
	})
}
