package kubernetes

import (
	k8sTesting "bitbucket.org/TIKI-Institut/ai-common-go/v2/kubernetes/testing"
	"context"
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	appsV1 "k8s.io/api/apps/v1"
	coreV1 "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/utils/pointer"
	"testing"
	"time"
)

func TestKubernetesAPIObjectHandling(t *testing.T) {

	t.Run("reuse target with scheme.Convert", func(t *testing.T) {
		podA := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "name-foo",
				Namespace: "namespace-foo",
			},
			Spec: appsV1.DeploymentSpec{
				MinReadySeconds: 666,
			},
		}

		podB := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "another-name-foo",
				Namespace: "another-namespace-foo",
			},
			Spec: appsV1.DeploymentSpec{
				Replicas: pointer.Int32Ptr(3),
			},
		}

		expectedA := podA.DeepCopy()
		expectedB := podB.DeepCopy()

		unstructuredTemp := unstructured.Unstructured{}
		err := scheme.Scheme.Convert(&podA, &unstructuredTemp, nil)
		assert.NoError(t, err)

		podTemp := appsV1.Deployment{}

		err = scheme.Scheme.Convert(&unstructuredTemp, &podTemp, nil)
		assert.NoError(t, err)

		assert.EqualValues(t, expectedA, &podTemp)

		err = scheme.Scheme.Convert(&podB, &unstructuredTemp, nil)
		assert.NoError(t, err)

		err = scheme.Scheme.Convert(&unstructuredTemp, &podTemp, nil)
		assert.NoError(t, err)

		assert.EqualValues(t, expectedB, &podTemp)

	})

}

func TestKubernetesDynamicClientHandling(t *testing.T) {

	t.Run("list objects from multiple namespaces", func(t *testing.T) {
		podA := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "name-foo",
				Namespace: "namespace-foo",
			},
			Spec: appsV1.DeploymentSpec{
				MinReadySeconds: 666,
			},
		}

		podB := appsV1.Deployment{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "another-name-foo",
				Namespace: "another-namespace-foo",
			},
			Spec: appsV1.DeploymentSpec{
				Replicas: pointer.Int32Ptr(3),
			},
		}

		ctx, dynamicClient := k8sTesting.CreateFakeClient(nil, k8sTesting.ConvertToUnstructured(&podA), k8sTesting.ConvertToUnstructured(&podB))

		cl, _, _, err := ClientForObject(ctx, &appsV1.Deployment{}, "")
		assert.NoError(t, err)

		result, err := cl.List(ctx, metaV1.ListOptions{})
		assert.NoError(t, err)

		assert.Len(t, result.Items, 2)

		actions := dynamicClient.Actions()
		assert.Len(t, actions, 1)
		k8sTesting.AssertList(t, actions[0], "deployments", "", "", "")
	})

}

func TestPatchHasChanges(t *testing.T) {

	t.Run("normal patch", func(t *testing.T) {
		assert.True(t, patchHasChanges([]byte(`{"peter":"pan""}`)))
	})

	t.Run("no changes patch", func(t *testing.T) {
		assert.False(t, patchHasChanges([]byte(`{}`)))
	})

	t.Run("malformed", func(t *testing.T) {
		assert.Panics(t, func() {
			patchHasChanges([]byte(``))
		})
	})

}

func TestInternalK8sProperties(t *testing.T) {

	baseCm := func() *coreV1.ConfigMap {
		return &coreV1.ConfigMap{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "name-foo",
				Namespace: "namespace-foo",
			},
		}
	}

	t.Run("patching", func(t *testing.T) {

		t.Run("SelfLink", func(t *testing.T) {
			cm := baseCm()
			cm.SelfLink = "some-SelfLink"

			ctx, client := k8sTesting.CreateFakeClient(context.Background(), cm)

			newCm := baseCm()
			newCm.Data = map[string]string{"bar": "foo"}

			_, err := Apply(ctx, newCm, "", "", false)
			assert.NoError(t, err)

			actions := client.Actions()
			assert.Len(t, actions, 2)
			k8sTesting.AssertGet(t, actions[0], "configmaps", "namespace-foo", "name-foo")
			k8sTesting.AssertPatch(t, actions[1], "configmaps", "namespace-foo", "name-foo", types.StrategicMergePatchType, []byte(`{"data":{"bar":"foo"}}`))
		})

		t.Run("UID", func(t *testing.T) {
			cm := baseCm()
			cm.UID = "some-UID"

			ctx, client := k8sTesting.CreateFakeClient(context.Background(), cm)

			newCm := baseCm()
			newCm.Data = map[string]string{"bar": "foo"}

			_, err := Apply(ctx, newCm, "", "", false)
			assert.NoError(t, err)

			actions := client.Actions()
			assert.Len(t, actions, 2)
			k8sTesting.AssertGet(t, actions[0], "configmaps", "namespace-foo", "name-foo")
			k8sTesting.AssertPatch(t, actions[1], "configmaps", "namespace-foo", "name-foo", types.StrategicMergePatchType, []byte(`{"data":{"bar":"foo"}}`))
		})

		t.Run("ResourceVersion", func(t *testing.T) {
			cm := baseCm()
			cm.ResourceVersion = "some-ResourceVersion"

			ctx, client := k8sTesting.CreateFakeClient(context.Background(), cm)

			newCm := baseCm()
			newCm.Data = map[string]string{"bar": "foo"}

			_, err := Apply(ctx, newCm, "", "", false)
			assert.NoError(t, err)

			actions := client.Actions()
			assert.Len(t, actions, 2)
			k8sTesting.AssertGet(t, actions[0], "configmaps", "namespace-foo", "name-foo")
			k8sTesting.AssertPatch(t, actions[1], "configmaps", "namespace-foo", "name-foo", types.StrategicMergePatchType, []byte(`{"data":{"bar":"foo"}}`))
		})

		t.Run("Generation", func(t *testing.T) {
			cm := baseCm()
			cm.Generation = 15

			ctx, client := k8sTesting.CreateFakeClient(context.Background(), cm)

			newCm := baseCm()
			newCm.Data = map[string]string{"bar": "foo"}

			_, err := Apply(ctx, newCm, "", "", false)
			assert.NoError(t, err)

			actions := client.Actions()
			assert.Len(t, actions, 2)
			k8sTesting.AssertGet(t, actions[0], "configmaps", "namespace-foo", "name-foo")
			k8sTesting.AssertPatch(t, actions[1], "configmaps", "namespace-foo", "name-foo", types.StrategicMergePatchType, []byte(`{"data":{"bar":"foo"}}`))
		})

		t.Run("CreationTimestamp", func(t *testing.T) {
			cm := baseCm()
			cm.CreationTimestamp = metaV1.Now()

			ctx, client := k8sTesting.CreateFakeClient(context.Background(), cm)

			newCm := baseCm()
			newCm.Data = map[string]string{"bar": "foo"}

			_, err := Apply(ctx, newCm, "", "", false)
			assert.NoError(t, err)

			actions := client.Actions()
			assert.Len(t, actions, 2)
			k8sTesting.AssertGet(t, actions[0], "configmaps", "namespace-foo", "name-foo")
			k8sTesting.AssertPatch(t, actions[1], "configmaps", "namespace-foo", "name-foo", types.StrategicMergePatchType, []byte(`{"data":{"bar":"foo"}}`))
		})

		t.Run("ManagedFields", func(t *testing.T) {
			cm := baseCm()
			cm.ManagedFields = []metaV1.ManagedFieldsEntry{{Manager: "blub"}}

			ctx, client := k8sTesting.CreateFakeClient(context.Background(), cm)

			newCm := baseCm()
			newCm.Data = map[string]string{"bar": "foo"}

			_, err := Apply(ctx, newCm, "", "", false)
			assert.NoError(t, err)

			actions := client.Actions()
			assert.Len(t, actions, 2)
			k8sTesting.AssertGet(t, actions[0], "configmaps", "namespace-foo", "name-foo")
			k8sTesting.AssertPatch(t, actions[1], "configmaps", "namespace-foo", "name-foo", types.StrategicMergePatchType, []byte(`{"data":{"bar":"foo"}}`))
		})
	})

	t.Run("creating", func(t *testing.T) {

		t.Run("SelfLink", func(t *testing.T) {
			cm := baseCm()
			cm.SelfLink = "some-SelfLink"

			ctx, client := k8sTesting.CreateFakeClient(context.Background())

			_, err := Apply(ctx, cm, "", "", false)
			assert.NoError(t, err)

			actions := client.Actions()
			assert.Len(t, actions, 2)
			k8sTesting.AssertGet(t, actions[0], "configmaps", "namespace-foo", "name-foo")
			k8sTesting.AssertCreate(t, actions[1], "configmaps", "namespace-foo", baseCm())
		})

		t.Run("UID", func(t *testing.T) {
			cm := baseCm()
			cm.UID = "some-UID"

			ctx, client := k8sTesting.CreateFakeClient(context.Background())

			_, err := Apply(ctx, cm, "", "", false)
			assert.NoError(t, err)

			actions := client.Actions()
			assert.Len(t, actions, 2)
			k8sTesting.AssertGet(t, actions[0], "configmaps", "namespace-foo", "name-foo")
			k8sTesting.AssertCreate(t, actions[1], "configmaps", "namespace-foo", baseCm())
		})

		t.Run("ResourceVersion", func(t *testing.T) {
			cm := baseCm()
			cm.ResourceVersion = "some-ResourceVersion"

			ctx, client := k8sTesting.CreateFakeClient(context.Background())

			_, err := Apply(ctx, cm, "", "", false)
			assert.NoError(t, err)

			actions := client.Actions()
			assert.Len(t, actions, 2)
			k8sTesting.AssertGet(t, actions[0], "configmaps", "namespace-foo", "name-foo")
			k8sTesting.AssertCreate(t, actions[1], "configmaps", "namespace-foo", baseCm())
		})

		t.Run("Generation", func(t *testing.T) {
			cm := baseCm()
			cm.Generation = 15

			ctx, client := k8sTesting.CreateFakeClient(context.Background())

			_, err := Apply(ctx, cm, "", "", false)
			assert.NoError(t, err)

			actions := client.Actions()
			assert.Len(t, actions, 2)
			k8sTesting.AssertGet(t, actions[0], "configmaps", "namespace-foo", "name-foo")
			k8sTesting.AssertCreate(t, actions[1], "configmaps", "namespace-foo", baseCm())
		})

		t.Run("CreationTimestamp", func(t *testing.T) {
			cm := baseCm()
			cm.CreationTimestamp = metaV1.Now()

			ctx, client := k8sTesting.CreateFakeClient(context.Background())

			_, err := Apply(ctx, cm, "", "", false)
			assert.NoError(t, err)

			actions := client.Actions()
			assert.Len(t, actions, 2)
			k8sTesting.AssertGet(t, actions[0], "configmaps", "namespace-foo", "name-foo")
			k8sTesting.AssertCreate(t, actions[1], "configmaps", "namespace-foo", baseCm())
		})

		t.Run("ManagedFields", func(t *testing.T) {
			cm := baseCm()
			cm.ManagedFields = []metaV1.ManagedFieldsEntry{{Manager: "blub"}}

			ctx, client := k8sTesting.CreateFakeClient(context.Background())

			_, err := Apply(ctx, cm, "", "", false)
			assert.NoError(t, err)

			actions := client.Actions()
			assert.Len(t, actions, 2)
			k8sTesting.AssertGet(t, actions[0], "configmaps", "namespace-foo", "name-foo")
			k8sTesting.AssertCreate(t, actions[1], "configmaps", "namespace-foo", baseCm())
		})
	})

}

func TestBasicK8sConversions(t *testing.T) {
	t.Run("timestamps", func(t *testing.T) {
		t.Run("timestamp conversion through deserializer", func(t *testing.T) {
			obj := `{"apiVersion":"v1","kind":"ConfigMap","metadata":{"creationTimestamp":"2022-10-19T14:10:00Z","name":"cm-new","namespace":"namespace-b"}}`

			cm := coreV1.ConfigMap{}
			codecFactory := serializer.NewCodecFactory(scheme.Scheme)
			deserializer := codecFactory.UniversalDeserializer()
			decodedObject, _, err := deserializer.Decode([]byte(obj), nil, &cm)
			require.NoError(t, err)

			assert.EqualValues(t, k8sTesting.FakeCreationTimestamp, decodedObject.(*coreV1.ConfigMap).CreationTimestamp)
		})

		t.Run("timestamp conversion through conversion", func(t *testing.T) {
			obj := `{"apiVersion":"v1","kind":"ConfigMap","metadata":{"creationTimestamp":"2022-10-19T14:10:00Z","name":"cm-new","namespace":"namespace-b"}}`

			unstructuredObj := unstructured.Unstructured{}
			require.NoError(t, json.Unmarshal([]byte(obj), &unstructuredObj))

			convertedObj := coreV1.ConfigMap{}
			require.NoError(t, scheme.Scheme.Convert(&unstructuredObj, &convertedObj, nil))

			assert.EqualValues(t, k8sTesting.FakeCreationTimestamp, convertedObj.CreationTimestamp)
		})

		t.Run("dst checks", func(t *testing.T) {

			loc, err := time.LoadLocation("Europe/Berlin")
			require.NoError(t, err)

			t.Run("non dst", func(t *testing.T) {
				ts := time.Date(2023, time.October, 28, 0, 0, 00, 0, loc)
				assert.EqualValues(t, "2023-10-28T00:00:00+02:00", ts.Format(time.RFC3339))
			})

			t.Run("dst", func(t *testing.T) {
				ts := time.Date(2023, time.October, 30, 0, 0, 00, 0, loc)
				assert.EqualValues(t, "2023-10-30T00:00:00+01:00", ts.Format(time.RFC3339))
			})
		})

	})

}
