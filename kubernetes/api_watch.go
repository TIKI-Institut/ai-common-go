package kubernetes

import (
	"context"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/watch"
)

func Watch(ctx context.Context, gvk schema.GroupVersionKind, namespace string, options metaV1.ListOptions) (watch.Interface, error) {

	// 1. get dynamic client
	client, _, err := Client(ctx, gvk, namespace)
	if err != nil {
		return nil, err
	}

	unstructuredResult, err := client.Watch(ctx, options)

	if err != nil {
		return nil, err
	}

	return unstructuredResult, nil
}
