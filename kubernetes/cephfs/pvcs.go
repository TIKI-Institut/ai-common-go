package cephfs

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/config"
	commonK8s "bitbucket.org/TIKI-Institut/ai-common-go/v2/kubernetes"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/kubernetes/watch"
	"context"
	"fmt"
	"github.com/rs/zerolog/log"
	"gopkg.in/ini.v1"
	coreV1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/kubernetes/scheme"
	"path/filepath"
	"strings"
	"sync"
)

const csiDriverName = "cephfs.csi.ceph.com"
const csiDriverAttributeSubvolumeName = "subvolumeName"
const metaFile = ".meta"
const targetSection = "GLOBAL"
const targetProperty = "path"

// NewPersistentVolumeClaims creates a new continuously updated map of PersistentVolumeClaim objects
// ensure to provide a K8s API configuration in the given context
// given K8s API Client in the context is either reused or new client is created if none exists
func NewPersistentVolumeClaims(ctx context.Context, cephConfig *config.CephFs) (*PersistentVolumeClaims, error) {
	log.Ctx(ctx).Debug().Msg("Collecting PersistentVolumeClaim objects")

	pvcMap := &PersistentVolumeClaims{
		context:    ctx,
		cephConfig: cephConfig,
		pvcMap:     map[PvcKey]*PvcValue{},
	}

	pvcGvk, err := commonK8s.GroupVersionKindForObject(&coreV1.PersistentVolumeClaim{})
	if err != nil {
		return nil, fmt.Errorf("unable to resolve GroupVersionKind for Kind 'PersistentVolumeClaim'")
	}

	eventHandlers := watch.NewEventHandlers(pvcGvk, []watch.Adder{pvcMap}, []watch.Modifier{pvcMap}, []watch.Deleter{pvcMap})
	if err = watch.Watch(ctx, eventHandlers); err != nil {
		return nil, err
	}

	return pvcMap, nil
}

// NewStaticPersistentVolumeClaims create a new instance of PersistentVolumeClaims without manipulating pvc map
// this can be used for testing or when a static pvc reference is enough
func NewStaticPersistentVolumeClaims(pvcMap map[PvcKey]*PvcValue) *PersistentVolumeClaims {
	return &PersistentVolumeClaims{pvcMap: pvcMap}
}

type PvcKey struct {
	Key       string
	Namespace string
}

// CutPrincipalNameFromPvcNamespace remove the principal prefix of pvc namespace
// this will remove the principal prefix e.g. "test-" from the pvc namespace
func (p PvcKey) CutPrincipalNameFromPvcNamespace(principal string) string {
	keyEntry := p.Key
	principalName := principal + "-"

	if strings.HasPrefix(p.Namespace, principalName) { // is a principal project namespace
		virtualDir := strings.Replace(p.Namespace, principalName, "", 1)
		if virtualDir != "" {
			keyEntry = fmt.Sprintf("%s-%s", virtualDir, p.Key)
		}
	}

	return keyEntry
}

type PvcValue struct {
	Error          error
	FullVolumePath string
	PVC            *coreV1.PersistentVolumeClaim

	// Note: PV can be nil, e.g. for unbound PVCs
	PV *coreV1.PersistentVolume
}

type PersistentVolumeClaims struct {
	context    context.Context
	cephConfig *config.CephFs
	pvcMap     map[PvcKey]*PvcValue
	mutex      sync.RWMutex
}

func (v *PersistentVolumeClaims) Add(obj runtime.Object) error {
	v.mutex.Lock()
	defer v.mutex.Unlock()

	return v.addOrUpdate(obj)
}

func (v *PersistentVolumeClaims) Modify(obj runtime.Object) error {
	v.mutex.Lock()
	defer v.mutex.Unlock()

	return v.addOrUpdate(obj)
}

func (v *PersistentVolumeClaims) Delete(obj runtime.Object) error {
	v.mutex.Lock()
	defer v.mutex.Unlock()

	// get corresponding PV
	if pvc, err := toPvc(obj); err != nil {
		return err
	} else {
		log.Debug().Str("namespace", pvc.ObjectMeta.Namespace).Str("pvc-name", pvc.ObjectMeta.Name).Msg("deleting pvc from collector")
		key := toKey(pvc)
		delete(v.pvcMap, key)
		return nil
	}
}

// All returns a new copy of the pvc map for safe iteration
func (v *PersistentVolumeClaims) All() map[PvcKey]*PvcValue {
	v.mutex.RLock()
	defer v.mutex.RUnlock()

	newCollection := map[PvcKey]*PvcValue{}

	for key, value := range v.pvcMap {
		var newValue PvcValue
		newValue = *value
		newCollection[key] = &newValue
	}

	return newCollection
}

// Filter will return a filtered copy of the pvc map for save iteration
func (v *PersistentVolumeClaims) Filter(filter func(key PvcKey, value *PvcValue) bool) map[PvcKey]*PvcValue {
	v.mutex.RLock()
	defer v.mutex.RUnlock()

	newCollection := map[PvcKey]*PvcValue{}

	for key, value := range v.pvcMap {
		if filter(key, value) {
			var newValue PvcValue
			newValue = *value
			newCollection[key] = &newValue
		}
	}

	return newCollection
}

func toPvc(obj runtime.Object) (*coreV1.PersistentVolumeClaim, error) {
	persistentVolumeClaim := &coreV1.PersistentVolumeClaim{}

	if err := scheme.Scheme.Convert(obj, persistentVolumeClaim, nil); err != nil {
		log.Error().AnErr("could not convert runtime object to pvc struct", err)
		return nil, err
	}

	return persistentVolumeClaim, nil
}

func toKey(pvc *coreV1.PersistentVolumeClaim) PvcKey {
	return PvcKey{
		Key:       pvc.Name,
		Namespace: pvc.Namespace,
	}
}

func (v *PersistentVolumeClaims) addOrUpdate(obj runtime.Object) error {
	pvc, err := toPvc(obj)

	if err != nil {
		return err
	}

	// check for the optional StorageClass filter
	if v.cephConfig.StorageClass == "" || (pvc.Spec.StorageClassName != nil && *pvc.Spec.StorageClassName == v.cephConfig.StorageClass) {
		log.Trace().Str("namespace", pvc.ObjectMeta.Namespace).Str("pvc-name", pvc.ObjectMeta.Name).Msg("adding PVC to collector")

		key := toKey(pvc)

		value := &PvcValue{
			PVC: pvc,
		}

		if pvc.Spec.VolumeName != "" {
			pv, err := getAssociatedPV(v.context, pvc)

			if errors.IsNotFound(err) {
				// Noop
			} else if err != nil {
				value.Error = err
			} else {
				fullPath, err := resolvePVCPath(v.cephConfig, pv)
				value.FullVolumePath = fullPath
				value.Error = err
				value.PV = pv
			}
		}

		v.pvcMap[key] = value
	} else {
		storageClassName := "<empty>"

		if pvc.Spec.StorageClassName != nil {
			storageClassName = *pvc.Spec.StorageClassName
		}

		log.Warn().Str("namespace", pvc.ObjectMeta.Namespace).Str("name", pvc.ObjectMeta.Name).Str("storage-class", storageClassName).Msg("skipping pvc with unknown storage class")
	}

	return nil
}

func getAssociatedPV(ctx context.Context, pvc *coreV1.PersistentVolumeClaim) (*coreV1.PersistentVolume, error) {
	pv := &coreV1.PersistentVolume{ObjectMeta: metaV1.ObjectMeta{Name: pvc.Spec.VolumeName}}
	err := commonK8s.GetObject(ctx, pv)

	if err != nil {
		return nil, err
	}

	return pv, nil
}

func resolvePVCPath(config *config.CephFs, pv *coreV1.PersistentVolume) (string, error) {
	volumeName, err := resolveSubVolumeName(pv)

	if err != nil {
		return "", err
	}

	subVolumeDirectory := filepath.ToSlash(filepath.Clean(filepath.Join(config.CephFsMount,
		config.CephFsVolumeDirectory,
		volumeName)))
	log.Trace().Msgf("Reading meta file of PV %q with path '%+++v'", pv.Name, subVolumeDirectory)

	localCephMountPath, err := resolveSubVolumePath(subVolumeDirectory)
	if err != nil {
		return "", err
	}

	fullMountPath := filepath.ToSlash(filepath.Clean(filepath.Join(config.CephFsMount, localCephMountPath)))
	return fullMountPath, nil
}

func resolveSubVolumeName(pv *coreV1.PersistentVolume) (string, error) {
	//asserting we really have a csi provisioned PV here...
	if pv.Spec.CSI == nil {
		return "", fmt.Errorf("pv %s in namespce %s is not provisioned by an CSI plugin", pv.Name, pv.Namespace)
	}

	//asserting if we really have a cephfs-pvc provisioned PV here...
	if pv.Spec.CSI.Driver != csiDriverName {
		return "", fmt.Errorf("pv %s in namespce %s is not provisioned by %s but by %s", pv.Name, pv.Namespace, csiDriverName, pv.Spec.CSI.Driver)
	}

	//asserting expected cephfs-csi-configuration
	subVolume, found := pv.Spec.CSI.VolumeAttributes[csiDriverAttributeSubvolumeName]
	if !found {
		return "", fmt.Errorf("missing attribute %s in cephfs-csi driver attributes of pv %s in namespce %s", csiDriverAttributeSubvolumeName, pv.Name, pv.Namespace)
	}

	return subVolume, nil
}

func resolveSubVolumePath(path string) (string, error) {

	targetFile := filepath.Join(path, metaFile)

	iniFile, err := ini.Load(targetFile)

	if err != nil {
		return "", fmt.Errorf("unable to load ini file (%v)", err)
	}

	section, err := iniFile.GetSection(targetSection)
	if err != nil {
		return "", fmt.Errorf("unable to retrieve section %s (%v)", targetSection, err)
	}

	if section == nil {
		return "", fmt.Errorf("section %q not found in metafile", targetSection)
	}

	key, err := section.GetKey(targetProperty)

	if err != nil {
		return "", fmt.Errorf("unable to retrieve property %s (%v)", targetProperty, err)
	}

	if key == nil {
		return "", fmt.Errorf("key %q not found in section", targetProperty)
	}

	return key.Value(), nil
}
