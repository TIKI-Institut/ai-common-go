package cephfs

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/config"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/kubernetes"
	kubernetesTesting "bitbucket.org/TIKI-Institut/ai-common-go/v2/kubernetes/testing"
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"testing"
	"time"
)

func pvcs(name, ns, label string) []*v1.PersistentVolumeClaim {

	var retVal []*v1.PersistentVolumeClaim

	for i := 1; i <= 10; i++ {
		retVal = append(retVal, &v1.PersistentVolumeClaim{
			ObjectMeta: metav1.ObjectMeta{
				Labels:    map[string]string{"tiki-dsp.io/principal": fmt.Sprintf("%s-%d", label, i)},
				Name:      fmt.Sprintf("%s-%d", name, i),
				Namespace: fmt.Sprintf("%s-%d", ns, i),
			},
			Spec: v1.PersistentVolumeClaimSpec{},
		})
	}

	// add principal user pvc
	retVal = append(retVal, &v1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{
			Labels:    map[string]string{"tiki-dsp.io/principal": "principal", "principal-user": "some-user"},
			Name:      name + "-" + "some-user",
			Namespace: "principal",
		},
		Spec: v1.PersistentVolumeClaimSpec{},
	})

	return retVal
}

func TestPrincipalCollection(t *testing.T) {
	t.Parallel()

	testCollector := PersistentVolumeClaims{
		context:    context.TODO(),
		cephConfig: nil,
		// test with an empty pvcMap
		pvcMap: map[PvcKey]*PvcValue{},
	}

	// add elements to pvcMap
	for _, item := range pvcs("name", "ns", "ns") {
		testCollector.pvcMap[PvcKey{
			Key:       item.Name,
			Namespace: item.Namespace,
		}] = &PvcValue{
			Error:          nil,
			FullVolumePath: "",
			PV:             nil,
			PVC:            item,
		}
	}

	// all
	collection := testCollector.All()

	assert.NotNil(t, collection)
	assert.Len(t, collection, 11)

	// filter
	collection = testCollector.Filter(func(key PvcKey, value *PvcValue) bool {
		return key.Key == "name-1"
	})
	assert.NotNil(t, collection)
	assert.Len(t, collection, 1)
}

func TestCollectorConcurrency(t *testing.T) {
	collectorConcurrent := PersistentVolumeClaims{
		context:    context.TODO(),
		cephConfig: nil,
		// test with an empty pvcMap
		pvcMap: map[PvcKey]*PvcValue{},
	}

	pvcs := pvcs("name", "ns", "ns")

	for _, item := range pvcs {
		collectorConcurrent.pvcMap[PvcKey{
			Key:       item.Name,
			Namespace: item.Namespace,
		}] = &PvcValue{
			Error:          nil,
			FullVolumePath: "",
			PV:             nil,
			PVC:            item,
		}
	}

	go func() {
		for _, item := range collectorConcurrent.All() {
			assert.NotNil(t, item.PVC)
		}
	}()

	go func() {
		err := collectorConcurrent.Delete(pvcs[0])
		assert.NoError(t, err)
	}()
}

func TestCollectionEventHandlers(t *testing.T) {
	cephFSStorageClassNameString := "ceph-fs"

	collectorConcurrent := PersistentVolumeClaims{
		context:    context.TODO(),
		cephConfig: &config.CephFs{StorageClass: cephFSStorageClassNameString},
		// test with an empty pvcMap
		pvcMap: map[PvcKey]*PvcValue{},
	}

	pvcs := pvcs("name", "ns", "ns")

	for i, item := range pvcs {
		if i%2 == 0 {
			item.Spec.StorageClassName = &cephFSStorageClassNameString
		}

		err := collectorConcurrent.Add(item)
		assert.NoError(t, err)
	}

	modifiedPvc := pvcs[0]
	modifiedPvc.Annotations = map[string]string{"test": "test"}

	err := collectorConcurrent.Modify(modifiedPvc)
	assert.NoError(t, err)

	err = collectorConcurrent.Delete(modifiedPvc)
	assert.NoError(t, err)
}

func TestPVCCollectorWithStorageClassFilter(t *testing.T) {
	storageClassFilter := "dummy-storage-class"
	ctx := context.TODO()

	pvc := &v1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "some-user-pvc",
			Namespace: "principal",
		},
		Spec: v1.PersistentVolumeClaimSpec{},
	}

	pvc2 := &v1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "another-user-pvc",
			Namespace: "principal",
		},
		Spec: v1.PersistentVolumeClaimSpec{StorageClassName: &storageClassFilter},
	}

	ctx, _ = kubernetesTesting.CreateFakeClient(ctx)

	collection, err := NewPersistentVolumeClaims(ctx, &config.CephFs{StorageClass: storageClassFilter})
	assert.NoError(t, err)

	err = collection.Add(pvc)
	assert.NoError(t, err)

	err = collection.Add(pvc2)
	assert.NoError(t, err)

	assert.Len(t, collection.All(), 1)
	for key, value := range collection.All() {
		assert.Equal(t, toKey(pvc2), key)
		assert.Equal(t, pvc2, value.PVC)
	}
}

func TestPVCCollectorWatch(t *testing.T) {
	ctx := context.TODO()

	pvcs := pvcs("name", "test", "ns")

	ctx, _ = kubernetesTesting.CreateFakeClient(ctx)

	collection, err := NewPersistentVolumeClaims(ctx, &config.CephFs{})
	assert.NoError(t, err)

	for _, pvc := range pvcs {
		_, err = kubernetes.Apply(ctx, pvc, "", "", true)
		assert.NoError(t, err)
	}

	assert.Eventually(t, func() bool { return len(collection.All()) == 11 }, time.Second, 100*time.Millisecond)

	modifiedPVC := pvcs[1]
	modifiedPVC.Annotations = map[string]string{"test": "test"}

	_, err = kubernetes.Apply(ctx, pvcs[1], "", "", true)
	assert.NoError(t, err)

	assert.Eventually(t, func() bool { return len(collection.All()) == 11 }, time.Second, 100*time.Millisecond)

	err = kubernetes.DeleteObject(ctx, pvcs[0])
	assert.NoError(t, err)

	assert.Eventually(t, func() bool { return len(collection.All()) == 10 }, time.Second, 100*time.Millisecond)
}

func TestResolveSubVolumeName(t *testing.T) {

	t.Run("normal", func(t *testing.T) {
		path, err := resolveSubVolumeName(&v1.PersistentVolume{
			Spec: v1.PersistentVolumeSpec{
				PersistentVolumeSource: v1.PersistentVolumeSource{
					CSI: &v1.CSIPersistentVolumeSource{
						Driver:           "cephfs.csi.ceph.com",
						VolumeAttributes: map[string]string{"subvolumeName": "some-volume-name"},
					},
				},
			},
			Status: v1.PersistentVolumeStatus{},
		})

		assert.NoError(t, err)
		assert.EqualValues(t, "some-volume-name", path)
	})

	t.Run("no csi", func(t *testing.T) {
		path, err := resolveSubVolumeName(&v1.PersistentVolume{
			Spec: v1.PersistentVolumeSpec{
				PersistentVolumeSource: v1.PersistentVolumeSource{
					CSI: nil,
				},
			},
			Status: v1.PersistentVolumeStatus{},
		})

		assert.EqualValues(t, "", path)
		assert.NotNil(t, err)

	})

	t.Run("wrong driver", func(t *testing.T) {
		path, err := resolveSubVolumeName(&v1.PersistentVolume{
			Spec: v1.PersistentVolumeSpec{
				PersistentVolumeSource: v1.PersistentVolumeSource{
					CSI: &v1.CSIPersistentVolumeSource{
						Driver:           "cephdddasdfsafs.csi.ceph.com",
						VolumeAttributes: map[string]string{"subvolumeName": "some-volume-name"},
					},
				},
			},
			Status: v1.PersistentVolumeStatus{},
		})

		assert.EqualValues(t, "", path)
		assert.NotNil(t, err)

	})

	t.Run("missing property", func(t *testing.T) {
		path, err := resolveSubVolumeName(&v1.PersistentVolume{
			Spec: v1.PersistentVolumeSpec{
				PersistentVolumeSource: v1.PersistentVolumeSource{
					CSI: &v1.CSIPersistentVolumeSource{
						Driver:           "cephfs.csi.ceph.com",
						VolumeAttributes: map[string]string{},
					},
				},
			},
			Status: v1.PersistentVolumeStatus{},
		})

		assert.EqualValues(t, "", path)
		assert.NotNil(t, err)

	})

}

func TestReadSubVolumePathFromMeta(t *testing.T) {

	t.Run("correct", func(t *testing.T) {
		path, err := resolveSubVolumePath("./testdata/correct")
		assert.NoError(t, err)
		assert.EqualValues(t, "/volumes/csi/csi-vol-70cbf2c4-74f7-11eb-9feb-56e27bcabbb6/8740d716-1d11-4166-8b4b-bf08853f7940", path)
	})

	t.Run("incorrect_property_not_found", func(t *testing.T) {
		path, err := resolveSubVolumePath("./testdata/incorrect_property_not_found")
		assert.EqualValues(t, "", path)
		assert.NotNil(t, err)
	})

	t.Run("incorrect_section_not_found", func(t *testing.T) {
		path, err := resolveSubVolumePath("./testdata/incorrect_section_not_found")
		assert.EqualValues(t, "", path)
		assert.NotNil(t, err)
	})

	t.Run("missing_meta_file", func(t *testing.T) {
		path, err := resolveSubVolumePath("./testdata/missing_meta_file")
		assert.EqualValues(t, "", path)
		assert.NotNil(t, err)
	})

}

func Test_pvcKey_CutPrincipalNameFromPvcNamespace(t *testing.T) {

	principalName := "test"

	tests := []struct {
		name string
		key  PvcKey
		want string
	}{
		{
			name: "project namespace",
			key: PvcKey{
				Key:       "some-pvc-name",
				Namespace: "test-project-name-environment",
			},
			want: "project-name-environment-some-pvc-name",
		},
		{
			name: "project namespace short",
			key: PvcKey{
				Key:       "pvc",
				Namespace: "test-project-env",
			},
			want: "project-env-pvc",
		},
		{
			name: "a pvc in principal namespace",
			key: PvcKey{
				Key:       "a-pvc-key",
				Namespace: "test",
			},
			want: "a-pvc-key",
		},
		{
			name: "a pvc name with start with principal name",
			key: PvcKey{
				Key:       "test-key",
				Namespace: "test-test-namespace",
			},
			want: "test-namespace-test-key",
		},
		{
			name: "principal name in the middle of the namespace name",
			key: PvcKey{
				Key:       "test-key",
				Namespace: "test-namespace-test-dev",
			},
			want: "namespace-test-dev-test-key",
		},
		{
			name: "other principal",
			key: PvcKey{
				Key:       "test-key",
				Namespace: "zol-test-namespace",
			},
			want: "test-key",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.key.CutPrincipalNameFromPvcNamespace(principalName); got != tt.want {
				t.Errorf("CutPrincipalNameFromPvcKey() = %v, want %v", got, tt.want)
			}
		})
	}
}

func ExamplePvcKey_CutPrincipalNameFromPvcNamespace() {
	principal := "test"

	key := PvcKey{
		Key:       "test-key",
		Namespace: "test-namespace-test-dev",
	}

	fmt.Println(key.CutPrincipalNameFromPvcNamespace(principal))
	// Output: namespace-test-dev-test-key
}
