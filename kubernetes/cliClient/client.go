package cliClient

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/cli"
	"context"
	"k8s.io/client-go/util/exec"
)

func NewPodCliFromContext(ctx context.Context, namespace string, pod string, container string) cli.CommandExecutor {

	//if a builder is registered take it; use default otherwise
	contextClient := ctx.Value(kubernetesCliClientBuilderContextValueKey)

	if contextClient != nil {
		builder := contextClient.(PodCLIBuilder)
		return builder.Build(ctx, namespace, pod, container)
	}

	//no client set; we assume default....
	return &podCLI{Context: ctx, targetNamespace: namespace, targetPod: pod, targetContainer: container}
}

type podCLI struct {
	context.Context
	targetNamespace string
	targetPod       string
	targetContainer string
}

func (k *podCLI) ExecuteCommand(cmd string) (stdout string, stderr string, err error) {
	return execShellInContainer(k.Context, k.targetNamespace, k.targetPod, k.targetContainer, cmd)
}

func (k *podCLI) CheckErrorForExitCode(err error) (int, bool) {
	exitCodeError, converted := err.(exec.CodeExitError)

	if !converted {
		return 0, false
	} else {
		return exitCodeError.Code, true
	}
}
