package cliClient

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/cli"
	"context"
)

type PodCLIBuilder interface {
	Build(ctx context.Context, namespace string, pod string, container string) cli.CommandExecutor
}

type kubernetesCliClientBuilderContextKey int

var kubernetesCliClientBuilderContextValueKey kubernetesCliClientBuilderContextKey

func NewContextWithCliClientBuilder(ctx context.Context, client PodCLIBuilder) context.Context {
	return context.WithValue(ctx, kubernetesCliClientBuilderContextValueKey, client)
}
