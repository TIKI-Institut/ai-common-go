package cliClient

import (
	commonKubernetes "bitbucket.org/TIKI-Institut/ai-common-go/v2/kubernetes"
	k8sContext "bitbucket.org/TIKI-Institut/ai-common-go/v2/kubernetes/context"
	"bytes"
	"context"
	"fmt"
	"io"
	v1 "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/remotecommand"
	"net/url"
	"strings"
)

// ExecOptions passed to ExecWithOptions
type ExecOptions struct {
	Command []string

	Namespace     string
	PodName       string
	ContainerName string

	Stdin         io.Reader
	CaptureStdout bool
	CaptureStderr bool

	// If false, whitespace in std{err,out} will be removed.
	PreserveWhitespace bool
}

//to be able to execute a command within a pod we need to start a special POST request
//against the K8s API.
//to get the target URL restClient.Interface of client-go is utilized...
func generatePodPostUrl(restConfig *rest.Config, options ExecOptions) (*url.URL, error) {

	parameterCodec := runtime.NewParameterCodec(scheme.Scheme)

	restClient, err := kubernetes.NewForConfig(restConfig)
	if err != nil {
		return nil, fmt.Errorf("unable to create temporary restClient : %w", err)
	}

	req := restClient.CoreV1().RESTClient().Post().
		Resource("pods").
		Name(options.PodName).
		Namespace(options.Namespace).
		SubResource("exec").
		Param("container", options.ContainerName)
	req.VersionedParams(&v1.PodExecOptions{
		Container: options.ContainerName,
		Command:   options.Command,
		Stdin:     options.Stdin != nil,
		Stdout:    options.CaptureStdout,
		Stderr:    options.CaptureStderr,
		TTY:       false,
	}, parameterCodec)

	return req.URL(), nil
}

// ExecWithOptions executes a command in the specified container,
// returning stdout, stderr and error. `options` allowed for
// additional parameters to be passed.
func execWithOptions(ctx context.Context, options ExecOptions) (string, string, error) {

	if restConfig := k8sContext.RestConfigFromContext(ctx); restConfig == nil {
		return "", "", fmt.Errorf("k8s rest configuration is required but was not provided")
	} else if k8sApiPodUrl, err := generatePodPostUrl(restConfig, options); err != nil {
		return "", "", err
	} else {
		var stdout, stderr bytes.Buffer
		err = execute("POST", k8sApiPodUrl, restConfig, options.Stdin, &stdout, &stderr, false)

		if options.PreserveWhitespace {
			return stdout.String(), stderr.String(), err
		}
		return strings.TrimSpace(stdout.String()), strings.TrimSpace(stderr.String()), err
	}
}

// execCommandInContainerWithFullOutput executes a command in the
// specified container and return stdout, stderr and error
func execCommandInContainer(ctx context.Context, namespace, podName, containerName string, cmd ...string) (string, string, error) {

	options := ExecOptions{
		Command:       cmd,
		Namespace:     namespace,
		PodName:       podName,
		ContainerName: containerName,

		Stdin:              nil,
		CaptureStdout:      true,
		CaptureStderr:      true,
		PreserveWhitespace: false,
	}

	return execWithOptions(ctx, options)
}

func execShellInContainer(ctx context.Context, namespace, podName, containerName string, cmd string) (stdout string, stderr string, err error) {
	return execCommandInContainer(ctx, namespace, podName, containerName, "/bin/sh", "-c", cmd)
}

func execCommandInPod(ctx context.Context, namespace string, podName string, cmd ...string) (string, string, error) {

	targetPod := v1.Pod{
		ObjectMeta: metaV1.ObjectMeta{
			Name:      podName,
			Namespace: namespace,
		},
	}

	err := commonKubernetes.GetObject(ctx, &targetPod)

	if err != nil {
		return "", "", fmt.Errorf("unable to get pod with name %s from namespace %s (%w)", podName, namespace, err)
	}

	return execCommandInContainer(ctx, namespace, podName, targetPod.Spec.Containers[0].Name, cmd...)
}

//goland:noinspection GoUnusedFunction
func execShellInPod(ctx context.Context, podName string, cmd string) (string, string, error) {
	return execCommandInPod(ctx, podName, "/bin/sh", "-c", cmd)
}

func execute(method string, url *url.URL, config *rest.Config, stdin io.Reader, stdout, stderr io.Writer, tty bool) error {

	exec, err := remotecommand.NewSPDYExecutor(config, method, url)
	if err != nil {
		return err
	}
	return exec.Stream(remotecommand.StreamOptions{
		Stdin:  stdin,
		Stdout: stdout,
		Stderr: stderr,
		Tty:    tty,
	})
}
