package context

import (
	"context"
	"fmt"
	"k8s.io/client-go/kubernetes"
)

type kubernetesClientSetContextKey int

var kubernetesClientSetContextKeyValue kubernetesClientSetContextKey

func NewContextWithClientSet(ctx context.Context, clientSet kubernetes.Interface) context.Context {
	return context.WithValue(ctx, kubernetesClientSetContextKeyValue, clientSet)
}

func ClientSetFromContext(ctx context.Context) (kubernetes.Interface, error) {
	contextClient := ctx.Value(kubernetesClientSetContextKeyValue)

	if contextClient != nil {
		return contextClient.(kubernetes.Interface), nil
	}

	//no client set; we assume default....
	if contextRestConfig := RestConfigFromContext(ctx); contextRestConfig == nil {
		return nil, fmt.Errorf("k8s rest configuration is required but was not provided")
	} else if clientSet, err := kubernetes.NewForConfig(contextRestConfig); err != nil {
		return nil, err
	} else {
		return clientSet, nil
	}
}
