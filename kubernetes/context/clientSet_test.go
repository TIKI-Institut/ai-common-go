package context

import (
	"context"
	"github.com/stretchr/testify/assert"
	"k8s.io/client-go/kubernetes/fake"
	"k8s.io/client-go/rest"
	"testing"
)

func TestClientSetFromContext(t *testing.T) {

	t.Run("client available", func(t *testing.T) {

		clientSet := fake.NewSimpleClientset()
		ctx := NewContextWithClientSet(context.Background(), clientSet)
		clientSetFromCtx, err := ClientSetFromContext(ctx)
		assert.NoError(t, err)
		assert.Equal(t, clientSet, clientSetFromCtx)
	})

	t.Run("client not in context", func(t *testing.T) {

		t.Run("config available", func(t *testing.T) {
			ctx := NewContextWithRestConfig(context.Background(), &rest.Config{})
			c, err := ClientSetFromContext(ctx)
			assert.NoError(t, err)
			assert.NotNil(t, c)
		})

		t.Run("config not in context", func(t *testing.T) {
			c, err := ClientSetFromContext(context.Background())
			assert.EqualError(t, err, "k8s rest configuration is required but was not provided")
			assert.Nil(t, c)
		})
	})
}
