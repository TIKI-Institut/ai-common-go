package context

import (
	"context"
	"fmt"
	"k8s.io/client-go/dynamic"
)

type kubernetesDynamicClientContextKey int

var kubernetesDynamicClientContextValueKey kubernetesDynamicClientContextKey

func NewContextWithDynamicClient(ctx context.Context, client dynamic.Interface) context.Context {
	return context.WithValue(ctx, kubernetesDynamicClientContextValueKey, client)
}

func DynamicClientFromContext(ctx context.Context) (dynamic.Interface, error) {
	contextClient := ctx.Value(kubernetesDynamicClientContextValueKey)

	if contextClient != nil {
		return contextClient.(dynamic.Interface), nil
	}

	//no client set; we assume default....
	if contextRestConfig := RestConfigFromContext(ctx); contextRestConfig == nil {
		return nil, fmt.Errorf("k8s rest configuration is required but was not provided")
	} else if dynamicClient, err := dynamic.NewForConfig(contextRestConfig); err != nil {
		return nil, err
	} else {
		return dynamicClient, nil
	}
}
