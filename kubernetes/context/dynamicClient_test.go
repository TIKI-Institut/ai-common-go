package context

import (
	"context"
	"github.com/stretchr/testify/assert"
	dynamicFake "k8s.io/client-go/dynamic/fake"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"testing"
)

func TestDynamicClientFromContext(t *testing.T) {

	t.Run("client available", func(t *testing.T) {
		dynamicClient := dynamicFake.NewSimpleDynamicClient(scheme.Scheme)
		ctx := NewContextWithDynamicClient(context.Background(), dynamicClient)
		dynClientFromCtx, err := DynamicClientFromContext(ctx)
		assert.NoError(t, err)
		assert.Equal(t, dynamicClient, dynClientFromCtx)
	})

	t.Run("client not in context", func(t *testing.T) {

		t.Run("config available", func(t *testing.T) {
			ctx := NewContextWithRestConfig(context.Background(), &rest.Config{})
			c, err := DynamicClientFromContext(ctx)
			assert.NoError(t, err)
			assert.NotNil(t, c)
		})

		t.Run("config not in context", func(t *testing.T) {
			c, err := DynamicClientFromContext(context.Background())
			assert.EqualError(t, err, "k8s rest configuration is required but was not provided")
			assert.Nil(t, c)
		})
	})
}
