package context

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/config"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/path"
	"context"
	"errors"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"os"
	"path/filepath"
)

type restConfigKey int

var restConfigValueKey restConfigKey

// NoKubernetesConfig is the error returned by GetKubernetesAPI when no
// Kubernetes Config file was found or the file does not exist.
var NoKubernetesConfigFile = errors.New("NoKubernetesConfigFile")

func NewContextWithRestConfig(ctx context.Context, config *rest.Config) context.Context {
	return context.WithValue(ctx, restConfigValueKey, config)
}

//goland:noinspection GoUnusedExportedFunction
func NewContextWithApiConfig(ctx context.Context, apiConfig config.KubernetesApiConfiguration) (context.Context, error) {
	if apiConfig.InCluster {
		if conf, err := rest.InClusterConfig(); err != nil {
			return nil, err
		} else {
			return NewContextWithRestConfig(ctx, conf), nil
		}
	} else {
		// Out of cluster config
		if apiConfig.KubernetesConfigFile == "" {
			if homeDir, err := os.UserHomeDir(); err != nil {
				return nil, err
			} else {
				apiConfig.KubernetesConfigFile = filepath.Join(homeDir, ".kube", "config")
			}
		}

		kubeConfigFile := path.AbsolutePath(apiConfig.KubernetesConfigFile)
		if !kubeConfigFile.Exists() {
			return nil, NoKubernetesConfigFile
		}

		if conf, err := clientcmd.BuildConfigFromFlags("", apiConfig.KubernetesConfigFile); err != nil {
			return nil, err
		} else {
			return NewContextWithRestConfig(ctx, conf), nil
		}
	}
}

func RestConfigFromContext(ctx context.Context) *rest.Config {
	//if no config is set we assume a "in-cluster" configuration
	if contextValue, ok := ctx.Value(restConfigValueKey).(*rest.Config); ok {
		return contextValue
	} else {
		return nil
	}
}
