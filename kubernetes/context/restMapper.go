package context

import (
	"context"
	"fmt"
	"k8s.io/apimachinery/pkg/api/meta"
	"k8s.io/client-go/discovery"
	"k8s.io/client-go/discovery/cached/memory"
	"k8s.io/client-go/restmapper"
)

type kubernetesRestMapperContextKey int

var kubernetesRestMapperContextValueKey kubernetesRestMapperContextKey

func NewContextWithRestMapper(ctx context.Context, config meta.RESTMapper) context.Context {
	return context.WithValue(ctx, kubernetesRestMapperContextValueKey, config)
}

func RestMapperFromContext(ctx context.Context) (meta.RESTMapper, error) {
	//if no mapper is set. we try to create it from the registered client
	contextRestMapper := ctx.Value(kubernetesRestMapperContextValueKey)

	if contextRestMapper != nil {
		return contextRestMapper.(meta.RESTMapper), nil
	}

	if restConfig := RestConfigFromContext(ctx); restConfig == nil {
		return nil, fmt.Errorf("k8s rest configuration is required but was not provided")
	} else {
		return restmapper.NewDeferredDiscoveryRESTMapper(memory.NewMemCacheClient(discovery.NewDiscoveryClientForConfigOrDie(restConfig))), nil
	}
}
