package kubernetes

import (
	"fmt"
	"strings"
)

const DspKubernetesLabelDomain = "tiki-dsp.io"
const DspKubernetesLabelPrefix = DspKubernetesLabelDomain + "/"

func DspLabel(name string) string {
	return fmt.Sprintf("%s%s", DspKubernetesLabelPrefix, name)
}

func IsDspLabel(label string) bool {
	return strings.HasPrefix(label, DspKubernetesLabelPrefix)
}
