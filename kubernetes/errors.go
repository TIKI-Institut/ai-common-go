package kubernetes

import (
	"errors"
	k8sErrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/util/validation/field"
)

//goland:noinspection GoUnusedExportedFunction
func IsPvcResizingError(err error) bool {
	statusError := &k8sErrors.StatusError{}
	if errors.As(err, &statusError) {
		for _, cause := range statusError.ErrStatus.Details.Causes {
			if string(cause.Type) == string(field.ErrorTypeForbidden) && cause.Field == "spec.resources.requests.storage" {
				return true
			}
		}
	}

	return false
}
