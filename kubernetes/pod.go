package kubernetes

import (
	"fmt"
	"os"
)

const DspNamespaceEnv = "DSP_NAMESPACE"
const DspPodEnv = "DSP_POD"

type PodReference struct {
	Namespace string `json:"namespace"`
	Pod       string `json:"pod"`
}

func GetCurrentPodReference() (*PodReference, error) {

	namespace, found := os.LookupEnv(DspNamespaceEnv)

	if !found {
		return nil, fmt.Errorf("env:%s not set; cannot resolve namespace", DspNamespaceEnv)
	}

	podName, found := os.LookupEnv(DspPodEnv)

	if !found {
		return nil, fmt.Errorf("env:%s not set; cannot resolve pod", DspPodEnv)
	}

	return &PodReference{namespace, podName}, nil
}
