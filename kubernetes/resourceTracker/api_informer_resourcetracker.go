package resourceTracker

import (
	commonKubernetes "bitbucket.org/TIKI-Institut/ai-common-go/v2/kubernetes"
	"fmt"
	"k8s.io/apimachinery/pkg/runtime/schema"
	k8sJson "k8s.io/apimachinery/pkg/util/json"
	"k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/tools/cache"
	"sync"
)

/*
TrackerDeleteResource contains information on deleted resources in the underlying cache.SharedIndexInformer.
There is the possibility that k8s already "finalized" the deleted object or has some trouble to do so.

	in case of a normal deletion

		TrackerDeleteResource.Resource contains a converted state
		TrackerDeleteResource.UnknownState will be false

	in case of an abnormal deletion

		TrackerDeleteResource.Resource might contain a converted state; if the underlying k8s resource is still convertible; NOTE: the contents might differ from the current k8s state as the presented resource might be outdated!
		TrackerDeleteResource.UnknownState will be true
		TrackerDeleteResource.RawResource contains the raw resource from k8s; usually tha last known state of the resource when the client still was connected to k8s
		TrackerDeleteResource.ResourceKey contains the key to the abnormal deleted object; usually delivered in form of "<namespace>/<name>"
*/
type TrackerDeleteResource[TObj commonKubernetes.Object] struct {
	Resource     TObj
	ResourceKey  string
	RawResource  interface{}
	UnknownState bool
}

type ResourceTracker[TObj commonKubernetes.Object] interface {

	// KnownResources returns all currently known resources
	// If a filter was used to create this tracker it will be applied on the KnownResources
	KnownResources() ([]TObj, error)

	// KnownResourcesByIndex returns currently known resources with the given <indexValue> within the index <indexName>
	// If a filter was used to create this tracker it will be applied on the KnownResourcesByIndex
	KnownResourcesByIndex(indexName string, indexValue string) ([]TObj, error)

	// RegisterAddedResourceChannel adds a new handler for the "Added Resource" event
	RegisterAddedResourceChannel(chan TObj)

	// UnregisterAddedResourceChannel removes a previously registered handler (the channel gets closed by UnregisterAddedResourceChannel)
	UnregisterAddedResourceChannel(chan TObj)

	// RegisterDeletedResourceChannel adds a new handler for the "Deleted Resource" event
	RegisterDeletedResourceChannel(chan TrackerDeleteResource[TObj])

	// UnregisterDeletedResourceChannel removes a previously registered handler (the channel gets closed by UnregisterDeletedResourceChannel)
	UnregisterDeletedResourceChannel(chan TrackerDeleteResource[TObj])

	// RegisterUpdatedResourceChannel adds a new handler for the "Updated Resource" event
	RegisterUpdatedResourceChannel(chan struct{ Old, New TObj })

	// UnregisterUpdatedResourceChannel removes a previously registered handler (the channel gets closed by UnregisterUpdatedResourceChannel)
	UnregisterUpdatedResourceChannel(chan struct{ Old, New TObj })
}

/*
NewResourceTracker attaches a typed resource tracker to the given shared informer cache.SharedIndexInformer.

It's mandatory that any objects published by cache.SharedIndexInformer are convertable to [TObj] via scheme.Schema.Convert.
Objects witch are not convertable will be passed to runtime.HandleError and not otherwise returned to the user.
*/
func NewResourceTracker[TObj commonKubernetes.Object](informer cache.SharedIndexInformer, filter commonKubernetes.ResourceFilter[TObj]) (ResourceTracker[TObj], error) {

	objectGvk, err := commonKubernetes.GroupVersionKindForObject(commonKubernetes.NewK8sObject[TObj]())
	if err != nil {
		return nil, err
	}

	result := resourceTracker[TObj]{
		objectGvk:                     objectGvk,
		informer:                      informer,
		filterFunc:                    filter,
		addedResourceHandlerClients:   map[chan TObj]bool{},
		deletedResourceHandlerClients: map[chan TrackerDeleteResource[TObj]]bool{},
		updatedResourceHandlerClients: map[chan struct{ Old, New TObj }]bool{},
	}

	var typedHandler commonKubernetes.TypedResourceEventHandler[TObj] = &result

	if result.filterFunc != nil {
		//add filtering
		typedHandler = commonKubernetes.TypedFilteringResourceEventHandler[TObj]{
			FilterFunc: result.filterFunc,
			Handler:    typedHandler,
		}
	}

	informer.AddEventHandler(commonKubernetes.ConversionEventHandler[TObj]{Target: typedHandler})

	return &result, err
}

type resourceTracker[TObj commonKubernetes.Object] struct {
	sync.RWMutex
	objectGvk schema.GroupVersionKind
	informer  cache.SharedIndexInformer

	filterFunc commonKubernetes.ResourceFilter[TObj]

	addedResourceHandlerClients   map[chan TObj]bool
	deletedResourceHandlerClients map[chan TrackerDeleteResource[TObj]]bool
	updatedResourceHandlerClients map[chan struct{ Old, New TObj }]bool
}

func (r *resourceTracker[TObj]) OnAdd(obj TObj) {
	r.RLock()
	defer r.RUnlock()

	for addedPodChan := range r.addedResourceHandlerClients {
		addedPodChan <- obj
	}
}

func (r *resourceTracker[TObj]) OnUpdate(oldObj, newObj TObj) {
	r.RLock()
	defer r.RUnlock()

	for addedPodChan := range r.updatedResourceHandlerClients {
		addedPodChan <- struct{ Old, New TObj }{Old: oldObj, New: newObj}
	}
}

func (r *resourceTracker[TObj]) OnDelete(obj TObj) {
	r.RLock()
	defer r.RUnlock()
	for addedPodChan := range r.deletedResourceHandlerClients {
		addedPodChan <- TrackerDeleteResource[TObj]{
			Resource:     obj,
			ResourceKey:  "",
			RawResource:  nil,
			UnknownState: false,
		}
	}
}

func (r *resourceTracker[TObj]) OnDeleteUnknown(i interface{}, key string) {
	r.RLock()
	defer r.RUnlock()
	for addedPodChan := range r.deletedResourceHandlerClients {
		addedPodChan <- TrackerDeleteResource[TObj]{
			Resource:     commonKubernetes.NewK8sObject[TObj](),
			ResourceKey:  key,
			RawResource:  i,
			UnknownState: true,
		}
	}
}

func (r *resourceTracker[TObj]) OnConversionError(i interface{}, stage commonKubernetes.ConversionStage, conversionError error) {

	jsonBytes, jsonBytesError := k8sJson.Marshal(i)
	if jsonBytesError != nil {
		conversionError = fmt.Errorf("failed to convert object during %s (%w)", string(stage), conversionError)
	} else {
		conversionError = fmt.Errorf("failed to convert object: %s during %s (%w)", jsonBytes, string(stage), conversionError)
	}
	runtime.HandleError(conversionError)
}

func (r *resourceTracker[TObj]) filteringResourceFilter(obj interface{}) bool {
	o, err := r.handledConversion("", obj)
	if err != nil {
		return false
	}
	return r.filterFunc(o)
}

func (r *resourceTracker[TObj]) convert(i interface{}) (TObj, error) {
	obj := commonKubernetes.NewK8sObject[TObj]()
	err := scheme.Scheme.Convert(i, obj, nil)
	if err != nil {
		return obj, fmt.Errorf("unable to convert to object of gvk:%s (%w)", r.objectGvk.String(), err)
	}
	return obj, nil
}

func (r *resourceTracker[TObj]) convertAll(d []interface{}) ([]TObj, error) {

	var filter commonKubernetes.ResourceFilter[TObj] = func(TObj) bool { return true }

	if r.filterFunc != nil {
		filter = r.filterFunc
	}

	result := make([]TObj, 0, len(d))

	for i := 0; i < len(d); i++ {
		elem, err := r.convert(d[i])
		if err != nil {
			return nil, fmt.Errorf("unable to convert elemnt #%d (%w)", i, err)
		}

		if filter(elem) {
			result = append(result, elem)
		}
	}

	return result, nil
}

func (r *resourceTracker[TObj]) handledConversion(stageHint string, obj interface{}) (TObj, error) {

	converted, err := r.convert(obj)
	if err == nil {
		return converted, nil
	}

	jsonBytes, jsonBytesError := k8sJson.Marshal(obj)
	if jsonBytesError != nil {
		err = fmt.Errorf("failed to convert object%s (%w)", stageHint, err)
	} else {
		err = fmt.Errorf("failed to convert object: %s%s (%w)", jsonBytes, stageHint, err)
	}
	runtime.HandleError(err)
	return converted, err
}

func (r *resourceTracker[TObj]) KnownResources() ([]TObj, error) {
	return r.convertAll(r.informer.GetStore().List())
}

func (r *resourceTracker[TObj]) KnownResourcesByIndex(indexName string, indexValue string) ([]TObj, error) {

	known, err := r.informer.GetIndexer().ByIndex(indexName, indexValue)
	if err != nil {
		return nil, fmt.Errorf("unable to retrieve index contents (%w)", err)
	}
	return r.convertAll(known)
}

func (r *resourceTracker[TObj]) RegisterAddedResourceChannel(client chan TObj) {
	r.Lock()
	defer r.Unlock()
	r.addedResourceHandlerClients[client] = true
}

func (r *resourceTracker[TObj]) UnregisterAddedResourceChannel(objs chan TObj) {
	r.Lock()
	defer r.Unlock()
	delete(r.addedResourceHandlerClients, objs)
	close(objs)
}

func (r *resourceTracker[TObj]) RegisterDeletedResourceChannel(objs chan TrackerDeleteResource[TObj]) {
	r.Lock()
	defer r.Unlock()
	r.deletedResourceHandlerClients[objs] = true
}

func (r *resourceTracker[TObj]) UnregisterDeletedResourceChannel(objs chan TrackerDeleteResource[TObj]) {
	r.Lock()
	defer r.Unlock()
	delete(r.deletedResourceHandlerClients, objs)
	close(objs)
}

func (r *resourceTracker[TObj]) RegisterUpdatedResourceChannel(objs chan struct{ Old, New TObj }) {
	r.Lock()
	defer r.Unlock()
	r.updatedResourceHandlerClients[objs] = true
}

func (r *resourceTracker[TObj]) UnregisterUpdatedResourceChannel(objs chan struct{ Old, New TObj }) {
	r.Lock()
	defer r.Unlock()
	delete(r.updatedResourceHandlerClients, objs)
	close(objs)
}
