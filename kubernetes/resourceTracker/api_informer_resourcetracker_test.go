package resourceTracker

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/kubernetes"
	k8sTesting "bitbucket.org/TIKI-Institut/ai-common-go/v2/kubernetes/testing"
	"context"
	"github.com/stretchr/testify/assert"
	v1 "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/tools/cache"
	"sync"
	"testing"
	"time"
)

func TestNewResourceTracker(t *testing.T) {

	demoConfigMaps := func() []runtime.Object {
		return []runtime.Object{
			&v1.ConfigMap{
				ObjectMeta: metaV1.ObjectMeta{
					Name:      "cm-a",
					Namespace: "namespace-a",
				},
			},
			&v1.ConfigMap{
				ObjectMeta: metaV1.ObjectMeta{
					Name:      "cm-b",
					Namespace: "namespace-a",
				},
			},
			&v1.ConfigMap{
				ObjectMeta: metaV1.ObjectMeta{
					Name:      "cm-c",
					Namespace: "namespace-a",
				},
			},
			&v1.ConfigMap{
				ObjectMeta: metaV1.ObjectMeta{
					Name:      "cm-a",
					Namespace: "namespace-b",
				},
			},
			&v1.ConfigMap{
				ObjectMeta: metaV1.ObjectMeta{
					Name:      "cm-b",
					Namespace: "namespace-b",
				},
			},
		}
	}

	expectedConfigMaps := demoConfigMaps()

	setup := func() (context.Context, context.CancelFunc, cache.SharedIndexInformer) {
		ctx, cancel := context.WithTimeout(context.Background(), time.Hour)

		ctx, _ = k8sTesting.CreateFakeClient(ctx, expectedConfigMaps...)
		informer, err := kubernetes.NewSharedIndexInformer[*v1.ConfigMap](ctx, nil, time.Hour*24, nil)
		assert.NoError(t, err)

		assert.True(t, kubernetes.StartAndSyncInformer(ctx, informer))

		return ctx, cancel, informer
	}

	teardown := func(ctx context.Context, cancel context.CancelFunc) {
		select {
		case <-time.After(100 * time.Millisecond):
			cancel()
		case <-ctx.Done():
			cancel()
		}
	}

	convertUpstream := func(d []*v1.ConfigMap) []runtime.Object {
		result := make([]runtime.Object, len(d), len(d))
		for i := 0; i < len(d); i++ {
			result[i] = d[i]
		}
		return result
	}

	waitTimeout := func(wg *sync.WaitGroup, timeout time.Duration) bool {
		c := make(chan struct{})
		go func() {
			defer close(c)
			wg.Wait()
		}()
		select {
		case <-c:
			return true
		case <-time.After(timeout):
			return false
		}
	}

	waitMillis := func(millis int64) {
		select {
		case <-time.After(time.Millisecond * time.Duration(millis)):
		}

	}

	t.Run("basic empty", func(t *testing.T) {

		ctx, cancel, informer := setup()

		tracker, err := NewResourceTracker[*v1.ConfigMap](informer, nil)
		assert.NotNil(t, tracker)
		assert.NoError(t, err)

		teardown(ctx, cancel)

	})

	t.Run("non filtered", func(t *testing.T) {
		t.Run("list known resources", func(t *testing.T) {

			ctx, cancel, informer := setup()

			tracker, err := NewResourceTracker[*v1.ConfigMap](informer, nil)
			assert.NotNil(t, tracker)
			assert.NoError(t, err)

			knownResources, err := tracker.KnownResources()
			assert.NoError(t, err)
			assert.Len(t, knownResources, 5)
			k8sTesting.AssertContainsAllObjects(t, expectedConfigMaps, convertUpstream(knownResources))

			teardown(ctx, cancel)
		})

		t.Run("list indexed known resources", func(t *testing.T) {

			ctx, cancel, informer := setup()

			tracker, err := NewResourceTracker[*v1.ConfigMap](informer, nil)
			assert.NotNil(t, tracker)
			assert.NoError(t, err)

			knownResources, err := tracker.KnownResourcesByIndex(cache.NamespaceIndex, "namespace-b")
			assert.NoError(t, err)

			assert.Len(t, knownResources, 2)
			k8sTesting.AssertContainsObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-a", Namespace: "namespace-b", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, knownResources)
			k8sTesting.AssertContainsObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-b", Namespace: "namespace-b", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, knownResources)

			teardown(ctx, cancel)
		})

		t.Run("add resource", func(t *testing.T) {

			ctx, cancel, informer := setup()

			tracker, err := NewResourceTracker[*v1.ConfigMap](informer, nil)
			assert.NotNil(t, tracker)
			assert.NoError(t, err)

			//wait the initial state to settle (this avoids all object getting pushed into the created channel)
			waitMillis(100)

			newResourceChan := make(chan *v1.ConfigMap)
			tracker.RegisterAddedResourceChannel(newResourceChan)
			chanWg := sync.WaitGroup{}
			chanWg.Add(1)

			additionalResource := v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-new", Namespace: "namespace-b"}}

			addedResource := additionalResource.DeepCopy()
			addedResource.SetCreationTimestamp(k8sTesting.FakeCreationTimestamp)

			_, err = kubernetes.Apply(ctx, &additionalResource, "", "", false)
			assert.NoError(t, err)

			go func() {
				for n := range newResourceChan {
					k8sTesting.AssertEqualObjects(t, addedResource, n)
				}
				chanWg.Done()
			}()

			//wait the stuff gets propagated through the system
			waitMillis(100)

			tracker.UnregisterAddedResourceChannel(newResourceChan)

			//check whether the added resource gets indexed
			knownResources, err := tracker.KnownResourcesByIndex(cache.NamespaceIndex, "namespace-b")
			assert.NoError(t, err)

			assert.Len(t, knownResources, 3)
			k8sTesting.AssertContainsObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-a", Namespace: "namespace-b", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, knownResources)
			k8sTesting.AssertContainsObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-b", Namespace: "namespace-b", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, knownResources)
			k8sTesting.AssertContainsObjects(t, addedResource, knownResources)

			//check whether the added resource is now known
			knownResources, err = tracker.KnownResources()
			assert.NoError(t, err)
			assert.Len(t, knownResources, 6)
			k8sTesting.AssertContainsAllObjects(t, expectedConfigMaps, convertUpstream(knownResources))
			k8sTesting.AssertContainsObjects(t, addedResource, convertUpstream(knownResources))

			teardown(ctx, cancel)

			if !waitTimeout(&chanWg, time.Second) {
				assert.Fail(t, "resource tracker channel was not closed")
			}

		})

		t.Run("delete resource", func(t *testing.T) {

			ctx, cancel, informer := setup()

			tracker, err := NewResourceTracker[*v1.ConfigMap](informer, nil)
			assert.NotNil(t, tracker)
			assert.NoError(t, err)

			//wait the initial state to settle (this avoids all object getting pushed into the created channel)
			waitMillis(100)

			newResourceChan := make(chan TrackerDeleteResource[*v1.ConfigMap])
			tracker.RegisterDeletedResourceChannel(newResourceChan)
			chanWg := sync.WaitGroup{}
			chanWg.Add(1)

			deletedResource := expectedConfigMaps[0]

			err = kubernetes.DeleteObject(ctx, deletedResource.(kubernetes.Object))
			assert.NoError(t, err)

			go func() {
				for n := range newResourceChan {
					k8sTesting.AssertEqualObjects(t, deletedResource, n.Resource)
					assert.False(t, n.UnknownState)
					assert.EqualValues(t, "", n.ResourceKey)
					assert.Nil(t, n.RawResource)
				}
				chanWg.Done()
			}()

			//wait the stuff gets propagated through the system
			waitMillis(100)

			tracker.UnregisterDeletedResourceChannel(newResourceChan)

			//check whether the deleted resource gets removed from the index
			knownResources, err := tracker.KnownResourcesByIndex(cache.NamespaceIndex, "namespace-a")
			assert.NoError(t, err)

			assert.Len(t, knownResources, 2)
			k8sTesting.AssertContainsObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-b", Namespace: "namespace-a", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, knownResources)
			k8sTesting.AssertContainsObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-c", Namespace: "namespace-a", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, knownResources)

			//check whether the deleted resource is no longer known
			knownResources, err = tracker.KnownResources()
			assert.NoError(t, err)

			assert.Len(t, knownResources, 4)
			k8sTesting.AssertContainsAllObjects(t, expectedConfigMaps[1:], convertUpstream(knownResources))

			teardown(ctx, cancel)

			if !waitTimeout(&chanWg, time.Second) {
				assert.Fail(t, "resource tracker channel was not closed")
			}
		})

		t.Run("modify resource", func(t *testing.T) {

			ctx, cancel, informer := setup()

			tracker, err := NewResourceTracker[*v1.ConfigMap](informer, nil)
			assert.NotNil(t, tracker)
			assert.NoError(t, err)

			//wait the initial state to settle (this avoids all object getting pushed into the created channel)
			waitMillis(100)

			newResourceChan := make(chan struct {
				Old, New *v1.ConfigMap
			})
			tracker.RegisterUpdatedResourceChannel(newResourceChan)
			chanWg := sync.WaitGroup{}
			chanWg.Add(1)

			oldResource := expectedConfigMaps[1]
			newResource := &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-b", Namespace: "namespace-a", CreationTimestamp: k8sTesting.FakeCreationTimestamp}, Data: map[string]string{"FOO": "BAR"}}

			_, err = kubernetes.Apply(ctx, newResource, "", "", false)
			assert.NoError(t, err)

			go func() {
				for n := range newResourceChan {
					k8sTesting.AssertEqualObjects(t, oldResource, n.Old)
					k8sTesting.AssertEqualObjects(t, newResource, n.New)
				}
				chanWg.Done()
			}()

			//wait the stuff gets propagated through the system
			waitMillis(100)

			tracker.UnregisterUpdatedResourceChannel(newResourceChan)

			//wait the stuff gets propagated through the system
			waitMillis(100)

			//check whether the deleted resource gets removed from the index
			knownResources, err := tracker.KnownResourcesByIndex(cache.NamespaceIndex, "namespace-a")
			assert.NoError(t, err)

			assert.Len(t, knownResources, 3)
			k8sTesting.AssertContainsObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-a", Namespace: "namespace-a", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, knownResources)
			k8sTesting.AssertContainsObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-b", Namespace: "namespace-a", CreationTimestamp: k8sTesting.FakeCreationTimestamp}, Data: map[string]string{"FOO": "BAR"}}, knownResources)
			k8sTesting.AssertContainsObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-c", Namespace: "namespace-a", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, knownResources)

			//check whether the deleted resource is no longer known
			knownResources, err = tracker.KnownResources()
			assert.NoError(t, err)

			assert.Len(t, knownResources, 5)
			k8sTesting.AssertContainsObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-a", Namespace: "namespace-a", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, knownResources)
			k8sTesting.AssertContainsObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-b", Namespace: "namespace-a", CreationTimestamp: k8sTesting.FakeCreationTimestamp}, Data: map[string]string{"FOO": "BAR"}}, knownResources)
			k8sTesting.AssertContainsObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-c", Namespace: "namespace-a", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, knownResources)
			k8sTesting.AssertContainsObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-a", Namespace: "namespace-b", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, knownResources)
			k8sTesting.AssertContainsObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-b", Namespace: "namespace-b", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, knownResources)

			teardown(ctx, cancel)

			if !waitTimeout(&chanWg, time.Second) {
				assert.Fail(t, "resource tracker channel was not closed")
			}
		})

	})

	t.Run("filtered", func(t *testing.T) {

		cmFilter := func(cm *v1.ConfigMap) bool {
			return cm.Name == "cm-b"
		}

		t.Run("list known resources", func(t *testing.T) {

			ctx, cancel, informer := setup()

			tracker, err := NewResourceTracker[*v1.ConfigMap](informer, cmFilter)
			assert.NotNil(t, tracker)
			assert.NoError(t, err)

			knownResources, err := tracker.KnownResources()
			assert.NoError(t, err)
			assert.Len(t, knownResources, 2)
			k8sTesting.AssertContainsObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-b", Namespace: "namespace-a", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, knownResources)
			k8sTesting.AssertContainsObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-b", Namespace: "namespace-b", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, knownResources)

			teardown(ctx, cancel)
		})

		t.Run("list indexed known resources", func(t *testing.T) {

			ctx, cancel, informer := setup()

			tracker, err := NewResourceTracker[*v1.ConfigMap](informer, cmFilter)
			assert.NotNil(t, tracker)
			assert.NoError(t, err)

			knownResources, err := tracker.KnownResourcesByIndex(cache.NamespaceIndex, "namespace-b")
			assert.NoError(t, err)

			assert.Len(t, knownResources, 1)
			k8sTesting.AssertContainsObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-b", Namespace: "namespace-b", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, knownResources)

			teardown(ctx, cancel)
		})

		t.Run("add resource", func(t *testing.T) {

			ctx, cancel, informer := setup()

			tracker, err := NewResourceTracker[*v1.ConfigMap](informer, cmFilter)
			assert.NotNil(t, tracker)
			assert.NoError(t, err)

			//wait the initial state to settle (this avoids all object getting pushed into the created channel)
			waitMillis(100)

			newResourceChan := make(chan *v1.ConfigMap)
			tracker.RegisterAddedResourceChannel(newResourceChan)
			chanWg := sync.WaitGroup{}
			chanWg.Add(1)

			additionalResource := v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-new", Namespace: "namespace-b"}}

			addedResource := additionalResource.DeepCopy()
			addedResource.SetCreationTimestamp(k8sTesting.FakeCreationTimestamp)

			_, err = kubernetes.Apply(ctx, &additionalResource, "", "", false)
			assert.NoError(t, err)

			go func() {
				for range newResourceChan {
					assert.Fail(t, "this should not be reached at all")
				}
				chanWg.Done()
			}()

			//wait the stuff gets propagated through the system
			waitMillis(100)

			tracker.UnregisterAddedResourceChannel(newResourceChan)

			//check whether the added resource gets indexed
			knownResources, err := tracker.KnownResourcesByIndex(cache.NamespaceIndex, "namespace-b")
			assert.NoError(t, err)

			assert.Len(t, knownResources, 1)
			k8sTesting.AssertContainsObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-b", Namespace: "namespace-b", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, knownResources)

			//check whether the added resource is now known
			knownResources, err = tracker.KnownResources()
			assert.NoError(t, err)
			assert.Len(t, knownResources, 2)
			k8sTesting.AssertContainsObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-b", Namespace: "namespace-a", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, knownResources)
			k8sTesting.AssertContainsObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-b", Namespace: "namespace-b", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, knownResources)

			teardown(ctx, cancel)

			if !waitTimeout(&chanWg, time.Second) {
				assert.Fail(t, "resource tracker channel was not closed")
			}

		})

		t.Run("delete resource", func(t *testing.T) {

			ctx, cancel, informer := setup()

			tracker, err := NewResourceTracker[*v1.ConfigMap](informer, cmFilter)
			assert.NotNil(t, tracker)
			assert.NoError(t, err)

			//wait the initial state to settle (this avoids all object getting pushed into the created channel)
			waitMillis(100)

			newResourceChan := make(chan TrackerDeleteResource[*v1.ConfigMap])
			tracker.RegisterDeletedResourceChannel(newResourceChan)
			chanWg := sync.WaitGroup{}
			chanWg.Add(1)

			deletedResource := expectedConfigMaps[0]

			err = kubernetes.DeleteObject(ctx, deletedResource.(kubernetes.Object))
			assert.NoError(t, err)

			go func() {
				for range newResourceChan {
					assert.Fail(t, "this should not be reached at all")
				}
				chanWg.Done()
			}()

			//wait the stuff gets propagated through the system
			waitMillis(100)

			tracker.UnregisterDeletedResourceChannel(newResourceChan)

			//check whether the deleted resource gets removed from the index
			knownResources, err := tracker.KnownResourcesByIndex(cache.NamespaceIndex, "namespace-a")
			assert.NoError(t, err)

			assert.Len(t, knownResources, 1)
			k8sTesting.AssertContainsObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-b", Namespace: "namespace-a", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, knownResources)

			//check whether the added resource is now known
			knownResources, err = tracker.KnownResources()
			assert.NoError(t, err)
			assert.Len(t, knownResources, 2)
			k8sTesting.AssertContainsObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-b", Namespace: "namespace-a", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, knownResources)
			k8sTesting.AssertContainsObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-b", Namespace: "namespace-b", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, knownResources)

			teardown(ctx, cancel)

			if !waitTimeout(&chanWg, time.Second) {
				assert.Fail(t, "resource tracker channel was not closed")
			}
		})

		t.Run("modify resource", func(t *testing.T) {

			ctx, cancel, informer := setup()

			tracker, err := NewResourceTracker[*v1.ConfigMap](informer, cmFilter)
			assert.NotNil(t, tracker)
			assert.NoError(t, err)

			//wait the initial state to settle (this avoids all object getting pushed into the created channel)
			waitMillis(100)

			newResourceChan := make(chan struct {
				Old, New *v1.ConfigMap
			})
			tracker.RegisterUpdatedResourceChannel(newResourceChan)
			chanWg := sync.WaitGroup{}
			chanWg.Add(1)

			oldResource := expectedConfigMaps[1]
			newResource := &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-b", Namespace: "namespace-a", CreationTimestamp: k8sTesting.FakeCreationTimestamp}, Data: map[string]string{"FOO": "BAR"}}

			_, err = kubernetes.Apply(ctx, newResource, "", "", false)
			assert.NoError(t, err)

			go func() {
				for n := range newResourceChan {
					k8sTesting.AssertEqualObjects(t, oldResource, n.Old)
					k8sTesting.AssertEqualObjects(t, newResource, n.New)
				}
				chanWg.Done()
			}()

			//wait the stuff gets propagated through the system
			waitMillis(100)

			tracker.UnregisterUpdatedResourceChannel(newResourceChan)

			//wait the stuff gets propagated through the system
			waitMillis(100)

			//check whether the deleted resource gets removed from the index
			knownResources, err := tracker.KnownResourcesByIndex(cache.NamespaceIndex, "namespace-a")
			assert.NoError(t, err)

			assert.Len(t, knownResources, 1)
			k8sTesting.AssertContainsObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-b", Namespace: "namespace-a", CreationTimestamp: k8sTesting.FakeCreationTimestamp}, Data: map[string]string{"FOO": "BAR"}}, knownResources)

			//check whether the deleted resource is no longer known
			knownResources, err = tracker.KnownResources()
			assert.NoError(t, err)

			assert.Len(t, knownResources, 2)
			k8sTesting.AssertContainsObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-b", Namespace: "namespace-a", CreationTimestamp: k8sTesting.FakeCreationTimestamp}, Data: map[string]string{"FOO": "BAR"}}, knownResources)
			k8sTesting.AssertContainsObjects(t, &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-b", Namespace: "namespace-b", CreationTimestamp: k8sTesting.FakeCreationTimestamp}}, knownResources)

			teardown(ctx, cancel)

			if !waitTimeout(&chanWg, time.Second) {
				assert.Fail(t, "resource tracker channel was not closed")
			}
		})

	})

	t.Run("wrong type", func(t *testing.T) {
		t.Run("list known resources", func(t *testing.T) {

			ctx, cancel, informer := setup()

			tracker, err := NewResourceTracker[*v1.Secret](informer, nil)
			assert.NotNil(t, tracker)
			assert.NoError(t, err)

			knownResources, err := tracker.KnownResources()
			assert.EqualError(t, err, "unable to convert elemnt #0 (unable to convert to object of gvk:/v1, Kind=Secret (converting (v1.ConfigMap) to (v1.Secret): unknown conversion))")
			assert.Nil(t, knownResources)

			teardown(ctx, cancel)
		})

		t.Run("list indexed known resources", func(t *testing.T) {

			ctx, cancel, informer := setup()

			tracker, err := NewResourceTracker[*v1.Secret](informer, nil)
			assert.NotNil(t, tracker)
			assert.NoError(t, err)

			knownResources, err := tracker.KnownResourcesByIndex(cache.NamespaceIndex, "namespace-b")
			assert.EqualError(t, err, "unable to convert elemnt #0 (unable to convert to object of gvk:/v1, Kind=Secret (converting (v1.ConfigMap) to (v1.Secret): unknown conversion))")
			assert.Nil(t, knownResources)

			teardown(ctx, cancel)
		})

		t.Run("add resource", func(t *testing.T) {

			ctx, cancel, informer := setup()

			tracker, err := NewResourceTracker[*v1.Secret](informer, nil)
			assert.NotNil(t, tracker)
			assert.NoError(t, err)

			//wait the initial state to settle (this avoids all object getting pushed into the created channel)
			waitMillis(100)

			newResourceChan := make(chan *v1.Secret)
			tracker.RegisterAddedResourceChannel(newResourceChan)
			chanWg := sync.WaitGroup{}
			chanWg.Add(1)

			additionalResource := v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-new", Namespace: "namespace-b"}}

			addedResource := additionalResource.DeepCopy()
			addedResource.SetCreationTimestamp(k8sTesting.FakeCreationTimestamp)

			_, err = kubernetes.Apply(ctx, &additionalResource, "", "", false)
			assert.NoError(t, err)

			go func() {
				for range newResourceChan {
					assert.Fail(t, "this should not be reached at all")
				}
				chanWg.Done()
			}()

			//wait the stuff gets propagated through the system
			waitMillis(100)

			tracker.UnregisterAddedResourceChannel(newResourceChan)

			teardown(ctx, cancel)

			if !waitTimeout(&chanWg, time.Second) {
				assert.Fail(t, "resource tracker channel was not closed")
			}

		})

		t.Run("delete resource", func(t *testing.T) {

			ctx, cancel, informer := setup()

			tracker, err := NewResourceTracker[*v1.Secret](informer, nil)
			assert.NotNil(t, tracker)
			assert.NoError(t, err)

			//wait the initial state to settle (this avoids all object getting pushed into the created channel)
			waitMillis(100)

			newResourceChan := make(chan TrackerDeleteResource[*v1.Secret])
			tracker.RegisterDeletedResourceChannel(newResourceChan)
			chanWg := sync.WaitGroup{}
			chanWg.Add(1)

			deletedResource := expectedConfigMaps[0]

			err = kubernetes.DeleteObject(ctx, deletedResource.(kubernetes.Object))
			assert.NoError(t, err)

			go func() {
				for range newResourceChan {
					assert.Fail(t, "this should not be reached at all")
				}
				chanWg.Done()
			}()

			//wait the stuff gets propagated through the system
			waitMillis(100)

			tracker.UnregisterDeletedResourceChannel(newResourceChan)

			teardown(ctx, cancel)

			if !waitTimeout(&chanWg, time.Second) {
				assert.Fail(t, "resource tracker channel was not closed")
			}
		})

		t.Run("modify resource", func(t *testing.T) {

			ctx, cancel, informer := setup()

			tracker, err := NewResourceTracker[*v1.Secret](informer, nil)
			assert.NotNil(t, tracker)
			assert.NoError(t, err)

			//wait the initial state to settle (this avoids all object getting pushed into the created channel)
			waitMillis(100)

			newResourceChan := make(chan struct {
				Old, New *v1.Secret
			})
			tracker.RegisterUpdatedResourceChannel(newResourceChan)
			chanWg := sync.WaitGroup{}
			chanWg.Add(1)

			newResource := &v1.ConfigMap{ObjectMeta: metaV1.ObjectMeta{Name: "cm-b", Namespace: "namespace-a", CreationTimestamp: k8sTesting.FakeCreationTimestamp}, Data: map[string]string{"FOO": "BAR"}}

			_, err = kubernetes.Apply(ctx, newResource, "", "", false)
			assert.NoError(t, err)

			go func() {
				for range newResourceChan {
					assert.Fail(t, "this should not be reached at all")
				}
				chanWg.Done()
			}()

			//wait the stuff gets propagated through the system
			waitMillis(100)

			tracker.UnregisterUpdatedResourceChannel(newResourceChan)

			teardown(ctx, cancel)

			if !waitTimeout(&chanWg, time.Second) {
				assert.Fail(t, "resource tracker channel was not closed")
			}
		})

	})

}
