package testing

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/kubernetes"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/kubernetes/resourceTracker"
	"fmt"
	"sync"
)

type ResourceTrackerIndexKey struct {
	IndexName, IndexValue string
}

//goland:noinspection GoUnusedExportedFunction
func CreateFakeResourceTracker[TObj kubernetes.Object](known []TObj, indexed map[ResourceTrackerIndexKey][]TObj) *FakeResourceTracker[TObj] {

	fakeTracker := FakeResourceTracker[TObj]{
		RWMutex:                       sync.RWMutex{},
		Resources:                     known,
		IndexedResources:              indexed,
		AddedResourceHandlerClients:   map[chan TObj]bool{},
		DeletedResourceHandlerClients: map[chan resourceTracker.TrackerDeleteResource[TObj]]bool{},
		UpdatedResourceHandlerClients: map[chan struct{ Old, New TObj }]bool{},
	}

	return &fakeTracker
}

type FakeResourceTracker[TObj kubernetes.Object] struct {
	sync.RWMutex

	Resources        []TObj
	IndexedResources map[ResourceTrackerIndexKey][]TObj

	AddedResourceHandlerClients   map[chan TObj]bool
	DeletedResourceHandlerClients map[chan resourceTracker.TrackerDeleteResource[TObj]]bool
	UpdatedResourceHandlerClients map[chan struct{ Old, New TObj }]bool
}

func (f *FakeResourceTracker[TObj]) KnownResources() ([]TObj, error) {
	return f.Resources, nil
}

func (f *FakeResourceTracker[TObj]) KnownResourcesByIndex(indexName string, indexValue string) ([]TObj, error) {

	indexedObject, found := f.IndexedResources[ResourceTrackerIndexKey{indexName, indexValue}]

	if !found {
		return nil, fmt.Errorf("unknown index entry with name:%s, value:%s", indexName, indexValue)
	}

	return indexedObject, nil
}

func (f *FakeResourceTracker[TObj]) RegisterAddedResourceChannel(client chan TObj) {
	f.Lock()
	defer f.Unlock()
	f.AddedResourceHandlerClients[client] = true
}

func (f *FakeResourceTracker[TObj]) UnregisterAddedResourceChannel(objs chan TObj) {
	f.Lock()
	defer f.Unlock()
	delete(f.AddedResourceHandlerClients, objs)
	close(objs)
}

func (f *FakeResourceTracker[TObj]) RegisterDeletedResourceChannel(objs chan resourceTracker.TrackerDeleteResource[TObj]) {
	f.Lock()
	defer f.Unlock()
	f.DeletedResourceHandlerClients[objs] = true
}

func (f *FakeResourceTracker[TObj]) UnregisterDeletedResourceChannel(objs chan resourceTracker.TrackerDeleteResource[TObj]) {
	f.Lock()
	defer f.Unlock()
	delete(f.DeletedResourceHandlerClients, objs)
	close(objs)
}

func (f *FakeResourceTracker[TObj]) RegisterUpdatedResourceChannel(objs chan struct{ Old, New TObj }) {
	f.Lock()
	defer f.Unlock()
	f.UpdatedResourceHandlerClients[objs] = true
}

func (f *FakeResourceTracker[TObj]) UnregisterUpdatedResourceChannel(objs chan struct{ Old, New TObj }) {
	f.Lock()
	defer f.Unlock()
	delete(f.UpdatedResourceHandlerClients, objs)
	close(objs)
}

func (f *FakeResourceTracker[TObj]) PublishAddResource(res TObj) {
	f.RLock()
	defer f.RUnlock()

	for addedPodChan := range f.AddedResourceHandlerClients {
		addedPodChan <- res
	}
}

func (f *FakeResourceTracker[TObj]) PublishUpdateResource(old, new TObj) {
	f.RLock()
	defer f.RUnlock()

	for addedPodChan := range f.UpdatedResourceHandlerClients {
		addedPodChan <- struct{ Old, New TObj }{old, new}
	}
}

func (f *FakeResourceTracker[TObj]) PublishDeleteResource(res TObj) {
	f.RLock()
	defer f.RUnlock()

	for addedPodChan := range f.DeletedResourceHandlerClients {
		addedPodChan <- resourceTracker.TrackerDeleteResource[TObj]{
			Resource:     res,
			UnknownState: false,
		}
	}
}

func (f *FakeResourceTracker[TObj]) PublishAbnormalDeleteResource(res TObj, rawRes interface{}, resKey string) {
	f.RLock()
	defer f.RUnlock()

	for addedPodChan := range f.DeletedResourceHandlerClients {
		addedPodChan <- resourceTracker.TrackerDeleteResource[TObj]{
			Resource:     res,
			ResourceKey:  resKey,
			RawResource:  rawRes,
			UnknownState: true,
		}
	}
}
