package serverSide

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/common"
	commonKubernetes "bitbucket.org/TIKI-Institut/ai-common-go/v2/kubernetes"
	"context"
	"fmt"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"time"
)

func annotate(ctx context.Context, obj metaV1.Object, fieldManager string, reason string) {

	annotations := obj.GetAnnotations()

	if annotations == nil {
		annotations = map[string]string{}
	}

	if len(reason) > 0 {
		annotations[commonKubernetes.DspLabel(fmt.Sprintf("%s_%s", commonKubernetes.DspChangeReasonAnnotation, fieldManager))] = reason
	}

	annotations[commonKubernetes.DspLabel(fmt.Sprintf("%s_%s", commonKubernetes.DspChangeDateAnnotation, fieldManager))] = common.NowTime(ctx).Format(time.RFC3339)

	if len(annotations) > 0 {
		obj.SetAnnotations(annotations)
	}

}
