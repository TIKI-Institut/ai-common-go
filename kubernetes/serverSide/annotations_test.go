package serverSide

import (
	commonFake "bitbucket.org/TIKI-Institut/ai-common-go/v2/common/fake"
	"context"
	"github.com/stretchr/testify/assert"
	coreV1 "k8s.io/api/core/v1"
	"testing"
	"time"
)

func TestAnnotate(t *testing.T) {

	ctx := context.Background()
	ctx = commonFake.WithNowTime(ctx, time.Date(2024, time.January, 8, 13, 32, 00, 00, time.UTC))

	t.Run("with reason", func(t *testing.T) {

		target := coreV1.Namespace{}

		annotate(ctx, &target, "test-manager", "test-reason")

		assert.EqualValues(t,
			map[string]string{
				"tiki-dsp.io/change-date_test-manager":   "2024-01-08T13:32:00Z",
				"tiki-dsp.io/change-reason_test-manager": "test-reason",
			},
			target.Annotations)
	})

	t.Run("without reason", func(t *testing.T) {

		target := coreV1.Namespace{}

		annotate(ctx, &target, "test-manager", "")

		assert.EqualValues(t,
			map[string]string{
				"tiki-dsp.io/change-date_test-manager": "2024-01-08T13:32:00Z",
			},
			target.Annotations)
	})

}
