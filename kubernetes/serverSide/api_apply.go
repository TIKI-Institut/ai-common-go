package serverSide

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/kubernetes"
	"context"
	"errors"
	"fmt"
	apiErrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/kubernetes/scheme"
)

var NoFieldManager = errors.New("no field manager set")

func Apply[T kubernetes.Object](ctx context.Context, obj T, fieldManager string, opts ...ApplyOpts) (T, error) {

	result := kubernetes.NewK8sObject[T]()

	if len(fieldManager) == 0 {
		return result, NoFieldManager
	}

	//Client
	client, _, _, err := kubernetes.ClientForObject(ctx, obj, obj.GetNamespace())
	if err != nil {
		return result, err
	}

	//Convert Structured => Unstructured
	unstructuredTarget := unstructured.Unstructured{}
	err = scheme.Scheme.Convert(obj, &unstructuredTarget, nil)
	if err != nil {
		return result, err
	}

	//Options
	options := ApplyOptions{}
	for _, opt := range opts {
		opt(&options)
	}
	options.applyOptions.FieldManager = fieldManager

	annotate(ctx, &unstructuredTarget, fieldManager, options.changeReason)

	//#1 patch server side
	//#2 convert back into a fully typed object
	//#3 PROFIT

	////////////
	// #1
	////////////
	var serverModifiedObject runtime.Object
	serverModifiedObject, err = client.Apply(ctx, obj.GetName(), &unstructuredTarget, options.applyOptions)
	if err != nil {
		if apiErrors.IsConflict(err) && options.forceAfterConflict {
			TakeOwnership(&options)
			serverModifiedObject, err = client.Apply(ctx, obj.GetName(), &unstructuredTarget, options.applyOptions)
			if err != nil {
				return result, fmt.Errorf("unable to apply changes after retrying to take ownership (%w)", err)
			}
		} else {
			return result, fmt.Errorf("unable to apply changes (%w)", err)
		}
	}

	////////////
	// #2
	////////////
	err = scheme.Scheme.Convert(serverModifiedObject, result, nil)
	if err != nil {
		return result, fmt.Errorf("unable to convert structured object to unstructured object (%w)", err)
	}

	return result, nil
}
