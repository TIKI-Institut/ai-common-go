package serverSide

import (
	commonFake "bitbucket.org/TIKI-Institut/ai-common-go/v2/common/fake"
	commonKubernetes "bitbucket.org/TIKI-Institut/ai-common-go/v2/kubernetes"
	k8sTesting "bitbucket.org/TIKI-Institut/ai-common-go/v2/kubernetes/testing"
	"context"
	"github.com/stretchr/testify/assert"
	coreV1 "k8s.io/api/core/v1"
	apiErrors "k8s.io/apimachinery/pkg/api/errors"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	clientGoTesting "k8s.io/client-go/testing"
	"sync/atomic"
	"testing"
	"time"
)

func TestApply(t *testing.T) {

	t.Run("no field manager", func(t *testing.T) {

		ctx := context.Background()

		serverModified, err := Apply[*coreV1.Namespace](ctx, &coreV1.Namespace{}, "")

		assert.EqualValues(t, &coreV1.Namespace{}, serverModified)
		assert.ErrorIs(t, err, NoFieldManager)

	})

	t.Run("test serverside apply", func(t *testing.T) {

		cm := coreV1.ConfigMap{
			ObjectMeta: metaV1.ObjectMeta{
				Name:      "cm-test",
				Namespace: "sandbox",
				Labels: map[string]string{
					"blub": "FOO",
				},
			},
		}

		expectedAppliedCm := cm.DeepCopy()
		expectedAppliedCm.Annotations = map[string]string{
			"tiki-dsp.io/change-date_unit-test": "2024-01-08T13:32:00Z",
		}

		expectedResultCm := expectedAppliedCm.DeepCopy()
		expectedResultCm.CreationTimestamp = k8sTesting.FakeCreationTimestamp

		t.Run("new object", func(t *testing.T) {

			t.Run("creation", func(t *testing.T) {
				ctx, dynamicClient := k8sTesting.CreateFakeClient(nil)

				ctx = commonFake.WithNowTime(ctx, time.Date(2024, time.January, 8, 13, 32, 00, 00, time.UTC))

				serverObject, err := Apply(ctx, &cm, "unit-test")

				assert.NoError(t, err)
				assert.EqualValues(t, expectedResultCm, serverObject)

				actions := dynamicClient.Actions()
				assert.Len(t, actions, 1)

				k8sTesting.AssertServerSideApply(t, actions[0], "configmaps", "sandbox", expectedAppliedCm)
			})

			t.Run("object is in tracker after creation", func(t *testing.T) {
				ctx, dynamicClient := k8sTesting.CreateFakeClient(nil)

				ctx = commonFake.WithNowTime(ctx, time.Date(2024, time.January, 8, 13, 32, 00, 00, time.UTC))

				serverObject, err := Apply(ctx, &cm, "unit-test")

				assert.NoError(t, err)
				assert.EqualValues(t, expectedResultCm, serverObject)

				loadedObj := coreV1.ConfigMap{
					ObjectMeta: metaV1.ObjectMeta{
						Name:      "cm-test",
						Namespace: "sandbox",
					},
				}

				err = commonKubernetes.GetObject(ctx, &loadedObj)
				assert.NoError(t, err)

				actions := dynamicClient.Actions()
				assert.Len(t, actions, 2)

				k8sTesting.AssertServerSideApply(t, actions[0], "configmaps", "sandbox", expectedAppliedCm)
				k8sTesting.AssertGet(t, actions[1], "configmaps", "sandbox", "cm-test")

				assert.EqualValues(t, expectedResultCm, &loadedObj)
			})

			t.Run("patch existing object, new property", func(t *testing.T) {
				ctx, dynamicClient := k8sTesting.CreateFakeClient(nil, cm.DeepCopy())

				ctx = commonFake.WithNowTime(ctx, time.Date(2024, time.January, 8, 13, 32, 00, 00, time.UTC))

				changedObject := cm.DeepCopy()
				changedObject.Data = map[string]string{"asdf": "1234"}

				localExpectedAppliedCm := expectedAppliedCm.DeepCopy()
				localExpectedAppliedCm.Data = map[string]string{"asdf": "1234"}

				localExpectedResultCm := localExpectedAppliedCm.DeepCopy()
				localExpectedResultCm.CreationTimestamp = k8sTesting.FakeCreationTimestamp

				serverObject, err := Apply(ctx, changedObject, "unit-test")

				assert.NoError(t, err)
				assert.EqualValues(t, localExpectedResultCm, serverObject)

				loadedObj := coreV1.ConfigMap{
					ObjectMeta: metaV1.ObjectMeta{
						Name:      "cm-test",
						Namespace: "sandbox",
					},
				}

				err = commonKubernetes.GetObject(ctx, &loadedObj)
				assert.NoError(t, err)

				actions := dynamicClient.Actions()
				assert.Len(t, actions, 2)

				k8sTesting.AssertServerSideApply(t, actions[0], "configmaps", "sandbox", localExpectedAppliedCm)
				k8sTesting.AssertGet(t, actions[1], "configmaps", "sandbox", "cm-test")
				assert.EqualValues(t, localExpectedResultCm, &loadedObj)
			})

			t.Run("patch existing object, field conflict do not take ownership", func(t *testing.T) {
				ctx, dynamicClient := k8sTesting.CreateFakeClient(nil, cm.DeepCopy())

				firstHit := atomic.Bool{}
				dynamicClient.PrependReactor("patch", "configmaps", func(ac clientGoTesting.Action) (handled bool, ret runtime.Object, err error) {
					if firstHit.Load() {
						return false, nil, nil
					}

					firstHit.Store(true)
					return true, nil, apiErrors.NewApplyConflict(nil, "some-field-conflict")
				})

				ctx = commonFake.WithNowTime(ctx, time.Date(2024, time.January, 8, 13, 32, 00, 00, time.UTC))

				changedObject := cm.DeepCopy()
				changedObject.Data = map[string]string{"asdf": "1234"}

				serverObject, err := Apply(ctx, changedObject, "unit-test")

				assert.EqualError(t, err, "unable to apply changes (some-field-conflict)")
				assert.EqualValues(t, &coreV1.ConfigMap{}, serverObject)

				actions := dynamicClient.Actions()
				assert.Len(t, actions, 1)

				expectedCm := cm.DeepCopy()
				expectedCm.Annotations = map[string]string{
					"tiki-dsp.io/change-date_unit-test": "2024-01-08T13:32:00Z",
				}
				expectedCm.Data = map[string]string{"asdf": "1234"}

				k8sTesting.AssertServerSideApply(t, actions[0], "configmaps", "sandbox", expectedCm)
			})

			t.Run("patch existing object, field conflict take ownership", func(t *testing.T) {
				ctx, dynamicClient := k8sTesting.CreateFakeClient(nil, cm.DeepCopy())

				firstHit := atomic.Bool{}
				dynamicClient.PrependReactor("patch", "configmaps", func(ac clientGoTesting.Action) (handled bool, ret runtime.Object, err error) {
					if firstHit.Load() {
						return false, nil, nil
					}

					firstHit.Store(true)
					return true, nil, apiErrors.NewApplyConflict(nil, "some-field-conflict")
				})

				ctx = commonFake.WithNowTime(ctx, time.Date(2024, time.January, 8, 13, 32, 00, 00, time.UTC))

				changedObject := cm.DeepCopy()
				changedObject.Data = map[string]string{"asdf": "1234"}

				localExpectedResultCm := expectedResultCm.DeepCopy()
				localExpectedResultCm.Data = map[string]string{"asdf": "1234"}

				localExpectedAppliedCm := expectedAppliedCm.DeepCopy()
				localExpectedAppliedCm.Data = map[string]string{"asdf": "1234"}

				serverObject, err := Apply(ctx, changedObject, "unit-test", TakeOwnershipAfterConflict)

				assert.NoError(t, err)
				assert.EqualValues(t, localExpectedResultCm, serverObject)

				actions := dynamicClient.Actions()
				assert.Len(t, actions, 2)

				k8sTesting.AssertServerSideApply(t, actions[0], "configmaps", "sandbox", localExpectedAppliedCm)
				k8sTesting.AssertServerSideApply(t, actions[1], "configmaps", "sandbox", localExpectedAppliedCm)
			})
		})
	})
}
