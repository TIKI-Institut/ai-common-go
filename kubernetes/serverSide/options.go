package serverSide

import metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

type ApplyOpts func(*ApplyOptions)

type ApplyOptions struct {
	changeReason       string
	forceAfterConflict bool
	applyOptions       metav1.ApplyOptions
}

func WithReason(reason string) ApplyOpts {
	return func(options *ApplyOptions) {
		options.changeReason = reason
	}
}

func TakeOwnership(options *ApplyOptions) {
	options.applyOptions.Force = true
}

func TakeOwnershipAfterConflict(options *ApplyOptions) {
	options.forceAfterConflict = true
}
