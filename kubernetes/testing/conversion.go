package testing

import (
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/kubernetes/scheme"
)

func ConvertToUnstructured(obj runtime.Object) *unstructured.Unstructured {
	tmp := unstructured.Unstructured{}

	err := scheme.Scheme.Convert(obj, &tmp, nil)

	if err != nil {
		panic(err)
	}
	return &tmp
}
