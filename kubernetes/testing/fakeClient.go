package testing

import (
	k8sApiContext "bitbucket.org/TIKI-Institut/ai-common-go/v2/kubernetes/context"
	"context"
	"fmt"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/meta/testrestmapper"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/apimachinery/pkg/util/strategicpatch"
	dynamicFake "k8s.io/client-go/dynamic/fake"
	"k8s.io/client-go/kubernetes/scheme"
	k8sTesting "k8s.io/client-go/testing"
	"time"
)

//FakeCreationTimestamp should use time.Local as time.Location here due to the metaV1.Date json deserialization impl
//see : k8s.io\apimachinery@v0.28.2\pkg\apis\meta\v1\time.go (UnmarshalJSON) 't.Time = pt.Local()'
//var FakeCreationTimestamp = metaV1.Date(2022, time.October, 19, 16, 10, 00, 00, time.Local)
var FakeCreationTimestampUtcRFC3339String = "2022-10-19T14:10:00Z"
var FakeCreationTimestamp metaV1.Time = metaV1.Time{}

func init() {
	parsed, err := time.Parse(time.RFC3339, FakeCreationTimestampUtcRFC3339String)
	if err != nil {
		panic(err)
	}
	FakeCreationTimestamp = metaV1.NewTime(parsed.Local())
}

func CreateFakeClient(ctx context.Context, objs ...runtime.Object) (context.Context, *dynamicFake.FakeDynamicClient) {

	if ctx == nil {
		ctx = context.Background()
	}

	restMapper := testrestmapper.TestOnlyStaticRESTMapper(scheme.Scheme)

	for i := 0; i < len(objs); i++ {
		if o, success := objs[i].(metaV1.Object); success {
			o.SetCreationTimestamp(FakeCreationTimestamp)
		}
	}

	dynamicClient := dynamicFake.NewSimpleDynamicClient(scheme.Scheme, objs...)

	dynamicClient.PrependReactor("patch", "*", handleServersidePatchForNonExisting(dynamicClient))
	dynamicClient.PrependReactor("create", "*", injectCreationTimestamp)

	ctx = k8sApiContext.NewContextWithDynamicClient(ctx, dynamicClient)
	ctx = k8sApiContext.NewContextWithRestMapper(ctx, restMapper)

	return ctx, dynamicClient
}

func injectCreationTimestamp(action k8sTesting.Action) (handled bool, ret runtime.Object, err error) {

	createAction, success := action.(k8sTesting.CreateAction)
	if !success {
		return false, nil, fmt.Errorf("given action is not a create action")
	}

	targetObject := createAction.GetObject()

	if metaObject, success := targetObject.(metaV1.Object); success {
		metaObject.SetCreationTimestamp(FakeCreationTimestamp)
		return false, targetObject, nil
	}

	return false, nil, nil
}

func handleServersidePatchForNonExisting(client *dynamicFake.FakeDynamicClient) k8sTesting.ReactionFunc {

	return func(action k8sTesting.Action) (bool, runtime.Object, error) {
		patchAction, success := action.(k8sTesting.PatchAction)
		if !success {
			return false, nil, fmt.Errorf("given action is not a patch action")
		}

		//Go ahead normally if not a Server-Side-Apply
		if patchAction.GetPatchType() != types.ApplyPatchType {
			return false, nil, nil
		}

		decodedPatchObject, err := runtime.Decode(unstructured.UnstructuredJSONScheme, patchAction.GetPatch())
		if err != nil {
			return false, nil, fmt.Errorf("unable to decode patch to object")
		}

		convertedPatchObject, success := decodedPatchObject.(metaV1.Object)
		if !success {
			return false, nil, fmt.Errorf("unable to convert patch object to metaV1.Object")
		}

		tracker := client.Tracker()
		trackerExisting, getError := tracker.Get(patchAction.GetResource(), patchAction.GetNamespace(), patchAction.GetName())

		if getError != nil {
			if errors.IsNotFound(getError) {
				//Create instead of patch....
				convertedPatchObject.SetCreationTimestamp(FakeCreationTimestamp)

				err = tracker.Create(patchAction.GetResource(), decodedPatchObject, patchAction.GetNamespace())
				if err != nil {
					return false, nil, fmt.Errorf("unable to create object")
				}

				getObj, err := tracker.Get(patchAction.GetResource(), patchAction.GetNamespace(), patchAction.GetName())
				return true, getObj, err
			}
			return false, nil, fmt.Errorf("unable to load tracker object (%w)", getError)
		}

		convertedTrackerExisting, success := trackerExisting.(metaV1.Object)
		if !success {
			return false, nil, fmt.Errorf("unable to convert tracker object to metaV1.Object")
		}

		//load patch object, copy opaque fields from existing to patch object; alter patch []byte s

		// 1. copy opaque fields
		convertedPatchObject.SetSelfLink(convertedTrackerExisting.GetSelfLink())
		convertedPatchObject.SetUID(convertedTrackerExisting.GetUID())
		convertedPatchObject.SetResourceVersion(convertedTrackerExisting.GetResourceVersion())
		convertedPatchObject.SetGeneration(convertedTrackerExisting.GetGeneration())
		convertedPatchObject.SetCreationTimestamp(convertedTrackerExisting.GetCreationTimestamp())
		convertedPatchObject.SetManagedFields(convertedTrackerExisting.GetManagedFields())

		patchSchema := tracker.LookupPatchMeta(trackerExisting)
		if patchSchema == nil {
			patchSchema, err = strategicpatch.NewPatchMetaFromStruct(trackerExisting)
			if err != nil {
				return true, nil, err
			}
		}

		oldBytes, err := runtime.Encode(unstructured.UnstructuredJSONScheme, trackerExisting)
		if err != nil {
			return false, nil, fmt.Errorf("unable to enocde existing object")
		}

		newBytes, err := runtime.Encode(unstructured.UnstructuredJSONScheme, decodedPatchObject)
		if err != nil {
			return false, nil, fmt.Errorf("unable to encode modified new object")
		}

		mergedByte, err := strategicpatch.StrategicMergePatchUsingLookupPatchMeta(oldBytes, newBytes, patchSchema)
		if err != nil {
			return true, nil, fmt.Errorf("unable to apply patch")
		}

		updateObject, err := runtime.Decode(unstructured.UnstructuredJSONScheme, mergedByte)
		if err != nil {
			return false, nil, fmt.Errorf("unable to decode modified object bytes")
		}

		if err = tracker.Update(patchAction.GetResource(), updateObject, patchAction.GetNamespace()); err != nil {
			return true, nil, fmt.Errorf("unable to update object bytes")
		}

		return true, updateObject, nil
	}

}
