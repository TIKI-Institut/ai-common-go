package testing

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	k8sTesting "k8s.io/client-go/testing"
	"testing"
)

func validateAction(t *testing.T, ac k8sTesting.Action, verb string, resource string, conversionSuccess bool) bool {

	if !conversionSuccess {
		return assert.Fail(t, fmt.Sprintf("wrong action type; expected '%s' got '%s'", verb, ac.GetVerb()))
	}

	if !ac.Matches(verb, resource) {
		return assert.Fail(t, fmt.Sprintf("wrong resource type; expected '%s' got '%s'", resource, ac.GetResource().Resource))
	}

	return true
}

func AssertList(t *testing.T, ac k8sTesting.Action, resource string, namespace string, fieldSelector string, labelSelector string) bool {

	acConverted, success := ac.(k8sTesting.ListAction)
	if validateAction(t, ac, "list", resource, success) {
		givenFieldSelector := ""
		if acConverted.GetListRestrictions().Fields != nil {
			givenFieldSelector = acConverted.GetListRestrictions().Fields.String()
		}

		givenLabelSelector := ""
		if acConverted.GetListRestrictions().Labels != nil {
			givenLabelSelector = acConverted.GetListRestrictions().Labels.String()
		}

		return assert.EqualValues(t, namespace, acConverted.GetNamespace()) &&
			assert.EqualValues(t, fieldSelector, givenFieldSelector) &&
			assert.EqualValues(t, labelSelector, givenLabelSelector)
	}

	return false
}

func AssertCreate(t *testing.T, ac k8sTesting.Action, resource string, namespace string, obj runtime.Object) bool {

	acConverted, success := ac.(k8sTesting.CreateAction)
	if validateAction(t, ac, "create", resource, success) {
		return assert.EqualValues(t, namespace, acConverted.GetNamespace()) &&
			AssertEqualObjects(t, obj, acConverted.GetObject())
	}
	return false
}

func AssertGet(t *testing.T, ac k8sTesting.Action, resource string, namespace string, name string) bool {

	acConverted, success := ac.(k8sTesting.GetAction)
	if validateAction(t, ac, "get", resource, success) {
		return assert.EqualValues(t, namespace, acConverted.GetNamespace()) &&
			assert.EqualValues(t, name, acConverted.GetName())
	}
	return false
}

func AssertDelete(t *testing.T, ac k8sTesting.Action, resource string, namespace string, name string) bool {

	acConverted, success := ac.(k8sTesting.DeleteAction)
	if validateAction(t, ac, "delete", resource, success) {
		return assert.EqualValues(t, namespace, acConverted.GetNamespace()) &&
			assert.EqualValues(t, name, acConverted.GetName())
	}
	return false
}

func reformatJson(t *testing.T, data []byte) (string, bool) {

	target := map[string]interface{}{}

	err := json.Unmarshal(data, &target)
	if !assert.NoError(t, err, "unable to parse json data") {
		return "", false
	}

	buf := bytes.Buffer{}
	jsonEncoder := json.NewEncoder(&buf)
	jsonEncoder.SetIndent("", "  ")
	err = jsonEncoder.Encode(target)
	if !assert.NoError(t, err, "unable to serialize json data") {
		return "", false
	}

	return buf.String(), true
}

func AssertPatch(t *testing.T, ac k8sTesting.Action, resource string, namespace string, name string, patchType types.PatchType, data []byte) bool {

	acConverted, success := ac.(k8sTesting.PatchAction)
	if validateAction(t, ac, "patch", resource, success) {
		//data is allways a json object sooo....
		expected, success := reformatJson(t, data)
		assert.True(t, success)

		actual, success := reformatJson(t, acConverted.GetPatch())
		assert.True(t, success)

		if !t.Failed() {
			return assert.EqualValues(t, namespace, acConverted.GetNamespace()) &&
				assert.EqualValues(t, name, acConverted.GetName()) &&
				assert.EqualValues(t, patchType, acConverted.GetPatchType()) &&
				assert.EqualValues(t, expected, actual)
		}
	}

	return false
}

func AssertWatch(t *testing.T, ac k8sTesting.Action, resource string, namespace string, fieldSelector string, labelSelector string, resourceVersion string) bool {

	acConverted, success := ac.(k8sTesting.WatchAction)
	if validateAction(t, ac, "watch", resource, success) {
		givenFieldSelector := ""
		if acConverted.GetWatchRestrictions().Fields != nil {
			givenFieldSelector = acConverted.GetWatchRestrictions().Fields.String()
		}

		givenLabelSelector := ""
		if acConverted.GetWatchRestrictions().Labels != nil {
			givenLabelSelector = acConverted.GetWatchRestrictions().Labels.String()
		}

		return assert.EqualValues(t, namespace, acConverted.GetNamespace()) &&
			assert.EqualValues(t, fieldSelector, givenFieldSelector) &&
			assert.EqualValues(t, labelSelector, givenLabelSelector) &&
			assert.EqualValues(t, resourceVersion, acConverted.GetWatchRestrictions().ResourceVersion)
	}

	return false
}

func AssertGenericAction(t *testing.T, ac k8sTesting.Action, verb string, resource string, namespace string, predicate func(t *testing.T, ac k8sTesting.GenericAction)) bool {

	acConverted, success := ac.(k8sTesting.GenericAction)
	if validateAction(t, ac, verb, resource, success) {
		assert.EqualValues(t, namespace, acConverted.GetNamespace())

		if t.Failed() {
			return false
		}

		if predicate != nil {
			predicate(t, acConverted)
		}

		return !t.Failed()
	}
	return false
}

func AssertServerSideApply(t *testing.T, ac k8sTesting.Action, resource string, namespace string, obj runtime.Object) bool {

	acConverted, success := ac.(k8sTesting.PatchAction)
	if !validateAction(t, ac, "patch", resource, success) {
		return false
	}

	if !assert.EqualValues(t, types.ApplyPatchType, acConverted.GetPatchType()) {
		return false
	}

	//Create instead of patch....
	patchObj, err := runtime.Decode(unstructured.UnstructuredJSONScheme, acConverted.GetPatch())
	if !assert.EqualValues(t, namespace, acConverted.GetNamespace()) || !assert.NoError(t, err) {
		return false
	}

	return AssertEqualObjects(t, obj, patchObj)

}
