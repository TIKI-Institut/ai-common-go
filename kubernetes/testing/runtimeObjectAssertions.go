package testing

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/kubernetes/scheme"
	"testing"
)

func isEqualObjects(a runtime.Object, b runtime.Object) (bool, error) {

	expectedGvks, _, err := scheme.Scheme.ObjectKinds(a)
	if err != nil {
		return false, err
	}

	actualGvks, _, err := scheme.Scheme.ObjectKinds(b)
	if err != nil {
		return false, err
	}

	expectedGvk := expectedGvks[0]
	actualGvk := actualGvks[0]

	if !assert.ObjectsAreEqualValues(expectedGvk, actualGvk) {
		return false, fmt.Errorf("GroupVersionKind of expected object differs from GroupVersionKind of given object")
	}

	typedExpectedContainer, err := scheme.Scheme.New(expectedGvk)
	if err != nil {
		return false, fmt.Errorf("unable to create typed container for expected object (%w)", err)
	}

	typedActualContainer, err := scheme.Scheme.New(actualGvk)
	if err != nil {
		return false, fmt.Errorf("unable to create typed container for actual object (%w)", err)
	}

	err = scheme.Scheme.Convert(a, typedExpectedContainer, nil)
	if err != nil {
		return false, fmt.Errorf("unable to convert expected object into its typed form (%w)", err)
	}

	err = scheme.Scheme.Convert(b, typedActualContainer, nil)
	if err != nil {
		return false, fmt.Errorf("unable to convert actual object into its typed form (%w)", err)
	}

	return assert.ObjectsAreEqualValues(typedExpectedContainer, typedActualContainer), nil
}

func AssertEqualObjects(t *testing.T, expected runtime.Object, actual runtime.Object) bool {

	expectedGvks, _, err := scheme.Scheme.ObjectKinds(expected)
	if !assert.NoError(t, err) {
		return false
	}

	actualGvks, _, err := scheme.Scheme.ObjectKinds(actual)
	if !assert.NoError(t, err) {
		return false
	}

	expectedGvk := expectedGvks[0]
	actualGvk := actualGvks[0]

	if !assert.EqualValues(t, expectedGvk, actualGvk, "GroupVersionKind of expected object differs from GroupVersionKind of given object") {
		return false
	}

	typedExpectedContainer, err := scheme.Scheme.New(expectedGvk)
	if !assert.NoError(t, err, "unable to create typed container for expected object") {
		return false
	}

	typedActualContainer, err := scheme.Scheme.New(actualGvk)
	if !assert.NoError(t, err, "unable to create typed container for actual object") {
		return false
	}

	err = scheme.Scheme.Convert(expected, typedExpectedContainer, nil)
	if !assert.NoError(t, err, "unable to convert expected object into its typed form") {
		return false
	}

	err = scheme.Scheme.Convert(actual, typedActualContainer, nil)
	if !assert.NoError(t, err, "unable to convert actual object into its typed form") {
		return false
	}

	return assert.EqualValues(t, typedExpectedContainer, typedActualContainer)
}

func AssertContainsObjects[T runtime.Object](t *testing.T, expected runtime.Object, in []T) bool {

	for i := 0; i < len(in); i++ {

		equal, err := isEqualObjects(expected, in[i])
		if !assert.NoError(t, err) {
			return false
		}

		if equal {
			return true
		}

	}

	return assert.Fail(t, fmt.Sprintf("exptected object is not in the list of given ones"))

}

func AssertContainsAllObjects[T runtime.Object](t *testing.T, expected []runtime.Object, in []T) bool {

	for i := 0; i < len(expected); i++ {

		if !AssertContainsObjects(t, expected[i], in) {
			return assert.Fail(t, fmt.Sprintf("exptected object #%d is not in the list of given ones", i))
		}
	}

	return true
}
