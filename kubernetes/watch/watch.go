package watch

import (
	commonK8s "bitbucket.org/TIKI-Institut/ai-common-go/v2/kubernetes"
	"context"
	"fmt"
	"github.com/rs/zerolog/log"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/watch"
	"time"
)

// EventHandlers
// Deprecated: This was replaced by ResourceTracker
type EventHandlers struct {
	groupVersionKind schema.GroupVersionKind

	adders    []Adder
	modifiers []Modifier
	deleters  []Deleter
}

type Adder interface {
	Add(k8sObject runtime.Object) error
}

type Modifier interface {
	Modify(k8sObject runtime.Object) error
}

type Deleter interface {
	Delete(k8sObject runtime.Object) error
}

// NewEventHandlers
// Deprecated: This was replaced by ResourceTracker
func NewEventHandlers(gvk schema.GroupVersionKind, adders []Adder, modifiers []Modifier, deleters []Deleter) *EventHandlers {
	return &EventHandlers{
		groupVersionKind: gvk,

		adders:    adders,
		modifiers: modifiers,
		deleters:  deleters,
	}
}

// Watch
// Deprecated: This was replaced by ResourceTracker
func Watch(ctx context.Context, eventHandlers *EventHandlers) error {

	if eventHandlers.groupVersionKind.Empty() {
		return fmt.Errorf("no group version kind for collection set")
	}

	channel := make(chan watch.Event)

	go func(ctx context.Context, conf *EventHandlers) {
		var wInterface watch.Interface
		var err error

		defer func() {
			if wInterface != nil {
				wInterface.Stop()
			}
		}()

	WatchLoop:
		for {
			wInterface, err = commonK8s.Watch(ctx, conf.groupVersionKind, "", metaV1.ListOptions{})
			if err != nil {
				log.Ctx(ctx).Err(err).Msg("failed to start k8s collector watch; wait 1min")
				<-time.After(time.Minute)
				continue
			}
		ResultLoop:
			for {
				select {
				case <-ctx.Done():
					break WatchLoop
				case e, ok := <-wInterface.ResultChan():
					if !ok {
						log.Ctx(ctx).Warn().Msg("k8s collector result channel closed, will restart watch.")
						wInterface.Stop()
						<-time.After(100 * time.Millisecond)
						break ResultLoop
					}

					channel <- e
				}
			}
		}
	}(ctx, eventHandlers)

	go func(ctx context.Context, eventHandlers *EventHandlers, channel chan watch.Event) {
		for event := range channel {

			runtimeObject := event.Object
			log.Ctx(ctx).Trace().Str("object", runtimeObject.GetObjectKind().GroupVersionKind().String()).Msg("processing runtime object")

			switch event.Type {

			case watch.Added:
				log.Ctx(ctx).Trace().Str("object", runtimeObject.GetObjectKind().GroupVersionKind().String()).Msg("adding object")
				for _, adder := range eventHandlers.adders {
					if err := adder.Add(runtimeObject); err != nil {
						log.Warn().Str("object", runtimeObject.GetObjectKind().GroupVersionKind().String()).Msg("could not add runtime object")
					}
				}

			case watch.Modified:
				log.Ctx(ctx).Trace().Str("object", runtimeObject.GetObjectKind().GroupVersionKind().String()).Msg("modifying object")
				for _, modifier := range eventHandlers.modifiers {
					if err := modifier.Modify(runtimeObject); err != nil {
						log.Warn().Str("object", runtimeObject.GetObjectKind().GroupVersionKind().String()).Msg("could not modify runtime object")
					}
				}

			case watch.Deleted:
				log.Ctx(ctx).Trace().Str("object", runtimeObject.GetObjectKind().GroupVersionKind().String()).Msg("deleting object")
				for _, deleter := range eventHandlers.deleters {
					if err := deleter.Delete(runtimeObject); err != nil {
						log.Warn().Str("object", runtimeObject.GetObjectKind().GroupVersionKind().String()).Msg("could not delete runtime object")
					}
				}

			default:
				log.Ctx(ctx).Warn().Str("event", string(event.Type)).Msg("unknown event was received on watch channel")
			}
		}
	}(ctx, eventHandlers, channel)

	return nil
}
