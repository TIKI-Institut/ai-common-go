package logging

import (
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"os"
)

type LoggerOptions func(logger zerolog.Logger) zerolog.Logger

//goland:noinspection GoUnusedExportedFunction
func SetupLogger(logLevelEnvVariable string, opts ...LoggerOptions) {
	logLvl := zerolog.InfoLevel
	if envLvl, ok := os.LookupEnv(logLevelEnvVariable); ok {
		if lvl, err := zerolog.ParseLevel(envLvl); err == nil { // parse string, this is built-in feature of zerolog
			logLvl = lvl
		}
	}
	zerolog.SetGlobalLevel(logLvl)

	logger := log.Level(logLvl)
	for i := 0; i < len(opts); i++ {
		logger = opts[i](logger)
	}
	log.Logger = logger
	log.Debug().Str("log level", logLvl.String()).Msg("Setting log level")
}
