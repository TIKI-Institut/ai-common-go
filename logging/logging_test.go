package logging

import (
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

func TestSetupLogger(t *testing.T) {
	t.Run("default logger", func(t *testing.T) {
		SetupLogger("SOME_NOT_EXISTING_ENV_VAR")
		assert.Equal(t, zerolog.InfoLevel, log.Logger.GetLevel())
		assert.Equal(t, zerolog.InfoLevel, zerolog.GlobalLevel())
	})
	t.Run("valid log level", func(t *testing.T) {
		assert.NoError(t, os.Setenv("VALID_LOG_LEVEL_ENV_VAR", "trace"))
		SetupLogger("VALID_LOG_LEVEL_ENV_VAR")
		assert.Equal(t, zerolog.TraceLevel, log.Logger.GetLevel())
		assert.Equal(t, zerolog.TraceLevel, zerolog.GlobalLevel())
	})
	t.Run("invalid log level", func(t *testing.T) {
		assert.NoError(t, os.Setenv("INVALID_LOG_LEVEL_ENV_VAR", "some_invalid_level"))
		SetupLogger("INVALID_LOG_LEVEL_ENV_VAR")
		assert.Equal(t, zerolog.InfoLevel, log.Logger.GetLevel())
		assert.Equal(t, zerolog.InfoLevel, zerolog.GlobalLevel())
	})
}
