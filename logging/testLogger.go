package logging

import (
	"bytes"
	"context"
	"fmt"
	"github.com/rs/zerolog"
	"sync"
	"testing"
)

type testLogger struct {
	*testing.T
}

type testBufferLogger struct {
	locker sync.Mutex
	buffer bytes.Buffer
}

func (t *testBufferLogger) String() string {
	t.locker.Lock()
	defer t.locker.Unlock()
	return t.buffer.String()
}

func (t *testBufferLogger) Write(p []byte) (n int, err error) {
	t.locker.Lock()
	defer t.locker.Unlock()
	return t.buffer.Write(p)
}

func (t *testLogger) Write(p []byte) (n int, err error) {
	t.T.Logf(string(p))
	return len(p), nil
}

func AttachTestLogger(t *testing.T, ctx context.Context, level zerolog.Level) (context.Context, fmt.Stringer) {

	target := &testLogger{T: t}
	bufferTarget := &testBufferLogger{}

	testLog := zerolog.New(zerolog.SyncWriter(zerolog.MultiLevelWriter(bufferTarget, target))).Level(level)

	return testLog.WithContext(ctx), bufferTarget
}
