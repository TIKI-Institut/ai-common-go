package logging

import (
	"context"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestAttachTestLogger(t *testing.T) {
	//Setup
	testCtx := context.TODO()
	testCtx, logBuffer := AttachTestLogger(t, testCtx, zerolog.TraceLevel)

	//Act
	//Produce log messages
	log.Ctx(testCtx).Info().Msg("this is an info")
	log.Ctx(testCtx).Warn().Msg("this is a warning")
	log.Ctx(testCtx).Error().Msg("this is an error")

	//Assert
	//Check if the log messages are in
	assert.EqualValues(t,
		`{"level":"info","message":"this is an info"}
{"level":"warn","message":"this is a warning"}
{"level":"error","message":"this is an error"}
`,
		logBuffer.String())

}
