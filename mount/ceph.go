package mount

import (
	"bufio"
	"context"
	"fmt"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"io"
	"os/exec"
	"strings"
)

type CephFSMountOptions struct {
	CephMonitors  string `mapstructure:"cephMonitors"`  // CephMonitors to use for mount cephfs
	CephSecretKey string `mapstructure:"cephSecretKey"` // CephSecretKey is necessary for authorization
	CephUser      string `mapstructure:"cephUser"`      // CephUser is necessary for authorization
	MdsNamespace  string `mapstructure:"mdsNamespace"`  // MdsNamespace the data pool for the cephfs mount
	MountPath     string `mapstructure:"mountPath"`     // MountPath of cephfs
}

func (co CephFSMountOptions) Verify() error {

	errFormat := "'%s' of CephFSMountOptions must not be empty"

	if co.CephMonitors == "" {
		return fmt.Errorf(errFormat, "cephMonitors")
	}
	if co.CephSecretKey == "" {
		return fmt.Errorf(errFormat, "cephSecretKey")
	}
	if co.CephUser == "" {
		return fmt.Errorf(errFormat, "cephUser")
	}
	if co.MdsNamespace == "" {
		return fmt.Errorf(errFormat, "mdsNamespace")
	}
	if co.MountPath == "" {
		return fmt.Errorf(errFormat, "mountPath")
	}

	return nil
}

func (co CephFSMountOptions) makeCommand(ctx context.Context) *exec.Cmd {

	cephMonitors := parseCephMonitors(co.CephMonitors)
	cmdOptions := "name=" + co.CephUser + ",secret=" + co.CephSecretKey + ",mds_namespace=" + co.MdsNamespace

	mountArgs := []string{"-t", "ceph", cephMonitors, co.MountPath, "--verbose", "-o", cmdOptions}

	if ctx == nil {
		return exec.Command("mount", mountArgs...)
	} else {
		return exec.CommandContext(ctx, "mount", mountArgs...)
	}
}

func (co CephFSMountOptions) Mount(ctx context.Context) error {

	mountCmd := co.makeCommand(ctx)

	var cancel context.CancelFunc
	ctx, cancel = context.WithCancel(ctx)

	defer cancel()

	logger := log.Ctx(ctx).With().Str("module", "CephFSMountOptions").Logger()

	logger.Debug().Msgf("executing: %s", co.String())

	stdOutPipeReader, stdOutPipeWriter := io.Pipe()
	stdErrPipeReader, stdErrPipeWriter := io.Pipe()

	mountCmd.Stdout = stdOutPipeWriter
	mountCmd.Stderr = stdErrPipeWriter

	readUnderlying := func(source io.Reader, lines chan string) {
		s := bufio.NewScanner(source)
		for s.Scan() {
			lines <- s.Text()
		}
	}

	read := func(source io.Reader, target func() *zerolog.Event) {
		input := make(chan string)
		go readUnderlying(source, input)
		for {
			select {
			case line := <-input:
				target().Msg(line)
			case <-ctx.Done():
				return
			}
		}
	}

	go read(stdOutPipeReader, logger.Debug)
	go read(stdErrPipeReader, logger.Error)

	return mountCmd.Run()
}

func (co CephFSMountOptions) String() string {
	return strings.Replace(co.makeCommand(nil).String(), co.CephSecretKey, "*********", -1)
}

func parseCephMonitors(rawMonitorString string) string {
	return strings.Replace(rawMonitorString, " ", "", -1) + ":/"
}
