package mount

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestParseCephMonitors(t *testing.T) {
	tests := []struct {
		name          string
		monitorString string
		want          string
	}{
		{
			name:          "test monitor string",
			monitorString: "10.100.4.1:6789,10.100.4.2:6789,10.100.4.3:6789",
			want:          "10.100.4.1:6789,10.100.4.2:6789,10.100.4.3:6789:/",
		},
		{
			name:          "test monitor string with spaces",
			monitorString: "10.100.4.1:6789 ,10.100.4.2:6789 ,10.100.4.3:6789",
			want:          "10.100.4.1:6789,10.100.4.2:6789,10.100.4.3:6789:/",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := parseCephMonitors(tt.monitorString); got != tt.want {
				t.Errorf("parseCephMonitors() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSecretRedaction(t *testing.T) {

	opt := CephFSMountOptions{
		CephMonitors:  "",
		CephSecretKey: "some-super-sensible-secret",
		CephUser:      "",
		MdsNamespace:  "",
		MountPath:     "",
	}

	cephMounter := opt
	assert.NotContains(t, cephMounter.String(), opt.CephSecretKey)
}

func TestCephFSMountOptions_Verify(t *testing.T) {

	type TestCase struct {
		name      string
		options   CephFSMountOptions
		errTestFn func(error)
	}

	tcs := []TestCase{
		{
			name: "valid mount options",
			options: CephFSMountOptions{
				CephMonitors:  "a",
				CephSecretKey: "b",
				CephUser:      "c",
				MdsNamespace:  "d",
				MountPath:     "e",
			},
			errTestFn: func(err error) {
				assert.NoError(t, err)
			},
		},
		{
			name: "empty CephMonitors",
			options: CephFSMountOptions{
				CephMonitors:  "",
				CephSecretKey: "b",
				CephUser:      "c",
				MdsNamespace:  "d",
				MountPath:     "e",
			},
			errTestFn: func(err error) {
				assert.Error(t, err)
			},
		},
		{
			name: "empty CephSecretKey",
			options: CephFSMountOptions{
				CephMonitors:  "a",
				CephSecretKey: "",
				CephUser:      "c",
				MdsNamespace:  "d",
				MountPath:     "e",
			},
			errTestFn: func(err error) {
				assert.Error(t, err)
			},
		},
		{
			name: "empty CephUser",
			options: CephFSMountOptions{
				CephMonitors:  "a",
				CephSecretKey: "b",
				CephUser:      "",
				MdsNamespace:  "d",
				MountPath:     "e",
			},
			errTestFn: func(err error) {
				assert.Error(t, err)
			},
		},
		{
			name: "empty MdsNamespace",
			options: CephFSMountOptions{
				CephMonitors:  "a",
				CephSecretKey: "b",
				CephUser:      "c",
				MdsNamespace:  "",
				MountPath:     "e",
			},
			errTestFn: func(err error) {
				assert.Error(t, err)
			},
		},
		{
			name: "empty MountPath",
			options: CephFSMountOptions{
				CephMonitors:  "a",
				CephSecretKey: "b",
				CephUser:      "c",
				MdsNamespace:  "d",
				MountPath:     "",
			},
			errTestFn: func(err error) {
				assert.Error(t, err)
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			tc.errTestFn(tc.options.Verify())
		})
	}

}
