package mount

import "context"

type Mounter interface {
	Mount(ctx context.Context) error
	String() string
}
