package client

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/config"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"github.com/eclipse/paho.mqtt.golang"
	"github.com/rs/zerolog/log"
	"io/ioutil"
	"sync"
)

type Factory func(options config.MqttClientConfig, onConnect func(mqtt.Client)) (mqtt.Client, error)

var factoryMutex sync.Mutex
var ClientFactory Factory

func init() {
	factoryMutex.Lock()
	defer factoryMutex.Unlock()

	ClientFactory = newClient
}

func SetFactory(fac Factory) {
	factoryMutex.Lock()
	defer factoryMutex.Unlock()

	log.Warn().Msg("Mqtt client factory got changed! This is only valid during tests")
	ClientFactory = fac
}

func buildTlsConfig(tlsC config.MqttTlsClientConfig) (*tls.Config, error) {

	if tlsC.ServerCaFile == "" && tlsC.ClientCertKeyFile == "" && tlsC.ClientCertFile == "" {
		return nil, nil
	}

	tlsConfig := new(tls.Config)
	tlsConfig.RootCAs = x509.NewCertPool()

	clientCert, err := tls.LoadX509KeyPair(tlsC.ClientCertFile, tlsC.ClientCertKeyFile)

	if err != nil {
		return nil, fmt.Errorf("could not read client certificate files (%w)", err)
	}

	tlsConfig.Certificates = []tls.Certificate{clientCert}

	pemData, err := ioutil.ReadFile(tlsC.ServerCaFile)

	if err != nil {
		return nil, fmt.Errorf("could not read server CA certificate file (%w)", err)
	}

	if !tlsConfig.RootCAs.AppendCertsFromPEM(pemData) {
		return nil, fmt.Errorf("failed to acc server CA certificate")
	}

	return tlsConfig, nil
}

func newClient(options config.MqttClientConfig, onConnect func(mqtt.Client)) (mqtt.Client, error) {

	clientConfig := mqtt.NewClientOptions()

	clientConfig = clientConfig.SetOnConnectHandler(onConnect)
	clientConfig = clientConfig.AddBroker(options.Broker)
	clientConfig = clientConfig.SetClientID(options.ClientId)

	tlsConfig, err := buildTlsConfig(options.MqttTlsClientConfig)

	if err != nil {
		return nil, err
	}

	if tlsConfig != nil {
		clientConfig.TLSConfig = tlsConfig
	}

	cl := mqtt.NewClient(clientConfig)

	connectionToken := cl.Connect()
	connectionToken.Wait()

	err = connectionToken.Error()

	if err != nil {
		return nil, err
	}

	return cl, nil
}
