package client

import (
	mqtt "github.com/eclipse/paho.mqtt.golang"
	"time"
)

type errorToken struct {
	err error
}

func (e *errorToken) Wait() bool {
	return true
}

func (e *errorToken) WaitTimeout(_ time.Duration) bool {
	return true
}

func (e *errorToken) Error() error {
	return e.err
}

func NewErrorToken(err error) mqtt.Token {
	return &errorToken{err}
}
