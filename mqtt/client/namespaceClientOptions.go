package client

import (
	"fmt"
	mqtt "github.com/eclipse/paho.mqtt.golang"
	"github.com/rs/zerolog/log"
	"os"
)

func NewNamespaceClientOptions(clientId string) *mqtt.ClientOptions {
	mqttHost, found := os.LookupEnv("NAMESPACE_BROKER_PORT_1883_TCP_ADDR")

	if !found {
		// TODO This log is not context-scoped, so will not be transferred to any client
		log.Warn().Msg("NAMESPACE_BROKER_PORT_1883_TCP_ADDR environment variable not set, using default value")
		mqttHost = "tcp://namespace-broker"
	}

	mqttPort, found := os.LookupEnv("NAMESPACE_BROKER_SERVICE_PORT")

	if !found {
		// TODO This log is not context-scoped, so will not be transferred to any client
		log.Warn().Msg("NAMESPACE_BROKER_SERVICE_PORT environment variable not set, using default value")
		mqttPort = "1883"
	}
	return mqtt.NewClientOptions().AddBroker(fmt.Sprintf("%s:%s", mqttHost, mqttPort)).SetClientID(clientId)

}
