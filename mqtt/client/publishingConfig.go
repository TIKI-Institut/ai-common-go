package client

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/common/check"
	"encoding/json"
	mqtt "github.com/eclipse/paho.mqtt.golang"
)

func NewTopicConfig(topic string) PublishingConfig {
	return PublishingConfig{SubscriptionConfig: SubscriptionConfig{topic: topic}}
}

type PublishingConfig struct {
	SubscriptionConfig
	retain bool
}

func (a *PublishingConfig) WithRetainFlag(retain bool) {
	a.retain = retain
}

func (a *PublishingConfig) RetainFlag() bool {
	return a.retain
}

func (a *PublishingConfig) Publish(target mqtt.Client, payload interface{}) mqtt.Token {
	if payloadBytes, err := MqttPayloadToBytes(payload); err != nil {
		return &errorToken{err}
	} else {
		return target.Publish(a.Topic(), byte(a.QualityOfService()), a.RetainFlag(), payloadBytes)
	}
}

func MqttPayloadToBytes(payload interface{}) ([]byte, error) {

	if !check.IsNil(payload) {
		return json.Marshal(payload)
	} else {
		return []byte{}, nil
	}
}
