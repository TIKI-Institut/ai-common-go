package client

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestMqttPayloadToBytes(t *testing.T) {

	t.Run("with value", func(t *testing.T) {
		bytes, err := MqttPayloadToBytes([]string{"a", "b", "c"})
		assert.NoError(t, err)
		assert.NotEqual(t, []byte{}, bytes)
	})

	t.Run("without value", func(t *testing.T) {
		var payload []string
		bytes, err := MqttPayloadToBytes(payload)
		assert.NoError(t, err)
		assert.Equal(t, []byte{}, bytes)
	})
}
