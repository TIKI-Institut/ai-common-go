package client

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/common/check"
	"encoding/json"
	mqtt "github.com/eclipse/paho.mqtt.golang"
)

type Subscription interface {
	PayloadFactory() interface{}
	SubscriptionHandler(payload interface{}, err error)
}

type SubscriptionConfig struct {
	topic string
	qos   QoS
}

func (a *SubscriptionConfig) WithQualityOfService(qos QoS) {
	a.qos = qos
}

func (a *SubscriptionConfig) QualityOfService() QoS {
	return a.qos
}

func (a *SubscriptionConfig) Topic() string {
	return a.topic
}

func (a *SubscriptionConfig) Subscribe(target mqtt.Client, subscription Subscription) mqtt.Token {
	return target.Subscribe(a.Topic(), byte(a.QualityOfService()), func(client mqtt.Client, message mqtt.Message) {

		pl := subscription.PayloadFactory()

		if check.IsNil(pl) {
			subscription.SubscriptionHandler(nil, nil)
			return
		}

		err := json.Unmarshal(message.Payload(), pl)

		if err != nil {
			subscription.SubscriptionHandler(nil, err)
		} else {
			subscription.SubscriptionHandler(pl, nil)
		}
	})
}
