package test

import (
	"fmt"
	mqtt "github.com/eclipse/paho.mqtt.golang"
)

type dummyMessage struct {
	topic   string
	payload []byte
}

func NewDummyMessage(topic string, payload []byte) mqtt.Message {
	return &dummyMessage{topic: topic, payload: payload}
}

func (e *dummyMessage) Duplicate() bool {
	panic(fmt.Errorf("Currently not supported"))
}

func (e *dummyMessage) Qos() byte {
	panic(fmt.Errorf("Currently not supported"))
}

func (e *dummyMessage) Retained() bool {
	panic(fmt.Errorf("Currently not supported"))
}
func (e *dummyMessage) Topic() string {
	return e.topic
}

func (e *dummyMessage) MessageID() uint16 {
	panic(fmt.Errorf("Currently not supported"))
}

func (e *dummyMessage) Payload() []byte {
	return e.payload
}

func (e *dummyMessage) Ack() {
	panic(fmt.Errorf("Currently not supported"))
}
