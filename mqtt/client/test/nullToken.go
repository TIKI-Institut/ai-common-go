package test

import "time"

type nullToken struct {
}

func (e *nullToken) Wait() bool {
	return true
}

func (e *nullToken) WaitTimeout(_ time.Duration) bool {
	return true
}

func (e *nullToken) Error() error {
	return nil
}
