package test

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/common/check"
	"encoding/json"
	"fmt"
	mqtt "github.com/eclipse/paho.mqtt.golang"
	"github.com/stretchr/testify/assert"
	"regexp"
	"strings"
	"testing"
)

type Publishment struct {
	Topic    string
	Qos      byte
	Retained bool
	Payload  interface{}
}

type SubscriptionBase struct {
	Topic string
	Qos   byte
}

type Subscription struct {
	SubscriptionBase

	Matcher  *regexp.Regexp
	Callback mqtt.MessageHandler
}

type TestClient struct {
	isConnected bool

	Publishments  []Publishment
	Subscriptions []Subscription
}

func NewTestClient() TestClient {
	return TestClient{
		isConnected:   false,
		Publishments:  []Publishment{},
		Subscriptions: []Subscription{},
	}
}

// IsConnected returns a bool signifying whether
// the client is connected or not.
func (t *TestClient) IsConnected() bool {
	return t.isConnected
}

// IsConnectionOpen return a bool signifying wether the client has an active
// connection to mqtt broker, i.e not in disconnected or reconnect mode
func (t *TestClient) IsConnectionOpen() bool {
	return t.isConnected
}

// Connect will create a connection to the message broker, by default
// it will attempt to connect at v3.1.1 and auto retry at v3.1 if that
// fails
func (t *TestClient) Connect() mqtt.Token {
	t.isConnected = true
	return &nullToken{}
}

// Disconnect will end the connection with the server, but not before waiting
// the specified number of milliseconds to wait for existing work to be
// completed.
func (t *TestClient) Disconnect(_ uint) {
	t.isConnected = false
}

// Publish will publish a message with the specified QoS and content
// to the specified topic.
// Returns a token to track delivery of the message to the broker
func (t *TestClient) Publish(topic string, qos byte, retained bool, payload interface{}) mqtt.Token {
	t.Publishments = append(t.Publishments, Publishment{Topic: topic, Qos: qos, Retained: retained, Payload: payload})
	return &nullToken{}
}

// Subscribe starts a new subscription. Provide a MessageHandler to be executed when
// a message is published on the topic provided, or nil for the default handler

/*
	NOTES :
	we manipulate the topic at this point into a regex to support wildcards in topics
	currently we only support lower case topics here....

*/

func buildRegex(topic string) *regexp.Regexp {

	//single topic layer wildcard
	topic = strings.Replace(topic, "/+/", "/[1-9a-z\\-]+/", -1)

	//multi topic layer wildcard
	topic = strings.Replace(topic, "/#", "/([1-9a-z\\-]+/)*[1-9a-z\\-]+", -1)

	//slash converter
	topic = strings.Replace(topic, "/", "\\/", -1)

	return regexp.MustCompile(topic)
}

func (t *TestClient) Subscribe(topic string, qos byte, callback mqtt.MessageHandler) mqtt.Token {
	t.Subscriptions = append(t.Subscriptions, Subscription{SubscriptionBase: SubscriptionBase{Topic: topic, Qos: qos}, Callback: callback, Matcher: buildRegex(topic)})
	return &nullToken{}
}

// SubscribeMultiple starts a new subscription for multiple topics. Provide a MessageHandler to
// be executed when a message is published on one of the topics provided, or nil for the
// default handler
func (t *TestClient) SubscribeMultiple(_ map[string]byte, _ mqtt.MessageHandler) mqtt.Token {
	panic(fmt.Errorf("currently not supported"))
}

// Unsubscribe will end the subscription from each of the topics provided.
// Messages published to those topics from other clients will no longer be
// received.
func (t *TestClient) Unsubscribe(_ ...string) mqtt.Token {
	panic(fmt.Errorf("currently not supported"))
}

// AddRoute allows you to add a handler for messages on a specific topic
// without making a subscription. For example having a different handler
// for parts of a wildcard subscription
func (t *TestClient) AddRoute(_ string, _ mqtt.MessageHandler) {
	panic(fmt.Errorf("currently not supported"))
}

// OptionsReader returns a ClientOptionsReader which is a copy of the clientoptions
// in use by the client.
func (t *TestClient) OptionsReader() mqtt.ClientOptionsReader {
	panic(fmt.Errorf("currently not supported"))
}

/*******
 *   Interaction
 */
func (t *TestClient) Send(testing *testing.T, topic string, payload interface{}) {
	// has to be set to empty slice so that it corresponds to mqtt behavior
	//noinspection GoPreferNilSlice
	payloadBytes := []byte{}

	if !check.IsNil(payload) {
		bs, err := json.Marshal(payload)

		if err != nil {
			assert.Fail(testing, err.Error())
			return
		}

		payloadBytes = bs
	}

	for _, v := range t.Subscriptions {
		if v.Matcher.MatchString(topic) {
			v.Callback(t, NewDummyMessage(v.Topic, payloadBytes))
		}
	}
}

/*******
 *   Assertions
 */
func (t *TestClient) AssertNumSubscriptions(testing *testing.T, num int) {
	assert.Equal(testing, num, len(t.Subscriptions))
}
func (t *TestClient) AssertNumPublishments(testing *testing.T, num int) {
	assert.Equal(testing, num, len(t.Publishments))
}
func (t *TestClient) AssertPublishments(testing *testing.T, offset int, topic string, qos byte, retained bool, payload interface{}) {

	// has to be set to empty slice so that it corresponds to mqtt behavior
	//noinspection GoPreferNilSlice
	payloadBytes := []byte{}

	if !check.IsNil(payload) {
		bs, err := json.Marshal(payload)

		if err != nil {
			assert.Fail(testing, err.Error())
			return
		}

		payloadBytes = bs
	}

	assert.Equal(testing, Publishment{Topic: topic, Qos: qos, Retained: retained, Payload: payloadBytes}, t.Publishments[offset])
}
func (t *TestClient) GetPublishedPayload(offset int) interface{} {
	return t.Publishments[offset].Payload
}
func (t *TestClient) AssertSubscription(testing *testing.T, offset int, topic string, qos byte) {
	assert.Equal(testing, SubscriptionBase{Topic: topic, Qos: qos}, t.Subscriptions[offset].SubscriptionBase)
}
