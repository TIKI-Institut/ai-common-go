package topics

import (
	client2 "bitbucket.org/TIKI-Institut/ai-common-go/v2/mqtt/client"
	mqtt "github.com/eclipse/paho.mqtt.golang"
)

type MainContainerShutdown struct {
}

/* make types great again  */
type mainContainerShutdownClient struct {
	handler func(err error)
}

func (h *mainContainerShutdownClient) PayloadFactory() interface{} {
	return nil
}

func (h *mainContainerShutdownClient) SubscriptionHandler(payload interface{}, err error) {
	h.handler(err)
}

/* impl  */
type MainContainerShutdownConfig struct {
	client2.PublishingConfig
}

func NewMainContainerShutdownConfig(podName string) MainContainerShutdownConfig {
	return MainContainerShutdownConfig{client2.NewTopicConfig(mainContainerShutdownTopic(podName))}
}

func (h *MainContainerShutdownConfig) Subscribe(target mqtt.Client, handler func(err error)) mqtt.Token {
	return h.SubscriptionConfig.Subscribe(target, &mainContainerShutdownClient{handler: handler})
}

func (h *MainContainerShutdownConfig) Publish(target mqtt.Client) mqtt.Token {
	return h.PublishingConfig.Publish(target, nil)
}
