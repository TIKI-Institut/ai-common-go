package topics

import (
	"fmt"
)

const topicPodInternalRoot = "internal"

const mainContainerShutdown = "mainContainerShutdown"

func podInternalTopic(podName, topic string) string {
	return fmt.Sprintf("%s/%s/%s", topicPodInternalRoot, podName, topic)
}

func mainContainerShutdownTopic(podName string) string {
	return podInternalTopic(podName, mainContainerShutdown)
}
