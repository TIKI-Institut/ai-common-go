package path

import (
	"fmt"
	"github.com/pkg/errors"
	"os"
	"path/filepath"
	"runtime"
)

type Path struct {
	basePath string
	path     string
}

func LocalPath() *Path {
	_, callerFile, _, _ := runtime.Caller(1)

	return &Path{
		basePath: filepath.Dir(callerFile),
		path:     "",
	}
}

func AbsolutePath(target string) *Path {

	return &Path{
		basePath: target,
		path:     "",
	}
}

func (p *Path) Child(targets ...string) *Path {
	return &Path{
		basePath: p.basePath,
		path:     filepath.Join(p.path, filepath.Join(targets...)),
	}
}

func (p *Path) String() string {
	return filepath.ToSlash(filepath.Join(p.basePath, p.path))
}

func (p *Path) Exists() bool {
	if _, err := os.Stat(p.String()); os.IsNotExist(err) {
		return false
	}

	return true
}

func (p *Path) Parent() *Path {
	return &Path{
		basePath: p.basePath,
		path:     filepath.Join(p.path, filepath.Join("..")),
	}
}

func (p *Path) IsDirectory() (bool, error) {

	stat, err := os.Stat(p.String())

	if os.IsNotExist(err) {
		return false, errors.New(fmt.Sprintf("%q dies not exist", p.String()))
	}

	return stat.IsDir(), nil
}
