package path

import (
	"github.com/stretchr/testify/assert"
	"path"
	"runtime"
	"testing"
)

func TestLocalPathCreation(t *testing.T) {

	_, callerFile, _, _ := runtime.Caller(0)

	p := LocalPath()
	assert.Equal(t, path.Dir(callerFile), p.String())

}

func TestAbsolutePathCreation(t *testing.T) {

	p := AbsolutePath("D:/test/dir")
	assert.Equal(t, "D:/test/dir", p.String())

}

func TestChild(t *testing.T) {
	_, callerFile, _, _ := runtime.Caller(0)

	pA := AbsolutePath("D:/test/dir")
	pL := LocalPath()

	pA = pA.Child("blub")
	pL = pL.Child("blub")

	assert.Equal(t, "D:/test/dir/blub", pA.String())
	assert.Equal(t, path.Dir(callerFile)+"/blub", pL.String())

}

func TestEmptyChild(t *testing.T) {

	pA := AbsolutePath("D:/test/dir")

	pA = pA.Child("")

	assert.Equal(t, "D:/test/dir", pA.String())

	pA = pA.Child("").Child("").Child("blub").Child("")

	assert.Equal(t, "D:/test/dir/blub", pA.String())

}

func TestParent(t *testing.T) {
	pA := AbsolutePath("D:/test/dir")
	pA = pA.Parent()

	assert.Equal(t, "D:/test", pA.String())
}

func TestExists(t *testing.T) {

	p := LocalPath()
	assert.Equal(t, true, p.Exists())

	p = p.Child("totallyRandomSubPath")
	assert.Equal(t, false, p.Exists())
}

func TestIsDirectory(t *testing.T) {
	p := LocalPath()
	isDir, err := p.IsDirectory()
	assert.Equal(t, true, isDir)
	assert.Equal(t, nil, err)

	isDir, err = p.Child("testdata", "aFile").IsDirectory()
	assert.Equal(t, false, isDir)
	assert.Equal(t, nil, err)

	isDir, err = p.Child("testdata").Child("bFile").IsDirectory()
	assert.Equal(t, false, isDir)
	assert.NotEqual(t, nil, err)
}
