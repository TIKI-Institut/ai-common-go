package qos

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/qos/model"
	"context"
	"fmt"
	"sync"
)

type qosEntryCollectorContextKey int

var qosEntryCollectorContextKeyValue qosEntryCollectorContextKey

type qosEntryCollector struct {
	sync.RWMutex
	syncEntries  entriesRepo
	asyncEntries entriesRepo
}

func EnsureContextWithQosEntryCollector(ctx context.Context) context.Context {

	if existingCollector := getCollector(ctx, false); existingCollector == nil {
		collector := &qosEntryCollector{
			syncEntries:  entriesRepo{repo: make(map[string]Entry), entryType: model.SyncEntryType},
			asyncEntries: entriesRepo{repo: make(map[string]Entry), entryType: model.AsyncEntryType},
		}
		return context.WithValue(ctx, qosEntryCollectorContextKeyValue, collector)
	}

	return ctx
}

func getCollector(ctx context.Context, panicIfAbsent bool) *qosEntryCollector {
	roleCollector, ok := ctx.Value(qosEntryCollectorContextKeyValue).(*qosEntryCollector)
	if !ok && panicIfAbsent {
		panic(fmt.Errorf("no qosEntryCollector was found in the supplied context"))
	}
	return roleCollector
}

func RegisterAsyncEntry(ctx context.Context, e Entry) {
	collector := getCollector(ctx, true)
	collector.Lock()
	defer collector.Unlock()
	collector.asyncEntries.repo[e.Name()] = e
}

func RegisterSyncEntry(ctx context.Context, e Entry) {
	collector := getCollector(ctx, true)
	collector.Lock()
	defer collector.Unlock()
	collector.syncEntries.repo[e.Name()] = e
}

func AsyncEntries(ctx context.Context) []model.QosEntryModel {
	collector := getCollector(ctx, true)
	collector.RLock()
	defer collector.RUnlock()
	return collector.asyncEntries.Entries()
}

func SyncEntries(ctx context.Context) []model.QosEntryModel {
	collector := getCollector(ctx, true)
	collector.RLock()
	defer collector.RUnlock()
	return collector.syncEntries.Entries()
}

func SyncEntry(ctx context.Context, name string) (e Entry, found bool) {
	collector := getCollector(ctx, true)
	collector.RLock()
	defer collector.RUnlock()
	return collector.syncEntries.Entry(name)
}

func AsyncEntry(ctx context.Context, name string) (e Entry, found bool) {
	collector := getCollector(ctx, true)
	collector.RLock()
	defer collector.RUnlock()
	return collector.asyncEntries.Entry(name)
}
