package qos

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/kubernetes"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/qos/model"
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"net/http"
	"strconv"
)

func ListSyncEntriesHandler(ctx context.Context) gin.HandlerFunc {
	return func(c *gin.Context) {
		pod, err := kubernetes.GetCurrentPodReference()
		if err != nil {
			log.Ctx(c).Error().Err(err).Send()
		}

		endpoint := model.SyncQosEndpoint{
			Entries:         SyncEntries(ctx),
			FlavorReference: model.CurrentFlavor(),
			PodReference:    *pod,
		}

		c.JSON(http.StatusOK, endpoint)
	}
}

func RunSyncEntryEndpointHandler(ctx context.Context) gin.HandlerFunc {

	return func(c *gin.Context) {
		transactionId := c.Query("transactionId")
		if transactionId == "" {
			c.String(http.StatusBadRequest, "Missing transactionId request parameter")
			return
		}
		transactionIdInt, err := strconv.ParseInt(transactionId, 10, 64)
		if err != nil {
			c.String(http.StatusBadRequest, fmt.Sprintf("Invalid transactionId: %s", transactionId))
			return
		}
		entryName := c.Query("name")
		if entryName == "" {
			c.String(http.StatusBadRequest, "Missing name request parameter")
			return
		}
		entry, entryExists := SyncEntry(ctx, entryName)
		var qosResult model.SyncQosRunResult
		if entryExists == false {
			qosResult = model.NotFoundSyncResult(transactionIdInt, fmt.Sprintf("Sync QoS entry %s was not found", entryName))
			c.JSON(http.StatusOK, qosResult)
			return

		}
		defer func() {
			if r := recover(); r != nil {
				qosResult = model.FailedSyncResult(transactionIdInt, fmt.Sprintf("Running sync QoS for id %s, name %s has failed", transactionId, entryName), entry.Name())
				c.JSON(http.StatusOK, qosResult)
				return
			}
		}()
		result := entry.Run()
		qosResult = model.SuccessSyncResult(transactionIdInt, fmt.Sprintf("Sync QoS run for %s completed", entryName), result, entry.Name())

		c.JSON(http.StatusOK, qosResult)
	}
}
