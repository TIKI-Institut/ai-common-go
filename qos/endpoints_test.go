package qos

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/kubernetes"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/qos/model"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/tests"
	"context"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestListSyncEntries(t *testing.T) {

	tests.WithEnvVars(
		func() {
			ctx := EnsureContextWithQosEntryCollector(context.Background())
			RegisterSyncEntry(ctx, NewFunctionEntry("testSyncQosEntry", func() float64 { return 4.5 }))

			resp := httptest.NewRecorder()

			c, _ := gin.CreateTestContext(resp)
			ListSyncEntriesHandler(ctx)(c)
			result := model.SyncQosEndpoint{}
			assert.Nil(t, json.Unmarshal(resp.Body.Bytes(), &result))
			assert.Equal(t, "somePod", result.PodReference.Pod)
			assert.Equal(t, "dspNamespace", result.PodReference.Namespace)
			assert.Equal(t, "dspName", result.FlavorReference.Name)
			assert.Equal(t, "testSyncQosEntry", result.Entries[0].Name)
			assert.Equal(t, "Sync", result.Entries[0].Type)
		},
		tests.EnvDecl{Name: kubernetes.DspNamespaceEnv, Value: "dspNamespace"},
		tests.EnvDecl{Name: kubernetes.DspPodEnv, Value: "somePod"},
		tests.EnvDecl{Name: "DSP_NAME", Value: "dspName"})

}

func TestRunSyncEntryEndpoint(t *testing.T) {

	tests.WithEnvVars(
		func() {

			t.Run("test valid run", func(t *testing.T) {
				ctx := EnsureContextWithQosEntryCollector(context.Background())
				RegisterSyncEntry(ctx, NewFunctionEntry("testSyncQosEntryRun", func() float64 { return 6.5 }))

				resp := httptest.NewRecorder()
				c, _ := gin.CreateTestContext(resp)
				c.Request = httptest.NewRequest("GET", "http://test?name=testSyncQosEntryRun&transactionId=23", nil)
				RunSyncEntryEndpointHandler(ctx)(c)
				result := model.SyncQosRunResult{}
				assert.Nil(t, json.Unmarshal(resp.Body.Bytes(), &result))
				assert.Equal(t, "somePod", result.PodReference.Pod)
				assert.Equal(t, "dspNamespace", result.PodReference.Namespace)
				assert.Equal(t, "dspName", result.FlavorReference.Name)
				assert.Equal(t, "testSyncQosEntryRun", result.QosEntry.Name)
				assert.Equal(t, "Sync", result.QosEntry.Type)
				assert.Equal(t, 6.5, *result.Score)

			})

			t.Run("test invalid function", func(t *testing.T) {
				ctx := EnsureContextWithQosEntryCollector(context.Background())
				RegisterSyncEntry(ctx, NewFunctionEntry("testSyncQosErrorEntryRun", func() float64 { panic("Some error") }))
				resp := httptest.NewRecorder()
				c, _ := gin.CreateTestContext(resp)
				c.Request = httptest.NewRequest("GET", "http://test?name=testSyncQosErrorEntryRun&transactionId=23", nil)
				RunSyncEntryEndpointHandler(ctx)(c)
				result := model.SyncQosRunResult{}
				assert.Nil(t, json.Unmarshal(resp.Body.Bytes(), &result))
				println(resp.Body.String())
				assert.Equal(t, "somePod", result.PodReference.Pod)
				assert.Equal(t, "dspNamespace", result.PodReference.Namespace)
				assert.Equal(t, "dspName", result.FlavorReference.Name)
				assert.Equal(t, "testSyncQosErrorEntryRun", result.QosEntry.Name)
				assert.Equal(t, "Sync", result.QosEntry.Type)
				assert.Nil(t, result.Score)
			})

			t.Run("test missing entry name param", func(t *testing.T) {
				ctx := EnsureContextWithQosEntryCollector(context.Background())
				RegisterSyncEntry(ctx, NewFunctionEntry("testSyncQosErrorEntryRun", func() float64 { panic("Some error") }))

				resp := httptest.NewRecorder()
				c, _ := gin.CreateTestContext(resp)
				c.Request = httptest.NewRequest("GET", "http://test?transactionId=23", nil)
				RunSyncEntryEndpointHandler(ctx)(c)
				assert.Equal(t, "Missing name request parameter", resp.Body.String())
				assert.Equal(t, http.StatusBadRequest, resp.Code)
			})

			t.Run("test missing transactionId param", func(t *testing.T) {
				ctx := EnsureContextWithQosEntryCollector(context.Background())
				RegisterSyncEntry(ctx, NewFunctionEntry("testSyncQosErrorEntryRun", func() float64 { panic("Some error") }))

				resp := httptest.NewRecorder()
				c, _ := gin.CreateTestContext(resp)
				c.Request = httptest.NewRequest("GET", "http://test?name=test123", nil)
				RunSyncEntryEndpointHandler(ctx)(c)
				assert.Equal(t, "Missing transactionId request parameter", resp.Body.String())
				assert.Equal(t, http.StatusBadRequest, resp.Code)
			})

			t.Run("test invalid transactionId param", func(t *testing.T) {
				ctx := EnsureContextWithQosEntryCollector(context.Background())
				RegisterSyncEntry(ctx, NewFunctionEntry("testSyncQosErrorEntryRun", func() float64 { panic("Some error") }))

				resp := httptest.NewRecorder()
				c, _ := gin.CreateTestContext(resp)
				c.Request = httptest.NewRequest("GET", "http://test?name=test123&transactionId=invalid", nil)
				RunSyncEntryEndpointHandler(ctx)(c)
				assert.Equal(t, "Invalid transactionId: invalid", resp.Body.String())
				assert.Equal(t, http.StatusBadRequest, resp.Code)
			})

		},
		tests.EnvDecl{Name: kubernetes.DspNamespaceEnv, Value: "dspNamespace"},
		tests.EnvDecl{Name: kubernetes.DspPodEnv, Value: "somePod"},
		tests.EnvDecl{Name: "DSP_NAME", Value: "dspName"})

}

func TestQosEndpointRegistration(t *testing.T) {
	ctx := EnsureContextWithQosEntryCollector(context.Background())
	testEntry := NewFunctionEntry("testSyncQosEntryRun", func() float64 { return 6.5 })
	RegisterSyncEntry(ctx, testEntry)

	entry, found := SyncEntry(ctx, "testSyncQosEntryRun")
	assert.True(t, found)
	assert.Equal(t, testEntry, entry)
}
