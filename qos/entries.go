package qos

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/qos/model"
)

type entriesRepo struct {
	repo      map[string]Entry
	entryType string
}

func (s *entriesRepo) Register(e Entry) { s.repo[e.Name()] = e }

func (s *entriesRepo) Entry(name string) (e Entry, found bool) {
	e, found = s.repo[name]
	return
}

func (s *entriesRepo) Entries() []model.QosEntryModel {
	var entries []model.QosEntryModel = nil

	if len(s.repo) > 0 {
		entries = make([]model.QosEntryModel, len(s.repo))
		i := 0
		for _, v := range s.repo {
			entries[i] = model.QosEntryModel{
				Name: v.Name(),
				Type: s.entryType,
			}
			i++
		}
	}

	return entries
}
