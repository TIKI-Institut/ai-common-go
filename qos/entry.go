package qos

type Entry interface {
	Run() float64
	Name() string
}

type functionEntry struct {
	name   string
	action func() float64
}

func NewFunctionEntry(name string, action func() float64) Entry {
	return &functionEntry{name: name, action: action}
}

func (f *functionEntry) Run() float64 {
	return f.action()
}

func (f *functionEntry) Name() string {
	return f.name
}
