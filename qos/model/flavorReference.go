package model

import "os"

type FlavorReference struct {
	Name          string `json:"name"`
	Principal     string `json:"principal"`
	Environment   string `json:"environment"`
	FlavorName    string `json:"flavorName"`
	FlavorVersion string `json:"flavorVersion"`
}

func CurrentFlavor() FlavorReference {
	return FlavorReference{
		Name:          os.Getenv("DSP_NAME"),
		Principal:     os.Getenv("DSP_PRINCIPAL"),
		Environment:   os.Getenv("DSP_ENVIRONMENT"),
		FlavorName:    os.Getenv("DSP_FLAVOR"),
		FlavorVersion: os.Getenv("DSP_FLAVOR_VERSION"),
	}

}
