package model

import "bitbucket.org/TIKI-Institut/ai-common-go/v2/kubernetes"

type AsyncQosEndpoint struct {
	Entries         []QosEntryModel `json:"entries"`
	FlavorReference FlavorReference `json:"flavorReference"`
}

type SyncQosEndpoint struct {
	Entries         []QosEntryModel         `json:"entries"`
	FlavorReference FlavorReference         `json:"flavorReference"`
	PodReference    kubernetes.PodReference `json:"podReference"`
}
