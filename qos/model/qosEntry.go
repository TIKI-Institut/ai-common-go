package model

import (
	"encoding/json"
	"errors"
	"fmt"
)

const AsyncEntryType string = "Async"
const SyncEntryType string = "Sync"

var TypeMismatchError = errors.New("QosEntry type mismatch")

type QosEntryModel struct {
	Name string `json:"name"`
	Type string `json:"type"`
}

type AsyncQosEntryModel string

func (a *AsyncQosEntryModel) UnmarshalJSON(data []byte) error {

	tmp := QosEntryModel{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return fmt.Errorf("unable to unmarshal 'AsyncQosEntry' (%w)", err)
	}

	if tmp.Type != AsyncEntryType {
		return TypeMismatchError
	}

	*a = AsyncQosEntryModel(tmp.Name)

	return nil
}

func (a *AsyncQosEntryModel) MarshalJSON() ([]byte, error) {
	return json.Marshal(&QosEntryModel{
		Name: string(*a),
		Type: AsyncEntryType,
	})
}

type SyncQosEntryModel string

func (s *SyncQosEntryModel) UnmarshalJSON(data []byte) error {

	tmp := QosEntryModel{}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return fmt.Errorf("unable to unmarshal 'SyncQosEntry' (%w)", err)
	}

	if tmp.Type != SyncEntryType {
		return TypeMismatchError
	}

	*s = SyncQosEntryModel(tmp.Name)

	return nil
}

func (s *SyncQosEntryModel) MarshalJSON() ([]byte, error) {
	return json.Marshal(&QosEntryModel{
		Name: string(*s),
		Type: SyncEntryType,
	})
}
