package model

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/kubernetes"
	"time"
)

type QosRunStatus int

const (
	SUCCESS QosRunStatus = iota
	FAILED
	NOT_FOUND
	RUNNING
)

type QosRunResult struct {
	Id              int64           `json:"id"`
	ProducedOn      time.Time       `json:"producedOn"`
	Status          QosRunStatus    `json:"status"`
	StatusMessage   string          `json:"statusMessage"`
	Score           *float64        `json:"score"`
	QosEntry        *QosEntryModel  `json:"qosEntry"`
	FlavorReference FlavorReference `json:"flavorReference"`
}

type AsyncQosRunResult QosRunResult

type SyncQosRunResult struct {
	QosRunResult `json:",inline"`
	PodReference kubernetes.PodReference `json:"podReference"`
}

func SuccessSyncResult(transactionId int64, statusMessage string, result float64, entryName string) SyncQosRunResult {
	pod, _ := kubernetes.GetCurrentPodReference()

	var entry *QosEntryModel = nil

	if entryName != "" {
		entry = &QosEntryModel{Name: entryName, Type: SyncEntryType}
	}

	return SyncQosRunResult{
		QosRunResult: QosRunResult{
			Id:              transactionId,
			ProducedOn:      time.Now(),
			Status:          SUCCESS,
			StatusMessage:   statusMessage,
			Score:           &result,
			QosEntry:        entry,
			FlavorReference: CurrentFlavor(),
		},
		PodReference: *pod,
	}
}

func FailedSyncResult(transactionId int64, statusMessage string, entryName string) SyncQosRunResult {
	pod, _ := kubernetes.GetCurrentPodReference()

	var entry *QosEntryModel = nil

	if entryName != "" {
		entry = &QosEntryModel{Name: entryName, Type: SyncEntryType}
	}

	return SyncQosRunResult{
		QosRunResult: QosRunResult{Id: transactionId,
			ProducedOn:      time.Now(),
			Status:          FAILED,
			StatusMessage:   statusMessage,
			Score:           nil,
			QosEntry:        entry,
			FlavorReference: CurrentFlavor(),
		},
		PodReference: *pod,
	}
}

func NotFoundSyncResult(transactionId int64, statusMessage string) SyncQosRunResult {
	pod, _ := kubernetes.GetCurrentPodReference()
	return SyncQosRunResult{
		QosRunResult: QosRunResult{
			Id:              transactionId,
			ProducedOn:      time.Now(),
			Status:          NOT_FOUND,
			StatusMessage:   statusMessage,
			Score:           nil,
			QosEntry:        nil,
			FlavorReference: CurrentFlavor(),
		},
		PodReference: *pod,
	}
}

func NotFoundAsyncResult(transactionId int64, statusMessage string) AsyncQosRunResult {
	return AsyncQosRunResult{
		Id:              transactionId,
		ProducedOn:      time.Now(),
		Status:          NOT_FOUND,
		StatusMessage:   statusMessage,
		Score:           nil,
		QosEntry:        nil,
		FlavorReference: CurrentFlavor(),
	}
}

func SuccessAsyncResult(transactionId int64, statusMessage string, result float64, entryName string) AsyncQosRunResult {

	var entry *QosEntryModel = nil

	if entryName != "" {
		entry = &QosEntryModel{Name: entryName, Type: AsyncEntryType}
	}

	return AsyncQosRunResult{
		Id:              transactionId,
		ProducedOn:      time.Now(),
		Status:          SUCCESS,
		StatusMessage:   statusMessage,
		Score:           &result,
		QosEntry:        entry,
		FlavorReference: CurrentFlavor(),
	}
}
