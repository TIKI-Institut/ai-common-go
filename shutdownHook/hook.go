package shutdownHook

import (
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/mqtt/client"
	"bitbucket.org/TIKI-Institut/ai-common-go/v2/mqtt/topics"
	mqtt "github.com/eclipse/paho.mqtt.golang"
)

func SubscribeToMainContainerShutdown(podName string, mqttClient mqtt.Client, cb func(error)) error {

	exitConfig := topics.NewMainContainerShutdownConfig(podName)
	exitConfig.WithQualityOfService(client.ExactlyOnce)

	token := exitConfig.Subscribe(mqttClient, cb)
	token.Wait()

	return token.Error()
}
