package tasks

import (
	"fmt"
	"sync"
)

type stopWorker func()

type Dispatcher interface {
	Dispatch(task Task)
}

type DispatcherImpl struct {
	sync.Mutex

	// A pool of workers channels that are registered with the dispatcher
	WorkerPool chan chan Task
	maxWorkers int

	taskQueue chan Task

	stopWorkers []stopWorker
	stopped     bool
}

func NewDispatcher(maxWorkers int, targetQueue chan Task) *DispatcherImpl {
	pool := make(chan chan Task, maxWorkers)
	return &DispatcherImpl{WorkerPool: pool, taskQueue: targetQueue, maxWorkers: maxWorkers, stopped: false}
}

func (d *DispatcherImpl) Run() error {

	d.Lock()
	defer d.Unlock()

	if d.stopWorkers != nil {
		return fmt.Errorf("dispatcher allready running")
	}

	d.stopWorkers = make([]stopWorker, d.maxWorkers)

	// starting n number of workers
	for i := 0; i < d.maxWorkers; i++ {
		worker := NewWorker(d.WorkerPool)
		worker.Start()

		d.stopWorkers[i] = (func(wrk *Worker) func() {
			return func() {
				wrk.Stop()
			}
		})(worker)
	}

	go d.dispatch()

	return nil
}

func (d *DispatcherImpl) Stop() error {

	d.Lock()
	defer d.Unlock()

	if d.stopped {
		return fmt.Errorf("dispatcher not running")
	}

	for i := range d.stopWorkers {
		d.stopWorkers[i]()
	}

	d.stopped = true

	return nil
}

func (d *DispatcherImpl) dispatch() {
	for {
		select {
		case job := <-d.taskQueue:
			// a job request has been received
			go func(job Task) {
				// try to obtain a worker job channel that is available.
				// this will block until a worker is idle
				jobChannel := <-d.WorkerPool

				// dispatch the job to the worker job channel
				jobChannel <- job
			}(job)
		}
	}
}

func (d *DispatcherImpl) Dispatch(job Task) {
	d.taskQueue <- job
}
