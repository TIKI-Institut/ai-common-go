package tasks

import (
	"github.com/stretchr/testify/assert"
	"sync"
	"testing"
	"time"
)

type testTask struct {
	sync.Mutex
	counter int
}

func (t *testTask) Perform() error {

	t.Lock()
	defer t.Unlock()

	t.counter++

	return nil
}

func TestDoubleStartOfDispatcher(t *testing.T) {

	dispatcher := NewDispatcher(1, make(chan Task))
	err := dispatcher.Run()
	assert.Nil(t, err)

	err = dispatcher.Run()
	assert.Error(t, err)

}

func TestDoubleStopOfDispatcher(t *testing.T) {

	dispatcher := NewDispatcher(1, make(chan Task))
	err := dispatcher.Run()
	assert.Nil(t, err)

	err = dispatcher.Stop()
	assert.Nil(t, err)

	err = dispatcher.Stop()
	assert.Error(t, err)

}

func TestSingleWorkerSingleJob(t *testing.T) {

	jobInstance := testTask{}

	dispatcher := NewDispatcher(1, make(chan Task))
	err := dispatcher.Run()
	assert.Nil(t, err)

	dispatcher.Dispatch(&jobInstance)

	time.Sleep(10 * time.Millisecond)
	dispatcher.Stop()

	jobInstance.Lock()
	defer jobInstance.Unlock()

	assert.Equal(t, 1, jobInstance.counter)
}
