package tasks

import (
	"github.com/rs/zerolog/log"
)

// Job represents the job to be run
type Task interface {
	Perform() error
}

// Worker represents the worker that executes the job
type Worker struct {
	WorkerPool chan chan Task
	JobChannel chan Task
	quit       chan bool
}

func NewWorker(workerPool chan chan Task) *Worker {
	return &Worker{
		WorkerPool: workerPool,
		JobChannel: make(chan Task),
		quit:       make(chan bool)}
}

// Start method starts the run loop for the worker, listening for a quit channel in
// case we need to stop it
func (w Worker) Start() {
	go func() {
		for {
			// register the current worker into the worker queue.
			w.WorkerPool <- w.JobChannel

			select {
			case job := <-w.JobChannel:
				// we have received a work request.
				if err := job.Perform(); err != nil {
					// TODO This log is not context-scoped, so will not be transferred to any client
					log.Error().Err(err).Msg("error performing task")
				}

			case <-w.quit:
				// we have received a signal to stop
				return
			}
		}
	}()
}

// Stop signals the worker to stop listening for work requests.
func (w Worker) Stop() {
	go func() {
		w.quit <- true
	}()
}
