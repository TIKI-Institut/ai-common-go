package tests

import (
	"context"
	"github.com/gin-gonic/gin"
	"net/http"
	"net/http/httptest"
)

// TestMiddleware is a helper to test a gin middleware
// routerMod allows you to modify route (i.e. install a middleware, attach a request handler)
// requestMod allows you to modify the request prior sending it
// returns a *httptest.ResponseRecorder for assertions
func TestMiddleware(ctx context.Context, method string, requestMod func(req *http.Request), engineMod func(engine *gin.Engine)) *httptest.ResponseRecorder {

	// don't use gin.Default() in tests, this will attach logger and recovery middlewares
	r := gin.New()
	r.ContextWithFallback = true

	if engineMod != nil {
		engineMod(r)
	}

	req, _ := http.NewRequestWithContext(ctx, method, "/", nil)

	if requestMod != nil {
		requestMod(req)
	}

	w := httptest.NewRecorder()

	r.ServeHTTP(w, req)

	return w
}
