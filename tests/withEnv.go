package tests

import (
	"os"
	"sync"
)

type EnvDecl struct {
	Name  string
	Value string
}

var envVarsMutex sync.Mutex

func WithEnvVars(bc func(), envVars ...EnvDecl) {
	envVarsMutex.Lock()
	defer envVarsMutex.Unlock()

	var backEnvVars []EnvDecl

	for _, v := range envVars {
		env, found := os.LookupEnv(v.Name)

		if found {
			backEnvVars = append(backEnvVars, EnvDecl{Name: v.Name, Value: env})
		}

		err := os.Setenv(v.Name, v.Value)
		if err != nil {
			panic(err)
		}
	}

	bc()

	for _, v := range envVars {
		err := os.Unsetenv(v.Name)
		if err != nil {
			panic(err)
		}
	}

	for _, v := range backEnvVars {
		err := os.Setenv(v.Name, v.Value)
		if err != nil {
			panic(err)
		}
	}
}
